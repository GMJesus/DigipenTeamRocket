/**
* \author	Aymeric Lambolez
*/

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <list>
#include <tuple>

#define XmlWriterSourcePath "../LevelEditor/XML/XmlWriter.cpp"
#define XmlParserSourcePath "../Engine/XML/XmlParser.cpp"
#define XmlWriterHeaderPath "../LevelEditor/XML/XmlWriter.h"
#define XmlParserHeaderPath "../Engine/XML/XmlParser.h"
#define ModuleFactorySource "../Engine/Modules/ModuleFactory.cpp"
#define ModuleInclude "../Engine/Modules/ModuleImport.h"
#define GameObjectHeader "../Engine/Game/GameObject.h"
#define GameObjectSource "../Engine/Game/GameObject.cpp"
#define ModuleHeader "../Engine/Modules/Module.h"
#define ModulePath "../Engine/Modules/"
#define MenuPath "../LevelEditor/Menus/"
#define LevelEditorHeader "../LevelEditor/FLTK_UI/EditorWindow.h"
#define LevelEditorSource "../LevelEditor/FLTK_UI/EditorWindow.cpp"
#define ClassCreatorScript "../ClassCreator/main.cpp"

#define ModuleTypePrefix "Module::Type::E_"

#define COMMENT_FORMAT "//@ModuleCreator "

std::string* lastParent = NULL;

std::string epur(std::string str)
{
	std::string tmp;
	int i = 0;
	bool blank = false;

	while (str[i] && (str[i] == ' ' || str[i] == '\t'))
		++i;
	while (str[i])
	{
		if (str[i] == ' ' || str[i] == '\t')
		{
			if (!blank)
			{
				blank = true;
				tmp.insert(tmp.end(), ' ');
			}
		}
		else
		{
			tmp.insert(tmp.end(), str[i]);
			blank = false;
		}
		++i;
	}
	return tmp;
}

void	GetParentProperties(const std::string& parent, std::list<std::tuple<std::string, std::string, bool>>& bindableProperties)
{
	std::ifstream file = std::ifstream(ModulePath + parent + ".h");
	std::string buf;
	bool parse = true;

	while (std::getline(file, buf))
	{
		if (buf.find("class " + parent + " : public ") != std::string::npos)
		{
			std::string tmp = buf.substr(16 + parent.size(), buf.size() - (16 + parent.size()));
			if (tmp == "Module")
				lastParent = new std::string(parent);
			GetParentProperties(tmp, bindableProperties);
		}
		else if (buf.find("private:") != std::string::npos)
			parse = false;
		else if (buf.find("protected:") != std::string::npos || buf.find("public:") != std::string::npos)
			parse = true;
		if (parse)
		{
			//std::cout << buf << std::endl;
			if (buf.find(COMMENT_FORMAT + std::string("BindableProperty")) != std::string::npos)
			{
				std::getline(file, buf);
				std::string ep = epur(buf);
				size_t pos;
				if ((pos = ep.find(' ')) == std::string::npos && (pos = ep.find('\t')) == std::string::npos)
					throw std::exception("Parent parsing error");
				std::tuple<std::string, std::string, bool> elem(ep.substr(0, pos), ep.substr(pos + 1, ep.size() - (pos + 2)), true);
				bindableProperties.push_back(elem);
			}
		}
	}
}

void	GetUserProperties(std::list<std::tuple<std::string, std::string, bool>>& bindableProperties)
{
	std::cout << "Put your bindable properties :" << std::endl;
	bool done = false;
	while (!done)
	{
		std::tuple<std::string, std::string, bool> t;
		std::cout << "Property name (put END if over) :";
		std::string str;
		std::cin >> str;
		if (str == "END")
			done = true;
		else
		{
			std::get<1>(t) = str;
			std::cout << "Property type :";
			std::cin >> str;
			std::get<0>(t) = str;
			std::get<2>(t) = false;
			bindableProperties.push_back(t);
		}
	}
}

std::string GetType(std::tuple<std::string, std::string, bool> tuple)
{
	return (std::get<0>(tuple) == "std::string" ? "const std::string&" : std::get<0>(tuple) == "Vec2" ? "const Vec2&" : std::get<0>(tuple));
}

void	CreateHeaderFile(const std::string& moduleName, const std::string& parentModule, std::list<std::tuple<std::string, std::string, bool>>& bindableProperties)
{
	std::ofstream file = std::ofstream(ModulePath + moduleName + ".h");

	if (!file)
		throw std::exception(("Can't open " + (ModulePath + moduleName) + ".h").c_str());
	std::string upperName = moduleName;
	std::transform(upperName.begin(), upperName.end(), upperName.begin(), ::toupper);
	file << "#ifndef " << upperName << "_H_" << std::endl;
	file << "#define " << upperName << "_H_" << std::endl << std::endl;
	file << "#include	\"Modules/" << parentModule << ".h\"" << std::endl << std::endl;

	file << "class " << moduleName << " : public " << parentModule << std::endl;
	file << "{" << std::endl;

	file << "public:" << std::endl;
	file << "\t" << moduleName << "(GameObject* owner);" << std::endl;
	file << "\t~" << moduleName << "();" << std::endl << std::endl;
	file << "\tvoid	Initialize();" << std::endl << std::endl;
	file << "\tvoid	Update(float deltaTime);" << std::endl << std::endl;
	file << "\tvoid	Destroy();" << std::endl << std::endl;
	file << "\tModule::Type	GetType()const;" << std::endl << std::endl;
	for (std::list<std::tuple<std::string, std::string, bool>>::const_iterator it = bindableProperties.begin(); it != bindableProperties.end(); ++it)
	{
		if (!std::get<2>(*it))
		{
			std::string propertyName = std::get<1>(*it);
			if (propertyName[0] == '_')
				propertyName = &propertyName[1];
			std::string tmp = propertyName;
			tmp[0] = toupper(tmp[0]);
			file << "\t" << GetType(*it) << "\t\t" << (std::get<0>(*it) == "bool" ? "Is" : "Get") << tmp << "()const;" << std::endl;
			file << "\tvoid\t\t" << (std::get<0>(*it) == "bool" ? "" : "Set") << tmp << "(" << GetType(*it) << " " << propertyName << ");" << std::endl;
		}
	}
	file << "protected:" << std::endl;
	for (std::list<std::tuple<std::string, std::string, bool>>::const_iterator it = bindableProperties.begin(); it != bindableProperties.end(); ++it)
		if (!std::get<2>(*it))
			file << "\t" << std::get<0>(*it) << "\t\t" << (std::get<1>(*it)[0] == '_' ? "" : "_") << std::get<1>(*it) << ";" << std::endl;
	file << "};" << std::endl << std::endl;
	file << "#endif" << std::endl;
	file.close();
}

void	CreateSourceFile(const std::string& moduleName, const std::string& parentModule, std::list<std::tuple<std::string, std::string, bool>>& bindableProperties)
{
	std::ofstream file = std::ofstream(ModulePath + moduleName + ".cpp");

	if (!file)
		throw std::exception(("Can't open " + (ModulePath + moduleName) + ".cpp").c_str());
	std::string upperName = moduleName;
	std::transform(upperName.begin(), upperName.end(), upperName.begin(), ::toupper);

	file << "#include	\"Modules/" << moduleName << ".h\"" << std::endl << std::endl;

	file << moduleName << "::" << moduleName << "(GameObject* owner) : " << parentModule << "(owner)" << std::endl;
	file << "{" << std::endl << std::endl << "}" << std::endl << std::endl;

	file << moduleName << "::~" << moduleName << "()" << std::endl;
	file << "{" << std::endl << std::endl << "}" << std::endl << std::endl;

	file << "void\t\t\t\t" << moduleName << "::Initialize()" << std::endl;
	file << "{" << std::endl << std::endl << "}" << std::endl << std::endl;

	file << "void\t\t\t\t" << moduleName << "::Update(float deltaTime)" << std::endl;
	file << "{" << std::endl << std::endl << "}" << std::endl << std::endl;

	file << "void\t\t\t\t" << moduleName << "::Destroy()" << std::endl;
	file << "{" << std::endl << std::endl << "}" << std::endl << std::endl;

	file << "Module::Type\t" << moduleName << "::GetType()const{ return " << ModuleTypePrefix << moduleName << "; }" << std::endl;
	for (std::list<std::tuple<std::string, std::string, bool>>::const_iterator it = bindableProperties.begin(); it != bindableProperties.end(); ++it)
	{
		if (!std::get<2>(*it))
		{
			std::string propertyName = std::get<1>(*it);
			if (propertyName[0] == '_')
				propertyName = &propertyName[1];
			std::string tmp = propertyName;
			tmp[0] = toupper(tmp[0]);
			file << "void\t\t\t\t" << moduleName << (std::get<0>(*it) == "bool" ? "::" : "::Set") << tmp << "(" << GetType(*it) << " " << propertyName << ")";
			file << "{ " << (std::get<1>(*it)[0] == '_' ? "" : "_") << std::get<1>(*it) << " = " << propertyName << "; }" << std::endl;
			file << GetType(*it) << "\t" << moduleName << (std::get<0>(*it) == "bool" ? "::Is" : "::Get") << tmp << "()const ";
			file << "{ return _" << std::get<1>(*it) << "; }" << std::endl << std::endl;
		}
	}
	file.close();
}

void	InsertModuleTypeInModuleImport(const std::string& moduleName)
{
	std::ifstream ifile(ModuleInclude);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + ModuleInclude).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		if (buf.find(std::string(COMMENT_FORMAT) + "IncludeStart") != std::string::npos)
		{
			std::string toAdd("#include\t\"");
			toAdd += moduleName + ".h\"";
			lines.push_back(toAdd);
		}
	}
	ifile.close();

	std::ofstream ofile(ModuleInclude);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + ModuleInclude).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}
	ofile.close();
	lines.clear();
}

void	GenerateModuleType(const std::string& moduleName)
{
	std::ifstream ifile(ModuleHeader);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + ModuleHeader).c_str());
	std::list<std::string>	lines;
	std::string buf;
	bool	count = false;
	int		classNb = 0;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		if (buf.find(std::string(COMMENT_FORMAT) + "ModuleTypeStart") != std::string::npos)
		{
			count = true;
		}
		else if (buf.find(std::string(COMMENT_FORMAT) + "ModuleTypeEnd") != std::string::npos)
		{
			count = false;
			lines.remove(lines.back());
			lines.back().append(",");
			lines.push_back("\t\tE_" + moduleName + " = " + std::to_string(classNb - 1));
			lines.push_back("\t\t//@ModuleCreator ModuleTypeEnd");
		}
		else if (buf.find(std::string(COMMENT_FORMAT) + "ModuleClassNumber") != std::string::npos)
		{
			lines.push_back("\tstatic const int\t\tMODULE_CLASS_NUMBER = " + std::to_string(classNb) + ";");
			std::getline(ifile, buf);
		}
		if (count)
			++classNb;
	}
	ifile.close();

	std::ofstream ofile(ModuleHeader);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + ModuleHeader).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}
	ofile.close();
	lines.clear();
}

void	InsertInModuleFactory(const std::string moduleName)
{
	std::ifstream ifile(ModuleFactorySource);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + ModuleFactorySource).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		if (buf.find(std::string(COMMENT_FORMAT) + "SwitchCreationStart") != std::string::npos)
		{
			std::string variableName = lastParent ? *lastParent : moduleName;
			variableName[0] = tolower(variableName[0]);
			std::string toAdd("\tcase Module::E_");
			toAdd += moduleName + ":";
			lines.push_back(toAdd);
			lines.push_back("\t\tparent->" + variableName + " = new " + moduleName + "(parent);");
			lines.push_back("\t\tparent->" + variableName + "->Initialize();");
			lines.push_back("\t\tbreak;");
		}
	}
	ifile.close();

	std::ofstream ofile(ModuleFactorySource);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + ModuleFactorySource).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}
	ofile.close();
	lines.clear();
}

void	InsertAsGameObjectVariable(const std::string& moduleName)
{
	std::ifstream ifile(GameObjectHeader);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + GameObjectHeader).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		if (buf.find(std::string(COMMENT_FORMAT) + "ModuleVariableDeclaration") != std::string::npos)
		{
			std::string variableName = moduleName;
			variableName[0] = tolower(variableName[0]);
			lines.push_back("\t" + moduleName + "*\t\t" + variableName + ";");
		}
		else if (buf.find(std::string(COMMENT_FORMAT) + "ClassDeclaration") != std::string::npos)
		{
			lines.push_back("class " + moduleName + ";");
		}
	}
	ifile.close();

	std::ofstream ofile(GameObjectHeader);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + GameObjectHeader).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}
	ofile.close();
	lines.clear();
}

void	InitializingGameObjectVariable(const std::string& moduleName)
{
	std::ifstream ifile(GameObjectSource);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + GameObjectSource).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		if (buf.find(std::string(COMMENT_FORMAT) + "ModuleInitialization") != std::string::npos)
		{
			std::string variableName = moduleName;
			variableName[0] = tolower(variableName[0]);
			lines.push_back("\t" + variableName + " = NULL;");
		}
	}
	ifile.close();

	std::ofstream ofile(GameObjectSource);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + GameObjectSource).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}
	ofile.close();
	lines.clear();
}

void	GenerateXMLParserHeader(const std::string& moduleName)
{
	std::ifstream ifile(XmlParserHeaderPath);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + XmlParserHeaderPath).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		if (buf.find(std::string(COMMENT_FORMAT) + "ModuleFunctionParsingStart") != std::string::npos)
		{
			lines.push_back("\t\tbool Parse" + moduleName + "(TiXmlElement* elem, GameObject* object);");
		}
	}
	ifile.close();

	std::ofstream ofile(XmlParserHeaderPath);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + XmlParserHeaderPath).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}
	ofile.close();
	lines.clear();
}

void	GenerateXMLParserSource(const std::string& moduleName, std::list<std::tuple<std::string, std::string, bool>> bindableProperties)
{
	std::ifstream ifile(XmlParserSourcePath);
	std::string variableName = lastParent ? *lastParent : moduleName;
	variableName[0] = tolower(variableName[0]);

	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + XmlParserSourcePath).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		if (buf.find(std::string(COMMENT_FORMAT) + "FuncArrayModuleParsingStart") != std::string::npos)
		{
			lines.push_back("\t_moduleCreationFunc[Module::Type::E_" + moduleName + "] = &XmlParser::Parse" + moduleName + ";");
		}
	}
	ifile.close();

	std::ofstream ofile(XmlParserSourcePath);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + XmlParserSourcePath).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}

	ofile << std::endl << "bool	XmlParser::Parse" << moduleName << "(TiXmlElement* elem, GameObject* object)" << std::endl;
	ofile << "{" << std::endl;
	ofile << "\tif (!object->" << variableName << ")" << std::endl;
	ofile << "\t\treturn false;" << std::endl;
	ofile << "\t" << moduleName << "*\tmodule = static_cast<" << moduleName << "*>(object->" << variableName << ");" << std::endl;
	ofile << "\tvoid* value = NULL;" << std::endl;
	bool tmpVecCreated = false;
	for (auto it = bindableProperties.begin(); it != bindableProperties.end(); ++it)
	{
		std::string propertyName = std::get<1>(*it);
		if (propertyName[0] == '_')
			propertyName = &propertyName[1];
		std::string tmp = propertyName;
		tmp[0] = toupper(tmp[0]);
		if (std::get<0>(*it) == "Vec2")
		{
			if (!tmpVecCreated)
			{
				ofile << "\tVec2 tmpVec2;" << std::endl;
				tmpVecCreated = true;
			}
			ofile << "value = new float;" << std::endl;
			ofile << "\telem->QueryFloatAttribute(\"" << propertyName << "X\", (float*)value);" << std::endl;
			ofile << "\ttmpVec2.x = *(float*)(value);" << std::endl;
			ofile << "\telem->QueryFloatAttribute(\"" << propertyName << "Y\", (float*)value);" << std::endl;
			ofile << "\ttmpVec2.y = *(float*)(value);" << std::endl;
			ofile << "\tmodule->Set" << tmp << "(tmpVec2);" << std::endl;
			ofile << "delete(value);" << std::endl;
		}
		else if (std::get<0>(*it) == "char*" || std::get<0>(*it) == "std::string")
		{
			if (std::get<0>(*it) == "std::string")
				ofile << "\tmodule->Set" << tmp << "(elem->Attribute(\"" << propertyName << "\"));" << std::endl;
			else
				ofile << "\tmodule->Set" << tmp << "(const_cast<char*>(elem->Attribute(\"" << propertyName << "\")));" << std::endl;
		}
		else
		{
			tmp = std::get<0>(*it);
			ofile << "value = new " << tmp << ";" << std::endl;
			tmp[0] = toupper(tmp[0]);
			ofile << "\telem->Query" << tmp << "Attribute(\"" << propertyName << "\", (" << std::get<0>(*it) << "*)value);" << std::endl;
			tmp = propertyName;
			tmp[0] = toupper(tmp[0]);
			ofile << "\tmodule->" << (std::get<0>(*it) == "bool" ? tmp : ("Set" + tmp)) << "(*(" << std::get<0>(*it) << "*)(value));" << std::endl;
			ofile << "delete(value);" << std::endl;
		}
	}
	ofile << "\treturn true;" << std::endl;
	ofile << "}" << std::endl;

	ofile.close();
	lines.clear();
}

void	GenerateXMLWriterHeader(const std::string& moduleName)
{
	std::ifstream ifile(XmlWriterHeaderPath);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + XmlWriterHeaderPath).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		if (buf.find(std::string(COMMENT_FORMAT) + "ModuleFunctionSerializationStart") != std::string::npos)
		{
			lines.push_back("\t\tbool\tSerialize" + moduleName + "(Module* module, TiXmlElement* node);");
		}
	}
	ifile.close();

	std::ofstream ofile(XmlWriterHeaderPath);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + XmlWriterHeaderPath).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}
	ofile.close();
	lines.clear();
}

void	GenerateXMLWriterSource(const std::string& moduleName, std::list<std::tuple<std::string, std::string, bool>> bindableProperties)
{
	std::ifstream ifile(XmlWriterSourcePath);
	std::string variableName = lastParent ? *lastParent : moduleName;
	variableName[0] = tolower(variableName[0]);

	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + XmlWriterSourcePath).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		if (buf.find(std::string(COMMENT_FORMAT) + "FuncArrayModuleSerializationStart") != std::string::npos)
		{
			//		_moduleSerializationFunc[Module::Type::E_AnimatedSpriteModule] = &XmlWriter::SerializeAnimatedSpriteModule;
			lines.push_back("\t\t_moduleSerializationFunc[Module::Type::E_" + moduleName + "] = &XmlWriter::Serialize" + moduleName + ";");
		}
	}
	ifile.close();
	lines.remove(lines.back());

	std::ofstream ofile(XmlWriterSourcePath);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + XmlWriterSourcePath).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}

	ofile << std::endl << "\tbool	XmlWriter::Serialize" << moduleName << "(Module* module, TiXmlElement* node)" << std::endl;
	ofile << "\t{" << std::endl;
	ofile << "\t\tif (!module)" << std::endl;
	ofile << "\t\t\treturn false;" << std::endl;
	ofile << "\t\t" << moduleName << "*\tmod = static_cast<" << moduleName << "*>(module);" << std::endl;
	bool tmpVecCreated = false;
	for (auto it = bindableProperties.begin(); it != bindableProperties.end(); ++it)
	{
		std::string propertyName = std::get<1>(*it);
		if (propertyName[0] == '_')
			propertyName = &propertyName[1];
		std::string tmp = propertyName;
		tmp[0] = toupper(tmp[0]);
		if (std::get<0>(*it) == "Vec2")
		{
			if (!tmpVecCreated)
			{
				ofile << "\t\tVec2 tmpVec2;" << std::endl;
				tmpVecCreated = true;
			}
			ofile << "\t\ttmpVec2 = mod->Get" << tmp << "();" << std::endl;
			ofile << "\t\tnode->SetDoubleAttribute(\"" << propertyName << "X\", tmpVec2.x);" << std::endl;
			ofile << "\t\tnode->SetDoubleAttribute(\"" << propertyName << "Y\", tmpVec2.y);" << std::endl;
		}
		else
		{
			ofile << "\t\tnode->Set" << (std::get<0>(*it) == "float" || std::get<0>(*it) == "double" ? "Double" : "") << "Attribute(\"" << propertyName << "\", mod->" << (std::get<0>(*it) == "bool" ? "Is" : "Get") << tmp << "()" << (std::get<0>(*it) == "std::string" ? ".c_str()" : "") << ");" << std::endl;
		}
	}
	ofile << "\t\treturn true;" << std::endl;
	ofile << "\t}" << std::endl;
	ofile << "}" << std::endl;

	ofile.close();
	lines.clear();
}

void	GenerateMenuClassHeader(const std::string& moduleName, std::list<std::tuple<std::string, std::string, bool>> bindableProperties)
{
	std::ofstream file = std::ofstream(MenuPath + moduleName + "Menu.h");

	if (!file)
		throw std::exception(("Can't open " + (ModulePath + moduleName) + "Menu.h").c_str());
	std::string upperName = moduleName + "MENU";
	std::transform(upperName.begin(), upperName.end(), upperName.begin(), ::toupper);
	file << "#ifndef " << upperName << "_H_" << std::endl;
	file << "#define " << upperName << "_H_" << std::endl << std::endl;
	file << "#include	\"Menus/Menu.h\"" << std::endl << std::endl;

	file << "class " << moduleName << "Menu : public Menu" << std::endl;
	file << "{" << std::endl;

	file << "public:" << std::endl;
	file << "\t" << moduleName << "Menu();" << std::endl;
	file << "\t~" << moduleName << "Menu();" << std::endl << std::endl;
	file << "\tvirtual void	Initialize(int offset);" << std::endl << std::endl;
	file << "\tvirtual void	Update(GameObject* currentGameObject);" << std::endl << std::endl;
	file << "\tvirtual void	Clear();" << std::endl << std::endl;
	file << "\tvirtual void InitGameObject(GameObject* object)const;" << std::endl << std::endl;
	file << "protected:" << std::endl;

	for (std::list<std::tuple<std::string, std::string, bool>>::const_iterator it = bindableProperties.begin(); it != bindableProperties.end(); ++it)
	{
		std::string type = std::get<0>(*it);
		if (type == "Vec2")
		{
			file <<"\tFl_Float_Input*\t\t" << std::get<1>(*it) << "X;" << std::endl;
			file << "\tFl_Float_Input*\t\t" << std::get<1>(*it) << "Y;" << std::endl;
		}
		else if (type == "std::string" || type == "char*")
		{
			file << "\tFl_Button*\t\t\t\t" << std::get<1>(*it) << "Button;" << std::endl;
			file << "\tFl_Box*\t\t\t\t\t" << std::get<1>(*it) << "Name;" << std::endl;
		}
		else if (type == "bool")
			file << "\tFl_Check_Button*\t\t" << std::get<1>(*it) << ";" << std::endl;
		else
		{
			type[0] = toupper(type[0]);
			file << "\tFl_" << (type == "Double" ? "Float" : type) << "_Input*\t\t\t" << std::get<1>(*it) << ";" << std::endl;
		}
	}

	file << "};" << std::endl << std::endl;
	file << "#endif" << std::endl;
	file.close();
}

void	GenerateMenuClassSource(const std::string& moduleName)
{
	std::ofstream file = std::ofstream(MenuPath + moduleName + "Menu.cpp");

	if (!file)
		throw std::exception(("Can't open " + (ModulePath + moduleName) + "Menu.h").c_str());
	file << "#include	\"Menus/" << moduleName << "Menu.h\"" << std::endl << std::endl;

	file << moduleName << "Menu::" << moduleName << "Menu()" << std::endl;
	file << "{" << std::endl << std::endl << "}" << std::endl << std::endl;
	file << moduleName << "Menu::~" << moduleName << "Menu()" << std::endl;
	file << "{" << std::endl << std::endl << "}" << std::endl << std::endl;

	file << "void\t" << moduleName << "Menu::Initialize(int offset)" << std::endl;
	file << "{" << std::endl << std::endl << "}" << std::endl << std::endl;

	file << "void\t" << moduleName << "Menu::InitGameObject(GameObject* object)const" << std::endl;
	file << "{" << std::endl << std::endl << "}" << std::endl << std::endl;

	file << "void\t" << moduleName << "Menu::Update(GameObject* currentGameObject)" << std::endl;
	file << "{" << std::endl << std::endl << "}" << std::endl << std::endl;

	file << "void\t" << moduleName << "Menu::Clear()" << std::endl;
	file << "{" << std::endl << std::endl << "}" << std::endl << std::endl;

	file.close();
}

void	GenerateLevelEditorFunctionsHeader(const std::string& moduleName)
{
	std::ifstream ifile(LevelEditorHeader);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + LevelEditorHeader).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		if (buf.find(std::string(COMMENT_FORMAT) + "ModuleMenuInclude") != std::string::npos)
		{
			lines.push_back("#include \"../Menus/" + moduleName + "Menu.h\"");
		}
		else if (buf.find(std::string(COMMENT_FORMAT) + "ModuleVeriableDeclarationStart") != std::string::npos)
		{
			std::string var = moduleName;
			var[0] = tolower(var[0]);
			lines.push_back("\t" + moduleName + "Menu*\t\t_" + var + "Menu;");
		}
	}
	ifile.close();

	std::ofstream ofile(LevelEditorHeader);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + LevelEditorHeader).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}
	ofile.close();
	lines.clear();
}

void	GenerateLevelEditorFunctionsSource(const std::string& moduleName)
{
	std::ifstream ifile(LevelEditorSource);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + LevelEditorSource).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		std::string var = "_" + moduleName + "Menu";
		var[1] = tolower(var[1]);
		if (buf.find(std::string(COMMENT_FORMAT) + "InitGameObjectStart") != std::string::npos)
		{
			lines.push_back("\t" + var + "->InitGameObject(currentGameObject);");
		}
		else if (buf.find(std::string(COMMENT_FORMAT) + "UpdateModuleMenuStart") != std::string::npos)
		{
			lines.push_back("\t\t" + var + "->Update(currentGameObject);");
		}
		else if (buf.find(std::string(COMMENT_FORMAT) + "VariableInitialization") != std::string::npos)
		{
					lines.push_back("\t" + var + " = new " + moduleName + "Menu();");
		}
		else if (buf.find(std::string(COMMENT_FORMAT) + "ClearModuleMenuStart") != std::string::npos)
		{
			lines.push_back("\t" + var + "->Clear();");
		}
	}
	ifile.close();

	std::ofstream ofile(LevelEditorSource);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + LevelEditorSource).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}
	ofile.close();
	lines.clear();
}

void	InsertInClassCreator(const std::string& moduleName)
{
	std::ifstream ifile(ClassCreatorScript);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + ClassCreatorScript).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		std::string var = "_" + moduleName;
		var[1] = tolower(var[1]);
		if (buf.find(std::string(COMMENT_FORMAT) + "ModuleVariableNameArrayStart") != std::string::npos)
		{
			std::string tmp = *lastParent;
			tmp[0] = tolower(tmp[0]);
			lines.push_back("\tmodulesNames[Module::Type::E_" + moduleName + "] = \"" + tmp + "\";");
		}
		else if (buf.find(std::string(COMMENT_FORMAT) + "ModuleNamesArrayStart") != std::string::npos)
		{
			lines.push_back("\tmodulesNames[Module::Type::E_" + moduleName + "] = \"" + moduleName + "\";");
		}
		else if (buf.find(std::string(COMMENT_FORMAT) + "ModuleRequestsStart") != std::string::npos)
		{
			lines.push_back("\tstd::cout << \"Adding " + moduleName + " ? [y/n] : \";");
			lines.push_back("\tstd::cin >> strInput;");
			lines.push_back("\tstd::cout << std::endl;");
			lines.push_back("\tmodules[Module::Type::E_" + moduleName + "] = strInput[0] == 'y';\n");
		}
	}
	ifile.close();

	std::ofstream ofile(ClassCreatorScript);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + ClassCreatorScript).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}
	ofile.close();
	lines.clear();
}

int main()
{
	std::string moduleName;
	std::string moduleParentName;
	std::list<std::tuple<std::string, std::string, bool>> bindableProperties;

	std::cout << "Module Name : ";
	std::cin >> moduleName;
	std::cout << std::endl;

	std::cout << "Module Parent Name (if no parent, put Module) : ";
	std::cin >> moduleParentName;
	std::cout << std::endl;

	GetParentProperties(moduleParentName, bindableProperties);
	GetUserProperties(bindableProperties);

	std::cout << "Creating header and source files..." << std::endl;
	CreateHeaderFile(moduleName, moduleParentName, bindableProperties);
	CreateSourceFile(moduleName, moduleParentName, bindableProperties);
	std::cout << "Done" << std::endl;

	std::cout << "Inserting header file in ModuleInclude.h..." << std::endl;
	InsertModuleTypeInModuleImport(moduleName);
	std::cout << "Done" << std::endl;

	std::cout << "Generating Module Type..." << std::endl;
	GenerateModuleType(moduleName);
	std::cout << "Done" << std::endl;

	std::cout << "Generating Module Creation in ModuleFactory..." << std::endl;
	InsertInModuleFactory(moduleName);
	std::cout << "Done" << std::endl;

	if (moduleParentName == "Module")
	{
		std::cout << "Generating GameObject variable..." << std::endl;
		InsertAsGameObjectVariable(moduleName);
		InitializingGameObjectVariable(moduleName);
		std::cout << "Done" << std::endl;
	}

	std::cout << "Generating class parsing..." << std::endl;
	GenerateXMLParserHeader(moduleName);
	GenerateXMLParserSource(moduleName, bindableProperties);
	std::cout << "Done" << std::endl;

	std::cout << "Generating class serializing..." << std::endl;
	GenerateXMLWriterHeader(moduleName);
	GenerateXMLWriterSource(moduleName, bindableProperties);
	std::cout << "Done" << std::endl;

	std::cout << "Generating Menu class..." << std::endl;
	GenerateMenuClassHeader(moduleName, bindableProperties);
	GenerateMenuClassSource(moduleName);
	std::cout << "Done" << std::endl;

	std::cout << "Inserting menu in LevelEditor..." << std::endl;
	GenerateLevelEditorFunctionsHeader(moduleName);
	GenerateLevelEditorFunctionsSource(moduleName);
	std::cout << "Done" << std::endl;

	std::cout << "Inserting module in ClassCreator..." << std::endl;
	InsertInClassCreator(moduleName);
	std::cout << "Done" << std::endl;
}