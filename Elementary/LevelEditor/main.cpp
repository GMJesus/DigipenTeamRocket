////////////////////////////////////////////////////////////////////////////////
// Filename: main.cpp
////////////////////////////////////////////////////////////////////////////////

#include <Windows.h>
#include "EditorEngine\EditorEngine.h"
#include "External Dependencies\FL\Fl.H"
#include "External Dependencies\FL\Fl_Window.H"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pScmdline, int iCmdshow)
{
	EditorEngine::Engine* EditorEngine;
	bool result;
	
	
	// Create the EditorEngine object.
	EditorEngine = EditorEngine::Engine::Inst();
	if(!EditorEngine)
	{
		return 0;
	}

	// Initialize and run the EditorEngine object.
	result = EditorEngine->Initialize();
	if(result)
	{
		EditorEngine->Run();
	}

	// Shutdown and release the EditorEngine object.
	EditorEngine->Shutdown();
	EditorEngine = NULL;

	return 0;
}