#ifndef		EDITORPARAMETERS_H_
# define	EDITORPARAMETERS_H_

namespace EditorEngine
{
	struct s_EditorParameters
	{
		char*	lastOpenedFile;
		int		toolWindowWidth;
		int		toolWindowHeight;
	};

	extern s_EditorParameters engineParameters;
}

#endif		// !LVLENGINEPARAMETERS_H_