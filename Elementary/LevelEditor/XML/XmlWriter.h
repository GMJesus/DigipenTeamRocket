/**
* \author	Aymeric Lambolez
*/

#ifndef		XMLWRITER_H_
# define	XMLWRITER_H_

#include	<map>

#include	"Game/GameObject.h"
#include	"Game/GameObjectFactory.h"
#include	"Utils/EditorParameters.h"
#include	"Utils/EngineParameters.h"
#include	"External libraries/TinyXML/tinyxml.h"

namespace XML
{
	class XmlWriter
	{
		typedef	bool(XmlWriter::*ObjectSerialization)(GameObject*, TiXmlElement* node);
		typedef	bool(XmlWriter::*ModuleSerialization)(Module*, TiXmlElement* node);
	public:
		XmlWriter();
		~XmlWriter();

		bool Serialize(const char* fileName);
		bool SavePreferences(const char* fileName);

	private:
		std::list<unsigned int>	_parsedObjects;
		TiXmlElement*	SerializeGameObject(GameObject* obj);
		TiXmlElement*	SerializeModule(Module* module);
		bool			SerializePlayerSpawn(GameObject* obj, TiXmlElement* node);
		void			AddObjectToElem(unsigned int id, GameObject* obj, TiXmlElement* elem, bool isChild = false);

		// Object Creation Functions
		ObjectSerialization*		_objectSerializationFunc;
		//@ClassCreator FuncArrayObjectSerializationStart
		bool	SerializeInteractivePlatform(GameObject* obj, TiXmlElement* node);
		bool	SerializeEndOfLevel(GameObject* obj, TiXmlElement* node);
		bool	SerializeCheckpoint(GameObject* obj, TiXmlElement* node);
		bool	SerializeDoor(GameObject* obj, TiXmlElement* node);
		bool	SerializeSwitch(GameObject* obj, TiXmlElement* node);
		bool	SerializeBackAndForthObject(GameObject* obj, TiXmlElement* node);
		bool	SerializeElementalInteractiveObject(GameObject* obj, TiXmlElement* node);
		bool	SerializeAnimatedElement(GameObject* obj, TiXmlElement* node);
		bool	SerializeGun(GameObject* obj, TiXmlElement* node);
		bool	SerializeProjectile(GameObject* obj, TiXmlElement* node);
		bool	SerializeStaticObject(GameObject* obj, TiXmlElement* node);
		bool	SerializeCharacter(GameObject* obj, TiXmlElement* node);
		bool	SerializeParticleSystem(GameObject* obj, TiXmlElement* node);
		bool	SerializePlayer(GameObject* obj, TiXmlElement* node);
		bool	SerializeActor(GameObject* obj, TiXmlElement* node);
		bool	SerializeDecorativeElement(GameObject* obj, TiXmlElement* node);
		bool	SerializeInvisibleWall(GameObject* obj, TiXmlElement* node);
		bool	SerializeBackgroundElement(GameObject* obj, TiXmlElement* node);

		// Module creation Functions
		ModuleSerialization*		_moduleSerializationFunc;
		//@ModuleCreator ModuleFunctionSerializationStart
		bool	SerializeInteractivePlatformLogicModule(Module* module, TiXmlElement* node);
		bool	SerializeEndLevelLogicModule(Module* module, TiXmlElement* node);
		bool	SerializeCheckpointLogicModule(Module* module, TiXmlElement* node);
		bool	SerializeDoorLogicModule(Module* module, TiXmlElement* node);
		bool	SerializeSwitchLogicModule(Module* module, TiXmlElement* node);
		bool	SerializeInteractiveObjectLogicModule(Module* module, TiXmlElement* node);
		bool	SerializeBackAndForthBehaviourModule(Module* module, TiXmlElement* node);
		bool	SerializeStateModule(Module* module, TiXmlElement* node);
		bool	SerializeGunLogicModule(Module* module, TiXmlElement* node);
		bool	SerializeProjLogicModule(Module* module, TiXmlElement* node);
		bool	SerializeSpriteSheetAnimationModule(Module* module, TiXmlElement* node);
		bool	SerializeCharacterLogicModule(Module* module, TiXmlElement* node);
		bool	SerializeAnimatedSpriteModule(Module* module, TiXmlElement* node);
		bool	SerializeBoxCollisionModule(Module* module, TiXmlElement* node);
		bool	SerializeLifeModule(Module* module, TiXmlElement* node);
		bool	SerializeLogicModule(Module* module, TiXmlElement* node);
		bool	SerializePhysicModule(Module* module, TiXmlElement* node);
		bool	SerializeSoundModule(Module* module, TiXmlElement* node);
		bool	SerializeSpriteModule(Module* module, TiXmlElement* node);
		bool	SerializeTransformModule(Module* module, TiXmlElement* node);
		bool	SerializeParticleSystemModule(Module* module, TiXmlElement* node);

	};
}

#endif		// !XMLWRITER_H_
