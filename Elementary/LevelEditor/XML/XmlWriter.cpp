/**
* \author	Aymeric Lambolez
*/

#include	"XmlWriter.h"

namespace XML
{
	XmlWriter::XmlWriter()
	{
		_moduleSerializationFunc = new ModuleSerialization[Module::MODULE_CLASS_NUMBER];
		//@ModuleCreator FuncArrayModuleSerializationStart
		_moduleSerializationFunc[Module::Type::E_InteractivePlatformLogicModule] = &XmlWriter::SerializeInteractivePlatformLogicModule;
		_moduleSerializationFunc[Module::Type::E_EndLevelLogicModule] = &XmlWriter::SerializeEndLevelLogicModule;
		_moduleSerializationFunc[Module::Type::E_CheckpointLogicModule] = &XmlWriter::SerializeCheckpointLogicModule;
		_moduleSerializationFunc[Module::Type::E_DoorLogicModule] = &XmlWriter::SerializeDoorLogicModule;
		_moduleSerializationFunc[Module::Type::E_SwitchLogicModule] = &XmlWriter::SerializeSwitchLogicModule;
		_moduleSerializationFunc[Module::Type::E_InteractiveObjectLogicModule] = &XmlWriter::SerializeInteractiveObjectLogicModule;
		_moduleSerializationFunc[Module::Type::E_BackAndForthBehaviourModule] = &XmlWriter::SerializeBackAndForthBehaviourModule;
		_moduleSerializationFunc[Module::Type::E_StateModule] = &XmlWriter::SerializeStateModule;
		_moduleSerializationFunc[Module::Type::E_GunLogicModule] = &XmlWriter::SerializeGunLogicModule;
		_moduleSerializationFunc[Module::Type::E_ProjLogicModule] = &XmlWriter::SerializeProjLogicModule;
		_moduleSerializationFunc[Module::Type::E_SpriteSheetAnimationModule] = &XmlWriter::SerializeSpriteSheetAnimationModule;
		_moduleSerializationFunc[Module::Type::E_CharacterLogicModule] = &XmlWriter::SerializeCharacterLogicModule;
		_moduleSerializationFunc[Module::Type::E_AnimatedSpriteModule] = &XmlWriter::SerializeAnimatedSpriteModule;
		_moduleSerializationFunc[Module::Type::E_BoxCollisionModule] = &XmlWriter::SerializeBoxCollisionModule;
		_moduleSerializationFunc[Module::Type::E_LifeModule] = &XmlWriter::SerializeLifeModule;
		_moduleSerializationFunc[Module::Type::E_LogicModule] = &XmlWriter::SerializeLogicModule;
		_moduleSerializationFunc[Module::Type::E_PhysicModule] = &XmlWriter::SerializePhysicModule;
		_moduleSerializationFunc[Module::Type::E_SoundModule] = &XmlWriter::SerializeSoundModule;
		_moduleSerializationFunc[Module::Type::E_SpriteModule] = &XmlWriter::SerializeSpriteModule;
		_moduleSerializationFunc[Module::Type::E_TransformModule] = &XmlWriter::SerializeTransformModule;
		_moduleSerializationFunc[Module::Type::E_ParticleSystemModule] = &XmlWriter::SerializeParticleSystemModule;

		_objectSerializationFunc = new ObjectSerialization[GameObject::GAMEOBJECT_CLASS_NUMBER];
		//@ClassCreator FuncArrayObjectSerializationStart
		_objectSerializationFunc[GameObject::Type::E_InteractivePlatform] = &XmlWriter::SerializeInteractivePlatform;
		_objectSerializationFunc[GameObject::Type::E_EndOfLevel] = &XmlWriter::SerializeEndOfLevel;
		_objectSerializationFunc[GameObject::Type::E_Checkpoint] = &XmlWriter::SerializeCheckpoint;
		_objectSerializationFunc[GameObject::Type::E_Door] = &XmlWriter::SerializeDoor;
		_objectSerializationFunc[GameObject::Type::E_Switch] = &XmlWriter::SerializeSwitch;
		_objectSerializationFunc[GameObject::Type::E_BackAndForthObject] = &XmlWriter::SerializeBackAndForthObject;
		_objectSerializationFunc[GameObject::Type::E_ElementalInteractiveObject] = &XmlWriter::SerializeElementalInteractiveObject;
		_objectSerializationFunc[GameObject::Type::E_AnimatedElement] = &XmlWriter::SerializeAnimatedElement;
		_objectSerializationFunc[GameObject::Type::E_Gun] = &XmlWriter::SerializeGun;
		_objectSerializationFunc[GameObject::Type::E_Projectile] = &XmlWriter::SerializeProjectile;
		_objectSerializationFunc[GameObject::Type::E_StaticObject] = &XmlWriter::SerializeStaticObject;
		_objectSerializationFunc[GameObject::Type::E_Character] = &XmlWriter::SerializeCharacter;
		_objectSerializationFunc[GameObject::Type::E_ParticleSystem] = &XmlWriter::SerializeParticleSystem;
		_objectSerializationFunc[GameObject::Type::E_Player] = &XmlWriter::SerializePlayer;
		_objectSerializationFunc[GameObject::Type::E_Actor] = &XmlWriter::SerializeActor;
		_objectSerializationFunc[GameObject::Type::E_BackgroundElement] = &XmlWriter::SerializeBackgroundElement;
		_objectSerializationFunc[GameObject::Type::E_Decorative_Element] = &XmlWriter::SerializeDecorativeElement;
		_objectSerializationFunc[GameObject::Type::E_InvisibleWall] = &XmlWriter::SerializeInvisibleWall;
	}

	XmlWriter::~XmlWriter()
	{

	}

	bool		XmlWriter::SavePreferences(const char* fileToSave)
	{
		TiXmlDocument doc;
		TiXmlDeclaration*	decl = new TiXmlDeclaration("1.0", "ISO-8859-1", "");
		doc.LinkEndChild(decl);
		TiXmlElement*		param = new TiXmlElement("Parameters");
		doc.LinkEndChild(param);

		TiXmlElement*		node = new TiXmlElement("Window");
		node->SetAttribute("ScreenWidth", windowDatas.screenWidth);
		node->SetAttribute("ScreenHeight", windowDatas.screenHeight);
		node->SetAttribute("Fullscreen", windowDatas.fullScreen ? "true" : "false");
		node->SetAttribute("VSync", windowDatas.vSync ? "true" : "false");
		param->LinkEndChild(node);

#ifdef GAME
		TiXmlElement*		node2 = new TiXmlElement("Engine");
		node2->SetAttribute("DrawFPS", GameEngine::engineParameters.drawFPS ? "true" : "false");
		node2->SetAttribute("DebugActive", GameEngine::engineParameters.debugActive ? "true" : "false");
		node2->SetAttribute("DebugCollision", GameEngine::engineParameters.debugCollision ? "true" : "false");
		param->LinkEndChild(node2);
#elif EDITOR
		TiXmlElement*		node2 = new TiXmlElement("Editor");
		node2->SetAttribute("lastOpenedFile", EditorEngine::engineParameters.lastOpenedFile);
		node2->SetAttribute("toolWindowWidth", EditorEngine::engineParameters.toolWindowWidth);
		node2->SetAttribute("toolWindowHeight", EditorEngine::engineParameters.toolWindowHeight);
		param->LinkEndChild(node2);
#endif
		doc.SaveFile(fileToSave);
		return true;
	}

	void		XmlWriter::AddObjectToElem(unsigned int id, GameObject* obj, TiXmlElement* elem, bool isChild)
	{
		if (std::find(_parsedObjects.begin(), _parsedObjects.end(), id) == _parsedObjects.end())
		{
			std::list<GameObject*> childs;
			if (obj->transformModule && (childs = obj->transformModule->GetChilds()).size() && (!obj->transformModule->GetParent() || isChild))
			{
				elem->LinkEndChild(SerializeGameObject(obj));
				for (std::list<GameObject*>::const_iterator childIt = childs.begin(); childIt != childs.end(); ++childIt)
					AddObjectToElem((*childIt)->Id(), *childIt, elem, true);
			}
			else if (!(obj->transformModule && obj->transformModule->GetParent()) || isChild)
				elem->LinkEndChild(SerializeGameObject(obj));
		}
	}

	bool		XmlWriter::Serialize(const char* fileName)
	{
		std::map<unsigned int, GameObject*> map = GameObjectFactory::Inst()->GetObjectMap();
		std::string name = fileName;
		if (!map.size())
			return false;
		TiXmlDocument doc;
		TiXmlDeclaration*	decl = new TiXmlDeclaration("1.0", "ISO-8859-1", "");
		doc.LinkEndChild(decl);
		TiXmlElement*		param = new TiXmlElement("Parameters");
		doc.LinkEndChild(param);

		for (std::map<unsigned int, GameObject*>::const_iterator it = map.begin(); it != map.end(); ++it)
			AddObjectToElem(it->first, it->second, param);

		doc.SaveFile(name.c_str());
		return true;
	}

	bool				XmlWriter::SerializePlayerSpawn(GameObject* obj, TiXmlElement* node)
	{
		node->SetAttribute("type", -1);
		Vec2 pos = obj->transformModule->Position();
		node->SetAttribute("x", pos.x);
		node->SetAttribute("y", pos.y);
		node->SetAttribute("name", "Player");
		SerializeCharacter(obj, node);
		return true;
	}

	TiXmlElement*		XmlWriter::SerializeGameObject(GameObject* obj)
	{
		if (!obj)
			return NULL;
		TiXmlElement*	node = new TiXmlElement("Object");
#ifdef EDITOR
		if (obj->GetType() != GameObject::Type::E_Character)
		{
#endif
			node->SetAttribute("type", obj->GetType());
			node->SetAttribute("name", obj->GetName().c_str());
			(this->*_objectSerializationFunc[obj->GetType()])(obj, node);
#ifdef EDITOR
		}
		else
			SerializePlayerSpawn(obj, node);
#endif
		_parsedObjects.push_back(obj->Id());
		return node;
	}

	TiXmlElement*		XmlWriter::SerializeModule(Module* module)
	{
		if (!module)
			return NULL;
		TiXmlElement*	node = new TiXmlElement("Module");
		node->SetAttribute("type", module->GetType());
		node->SetAttribute("enable", module->IsEnable() ? "true" : "false");
		(this->*_moduleSerializationFunc[module->GetType()])(module, node);
		return node;
	}

	bool		XmlWriter::SerializeAnimatedSpriteModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;

		AnimatedSpriteModule*	mod = static_cast<AnimatedSpriteModule*>(module);
		node->SetAttribute("path", mod->GetTexturePlane().GetTexturePath() ? mod->GetTexturePlane().GetTexturePath() : "");
		node->SetAttribute("width", mod->GetWidth());
		node->SetAttribute("height", mod->GetHeight());
		node->SetAttribute("layer", mod->GetLayer());
		node->SetAttribute("x", mod->GetX());
		node->SetAttribute("y", mod->GetY());

		return true;
	}

	bool		XmlWriter::SerializeBoxCollisionModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;
		BoxCollisionModule*	mod = static_cast<BoxCollisionModule*>(module);
		node->SetDoubleAttribute("width", mod->GetBoxWidth());
		node->SetDoubleAttribute("height", mod->GetBoxHeight());
		node->SetAttribute("generateHitEvents", mod->GenerateHitEvents() ? "true" : "false");
		node->SetAttribute("collisionEnabled", mod->CollisionEnabled() ? "true" : "false");

		return true;
	}

	bool		XmlWriter::SerializeLifeModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;
		LifeModule*	mod = static_cast<LifeModule*>(module);
		node->SetAttribute("maxHealth", mod->GetMaxHealth());
		node->SetAttribute("immortal", mod->IsImmortal());

		return true;
	}

	bool		XmlWriter::SerializeLogicModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;

		return true;
	}

	bool		XmlWriter::SerializePhysicModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;
		PhysicModule*	mod = static_cast<PhysicModule*>(module);
		node->SetAttribute("enableGravity", mod->GravityEnabled() ? "true" : "false");
		node->SetDoubleAttribute("absorption", mod->GetAbsorption());
		if (mod->GetInvMass() != 0)
			node->SetDoubleAttribute("mass", 1 / mod->GetInvMass());
		else
			node->SetDoubleAttribute("mass", 0);

		return true;
	}

	bool		XmlWriter::SerializeSoundModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;

		SoundModule*	mod = static_cast<SoundModule*>(module);
		node->SetAttribute("path", mod->GetPath().c_str());
		node->SetAttribute("loop", mod->IsLoopSound());
		node->SetAttribute("volume", mod->Getvolume());
		return true;
	}

	bool		XmlWriter::SerializeParticleSystemModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;

		ParticleSystemModule*	mod = static_cast<ParticleSystemModule*>(module);
		if (mod->GetTexture())
			node->SetAttribute("path", mod->GetTexture()->GetTexturePath());
		node->SetAttribute("angle", mod->GetAngle());
		node->SetAttribute("lifeTime", mod->GetLifeTime());
		node->SetAttribute("VelocityX", mod->GetInitialVelocity().x);
		node->SetAttribute("VelocityY", mod->GetInitialVelocity().y);
		node->SetAttribute("width", mod->GetParticlesWidth());
		node->SetAttribute("height", mod->GetParticlesHeight());
		node->SetAttribute("enableGravity", mod->IsGravityEnabled());
		node->SetAttribute("layer", mod->GetLayer());
		node->SetAttribute("spawningRate", mod->GetSpawningRate());

		return true;
	}

	bool		XmlWriter::SerializeSpriteModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;
		SpriteModule*	mod = static_cast<SpriteModule*>(module);
		node->SetAttribute("path", mod->GetTexturePlane().GetTexturePath() ? mod->GetTexturePlane().GetTexturePath() : "");
		node->SetAttribute("width", mod->GetWidth());
		node->SetAttribute("height", mod->GetHeight());
		node->SetAttribute("layer", mod->GetLayer());
		node->SetAttribute("x", mod->GetX());
		node->SetAttribute("y", mod->GetY());
		return true;
	}

	bool		XmlWriter::SerializeTransformModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;
		TransformModule*	mod = static_cast<TransformModule*>(module);
		node->SetDoubleAttribute("x", mod->Position().x);
		node->SetDoubleAttribute("y", mod->Position().y);
		node->SetDoubleAttribute("angle", mod->Rotation());
		if (mod->GetParent())
		{
			node->SetAttribute("parent", strdup(mod->GetParent()->GetName().c_str()));
		}
		return true;
	}

	bool		XmlWriter::SerializeActor(GameObject* obj, TiXmlElement* node)
	{
		if (!obj)
			return false;
		node->LinkEndChild(SerializeModule(obj->displayModule));
		node->LinkEndChild(SerializeModule(obj->boxCollisionModule));
		node->LinkEndChild(SerializeModule(obj->transformModule));
		node->LinkEndChild(SerializeModule(obj->physicModule));
		node->LinkEndChild(SerializeModule(obj->logicModule));

		return true;
	}

	bool		XmlWriter::SerializeBackgroundElement(GameObject* obj, TiXmlElement* node)
	{
		if (!obj)
			return false;
		node->LinkEndChild(SerializeModule(obj->displayModule));
		return true;
	}

	bool		XmlWriter::SerializeDecorativeElement(GameObject* obj, TiXmlElement* node)
	{
		if (!obj)
			return false;
		node->LinkEndChild(SerializeModule(obj->displayModule));
		node->LinkEndChild(SerializeModule(obj->transformModule));
		return true;
	}

	bool		XmlWriter::SerializeInvisibleWall(GameObject* obj, TiXmlElement* node)
	{
		if (!obj)
			return false;
		node->LinkEndChild(SerializeModule(obj->boxCollisionModule));
		node->LinkEndChild(SerializeModule(obj->transformModule));
		return true;
	}

	bool		XmlWriter::SerializePlayer(GameObject* obj, TiXmlElement* node)
	{
		if (!obj)
			return false;
		node->LinkEndChild(SerializeModule(obj->displayModule));
		node->LinkEndChild(SerializeModule(obj->boxCollisionModule));
		node->LinkEndChild(SerializeModule(obj->lifeModule));
		node->LinkEndChild(SerializeModule(obj->logicModule));
		node->LinkEndChild(SerializeModule(obj->physicModule));
		node->LinkEndChild(SerializeModule(obj->transformModule));
		return true;
	}

	bool		XmlWriter::SerializeParticleSystem(GameObject* obj, TiXmlElement* node)
	{
		if (!obj)
			return false;
		node->LinkEndChild(SerializeModule(obj->transformModule));
		node->LinkEndChild(SerializeModule(obj->displayModule));
		return true;
	}

	bool	XmlWriter::SerializeSpriteSheetAnimationModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;
		SpriteSheetAnimationModule*	mod = static_cast<SpriteSheetAnimationModule*>(module);
		node->SetAttribute("enable", mod->IsEnable());
		node->SetAttribute("isVisible", mod->IsVisible());
		node->SetAttribute("layer", mod->GetLayer());
		node->SetAttribute("x", mod->GetX());
		node->SetAttribute("y", mod->GetY());
		node->SetAttribute("totalRaw", mod->GetTotalRaw());
		node->SetAttribute("totalColumn", mod->GetTotalColumn());
		node->SetAttribute("spriteSheetPath", mod->GetSpriteSheetPath() ? mod->GetSpriteSheetPath() : "");
		Vec2 tmpVec2;
		tmpVec2 = mod->GetStartingPos();
		node->SetDoubleAttribute("startingPosX", tmpVec2.x);
		node->SetDoubleAttribute("startingPosY", tmpVec2.y);
		node->SetAttribute("spriteSheetWidth", mod->GetSpriteSheetWidth());
		node->SetAttribute("spriteSheetHeight", mod->GetSpriteSheetHeight());
		node->SetAttribute("SpriteWidth", mod->GetSpriteWidth());
		node->SetAttribute("SpriteHeight", mod->GetSpriteHeight());
		node->SetAttribute("frameNumber", mod->GetFrameNumber());
		node->SetDoubleAttribute("duration", mod->GetDuration());
		return true;
	}

	bool	XmlWriter::SerializeCharacterLogicModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;
		CharacterLogicModule*	mod = static_cast<CharacterLogicModule*>(module);
		node->SetAttribute("enable", mod->IsEnable());
		return true;
	}

	bool	XmlWriter::SerializeProjLogicModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;
		ProjLogicModule*	mod = static_cast<ProjLogicModule*>(module);
		node->SetAttribute("enable", mod->IsEnable());
		return true;
	}

	bool	XmlWriter::SerializeGunLogicModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;
		GunLogicModule*	mod = static_cast<GunLogicModule*>(module);
		node->SetAttribute("enable", mod->IsEnable());
		return true;
	}

	bool		XmlWriter::SerializeCharacter(GameObject* obj, TiXmlElement* node)
	{
		if (!obj)
			return false;
		node->LinkEndChild(SerializeModule(obj->displayModule));
		node->LinkEndChild(SerializeModule(obj->boxCollisionModule));
		node->LinkEndChild(SerializeModule(obj->lifeModule));
		node->LinkEndChild(SerializeModule(obj->physicModule));
		node->LinkEndChild(SerializeModule(obj->transformModule));
		node->LinkEndChild(SerializeModule(obj->logicModule));
		return true;
	}


	bool		XmlWriter::SerializeStaticObject(GameObject* obj, TiXmlElement* node)
	{
		if (!obj)
			return false;
		node->LinkEndChild(SerializeModule(obj->displayModule));
		node->LinkEndChild(SerializeModule(obj->boxCollisionModule));
		node->LinkEndChild(SerializeModule(obj->physicModule));
		node->LinkEndChild(SerializeModule(obj->transformModule));
		return true;
	}

	bool		XmlWriter::SerializeProjectile(GameObject* obj, TiXmlElement* node)
	{
		if (!obj)
			return false;
		node->LinkEndChild(SerializeModule(obj->boxCollisionModule));
		node->LinkEndChild(SerializeModule(obj->soundModule));
		node->LinkEndChild(SerializeModule(obj->transformModule));
		node->LinkEndChild(SerializeModule(obj->displayModule));
		node->LinkEndChild(SerializeModule(obj->logicModule));
		return true;
	}

	bool		XmlWriter::SerializeGun(GameObject* obj, TiXmlElement* node)
	{
		if (!obj)
			return false;
		node->LinkEndChild(SerializeModule(obj->displayModule));
		node->LinkEndChild(SerializeModule(obj->soundModule));
		node->LinkEndChild(SerializeModule(obj->transformModule));
		node->LinkEndChild(SerializeModule(obj->logicModule));
		return true;
	}

	bool	XmlWriter::SerializeStateModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;
		StateModule*	mod = static_cast<StateModule*>(module);
		node->SetAttribute("enable", mod->IsEnable());
		node->SetAttribute("state", mod->GetState());
		node->SetDoubleAttribute("propagationTime", mod->GetPropagationTime());
		node->SetDoubleAttribute("destroyTime", mod->GetDestroyTime());
		Vec2 tmpVec2;
		tmpVec2 = mod->GetFireStart();
		node->SetDoubleAttribute("fireStartX", tmpVec2.x);
		node->SetDoubleAttribute("fireStartY", tmpVec2.y);
		node->SetAttribute("fireLength", mod->GetFireLength());
		tmpVec2 = mod->GetWaterStart();
		node->SetDoubleAttribute("waterStartX", tmpVec2.x);
		node->SetDoubleAttribute("waterStartY", tmpVec2.y);
		node->SetAttribute("waterLength", mod->GetWaterLength());
		tmpVec2 = mod->GetElectricStart();
		node->SetDoubleAttribute("ElectricStartX", tmpVec2.x);
		node->SetDoubleAttribute("ElectricStartY", tmpVec2.y);
		node->SetAttribute("ElectricLength", mod->GetElectricLength());
		return true;
	}

	bool		XmlWriter::SerializeElementalInteractiveObject(GameObject* obj, TiXmlElement* node)
	{
		if (!obj)
			return false;
		node->LinkEndChild(SerializeModule(obj->boxCollisionModule));
		node->LinkEndChild(SerializeModule(obj->logicModule));
		node->LinkEndChild(SerializeModule(obj->physicModule));
		node->LinkEndChild(SerializeModule(obj->soundModule));
		node->LinkEndChild(SerializeModule(obj->transformModule));
		node->LinkEndChild(SerializeModule(obj->displayModule));
		node->LinkEndChild(SerializeModule(obj->stateModule));
		return true;
	}

	bool		XmlWriter::SerializeAnimatedElement(GameObject* obj, TiXmlElement* node)
	{
		if (!obj)
			return false;
		node->LinkEndChild(SerializeModule(obj->displayModule));
		node->LinkEndChild(SerializeModule(obj->transformModule));
		return true;
	}

	bool	XmlWriter::SerializeBackAndForthBehaviourModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;
		BackAndForthBehaviourModule*	mod = static_cast<BackAndForthBehaviourModule*>(module);
		node->SetAttribute("enable", mod->IsEnable());
		node->SetDoubleAttribute("speed", mod->GetSpeed());
		node->SetAttribute("ignoreGroundModification", mod->IsIgnoreGroundModification());
		return true;
	}

	bool		XmlWriter::SerializeBackAndForthObject(GameObject* obj, TiXmlElement* node)
	{
		if (!obj)
			return false;
		node->LinkEndChild(SerializeModule(obj->boxCollisionModule));
		node->LinkEndChild(SerializeModule(obj->physicModule));
		node->LinkEndChild(SerializeModule(obj->soundModule));
		node->LinkEndChild(SerializeModule(obj->transformModule));
		node->LinkEndChild(SerializeModule(obj->displayModule));
		node->LinkEndChild(SerializeModule(obj->aIModule));
		return true;
	}

	bool	XmlWriter::SerializeInteractiveObjectLogicModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;
		InteractiveObjectLogicModule*	mod = static_cast<InteractiveObjectLogicModule*>(module);
		node->SetAttribute("enable", mod->IsEnable());
		node->SetAttribute("SwitchActive", mod->IsSwitchActive());
		return true;
	}

	bool	XmlWriter::SerializeSwitchLogicModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;
		SwitchLogicModule*	mod = static_cast<SwitchLogicModule*>(module);
		node->SetAttribute("enable", mod->IsEnable());
		node->SetAttribute("objectToInteract", mod->GetObjectToInteract().c_str());
		return true;
	}

	bool	XmlWriter::SerializeDoorLogicModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;
		DoorLogicModule*	mod = static_cast<DoorLogicModule*>(module);
		node->SetAttribute("enable", mod->IsEnable());
		node->SetAttribute("isOpen", mod->IsIsOpen());
		return true;
	}

	bool		XmlWriter::SerializeSwitch(GameObject* obj, TiXmlElement* node)
	{
		if (!obj)
			return false;
		node->LinkEndChild(SerializeModule(obj->boxCollisionModule));
		node->LinkEndChild(SerializeModule(obj->physicModule));
		node->LinkEndChild(SerializeModule(obj->soundModule));
		node->LinkEndChild(SerializeModule(obj->transformModule));
		node->LinkEndChild(SerializeModule(obj->displayModule));
		node->LinkEndChild(SerializeModule(obj->logicModule));
		return true;
	}

	bool		XmlWriter::SerializeDoor(GameObject* obj, TiXmlElement* node)
	{
		if (!obj)
			return false;
		node->LinkEndChild(SerializeModule(obj->boxCollisionModule));
		node->LinkEndChild(SerializeModule(obj->physicModule));
		node->LinkEndChild(SerializeModule(obj->soundModule));
		node->LinkEndChild(SerializeModule(obj->transformModule));
		node->LinkEndChild(SerializeModule(obj->displayModule));
		node->LinkEndChild(SerializeModule(obj->logicModule));
		return true;
	}

	bool	XmlWriter::SerializeCheckpointLogicModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;
		CheckpointLogicModule*	mod = static_cast<CheckpointLogicModule*>(module);
		node->SetAttribute("enable", mod->IsEnable());
		return true;
	}

	bool		XmlWriter::SerializeCheckpoint(GameObject* obj, TiXmlElement* node)
	{
		if (!obj)
			return false;
		node->LinkEndChild(SerializeModule(obj->boxCollisionModule));
		node->LinkEndChild(SerializeModule(obj->soundModule));
		node->LinkEndChild(SerializeModule(obj->transformModule));
		node->LinkEndChild(SerializeModule(obj->displayModule));
		node->LinkEndChild(SerializeModule(obj->logicModule));
		return true;
	}

	bool	XmlWriter::SerializeEndLevelLogicModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;
		EndLevelLogicModule*	mod = static_cast<EndLevelLogicModule*>(module);
		node->SetAttribute("enable", mod->IsEnable());
		return true;
	}

	bool		XmlWriter::SerializeEndOfLevel(GameObject* obj, TiXmlElement* node)
	{
		if (!obj)
			return false;
		node->LinkEndChild(SerializeModule(obj->boxCollisionModule));
		node->LinkEndChild(SerializeModule(obj->transformModule));
		node->LinkEndChild(SerializeModule(obj->logicModule));
		return true;
	}

	bool	XmlWriter::SerializeInteractivePlatformLogicModule(Module* module, TiXmlElement* node)
	{
		if (!module)
			return false;
		InteractivePlatformLogicModule*	mod = static_cast<InteractivePlatformLogicModule*>(module);
		node->SetAttribute("enable", mod->IsEnable());
		node->SetAttribute("isActivate", mod->IsIsActivate());
		Vec2 tmpVec2;
		tmpVec2 = mod->GetPosStart();
		node->SetDoubleAttribute("posStartX", tmpVec2.x);
		node->SetDoubleAttribute("posStartY", tmpVec2.y);
		tmpVec2 = mod->GetPosEnd();
		node->SetDoubleAttribute("posEndX", tmpVec2.x);
		node->SetDoubleAttribute("posEndY", tmpVec2.y);
		tmpVec2 = mod->GetPosPassBy();
		node->SetDoubleAttribute("posPassByX", tmpVec2.x);
		node->SetDoubleAttribute("posPassByY", tmpVec2.y);
		return true;
	}

	bool		XmlWriter::SerializeInteractivePlatform(GameObject* obj, TiXmlElement* node)
	{
		if (!obj)
			return false;
		node->LinkEndChild(SerializeModule(obj->boxCollisionModule));
		node->LinkEndChild(SerializeModule(obj->soundModule));
		node->LinkEndChild(SerializeModule(obj->transformModule));
		node->LinkEndChild(SerializeModule(obj->displayModule));
		node->LinkEndChild(SerializeModule(obj->stateModule));
		node->LinkEndChild(SerializeModule(obj->logicModule));
		return true;
	}
}
