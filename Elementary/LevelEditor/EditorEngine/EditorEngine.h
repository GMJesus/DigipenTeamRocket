/**
* \file      EditorEngine
* \author    Alexy DURAND
* \brief     Singleton. LevelEditor Main Engine
*
*/

#ifndef		EDITORENGINE_H_
# define	EDITORENGINE_H_

#include	"XML/XmlParser.h"
#include	"XML/XmlWriter.h"
#include	"Utils/WindowDatas.h"
#include	"GameEngine/System.h"
#include	"Inputs/InputManager.h"
#include	"GameEngine/Quadtree.h"
#include	"External Dependencies/FL/Fl_Window.H"
#include	"External Dependencies/FL/x.H"
#include	"External Dependencies/FL/fl_draw.H"
#include	"Utils/EditorParameters.h"

#define		EDITOR
#define		MOUSE_MOVE_CAMERA_OFFSET 50
#define		MOUSE_MOVE_CAMERA_SPEED	 40

class EditorWindow;

class EngineWindow : public Fl_Window
{
public:
	/**
	* \brief    Constructeur.
	* \param    w			 Window Width
	* \param    h			 Window Height
	* \param    name		 Window Name
	*/
	EngineWindow(int w, int h, char* name) : Fl_Window(w, h, name){ _isHandle = false; };

	/**
	* \brief    Handle window events
	* \param    e				Event.
	* \return   Event to loop.
	*/
	int handle(int e);
	void	SetEditorWindow(EditorWindow* window);

	/**
	* \brief    Search the object on which the user clicked
	*/
	void	GetObjectClicked();

	/**
	* \brief    Move selected object
	* \param    x		 X Position
	* \param    y		 Y Position
	*/
	void	MoveObject(int x, int y);

	/**
	* \brief    Rotate selected object
	* \param    rotation		Rotation Angle
	*/
	void	RotateObject(int rotation);

	EditorWindow* _window;
	std::map<unsigned int, GameObject*>		_objectMap;
	bool					_isHandle;
};

/**
* \brief	Namespace defining the Editor Engine datas
*/
namespace EditorEngine
{
	class Engine
	{
	public:
		/**
		* \brief    Permet d'acc�der � la classe depuis n'importe quel fichier.
		* \return   Une instance sur le singleton de la classe.
		*/
		static Engine*	Inst();

		/**
		* \brief    Initilialise les classes d�pendantes d'UIEngine.
		* \return   Un bool�en permettant de savoir si la fonction est entr�e en erreur.
		*/
		bool	Initialize();

		/**
		* \brief    G�re la boucle de mise � jour de l'�diteur.
		*/
		void	Run();

		/**
		* \brief    Lib�re toutes les classes d�pendantes de la m�moire.
		*/
		void	Shutdown();

		/**
		* \brief    Charge un level.
		* \param    filepath        Chemin du level � charger.
		* \return   Un bool�en permettant de savoir si la fonction est entr�e en erreur.
		*/

		bool	LoadLevel(char* filepath);
		void	Stop();
		static GameEngine::Quadtree*	GetQuadtree();

		/**
		* \brief    Dessine un rectangle.
		* \param    x        Position en X.
		* \param    y        Position en Y.
		* \param    h        Hauteur
		* \param    w        Largeur
		* \param    color    Couleur
		*/
		void	DrawRectangle(int x, int y, int h, int w, Fl_Color color);

		EngineWindow*	GetEngineWindow();


		bool	_isInitialized = false;

	private:

		/**
		* \brief    Constructeur.
		*/
		Engine();

		/**
		* \brief    Destructeur.
		*/
		~Engine();

		/**
		* \brief      Instance de la classe.
		*/
		static Engine*	_inst;

		/**
		* \brief    Boucle de mise � jour de l'�diteur.
		* \return   Un bool�en permettant de savoir si la fonction est entr�e en erreur.
		*/
		bool	MainLoop();

		/**
		* \brief    Charge les param�tres de l'�diteur.
		* \return   Un bool�en permettant de savoir si la fonction est entr�e en erreur.
		*/
		bool	LoadPreferences();

		/**
		* \brief    G�re les �v�nements du clavier.
		*/
		void	HandleKeyboardEvents();

		/**
		* \brief    G�re la position de la sourtis.
		*/
		void	HandleMousePosition(float);
		
		const char*	PREFERENCES_PATH = "Resources/EditorPreferences.ini";

		System*					_system;
		EngineWindow*			_engineWindow;
		InputManager*			_input;
		EditorWindow*			_window;
		Graphic::GraphicEngine*	_graphicEngine;
		unsigned long			_lastFrameTime;
		bool					_stop;
		char*					_levelToLoad;
	};

	extern s_EditorParameters engineParameters;
}





#endif		// !EDITORENGINE_H_
