#include "EditorEngine.h"
#include "../EditorEngine/EditorWindow.h"
#include "Graphics/Camera.h"

s_WindowDatas windowDatas;

namespace EditorEngine
{
	s_EditorParameters engineParameters;
	
	Engine* Engine::_inst = NULL;

	Engine::Engine()
	{
		_system = NULL;
		_input = NULL;
		_graphicEngine = NULL;
		_engineWindow = NULL;
		_window = NULL;
		_stop = false;
		_levelToLoad = NULL;
	}

	Engine::~Engine()
	{
	}

	Engine*	Engine::Inst()
	{
		if (!_inst)
			_inst = new Engine();
		return _inst;
	}

	bool	Engine::LoadPreferences()
	{
		bool ret;
		XML::XmlParser* parser = new XML::XmlParser();

		ret = parser->GetConfig(PREFERENCES_PATH);
		delete (parser);
		return ret;
	}

	bool	Engine::LoadLevel(char* filePath)
	{
		if (_levelToLoad)
		{
			_levelToLoad = NULL;
			bool ret;
			if (!filePath)
				return false;
			GameObjectFactory::Inst()->ResetMap(true);

			XML::XmlParser* parser = new XML::XmlParser();
			ret = parser->GetLevel(filePath);
			delete(parser);
			return ret;
		}
		_levelToLoad = filePath;
		return true;
	}

	bool	Engine::Initialize()
	{
		LogWriter::Instance().Initialize();
		if (!LoadPreferences())
		{
			windowDatas.screenHeight = 1080;
			windowDatas.screenWidth = 1920;
			windowDatas.fullScreen = false;
			windowDatas.vSync = false;
			engineParameters.toolWindowWidth = 300;
			engineParameters.toolWindowHeight = 500;
			engineParameters.lastOpenedFile = NULL;
			LogWriter::Instance().addLogMessage("Could not load Editor Preferences");
		}

		_system = new System();
		if (!(_system && _system->Initialize(windowDatas)))
		{
			MessageBoxW(windowDatas.m_hwnd, L"Could not initalize the System object", L"Error", MB_OK);
			return false;
		}

		_engineWindow = new EngineWindow(windowDatas.screenWidth, windowDatas.screenHeight, "Editor");
		_engineWindow->show();
		windowDatas.m_hwnd = fl_xid(_engineWindow);

		if (!(InputManager::Instance().Initialize(windowDatas.m_hinstance, windowDatas.m_hwnd, windowDatas.screenWidth, windowDatas.screenHeight)))
		{
			MessageBoxW(windowDatas.m_hwnd, L"Could not initalize the Input object", L"Error", MB_OK);
			return false;
		}

		_graphicEngine = new Graphic::GraphicEngine();
		if (!(_graphicEngine && _graphicEngine->Initialize(windowDatas.screenWidth, windowDatas.screenHeight, windowDatas.m_hwnd, windowDatas)))
		{
			MessageBoxW(windowDatas.m_hwnd, L"Could not initalize the GraphicEngine object", L"Error", MB_OK);
			return false;
		}

		Graphic::Camera::Instance()->SetPosition(0.0f, 0.0f, -1.0f);
		Graphic::Camera::Instance()->Render();
		LogWriter::Instance().addLogMessage("Before");
		UIEngine::UI::Instance().Initialize(DirectXInterface::Instance().GetWorldMatrix(), DirectXInterface::Instance().GetOrthoMatrix(), Graphic::Camera::Instance()->GetViewMatrix(), windowDatas);
		LogWriter::Instance().addLogMessage("After");
		Graphic::Camera::Instance()->SetPosition(0.0f, 0.0f, -10.0f);
		
		Fl::scheme("gtk+");
		_window = new EditorWindow(engineParameters.toolWindowWidth, engineParameters.toolWindowHeight, "Elementary Editor");
		_window->Initialize();
		_window->show();

		_engineWindow->SetEditorWindow(_window);

		_lastFrameTime = timeGetTime();

		if (engineParameters.lastOpenedFile)
		{
			LoadLevel(engineParameters.lastOpenedFile);
			_window->SetCurrentFile(engineParameters.lastOpenedFile);
		}

		_isInitialized = true;
		return true;
	}

	void	Engine::Stop()
	{
		_stop = true;
	}

	void	Engine::Run()
	{
		MSG msg;
		bool done, result;
		int k = 0;

		// Initialize the message structure.
		ZeroMemory(&msg, sizeof(MSG));

		// Loop until there is a quit message from the window or the user.
		done = false;
		while (!done && !_stop)
		{
			// Handle the windows messages.
			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

			// If windows signals to end the application then exit out.
			if (msg.message == WM_QUIT)
				done = true;
			else
			{
				// Otherwise do the frame processing.  If frame processing fails then exit.
				result = MainLoop();
				if (!result)
				{
					MessageBoxW(windowDatas.m_hwnd, L"Frame Processing Failed", L"Error", MB_OK);
					done = true;
				}
			}
		}
		return;
	}

	GameEngine::Quadtree*	Engine::GetQuadtree()
	{
		GameEngine::Quadtree* quadtree;

		std::map<unsigned int, GameObject*> map = GameObjectFactory::Inst()->GetObjectMap();
		Vec2 cameraPosition = Graphic::Camera::Instance()->Get2DPosition();
		quadtree = new GameEngine::Quadtree(Graphic::Camera::Instance()->Get2DPosition(), Vec2{ 1920, 1080 });
		for (std::map<unsigned int, GameObject*>::const_iterator it = map.begin(); it != map.end(); ++it)
		{
			quadtree->Insert(it->second);
		}

		return quadtree;
	}

	bool	Engine::MainLoop()
	{
		float deltaTime = (timeGetTime() - _lastFrameTime) / 1000.0f;
		_lastFrameTime = timeGetTime();
		GameEngine::Quadtree* quadtree = GetQuadtree();
		std::list<DisplayModule*> orderLayer;

		if (!InputManager::Instance().Frame())
			return false;
		if (!_graphicEngine->Update())
			return false;

		const std::map<unsigned int, GameObject*> map = GameObjectFactory::Inst()->GetNoTransformObjectMap();
		for (std::map<unsigned int, GameObject*>::const_iterator it = map.begin(); it != map.end(); ++it)
		{
			if (it->second->displayModule)
			{
				it->second->displayModule->Update(deltaTime);
				it->second->displayModule->Draw();
			}
		}
		Collision::AABB* a = new Collision::AABB();
		a->Initialize(Vec2{ 0, 0 }, 1920, 1080);
		std::list<GameObject*>* list = quadtree->QueryRange(a);
		if (list)
		{
			for (auto it = list->begin(); it != list->end(); ++it)
			{
				if ((*it)->displayModule)
				{
					orderLayer.push_back((*it)->displayModule);
					(*it)->displayModule->Update(deltaTime);
				}
			}
			if (orderLayer.size() > 0)
			{
				orderLayer.sort([](DisplayModule * a, DisplayModule * b) { return a->GetLayer() < b->GetLayer(); });
				for (auto it = orderLayer.begin(); it != orderLayer.end(); ++it)
				{
					(*it)->Draw();
				}
			}
			for (auto it = list->begin(); it != list->end(); ++it)
			{
				(*it)->DrawDebug();
			}
		}

		_window->Update();
		if (!UIEngine::UI::Instance().Update(deltaTime))
			return false;

		HandleKeyboardEvents();
		HandleMousePosition(deltaTime);

		if (!_graphicEngine->Render())
			return false;

		if (_levelToLoad)
			LoadLevel(_levelToLoad);

		return true;
	}

	void	Engine::HandleMousePosition(float deltaTime)
	{
		int  mousePixelPositionX;
		int  mousePixelPositionY;
		InputManager::Instance().GetMouseLocation(mousePixelPositionX, mousePixelPositionY);

		if (mousePixelPositionX < MOUSE_MOVE_CAMERA_OFFSET)
			Graphic::Camera::Instance()->MoveX(MOUSE_MOVE_CAMERA_SPEED * -1 * deltaTime);//Move Left
		else if (mousePixelPositionX >(windowDatas.screenWidth - MOUSE_MOVE_CAMERA_OFFSET))
			Graphic::Camera::Instance()->MoveX(MOUSE_MOVE_CAMERA_SPEED * deltaTime);//Move Right
		if (mousePixelPositionY < MOUSE_MOVE_CAMERA_OFFSET)
			Graphic::Camera::Instance()->MoveY(MOUSE_MOVE_CAMERA_SPEED * deltaTime);//Move Down
		else if (mousePixelPositionY >(windowDatas.screenHeight - MOUSE_MOVE_CAMERA_OFFSET))
			Graphic::Camera::Instance()->MoveY(MOUSE_MOVE_CAMERA_SPEED * -1 * deltaTime);//Move Up
	}

	void	Engine::HandleKeyboardEvents()
	{
		if (InputManager::Instance().IsPressed(E_Key::Input_Down))
			Graphic::Camera::Instance()->MoveY(-1.0f);
		if (InputManager::Instance().IsPressed(E_Key::Input_Up))
			Graphic::Camera::Instance()->MoveY(1.0f);
		if (InputManager::Instance().IsPressed(E_Key::Input_Right))
			Graphic::Camera::Instance()->MoveX(1.0f);
		if (InputManager::Instance().IsPressed(E_Key::Input_Left))
			Graphic::Camera::Instance()->MoveX(-1.0f);
		if (InputManager::Instance().IsPressed(E_Key::Input_W))
		{
			if (_window->currentGameObject && static_cast<SpriteModule *>(_window->currentGameObject->displayModule))
				_window->_spriteModuleMenu->_width->static_value(strdup(std::to_string(atoi(_window->_spriteModuleMenu->_width->value()) - 1).c_str()));
		}
		if (InputManager::Instance().IsPressed(E_Key::Input_X))
		{
			if (_window->currentGameObject && static_cast<SpriteModule *>(_window->currentGameObject->displayModule))
				_window->_spriteModuleMenu->_width->static_value(strdup(std::to_string(atoi(_window->_spriteModuleMenu->_width->value()) + 1).c_str()));
		}
		if (InputManager::Instance().IsPressed(E_Key::Input_C))
		{
			if (_window->currentGameObject && static_cast<SpriteModule *>(_window->currentGameObject->displayModule))
				_window->_spriteModuleMenu->_height->static_value(strdup(std::to_string(atoi(_window->_spriteModuleMenu->_height->value()) - 1).c_str()));
		}
		if (InputManager::Instance().IsPressed(E_Key::Input_V))
		{
			if (_window->currentGameObject && static_cast<SpriteModule *>(_window->currentGameObject->displayModule))
				_window->_spriteModuleMenu->_height->static_value(strdup(std::to_string(atoi(_window->_spriteModuleMenu->_height->value()) + 1).c_str()));
		}
	}

	void	Engine::Shutdown()
	{
		XML::XmlWriter* writer = new XML::XmlWriter();
		writer->SavePreferences(PREFERENCES_PATH);
		InputManager::Instance().Shutdown();
		if (_system)
		{
			_system->Shutdown();
			delete (_system);
			_system = NULL;
		}
		if (_graphicEngine)
		{
			_graphicEngine->Shutdown();
			delete (_graphicEngine);
			_graphicEngine = NULL;
		}
		GameObjectFactory::Inst()->Shutdown();
		UIEngine::UI::Instance().Shutdown();
		LogWriter::Instance().Shutdown();
	}

	EngineWindow*	Engine::GetEngineWindow()
	{
		return _engineWindow;
	}

}


int EngineWindow::handle(int e)
{
	if (EditorEngine::Engine::Inst()->_isInitialized == true)
	{
		Vec2* mouseWorldPosition = Graphic::Camera::Instance()->GetMouseWorldPosition();
		switch (e)
		{
		case FL_PUSH:
			GetObjectClicked();
			delete (mouseWorldPosition);
			return 1;
		case FL_DRAG:
			if (_isHandle == false)
			{
				GetObjectClicked();
				_isHandle = true;
			}
			MoveObject(mouseWorldPosition->x, mouseWorldPosition->y);
			delete (mouseWorldPosition);
			return 1;
		case FL_MOUSEWHEEL:
			RotateObject(Fl::event_dy());
			delete (mouseWorldPosition);
			return 1;
		case FL_RELEASE:
			_isHandle = false;
			delete (mouseWorldPosition);
			return 1;
		}
	}
	return Fl_Window::handle(e);
}

void	EngineWindow::RotateObject(int rotation)
{
	rotation *= 2;
	if (_window->currentGameObject && _window->currentGameObject->transformModule)
	{
		std::stringstream angle;
		angle << fixed << std::setprecision(2) << (float)rotation + atof(_window->_transformModuleMenu->_angle->value());
		_window->_transformModuleMenu->_angle->static_value(strdup(angle.str().c_str()));
	}
}

void	EngineWindow::MoveObject(int x, int y)
{
	if ((int)_window->_isLocked->value() == 1)
		return;
	if (_window->currentGameObject && _window->currentGameObject->transformModule)
	{
		_window->_transformModuleMenu->_xPosition->static_value(strdup(std::to_string(x).c_str()));
		_window->_transformModuleMenu->_yPosition->static_value(strdup(std::to_string(y).c_str()));
	}
		
}

void	EngineWindow::GetObjectClicked()
{
	GameEngine::Quadtree* quadtree = EditorEngine::Engine::GetQuadtree();
	Collision::AABB* _aabb = new Collision::AABB;
	_aabb->Initialize(*Graphic::Camera::Instance()->GetMouseWorldPosition(), 0.5f, 0.5f);
	auto list = quadtree->QueryRange(_aabb, true);
	if (list && list->size() != 0)
	{
		_window->isCreated = true;
		_window->isSelected = true;
		if (list->size() > 1 && _window->currentGameObject && _window->currentGameObject == list->front())
			_window->currentGameObject = list->back();
		else
			_window->currentGameObject = list->front();
		_window->Initialize();
		_window->InitSelectedGameObject();
	}
}

void EngineWindow::SetEditorWindow(EditorWindow *window)
{
	_window = window;
}
