/**
* \file      EditorWindow
* \author    Alexy DURAND
* \brief     Handle the window who manages the objects edition in the editor.
*
*/

#ifndef		EDITORWINDOW_H_
# define	EDITORWINDOW_H_

#ifdef NDEBUG
#define RESOURCES_PATH "./Ressources/"
#else
#define RESOURCES_PATH "./Ressources/"
#endif

#include "External Dependencies/FL/Fl_Double_Window.H"
#include "External Dependencies/FL/Fl_Menu_Item.H"
#include "External Dependencies/FL/Fl_Menu_Bar.H"
#include "External Dependencies/FL/Fl_file_chooser.H"
#include "External Dependencies/FL/Fl_Scroll.H"

#include "External Dependencies/FL/Fl_Float_Input.H"
#include "External Dependencies/FL/Fl_Int_Input.H"
#include "EditorEngine/EditorEngine.h"
#include "Game\GameObjectFactory.h"
#include "Game\GameObject.h"
#include "Modules\BoxCollisionModule.h"
#include "Graphics\TextureLoader.h"

//@ModuleCreator ModuleMenuInclude
#include "../Menus/InteractivePlatformLogicModuleMenu.h"
#include "../Menus/EndLevelLogicModuleMenu.h"
#include "../Menus/CheckpointLogicModuleMenu.h"
#include "../Menus/DoorLogicModuleMenu.h"
#include "../Menus/SwitchLogicModuleMenu.h"
#include "../Menus/InteractiveObjectLogicModuleMenu.h"
#include "../Menus/BackAndForthBehaviourModuleMenu.h"
#include "../Menus/StateModuleMenu.h"
#include "../Menus/GunLogicModuleMenu.h"
#include "../Menus/ProjLogicModuleMenu.h"
#include "../Menus/SpriteSheetAnimationModuleMenu.h"
#include "../Menus/CharacterLogicModuleMenu.h"
#include "../Menus/SpriteModuleMenu.h"
#include "../Menus/TransformModuleMenu.h"
#include "../Menus/AnimatedSpriteModuleMenu.h"
#include "../Menus/LifeModuleMenu.h"
#include "../Menus/BoxCollisionModuleMenu.h"
#include "../Menus/PhysicModuleMenu.h"
#include "../Menus/SoundModuleMenu.h"
#include "../Menus/ParticleSystemModuleMenu.h"


class EditorWindow : public Fl_Double_Window
{
	typedef	void(EditorWindow::*MenuCreation)();
public:
	/**
	* \brief    Constructor
	* \param    w			 Window Width
	* \param    h			 Window Height
	* \param    name		 Window Name
	*/
	EditorWindow(int w, int h, char* name);

	/**
	* \brief    Update the window
	*/
	void					Update();

	/**
	* \brief    Create the tool that permits to add, delete or rename an object.
	*/
	void					CreateToolbar(int Xpos, int Ypos, int width, int height);
	
	GameObject				*GetCurrentGameObject();

	EditorEngine::Engine* _editorEngine;

	Fl_Button				*add1;
	Fl_Button				*delete1;
	Fl_Choice				*object_group;
	Fl_Menu_Item			*object_group_items;
	Fl_Input				*gameObjectName;
	Fl_Check_Button			*_isLocked;
	Fl_Button				*_recenterCamera;
	GameObject				*currentGameObject;
	GameObject::Type		_selectedObject;

	//@ModuleCreator ModuleVeriableDeclarationStart
	InteractivePlatformLogicModuleMenu*		_interactivePlatformLogicModuleMenu;
	CheckpointLogicModuleMenu*						_checkpointLogicModuleMenu;
	DoorLogicModuleMenu*							_doorLogicModuleMenu;
	SwitchLogicModuleMenu*							_switchLogicModuleMenu;
	InteractiveObjectLogicModuleMenu*				_interactiveObjectLogicModuleMenu;
	BackAndForthBehaviourModuleMenu*				_backAndForthBehaviourModuleMenu;
	StateModuleMenu*								_stateModuleMenu;
	//GunLogicModuleMenu*								_gunLogicModuleMenu;
	//ProjLogicModuleMenu*							_projLogicModuleMenu;
	SpriteSheetAnimationModuleMenu*					_spriteSheetAnimationModuleMenu;
	CharacterLogicModuleMenu*						_characterLogicModuleMenu;
	SpriteModuleMenu*								_spriteModuleMenu;
	TransformModuleMenu*							_transformModuleMenu;
	AnimatedSpriteModuleMenu*						_animatedSpriteModuleMenu;
	LifeModuleMenu*									_lifeModuleMenu;
	BoxCollisionModuleMenu*							_boxCollisionModuleMenu;
	PhysicModuleMenu*								_physicModuleMenu;
	SoundModuleMenu*								_soundModuleMenu;
	ParticleSystemModuleMenu*						_ParticleSystemModuleMenu;

	bool					isCreated = false;
	bool					isDeleted = false;
	bool					isSelected = false;


	void					SetSelectedObject(GameObject::Type type);

	static void				static_add_cb(Fl_Widget *, void *data);
	static void				static_delete_cb(Fl_Widget *, void *data);

	static void				bitmap_sprite_cb(Fl_Widget* buttonptr, void *data);
	static void				bitmap_animatedsprite_cb(Fl_Widget* buttonptr, void *data);
	static void				sound_cb(Fl_Widget* buttonptr, void *data);
	static void				cb_object(Fl_Widget *, void *);
	static void				static_new_cb(Fl_Widget* w, void* data);
	static void				static_open_cb(Fl_Widget* w, void* data);
	static void				static_save_cb(Fl_Widget* w, void* data);
	static void				static_saveas_cb(Fl_Widget* w, void* data);
	static void				static_quit_cb(Fl_Widget* w, void* data);
	static void				static_recenter_cb(Fl_Widget *, void *data);

	/**
	* \brief    Create an empty level.
	*/
	void					NewFile();

	/**
	* \brief    Open a level
	*/
	void					OpenFile();

	/**
	* \brief    Save a level
	*/
	void					SaveFile();

	/**
	* \brief    Save As a level
	*/
	void					SaveFileAs();

	/**
	* \brief    Close the editor
	*/
	void					Quit();

	/**
	* \brief    Let the user selects a texture
	* \return   Path of the texture
	*/
	static char*			OpenBitmap();

	/**
	* \brief    Let the user selects a sound
	* \return   Path of the sound
	*/
	static char*			OpenSound();

	/**
	* \brief    Create a GameObject
	*/
	void					CreateGameObject(void* data);

	/**
	* \brief    Delete a GameObject
	*/
	void					DeleteGameObject(void *data);

	/**
	* \brief    Clear the window and delete ModuleMenus.
	*/
	void					ClearModulesMenus();

	/**
	* \brief    Initialize ModulesMenus of the selected object.
	*/
	void					InitSelectedGameObject();
	void					SetSoundName();

	/**
	* \brief    Recenter the camera on the player.
	*/
	void					RecenterCamera();

	/**
	* \brief    Save the actual path of the level.
	*/
	void					SetCurrentFile(const char* file);

	/**
	* \brief    Save the actual path of the level.
	*/
	void					SetCurrentFile(char* file);

	/**
	* \brief    Save the actual path of the level.
	*/
	void					SetCurrentFile(const std::string& file);

	/**
	* \brief    Free the save of the actual path of the level.
	*/
	void					UnsetCurrentFile();
	const char*				GetCurrentFile()const;

	//@ClassCreator ObjectCreationFuncStart
	void			CreateInteractivePlatformMenu();
	void					CreateEndOfLevelMenu();
	void					CreateCheckpointMenu();
	void					CreateDoorMenu();
	void					CreateSwitchMenu();
	void					CreateBackAndForthObjectMenu();
	void					CreateElementalInteractiveObjectMenu();
	void					CreateAnimatedElementMenu();
	//void					CreateGunMenu();
	//void					CreateProjectileMenu();
	void					CreateStaticObjectMenu();
	void					CreateCharacterMenu();
	void					CreateParticleSystemMenu();
	void					CreatePlayerMenu();
	void					CreateActorMenu();
	void					CreateBackgroundElementMenu();
	void					CreateInvisibleWallMenu();
	void					CreateDecorativeElementMenu();

	/**
	* \brief    Initialize the window.
	* \return   A boolean that let the user knows if the function ended with an error. 
	*/
	bool					Initialize();
private:
	Fl_Menu_Bar*			_menu;
	char*					_currentFile;
	bool					_noFile;
	int						_width;
	int						_height;
	int						_offset;
	MenuCreation*			_menuCreationFunc;
	bool					textureChanged = false;

	char*					ExtractRelativeFilePath(char* file);
};




#endif		// !EDITORWINDOW_H_
