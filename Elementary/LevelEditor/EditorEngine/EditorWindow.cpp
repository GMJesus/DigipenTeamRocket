#include "EditorWindow.h"

EditorWindow::EditorWindow(int w, int h, char* name) : Fl_Double_Window(w, h, name)
{
	_width = w;
	_height = h;
	_noFile = true;
	_menu = NULL;
	_currentFile = NULL;
	_offset = 0;
	currentGameObject = NULL;

	_menuCreationFunc = new MenuCreation[GameObject::GAMEOBJECT_CLASS_NUMBER];
	//@ClassCreator ObjectCreationFuncStart
	_menuCreationFunc[GameObject::Type::E_InteractivePlatform] = &EditorWindow::CreateInteractivePlatformMenu;
	_menuCreationFunc[GameObject::Type::E_EndOfLevel] = &EditorWindow::CreateEndOfLevelMenu;
	_menuCreationFunc[GameObject::Type::E_Checkpoint] = &EditorWindow::CreateCheckpointMenu;
	_menuCreationFunc[GameObject::Type::E_Door] = &EditorWindow::CreateDoorMenu;
	_menuCreationFunc[GameObject::Type::E_Switch] = &EditorWindow::CreateSwitchMenu;
	_menuCreationFunc[GameObject::Type::E_BackAndForthObject] = &EditorWindow::CreateBackAndForthObjectMenu;
	_menuCreationFunc[GameObject::Type::E_ElementalInteractiveObject] = &EditorWindow::CreateElementalInteractiveObjectMenu;
	_menuCreationFunc[GameObject::Type::E_AnimatedElement] = &EditorWindow::CreateAnimatedElementMenu;
	//_menuCreationFunc[GameObject::Type::E_Gun] = &EditorWindow::CreateGunMenu;
	//_menuCreationFunc[GameObject::Type::E_Projectile] = &EditorWindow::CreateProjectileMenu;
	_menuCreationFunc[GameObject::Type::E_StaticObject] = &EditorWindow::CreateStaticObjectMenu;
	_menuCreationFunc[GameObject::Type::E_Character] = &EditorWindow::CreateCharacterMenu;
	_menuCreationFunc[GameObject::Type::E_ParticleSystem] = &EditorWindow::CreateParticleSystemMenu;
	_menuCreationFunc[GameObject::Type::E_Player] = &EditorWindow::CreatePlayerMenu;
	_menuCreationFunc[GameObject::Type::E_Actor] = &EditorWindow::CreateActorMenu;
	_menuCreationFunc[GameObject::Type::E_BackgroundElement] = &EditorWindow::CreateBackgroundElementMenu;
	_menuCreationFunc[GameObject::Type::E_Decorative_Element] = &EditorWindow::CreateDecorativeElementMenu;
	_menuCreationFunc[GameObject::Type::E_InvisibleWall] = &EditorWindow::CreateInvisibleWallMenu;
	_selectedObject = GameObject::E_Actor;

	//@ModuleCreator VariableInitialization
	_interactivePlatformLogicModuleMenu = new InteractivePlatformLogicModuleMenu();
	_checkpointLogicModuleMenu = new CheckpointLogicModuleMenu();
	_doorLogicModuleMenu = new DoorLogicModuleMenu();
	_switchLogicModuleMenu = new SwitchLogicModuleMenu();
	_interactiveObjectLogicModuleMenu = new InteractiveObjectLogicModuleMenu();
	_backAndForthBehaviourModuleMenu = new BackAndForthBehaviourModuleMenu();
	_stateModuleMenu = new StateModuleMenu();
	//_gunLogicModuleMenu = new GunLogicModuleMenu();
	//_projLogicModuleMenu = new ProjLogicModuleMenu();
	_spriteModuleMenu = new SpriteModuleMenu();
	_characterLogicModuleMenu = new CharacterLogicModuleMenu();
	_spriteSheetAnimationModuleMenu = new SpriteSheetAnimationModuleMenu();
	_transformModuleMenu = new TransformModuleMenu();
	_animatedSpriteModuleMenu = new AnimatedSpriteModuleMenu();
	_lifeModuleMenu = new LifeModuleMenu();
	_boxCollisionModuleMenu = new BoxCollisionModuleMenu();
	_physicModuleMenu = new PhysicModuleMenu();
	_soundModuleMenu = new SoundModuleMenu();
	_ParticleSystemModuleMenu = new ParticleSystemModuleMenu();
}

bool	EditorWindow::Initialize()
{
	this->begin();
	if (!isCreated)
	{
		_menu = new Fl_Menu_Bar(0, 0, _width, 30);
		if (!_menu)
			return false;
		_menu->add("&File/&New File", FL_CTRL + 'n', (Fl_Callback *)static_new_cb, this);
		_menu->add("&File/&Open File...", FL_CTRL + 'o', (Fl_Callback *)static_open_cb, this, FL_MENU_DIVIDER);
		_menu->add("&File/&Save File", FL_CTRL + 's', (Fl_Callback *)static_save_cb, this);
		_menu->add("&File/Save File &As...", FL_CTRL + FL_SHIFT + 's', (Fl_Callback *)static_saveas_cb, this, FL_MENU_DIVIDER);
		_menu->add("&File/E&xit", FL_CTRL + 'q', (Fl_Callback *)static_quit_cb, this);
		set_non_modal();
		CreateToolbar(0, 30, this->w(), 30);
		_isLocked = new Fl_Check_Button(50, 65, 60, 25, " Locked");
		_isLocked->value(1);
		_recenterCamera = new Fl_Button(200, 65, 30, 25, "R");
		_recenterCamera->callback(static_recenter_cb, this);
	}
	_offset = 90;
	if (isCreated)
	{
		ClearModulesMenus();
		if (!isDeleted)
		{
			if (isSelected)
			{
				_selectedObject = currentGameObject->GetType();
				(this->*_menuCreationFunc[_selectedObject])();
				isSelected = false;
			}
			else
				(this->*_menuCreationFunc[_selectedObject])();
				
		}
		else
			isDeleted = false;
		this->redraw();
	}
	this->end();
	return true;
}

void	EditorWindow::static_recenter_cb(Fl_Widget* w, void* data)
{
	((EditorWindow*)data)->RecenterCamera();
}

void	EditorWindow::RecenterCamera()
{
	std::map<unsigned int, GameObject*>::const_iterator it = GameObjectFactory::Inst()->GetObjectMap().begin();
	while (it != GameObjectFactory::Inst()->GetObjectMap().end())
	{
		if (it->second->GetType() == GameObject::Type::E_Character)
		{
			Graphic::Camera::Instance()->SetPosition(it->second->transformModule->Position().x, it->second->transformModule->Position().y, -10.0f);
			return;
		}
		++it;
	}
}

void EditorWindow::CreateActorMenu()
{
	_spriteModuleMenu->Initialize(_offset);
	_offset += _spriteModuleMenu->GetHeight();
	_transformModuleMenu->Initialize(_offset);
	_offset += _transformModuleMenu->GetHeight();
	_boxCollisionModuleMenu->Initialize(_offset);
	_offset += _boxCollisionModuleMenu->GetHeight();
	_physicModuleMenu->Initialize(_offset);
	_offset += _physicModuleMenu->GetHeight();
}

void EditorWindow::CreateBackgroundElementMenu()
{
	_spriteModuleMenu->Initialize(_offset);
	_offset += _spriteModuleMenu->GetHeight();
}

void EditorWindow::CreateDecorativeElementMenu()
{
	_spriteModuleMenu->Initialize(_offset);
	_offset += _spriteModuleMenu->GetHeight();
	_transformModuleMenu->Initialize(_offset);
	_offset += _transformModuleMenu->GetHeight();
}

void EditorWindow::CreateInvisibleWallMenu()
{
	_transformModuleMenu->Initialize(_offset);
	_offset += _transformModuleMenu->GetHeight();
	_boxCollisionModuleMenu->Initialize(_offset);
	_offset += _boxCollisionModuleMenu->GetHeight();
}

void EditorWindow::cb_object(Fl_Widget *w, void* data)
{
	((EditorWindow*)((Fl_Menu_Item*)(w->parent())))->SetSelectedObject((GameObject::Type)atoi((char*)(data)));
}

void EditorWindow::SetSelectedObject(GameObject::Type type)
{
	_selectedObject = type;
}

char* EditorWindow::OpenBitmap()
{
	char *fileName = fl_file_chooser("Choose Bitmap", "*.png", "./Ressources/");
	if (fileName == NULL)
		return "";
	std::string str(fileName);
	int offset = str.find_last_of('/');
	return (strdup(str.substr(offset + 1, str.size() - offset).c_str()));
}

char* EditorWindow::OpenSound()
{
	char *fileName = fl_file_chooser("Choose Sound", "*.mp3", "./Ressources/");
	std::string str(fileName);
	if (fileName == NULL)
		return "";
	int offset = str.find_last_of('/');
	return (strdup(str.substr(offset + 1, str.size() - offset).c_str()));
}

void EditorWindow::CreateToolbar(int Xpos, int Ypos, int width, int height)
{
	box(FL_UP_BOX);

	Xpos += 3;
	Ypos += 2;
	width = height;
	height -= 4;

	add1 = new Fl_Button(Xpos, Ypos, width, height, "+");
	Xpos += width + 5;
	delete1 = new Fl_Button(Xpos, Ypos, width, height, "-");
	Xpos += width + 5;
	object_group = new Fl_Choice(Xpos, Ypos, 110, height);
	Xpos += 111;

	add1->tooltip("Add Object");
	add1->callback(static_add_cb, this);
	delete1->tooltip("Delete Object");
	delete1->callback(static_delete_cb, this);

	object_group_items = new Fl_Menu_Item[GameObject::GAMEOBJECT_CLASS_NUMBER + 1];
	for (int i = 0; i < GameObject::GAMEOBJECT_CLASS_NUMBER; i++)
		object_group_items[i].text = NULL;
	object_group_items->add("Actor", 0, cb_object, strdup(const_cast<char *>(std::to_string(GameObject::E_Actor).c_str())), 0);
	object_group_items->add("Background Element", 0, cb_object, strdup(const_cast<char *>(std::to_string(GameObject::E_BackgroundElement).c_str())), 0);
	object_group_items->add("Decorative Element", 0, cb_object, strdup(const_cast<char *>(std::to_string(GameObject::E_Decorative_Element).c_str())), 0);
	object_group_items->add("Invisible Wall", 0, cb_object, strdup(const_cast<char *>(std::to_string(GameObject::E_InvisibleWall).c_str())), 0);
	object_group_items->add("Player", 0, cb_object, strdup(const_cast<char *>(std::to_string(GameObject::E_Player).c_str())), 0);
	object_group_items->add("ParticleSystem", 0, cb_object, strdup(const_cast<char *>(std::to_string(GameObject::E_ParticleSystem).c_str())), 0);
	object_group_items->add("Character", 0, cb_object, strdup(const_cast<char *>(std::to_string(GameObject::E_Character).c_str())), 0);
	object_group_items->add("StaticObject", 0, cb_object, strdup(const_cast<char *>(std::to_string(GameObject::E_StaticObject).c_str())), 0);
	//object_group_items->add("Projectile", 0, cb_object, strdup(const_cast<char *>(std::to_string(GameObject::E_Projectile).c_str())), 0);
	//object_group_items->add("Gun", 0, cb_object, strdup(const_cast<char *>(std::to_string(GameObject::E_Gun).c_str())), 0);
	object_group_items->add("ElementalInteractiveObject", 0, cb_object, const_cast<char *>(strdup(std::to_string(GameObject::E_ElementalInteractiveObject).c_str())), 0);
	object_group_items->add("AnimatedElement", 0, cb_object, strdup(const_cast<char *>(std::to_string(GameObject::E_AnimatedElement).c_str())), 0);
	object_group_items->add("BackAndForthObject", 0, cb_object, strdup(const_cast<char *>(std::to_string(GameObject::E_BackAndForthObject).c_str())), 0);
	object_group_items->add("Switch", 0, cb_object, strdup(const_cast<char *>(std::to_string(GameObject::E_Switch).c_str())), 0);
	object_group_items->add("Door", 0, cb_object, strdup(const_cast<char *>(std::to_string(GameObject::E_Door).c_str())), 0);
	object_group_items->add("Checkpoint", 0, cb_object, strdup(const_cast<char *>(std::to_string(GameObject::E_Checkpoint).c_str())), 0);
	object_group_items->add("EndOfLevel", 0, cb_object, strdup(const_cast<char *>(std::to_string(GameObject::E_EndOfLevel).c_str())), 0);
	object_group_items->add("InteractivePlatform", 0, cb_object, strdup(const_cast<char *>(std::to_string(GameObject::E_InteractivePlatform).c_str())), 0);
	//@ClassCreator ObjectGroupItemGenerationEnd
	object_group->menu(object_group_items);

	gameObjectName = new Fl_Input(Xpos, Ypos, 110, height, "");
	if (gameObjectName)
		gameObjectName->static_value("Name");
}

GameObject			*EditorWindow::GetCurrentGameObject()
{
	return currentGameObject;
}

void	EditorWindow::static_new_cb(Fl_Widget* w, void* data)
{
	((EditorWindow*)data)->NewFile();
}

void	EditorWindow::NewFile()
{
	UnsetCurrentFile();
	GameObjectFactory::Inst()->ResetMap(true);
}


void	EditorWindow::static_open_cb(Fl_Widget* w, void* data)
{
	((EditorWindow*)data)->OpenFile();
}

void	EditorWindow::OpenFile()
{
	char *fileName = fl_file_chooser("Open File", "*.elemlvl", _currentFile && !_noFile ? _currentFile : "");
	SetCurrentFile(fileName);
	EditorEngine::Engine::Inst()->LoadLevel(fileName);
}

void	EditorWindow::static_save_cb(Fl_Widget* w, void* data)
{
	((EditorWindow*)data)->SaveFile();
}

void	EditorWindow::SaveFile()
{
	if (_noFile)
		SaveFileAs();
	XML::XmlWriter* writer = new XML::XmlWriter();
	writer->Serialize(_currentFile);
	delete writer;
}

void	EditorWindow::static_saveas_cb(Fl_Widget* w, void* data)
{
	((EditorWindow*)data)->SaveFileAs();
}

void	EditorWindow::SaveFileAs()
{
	SetCurrentFile(fl_file_chooser("Open File", "*.elemlvl", !_currentFile ? "" : _currentFile));
	SaveFile();
}

void	EditorWindow::static_quit_cb(Fl_Widget* w, void* data)
{
	((EditorWindow*)data)->Quit();

}

void	EditorWindow::Quit()
{
	EditorEngine::Engine::Inst()->Stop();
}

const char*	EditorWindow::GetCurrentFile()const
{
	return _currentFile;
}

char*	EditorWindow::ExtractRelativeFilePath(char* file)
{
	std::string tmp = file;

	int pos = tmp.find("Ressources/Levels/");
	if (pos == std::string::npos)
	{
		return NULL;
	}
	std::string relativePath = "../Engine/";
	relativePath += &tmp[pos];
	tmp.clear();
	delete(file);
	return strdup(relativePath.c_str());
}

void EditorWindow::SetCurrentFile(const char* file)
{
	if (!file)
		return;
	_currentFile = strdup(file);
	EditorEngine::engineParameters.lastOpenedFile = ExtractRelativeFilePath(strdup(file));
	_noFile = false;
	ClearModulesMenus();
}

void EditorWindow::SetCurrentFile(char* file)
{
	if (!file)
		return;
	_currentFile = file;
	_noFile = false;
	EditorEngine::engineParameters.lastOpenedFile = ExtractRelativeFilePath(strdup(file));
	currentGameObject = NULL;
	ClearModulesMenus();
}

void EditorWindow::SetCurrentFile(const std::string& file)
{
	_currentFile = strdup(file.c_str());
	_noFile = false;
	currentGameObject = NULL;
	EditorEngine::engineParameters.lastOpenedFile = ExtractRelativeFilePath(strdup(file.c_str()));
	ClearModulesMenus();
}

void EditorWindow::UnsetCurrentFile()
{
	_currentFile = NULL;
	_noFile = true;
	currentGameObject = NULL;
	ClearModulesMenus();
}

void EditorWindow::static_add_cb(Fl_Widget *w, void *data)
{
	((EditorWindow*)data)->CreateGameObject(data);
}

void	EditorWindow::CreateGameObject(void* data)
{
	isCreated = true;
	this->Initialize();
	currentGameObject = GameObjectFactory::Inst()->CreateGameObject(_selectedObject, gameObjectName->value() && gameObjectName->value() != '\0' ? gameObjectName->value() : "Name");
	InitSelectedGameObject();
}

void EditorWindow::static_delete_cb(Fl_Widget *w, void *data)
{
	((EditorWindow*)data)->DeleteGameObject(data);
}

void	EditorWindow::DeleteGameObject(void* data)
{
	if (currentGameObject)
	{
		isDeleted = true;
		LogWriter::Instance().addLogMessage("Deleting Object" + std::to_string(currentGameObject->Id()), E_MsgType::Log);
		this->Initialize();
		GameObjectFactory::Inst()->DeleteObject(currentGameObject->Id());
	}
}

void EditorWindow::InitSelectedGameObject()
{
	if (!currentGameObject)
		return;
	//@ModuleCreator InitGameObjectStart
	_interactivePlatformLogicModuleMenu->InitGameObject(currentGameObject);
	_checkpointLogicModuleMenu->InitGameObject(currentGameObject);
	_doorLogicModuleMenu->InitGameObject(currentGameObject);
	_switchLogicModuleMenu->InitGameObject(currentGameObject);
	_interactiveObjectLogicModuleMenu->InitGameObject(currentGameObject);
	_backAndForthBehaviourModuleMenu->InitGameObject(currentGameObject);
	_stateModuleMenu->InitGameObject(currentGameObject);
	//_gunLogicModuleMenu->InitGameObject(currentGameObject);
	//_projLogicModuleMenu->InitGameObject(currentGameObject);
	_spriteSheetAnimationModuleMenu->InitGameObject(currentGameObject);
	_characterLogicModuleMenu->InitGameObject(currentGameObject);
	_spriteModuleMenu->InitGameObject(currentGameObject);
	_transformModuleMenu->InitGameObject(currentGameObject);
	_animatedSpriteModuleMenu->InitGameObject(currentGameObject);
	_lifeModuleMenu->InitGameObject(currentGameObject);
	_boxCollisionModuleMenu->InitGameObject(currentGameObject);
	_physicModuleMenu->InitGameObject(currentGameObject);
	_soundModuleMenu->InitGameObject(currentGameObject);
	_ParticleSystemModuleMenu->InitGameObject(currentGameObject);
	gameObjectName->static_value(currentGameObject->GetName().c_str());
	object_group->value(currentGameObject->GetType());
	this->flush();
}

void EditorWindow::Update()
{
	if (currentGameObject)
	{
		//@ModuleCreator UpdateModuleMenuStart
		_interactivePlatformLogicModuleMenu->Update(currentGameObject);
		_checkpointLogicModuleMenu->Update(currentGameObject);
		_doorLogicModuleMenu->Update(currentGameObject);
		_switchLogicModuleMenu->Update(currentGameObject);
		_interactiveObjectLogicModuleMenu->Update(currentGameObject);
		_backAndForthBehaviourModuleMenu->Update(currentGameObject);
		_stateModuleMenu->Update(currentGameObject);
		//_gunLogicModuleMenu->Update(currentGameObject);
		//_projLogicModuleMenu->Update(currentGameObject);
		_spriteSheetAnimationModuleMenu->Update(currentGameObject);
		_characterLogicModuleMenu->Update(currentGameObject);
		_spriteModuleMenu->Update(currentGameObject);
		_transformModuleMenu->Update(currentGameObject);
		_animatedSpriteModuleMenu->Update(currentGameObject);
		_lifeModuleMenu->Update(currentGameObject);
		_boxCollisionModuleMenu->Update(currentGameObject);
		_physicModuleMenu->Update(currentGameObject);
		_soundModuleMenu->Update(currentGameObject);
		_ParticleSystemModuleMenu->Update(currentGameObject);
		if (gameObjectName->value() && gameObjectName->value()[1] != '\0' && gameObjectName->value() != "")
			currentGameObject->SetName(strdup(gameObjectName->value()));
	}
	this->flush();
}

void EditorWindow::ClearModulesMenus()
{
	//@ModuleCreator ClearModuleMenuStart
	_interactivePlatformLogicModuleMenu->Clear();
	_checkpointLogicModuleMenu->Clear();
	_doorLogicModuleMenu->Clear();
	_switchLogicModuleMenu->Clear();
	_interactiveObjectLogicModuleMenu->Clear();
	_backAndForthBehaviourModuleMenu->Clear();
	_stateModuleMenu->Clear();
	//_gunLogicModuleMenu->Clear();
	//_projLogicModuleMenu->Clear();
	_spriteSheetAnimationModuleMenu->Clear();
	_characterLogicModuleMenu->Clear();
	_spriteModuleMenu->Clear();
	_transformModuleMenu->Clear();
	_animatedSpriteModuleMenu->Clear();
	_lifeModuleMenu->Clear();
	_boxCollisionModuleMenu->Clear();
	_physicModuleMenu->Clear();
	_soundModuleMenu->Clear();
	_ParticleSystemModuleMenu->Clear();
}

void EditorWindow::CreatePlayerMenu()
{
	_animatedSpriteModuleMenu->Initialize(_offset);
	_offset += _animatedSpriteModuleMenu->GetHeight();
	_boxCollisionModuleMenu->Initialize(_offset);
	_offset += _boxCollisionModuleMenu->GetHeight();
	_lifeModuleMenu->Initialize(_offset);
	_offset += _lifeModuleMenu->GetHeight();
	_physicModuleMenu->Initialize(_offset);
	_offset += _physicModuleMenu->GetHeight();
	_transformModuleMenu->Initialize(_offset);
	_offset += _transformModuleMenu->GetHeight();
}

void EditorWindow::CreateParticleSystemMenu()
{
	_transformModuleMenu->Initialize(_offset);
	_offset += _transformModuleMenu->GetHeight();
	_ParticleSystemModuleMenu->Initialize(_offset);
	_offset += _ParticleSystemModuleMenu->GetHeight();
}

void EditorWindow::CreateCharacterMenu()
{
	_spriteSheetAnimationModuleMenu->Initialize(_offset);
	_offset += _spriteSheetAnimationModuleMenu->GetHeight();
	_boxCollisionModuleMenu->Initialize(_offset);
	_offset += _boxCollisionModuleMenu->GetHeight();
	_lifeModuleMenu->Initialize(_offset);
	_offset += _lifeModuleMenu->GetHeight();
	_physicModuleMenu->Initialize(_offset);
	_offset += _physicModuleMenu->GetHeight();
	_transformModuleMenu->Initialize(_offset);
	_offset += _transformModuleMenu->GetHeight();
	_characterLogicModuleMenu->Initialize(_offset);
	_offset += _characterLogicModuleMenu->GetHeight();
}

void EditorWindow::CreateStaticObjectMenu()
{
	_spriteModuleMenu->Initialize(_offset);
	_offset += _spriteModuleMenu->GetHeight();
	_boxCollisionModuleMenu->Initialize(_offset);
	_offset += _boxCollisionModuleMenu->GetHeight();
	_physicModuleMenu->Initialize(_offset);
	_offset += _physicModuleMenu->GetHeight();
	_transformModuleMenu->Initialize(_offset);
	_offset += _transformModuleMenu->GetHeight();
}

//void EditorWindow::CreateProjectileMenu()
//{
//	_boxCollisionModuleMenu->Initialize(_offset);
//	_offset += _boxCollisionModuleMenu->GetHeight();
//	_soundModuleMenu->Initialize(_offset);
//	_offset += _soundModuleMenu->GetHeight();
//	_transformModuleMenu->Initialize(_offset);
//	_offset += _transformModuleMenu->GetHeight();
//	_spriteSheetAnimationModuleMenu->Initialize(_offset);
//	_offset += _spriteSheetAnimationModuleMenu->GetHeight();
//	_projLogicModuleMenu->Initialize(_offset);
//	_offset += _projLogicModuleMenu->GetHeight();
//}
//
//void EditorWindow::CreateGunMenu()
//{
//	_spriteModuleMenu->Initialize(_offset);
//	_offset += _spriteModuleMenu->GetHeight();
//	_soundModuleMenu->Initialize(_offset);
//	_offset += _soundModuleMenu->GetHeight();
//	_transformModuleMenu->Initialize(_offset);
//	_offset += _transformModuleMenu->GetHeight();
//	_gunLogicModuleMenu->Initialize(_offset);
//	_offset += _gunLogicModuleMenu->GetHeight();
//}

void EditorWindow::CreateElementalInteractiveObjectMenu()
{
	_boxCollisionModuleMenu->Initialize(_offset);
	_offset += _boxCollisionModuleMenu->GetHeight();
	_physicModuleMenu->Initialize(_offset);
	_offset += _physicModuleMenu->GetHeight();
	_soundModuleMenu->Initialize(_offset);
	_offset += _soundModuleMenu->GetHeight();
	_transformModuleMenu->Initialize(_offset);
	_offset += _transformModuleMenu->GetHeight();
	_spriteSheetAnimationModuleMenu->Initialize(_offset);
	_offset += _spriteSheetAnimationModuleMenu->GetHeight();
	_stateModuleMenu->Initialize(_offset);
	_offset += _stateModuleMenu->GetHeight();
}

void EditorWindow::CreateAnimatedElementMenu()
{
	_animatedSpriteModuleMenu->Initialize(_offset);
	_offset += _animatedSpriteModuleMenu->GetHeight();
	_transformModuleMenu->Initialize(_offset);
	_offset += _transformModuleMenu->GetHeight();
}

void EditorWindow::CreateBackAndForthObjectMenu()
{
	_boxCollisionModuleMenu->Initialize(_offset);
	_offset += _boxCollisionModuleMenu->GetHeight();
	_physicModuleMenu->Initialize(_offset);
	_offset += _physicModuleMenu->GetHeight();
	_soundModuleMenu->Initialize(_offset);
	_offset += _soundModuleMenu->GetHeight();
	_transformModuleMenu->Initialize(_offset);
	_offset += _transformModuleMenu->GetHeight();
	_spriteSheetAnimationModuleMenu->Initialize(_offset);
	_offset += _spriteSheetAnimationModuleMenu->GetHeight();
	_backAndForthBehaviourModuleMenu->Initialize(_offset);
	_offset += _backAndForthBehaviourModuleMenu->GetHeight();
}

void EditorWindow::CreateSwitchMenu()
{
	_boxCollisionModuleMenu->Initialize(_offset);
	_offset += _boxCollisionModuleMenu->GetHeight();
	_physicModuleMenu->Initialize(_offset);
	_offset += _physicModuleMenu->GetHeight();
	_soundModuleMenu->Initialize(_offset);
	_offset += _soundModuleMenu->GetHeight();
	_transformModuleMenu->Initialize(_offset);
	_offset += _transformModuleMenu->GetHeight();
	_spriteSheetAnimationModuleMenu->Initialize(_offset);
	_offset += _spriteSheetAnimationModuleMenu->GetHeight();
	_switchLogicModuleMenu->Initialize(_offset);
	_offset += _switchLogicModuleMenu->GetHeight();
}

void EditorWindow::CreateDoorMenu()
{
	_boxCollisionModuleMenu->Initialize(_offset);
	_offset += _boxCollisionModuleMenu->GetHeight();
	_physicModuleMenu->Initialize(_offset);
	_offset += _physicModuleMenu->GetHeight();
	_soundModuleMenu->Initialize(_offset);
	_offset += _soundModuleMenu->GetHeight();
	_transformModuleMenu->Initialize(_offset);
	_offset += _transformModuleMenu->GetHeight();
	_spriteSheetAnimationModuleMenu->Initialize(_offset);
	_offset += _spriteSheetAnimationModuleMenu->GetHeight();
	_doorLogicModuleMenu->Initialize(_offset);
	_offset += _doorLogicModuleMenu->GetHeight();
}

void EditorWindow::CreateCheckpointMenu()
{
	_boxCollisionModuleMenu->Initialize(_offset);
	_offset += _boxCollisionModuleMenu->GetHeight();
	_soundModuleMenu->Initialize(_offset);
	_offset += _soundModuleMenu->GetHeight();
	_transformModuleMenu->Initialize(_offset);
	_offset += _transformModuleMenu->GetHeight();
	_spriteSheetAnimationModuleMenu->Initialize(_offset);
	_offset += _spriteSheetAnimationModuleMenu->GetHeight();
	_checkpointLogicModuleMenu->Initialize(_offset);
	_offset += _checkpointLogicModuleMenu->GetHeight();
}

void EditorWindow::CreateEndOfLevelMenu()
{
	_boxCollisionModuleMenu->Initialize(_offset);
	_offset += _boxCollisionModuleMenu->GetHeight();
	_transformModuleMenu->Initialize(_offset);
	_offset += _transformModuleMenu->GetHeight();
}

void EditorWindow::CreateInteractivePlatformMenu()
{
	_boxCollisionModuleMenu->Initialize(_offset);
	_offset += _boxCollisionModuleMenu->GetHeight();
	_soundModuleMenu->Initialize(_offset);
	_offset += _soundModuleMenu->GetHeight();
	_transformModuleMenu->Initialize(_offset);
	_offset += _transformModuleMenu->GetHeight();
	_spriteSheetAnimationModuleMenu->Initialize(_offset);
	_offset += _spriteSheetAnimationModuleMenu->GetHeight();
	_stateModuleMenu->Initialize(_offset);
	_offset += _stateModuleMenu->GetHeight();
	_interactivePlatformLogicModuleMenu->Initialize(_offset);
	_offset += _interactivePlatformLogicModuleMenu->GetHeight();
}
