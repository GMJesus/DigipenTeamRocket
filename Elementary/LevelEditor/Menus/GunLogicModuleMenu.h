/**
* \file      GunLogicModuleMenu
* \author    Baptiste DUMAS
* \brief     Module Menu of the GunLogicModule
*
*/

#ifndef GUNLOGICMODULEMENU_H_
#define GUNLOGICMODULEMENU_H_

#include	"Menus/Menu.h"

class GunLogicModuleMenu : public Menu
{
public:
	GunLogicModuleMenu();
	~GunLogicModuleMenu();

	virtual void	Initialize(int offset);

	virtual void	Update(GameObject* currentGameObject);

	virtual void	Clear();

	virtual void InitGameObject(GameObject* object)const;

protected:
	Fl_Check_Button*		_enable;
};

#endif
