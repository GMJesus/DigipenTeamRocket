/**
* \file		 InteractivePlatformLogicModuleMenu
* \author    Baptiste DUMAS
* \brief     Module Menu of the InteractivePlatformLogicModule
*/

#ifndef INTERACTIVEPLATFORMLOGICMODULEMENU_H_
#define INTERACTIVEPLATFORMLOGICMODULEMENU_H_

#include	"Menus/Menu.h"

class InteractivePlatformLogicModuleMenu : public Menu
{
public:
	InteractivePlatformLogicModuleMenu();
	~InteractivePlatformLogicModuleMenu();

	virtual void	Initialize(int offset);

	virtual void	Update(GameObject* currentGameObject);

	virtual void	Clear();

	virtual void InitGameObject(GameObject* object)const;

protected:
	Fl_Box*				_box;
	Fl_Check_Button*	_enable;
	Fl_Check_Button*	_isActivate;
	Fl_Float_Input*		_posStartX;
	Fl_Float_Input*		_posStartY;
	Fl_Float_Input*		_posEndX;
	Fl_Float_Input*		_posEndY;
	Fl_Float_Input*		_posPassByX;
	Fl_Float_Input*		_posPassByY;
	Fl_Choice*			_type_group;
	Fl_Menu_Item*		_type_group_items;
};

#endif
