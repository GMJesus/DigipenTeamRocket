#include "PhysicModuleMenu.h"
#include	"../EditorEngine/EditorWindow.h"

PhysicModuleMenu::PhysicModuleMenu() : Menu()
{
	_box = NULL;
	_mass = NULL;
	_absorption = NULL;
	_menuHeight = 90;
	_isActive = false;
}

PhysicModuleMenu::~PhysicModuleMenu()
{

}

void	PhysicModuleMenu::Initialize(int offset)
{
	_box = new Fl_Box(00, offset + 5, 300, 25, "Physic Module");
	_box->box(FL_UP_BOX);
	_box->color(FL_DARK_YELLOW);
	_box->labelfont(FL_BOLD);
	_mass = new Fl_Float_Input(60, offset + 35, 60, 25, "Mass :");
	_absorption = new Fl_Float_Input(210, offset + 35, 60, 25, "Absorption :");
	_enableGravity = new Fl_Check_Button(60, offset + 65, 60, 25, "Enable Gravity");
	_isActive = true;
}

void	PhysicModuleMenu::InitGameObject(GameObject* object)const
{
	if (_isActive)
	{
		std::stringstream mass;
		mass << fixed << std::setprecision(2) << object->physicModule->GetInvMass();
		_mass->static_value(strdup(mass.str().c_str()));

		std::stringstream absorption;
		absorption << fixed << std::setprecision(2) << object->physicModule->GetAbsorption();
		_absorption->static_value(strdup(absorption.str().c_str()));

		_enableGravity->value(object->physicModule->GravityEnabled());
	}
}

void	PhysicModuleMenu::Update(GameObject* currentObject)
{
	if (!_isActive)
		return;
	currentObject->physicModule->SetMass(1 / atof(_mass->value()));
	currentObject->physicModule->SetAbsorption(atof(_absorption->value()));
	currentObject->physicModule->EnableGravity(_enableGravity->value());
}

void	PhysicModuleMenu::Clear()
{
	if (_isActive == true)
	{
		_isActive = false;
		_box->hide();
		_mass->hide();
		_absorption->hide();
		_enableGravity->hide();
	}
}