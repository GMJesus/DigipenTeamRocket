#include "AnimatedSpriteModuleMenu.h"
#include	"../EditorEngine/EditorWindow.h"

AnimatedSpriteModuleMenu::AnimatedSpriteModuleMenu() : Menu()
{
	_box = NULL;
	_x = NULL;
	_y = NULL;
	_angle = NULL;
	_initialWidth = NULL;
	_initialHeight = NULL;
	_width = NULL;
	_height = NULL;
	_texturePlaneButton = NULL;
	_texturePlaneName = NULL;
	_textureChanged = NULL;
	_menuHeight = 175;
	_isActive = false;
}

AnimatedSpriteModuleMenu::~AnimatedSpriteModuleMenu()
{

}

void	AnimatedSpriteModuleMenu::Initialize(int offset)
{
	_isActive = true;
	_box = new Fl_Box(00, offset + 5, 300, 25, "Animated Sprite Module");
	_box->box(FL_UP_BOX);
	_box->color(FL_DARK_YELLOW);
	_box->labelfont(FL_BOLD);
	_x = new Fl_Int_Input(60, offset + 35, 80, 25, "X :");
	_y = new Fl_Int_Input(200, offset + 35, 80, 25, "Y :");
	_initialWidth = new Fl_Int_Input(100, offset + 65, 60, 25, "Initial Width :");
	_initialHeight = new Fl_Int_Input(220, offset + 65, 60, 25, "Height :");
	_width = new Fl_Int_Input(60, offset + 95, 80, 25, "Width :");
	_height = new Fl_Int_Input(200, offset + 95, 80, 25, "Height :");
	_angle = new Fl_Float_Input(60, offset + 125, 80, 25, "Angle :");
	_texturePlaneButton = new Fl_Button(10, offset + 155, 50, 20, "Bitmap");
	_texturePlaneButton->callback(bitmap_sprite_cb, this);
	_texturePlaneName = new Fl_Box(35, offset + 155, 230, 20, "");
	_isActive = true;
}

void	AnimatedSpriteModuleMenu::bitmap_sprite_cb(Fl_Widget* widget, void* data)
{
	((AnimatedSpriteModuleMenu*)data)->SetBitmapSpriteName();
}

void	AnimatedSpriteModuleMenu::SetBitmapSpriteName()
{
	_textureChanged = true;
	_texturePlaneName->label(EditorWindow::OpenBitmap());
}

void	AnimatedSpriteModuleMenu::InitGameObject(GameObject* object)const
{
	if (_isActive)
	{
		_x->static_value(strdup(std::to_string(static_cast<AnimatedSpriteModule *>(object->displayModule)->GetX()).c_str()));
		_y->static_value(strdup(std::to_string(static_cast<AnimatedSpriteModule *>(object->displayModule)->GetY()).c_str()));
		_initialWidth->static_value(strdup(std::to_string(static_cast<AnimatedSpriteModule *>(object->displayModule)->GetInitialWidth()).c_str()));
		_initialHeight->static_value(strdup(std::to_string(static_cast<AnimatedSpriteModule *>(object->displayModule)->GetInitialHeight()).c_str()));
		_width->static_value(strdup(std::to_string(static_cast<AnimatedSpriteModule *>(object->displayModule)->GetWidth()).c_str()));
		_height->static_value(strdup(std::to_string(static_cast<AnimatedSpriteModule *>(object->displayModule)->GetHeight()).c_str()));
		std::stringstream angle;
		angle << fixed << std::setprecision(2) << static_cast<AnimatedSpriteModule *>(object->displayModule)->GetAngle();
		_angle->static_value(strdup(angle.str().c_str()));
		_texturePlaneName->label(static_cast<AnimatedSpriteModule *>(object->displayModule)->GetTexturePath());
	}
}

void	AnimatedSpriteModuleMenu::Update(GameObject* currentObject)
{
	if (!_isActive)
		return;
	static_cast<AnimatedSpriteModule *>(currentObject->displayModule)->SetX(atoi(_x->value()));
	static_cast<AnimatedSpriteModule *>(currentObject->displayModule)->SetY(atoi(_y->value()));
	static_cast<AnimatedSpriteModule *>(currentObject->displayModule)->SetInitialWidth(atoi(_initialWidth->value()));
	static_cast<AnimatedSpriteModule *>(currentObject->displayModule)->SetInitialHeight(atoi(_initialHeight->value()));
	static_cast<AnimatedSpriteModule *>(currentObject->displayModule)->SetCurrentWidth(atoi(_width->value()));
	static_cast<AnimatedSpriteModule *>(currentObject->displayModule)->SetCurrentHeight(atoi(_height->value()));
	static_cast<AnimatedSpriteModule *>(currentObject->displayModule)->SetAngle(atof(_angle->value()));
	if (_textureChanged)
	{
		std::string newLabel(std::string(RESOURCES_PATH) + _texturePlaneName->label());
		static_cast<AnimatedSpriteModule *>(currentObject->displayModule)->SetTexture(Graphic::TextureLoader::Instance()->GetTexture(&std::wstring(newLabel.begin(), newLabel.end())[0]));
		_textureChanged = !_textureChanged;
	}
}

void	AnimatedSpriteModuleMenu::Clear()
{
	if (_isActive == true)
	{
		_isActive = false;
		_box->hide();
		_x->hide();
		_y->hide();
		_initialWidth->hide();
		_initialHeight->hide();
		_width->hide();
		_height->hide();
		_angle->hide();
		_texturePlaneButton->hide();
		_texturePlaneName->hide();
	}
}