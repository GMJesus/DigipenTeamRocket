#include	"Menus/SwitchLogicModuleMenu.h"

SwitchLogicModuleMenu::SwitchLogicModuleMenu()
{
	_menuHeight = 150;
	_isActive = false;
}

SwitchLogicModuleMenu::~SwitchLogicModuleMenu()
{

}

void	SwitchLogicModuleMenu::Initialize(int offset)
{
	_box = new Fl_Box(00, offset + 5, 300, 25, "Switch Logic Module");
	_box->box(FL_UP_BOX);
	_box->color(FL_DARK_YELLOW);
	_box->labelfont(FL_BOLD);
	_objectToInteract = new Fl_Input(30, offset + 35, 60, 25, "Object To Interact");
	_isActive = true;
}

void	SwitchLogicModuleMenu::InitGameObject(GameObject* object)const
{
	if (_isActive)
	{
		_objectToInteract->value(const_cast<char*>(static_cast<SwitchLogicModule*>(object->logicModule)->GetObjectToInteract().c_str()));
	}
}

void	SwitchLogicModuleMenu::Update(GameObject* currentGameObject)
{
	if (_isActive)
		static_cast<SwitchLogicModule*>(currentGameObject->logicModule)->SetObjectToInteract(_objectToInteract->value());
}

void	SwitchLogicModuleMenu::Clear()
{
	if (_isActive)
	{
		_isActive = false;
		_objectToInteract->hide();
		_box->hide();
	}
}

