/**
* \file      SwitchLogicModuleMenu
* \author    Baptiste DUMAS
* \brief     Module Menu of the SwitchLogicModule
*
*/

#ifndef SWITCHLOGICMODULEMENU_H_
#define SWITCHLOGICMODULEMENU_H_

#include	"Menus/Menu.h"
#include	"Modules\SwitchLogicModule.h"

class SwitchLogicModuleMenu : public Menu
{
public:
	SwitchLogicModuleMenu();
	~SwitchLogicModuleMenu();

	virtual void	Initialize(int offset);

	virtual void	Update(GameObject* currentGameObject);

	virtual void	Clear();

	virtual void InitGameObject(GameObject* object)const;

protected:
	Fl_Check_Button*		_enable;
	Fl_Input*				_objectToInteract;
};

#endif
