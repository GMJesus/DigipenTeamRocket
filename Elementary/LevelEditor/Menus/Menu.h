/**
* \file      Menu
* \author    Alexy DURAND
* \brief     Parent Class of ModulesMenus
*
*/


#ifndef		MENU_H_
# define	MENU_H_

#include <iostream>
#include <sstream>
#include <iomanip>

#include "External Dependencies/FL/Fl_Float_Input.H"
#include "External Dependencies/FL/Fl_Int_Input.H"
#include "External Dependencies/FL/Fl_Box.H"
#include "External Dependencies/FL/Fl_Button.H"
#include "External Dependencies/Fl/Fl_Check_Button.H"
#include "External Dependencies/Fl/Fl_Menu_Item.H"
#include "External Dependencies/Fl/Fl_Choice.H"
#include "../../Engine/Game/GameObject.h"

class Menu
{
public:
	/**
	* \brief    Constructor
	*/
	Menu();

	/**
	* \brief    Destructor
	*/
	~Menu();

	/**
	* \brief    Update the value of the Module Menu in the GameObject.
	* \param    currentObject		GameObject to update.
	*/
	virtual void	Update(GameObject* currentObject) = 0;


	/**
	* \brief    Initialize the Module Menu
	* \param    offset				Position of the menu.
	*/
	virtual void	Initialize(int offset) = 0;

	/**
	* \brief   Delete the Module Menu
	*/
	virtual void	Clear() = 0;

	/**
	* \brief    Update the value of the selected GameObject in the Module Menu.
	* \param    currentObject		Selected GameObject.
	*/
	virtual void	InitGameObject(GameObject* object) const = 0;
	int				GetHeight()const;

protected:
	int				_menuHeight;
	bool			_isActive = false;
	Fl_Box*			_box;
};

#endif		// !MENU_H_
