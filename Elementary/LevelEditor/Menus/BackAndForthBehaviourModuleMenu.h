/**
* \file      BackAndForthBehaviourModuleMenu
* \author    Alexy DURAND
* \brief     Module Menu of the BackAndForthBehaviourModule
*
*/

#ifndef BACKANDFORTHBEHAVIOURMODULEMENU_H_
#define BACKANDFORTHBEHAVIOURMODULEMENU_H_

#include	"Menus/Menu.h"

class BackAndForthBehaviourModuleMenu : public Menu
{
public:
	BackAndForthBehaviourModuleMenu();
	~BackAndForthBehaviourModuleMenu();

	virtual void	Initialize(int offset);

	virtual void	Update(GameObject* currentGameObject);

	virtual void	Clear();

	virtual void InitGameObject(GameObject* object)const;

protected:
	Fl_Check_Button*		_enable;
	Fl_Float_Input*			speed;
	Fl_Check_Button*		ignoreGroundModification;
};

#endif
