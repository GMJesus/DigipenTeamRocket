/**
* \file      CharacterLogicModuleMenu
* \author    Alexy DURAND
* \brief     Module Menu of the CharacterLogicModule
*
*/

#ifndef CHARACTERLOGICMODULEMENU_H_
# define CHARACTERLOGICMODULEMENU_H_

#include "Menu.h"

class CharacterLogicModuleMenu : public Menu
{
public:
	CharacterLogicModuleMenu();
	~CharacterLogicModuleMenu();

	virtual void		Update(GameObject* currentObject);
	virtual void		Initialize(int offset);
	virtual void		Clear();
	virtual void		InitGameObject(GameObject* object)const;

protected:
};

#endif