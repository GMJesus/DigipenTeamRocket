/**
* \file      TransformModuleMenu
* \author    Alexy DURAND
* \brief     Module Menu of the TransformModule
*
*/

#ifndef TRANSFORMMODULEMENU_H_
# define TRANSFORMMODULEMENU_H_

#include "Menu.h"

class TransformModuleMenu : public Menu
{
public:
	TransformModuleMenu();
	~TransformModuleMenu();

	virtual void		Update(GameObject* currentObject);
	virtual void		Initialize(int offset);
	virtual void		Clear();
	virtual void		InitGameObject(GameObject* object)const;

	Fl_Box*					_box;
	Fl_Float_Input*			_xPosition;
	Fl_Float_Input*			_yPosition;
	Fl_Float_Input*			_angle;
	Fl_Float_Input*			_scale;
	Fl_Input*				_parent;
};

#endif