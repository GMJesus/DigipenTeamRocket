/**
* \file      CheckpointLogicModuleMenu
* \author    Alexy DURAND
* \brief     Module Menu of the CheckpointLogicModule
*
*/

#ifndef CHECKPOINTLOGICMODULEMENU_H_
#define CHECKPOINTLOGICMODULEMENU_H_

#include	"Menus/Menu.h"

class CheckpointLogicModuleMenu : public Menu
{
public:
	CheckpointLogicModuleMenu();
	~CheckpointLogicModuleMenu();

	virtual void	Initialize(int offset);

	virtual void	Update(GameObject* currentGameObject);

	virtual void	Clear();

	virtual void InitGameObject(GameObject* object)const;

protected:
	Fl_Check_Button*		_enable;
};

#endif
