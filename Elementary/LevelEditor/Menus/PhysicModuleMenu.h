/**
* \file      PhysicModuleMenu
* \author    Alexy DURAND
* \brief     Module Menu of the PhysicModule
*
*/

#ifndef PHYSICMODULEMENU_H_
# define PHYSICMODULEMENU_H_

#include "Menu.h"

class PhysicModuleMenu : public Menu
{
public:
	PhysicModuleMenu();
	~PhysicModuleMenu();

	virtual void		Update(GameObject* currentObject);
	virtual void		Initialize(int offset);
	virtual void		Clear();
	virtual void		InitGameObject(GameObject* object)const;

protected:
	Fl_Box*					_box;
	Fl_Float_Input*			_mass;
	Fl_Float_Input*			_absorption;
	Fl_Check_Button*		_enableGravity;
};

#endif