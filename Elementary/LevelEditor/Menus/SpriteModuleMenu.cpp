#include "SpriteModuleMenu.h"
#include	"../EditorEngine/EditorWindow.h"

SpriteModuleMenu::SpriteModuleMenu() : Menu()
{
	_x = NULL;
	_y = NULL;
	_angle = NULL;
	_width = NULL;
	_height = NULL;
	_texturePlaneButton = NULL;
	_texturePlaneName = NULL;
	_textureChanged = false;
	_menuHeight = 175;
	_isActive = false;
}

SpriteModuleMenu::~SpriteModuleMenu()
{

}

void	SpriteModuleMenu::Initialize(int offset)
{
	_box = new Fl_Box(00, offset + 5, 300, 25, "Sprite Module");
	_box->box(FL_UP_BOX);
	_box->color(FL_DARK_YELLOW);
	_box->labelfont(FL_BOLD);
	_x = new Fl_Int_Input(60, offset + 35, 80, 25, "X :");
	_y = new Fl_Int_Input(200, offset + 35, 80, 25, "Y :");
	_width = new Fl_Int_Input(60, offset + 65, 80, 25, "Width :");
	_height = new Fl_Int_Input(200, offset + 65, 80, 25, "Height :");
	_angle = new Fl_Float_Input(60, offset + 95, 80, 25, "Angle :");
	_layer = new Fl_Int_Input(60, offset + 125, 80, 25, "Layer :");
	_texturePlaneButton = new Fl_Button(10, offset + 155, 50, 20, "Bitmap");
	_texturePlaneButton->callback(bitmap_sprite_cb, this);
	_texturePlaneName = new Fl_Box(35, offset + 155, 230, 20, "");
	_isActive = true;
}

void	SpriteModuleMenu::InitGameObject(GameObject* object)const
{
	if (_isActive)
	{
		_x->static_value(strdup(std::to_string(static_cast<SpriteModule *>(object->displayModule)->GetX()).c_str()));
		_y->static_value(strdup(std::to_string(static_cast<SpriteModule *>(object->displayModule)->GetY()).c_str()));
		_width->static_value(strdup(std::to_string(static_cast<SpriteModule *>(object->displayModule)->GetWidth()).c_str()));
		_height->static_value(strdup(std::to_string(static_cast<SpriteModule *>(object->displayModule)->GetHeight()).c_str()));
		_layer->static_value(strdup(std::to_string(static_cast<SpriteModule *>(object->displayModule)->GetLayer()).c_str()));
		std::stringstream angle;
		angle << fixed << std::setprecision(2) << static_cast<SpriteModule *>(object->displayModule)->GetAngle();
		_angle->static_value(strdup(angle.str().c_str()));
		_texturePlaneName->label(static_cast<SpriteModule *>(object->displayModule)->GetTexturePath());
	}
}

void	SpriteModuleMenu::Update(GameObject* currentObject)
{
	if (!_isActive)
		return;
	static_cast<SpriteModule *>(currentObject->displayModule)->SetX(atoi(_x->value()));
	static_cast<SpriteModule *>(currentObject->displayModule)->SetY(atoi(_y->value()));
	static_cast<SpriteModule *>(currentObject->displayModule)->SetWidth(atoi(_width->value()));
	static_cast<SpriteModule *>(currentObject->displayModule)->SetHeight(atoi(_height->value()));
	static_cast<SpriteModule *>(currentObject->displayModule)->SetAngle(atof(_angle->value()));
	static_cast<SpriteModule *>(currentObject->displayModule)->SetLayer(atoi(_layer->value()));
	if (_textureChanged)
	{
		std::string newLabel(std::string(RESOURCES_PATH) + _texturePlaneName->label());
		static_cast<SpriteModule *>(currentObject->displayModule)->SetTexture(Graphic::TextureLoader::Instance()->GetTexture(&std::wstring(newLabel.begin(), newLabel.end())[0]));
		_textureChanged = !_textureChanged;
	}
}

void	SpriteModuleMenu::Clear()
{
	if (_isActive == true)
	{
		_isActive = false;
		_box->hide();
		_x->hide();
		_y->hide();
		_width->hide();
		_height->hide();
		_angle->hide();
		_texturePlaneButton->hide();
		_texturePlaneName->hide();
		_layer->hide();
	}
}

void	SpriteModuleMenu::bitmap_sprite_cb(Fl_Widget* widget, void* data)
{
	((SpriteModuleMenu*)data)->SetBitmapSpriteName();
}

void	SpriteModuleMenu::SetBitmapSpriteName()
{
	_textureChanged = true;
	_texturePlaneName->label(EditorWindow::OpenBitmap());
}