/**
* \file      LifeModuleMenu
* \author    Alexy DURAND
* \brief     Module Menu of the LifeModule
*
*/

#ifndef LIFEMODULEMENU_H_
# define LIFEMODULEMENU_H_

#include "Menu.h"

class LifeModuleMenu : public Menu
{
public:
	LifeModuleMenu();
	~LifeModuleMenu();

	virtual void		Update(GameObject* currentObject);
	virtual void		Initialize(int offset);
	virtual void		Clear();
	virtual void		InitGameObject(GameObject* object)const;

protected:
	Fl_Box*					_box;
	Fl_Int_Input*			_health;
	Fl_Int_Input*			_maxHealth;
	Fl_Check_Button*		_isImmortal;
};

#endif