#include	"Menus/BackAndForthBehaviourModuleMenu.h"
#include	"Modules/BackAndForthBehaviourModule.h"

BackAndForthBehaviourModuleMenu::BackAndForthBehaviourModuleMenu()
{
	_enable = NULL;
	speed = NULL;
	ignoreGroundModification = NULL;
	_menuHeight = 100;
}

BackAndForthBehaviourModuleMenu::~BackAndForthBehaviourModuleMenu()
{

}

void	BackAndForthBehaviourModuleMenu::Initialize(int offset)
{
	_box = new Fl_Box(00, offset + 5, 300, 25, "Back And Forth Behaviour Module");
	_box->box(FL_UP_BOX);
	_box->color(FL_DARK_YELLOW);
	_box->labelfont(FL_BOLD);
	_enable = new Fl_Check_Button(50, offset + 35, 60, 25, "Enable");
	ignoreGroundModification = new Fl_Check_Button(50, offset + 65, 60, 25, "Ignore Ground Modification");
	speed = new Fl_Float_Input(50, offset + 95, 60, 25, "Speed");
	_isActive = true;
}

void	BackAndForthBehaviourModuleMenu::InitGameObject(GameObject* object)const
{
	if (_isActive)
	{
		BackAndForthBehaviourModule* mod = static_cast<BackAndForthBehaviourModule*>(object->aIModule);
		_enable->value(mod->IsEnable());
		speed->static_value(strdup(std::to_string(mod->GetSpeed()).c_str()));
		ignoreGroundModification->value(mod->IsIgnoreGroundModification());
	}
}

void	BackAndForthBehaviourModuleMenu::Update(GameObject* currentGameObject)
{
	if (_isActive)
	{
		BackAndForthBehaviourModule* mod = static_cast<BackAndForthBehaviourModule*>(currentGameObject->aIModule);
		mod->Enable(_enable->value());
		mod->SetSpeed(atof(speed->value()));
		mod->IgnoreGroundModification(ignoreGroundModification->value());
	}
}

void	BackAndForthBehaviourModuleMenu::Clear()
{
	if (_isActive == true)
	{
		_enable->hide();
		speed->hide();
		ignoreGroundModification->hide();
		_isActive = false;
		_box->hide();
	}
}

