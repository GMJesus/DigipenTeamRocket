#include	"Menus/DoorLogicModuleMenu.h"

DoorLogicModuleMenu::DoorLogicModuleMenu()
{
	_isActive = false;
}

DoorLogicModuleMenu::~DoorLogicModuleMenu()
{

}

void	DoorLogicModuleMenu::Initialize(int offset)
{
	_box = new Fl_Box(00, offset + 5, 300, 25, "Door Logic Module");
	_box->box(FL_UP_BOX);
	_box->color(FL_DARK_YELLOW);
	_box->labelfont(FL_BOLD);
	_isOpen = new Fl_Check_Button(30, offset + 35, 60, 25, "Open");
}

void	DoorLogicModuleMenu::InitGameObject(GameObject* object)const
{
	if (_isActive)
	{
		_isOpen->value(static_cast<DoorLogicModule*>(object->logicModule)->IsIsOpen());
	}
}

void	DoorLogicModuleMenu::Update(GameObject* currentGameObject)
{
	if (_isActive)
		static_cast<DoorLogicModule*>(currentGameObject->logicModule)->IsOpen(_isOpen->value());
}

void	DoorLogicModuleMenu::Clear()
{
	if (_isActive)
	{
		_box->hide();
		_isOpen->hide();
	}
}

