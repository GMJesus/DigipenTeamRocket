/**
* \file      SoundModuleMenu
* \author    Alexy DURAND
* \brief     Module Menu of the SoundModule
*
*/


#ifndef SOUNDMODULEMENU_H_
# define SOUNDMODULEMENU_H_

#include "Menu.h"
#include "Sounds/SoundManager.h"

class SoundModuleMenu : public Menu
{
public:
	SoundModuleMenu();
	~SoundModuleMenu();

	virtual void		Update(GameObject* currentObject);
	virtual void		Initialize(int offset);
	virtual void		Clear();
	virtual void		InitGameObject(GameObject* object)const;

	static void			sound_cb(Fl_Widget* widget, void* data);
	void				SetSoundName();

	static void			play_cb(Fl_Widget* widget, void* data);
	void				Play();

protected:
	Fl_Box*					_box;
	Fl_Button*					_play;
	Fl_Button*				_soundButton;
	Fl_Box*					_soundName;
	Fl_Check_Button*		_loop;
	Fl_Float_Input*			_volume;
	bool					_soundChanged;
};

#endif