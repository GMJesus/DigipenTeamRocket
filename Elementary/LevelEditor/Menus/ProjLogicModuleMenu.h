/**
* \file      ProjLogicModuleMenu
* \author    Baptiste DUMAS
* \brief     Module Menu of the ProjLogicModule
*
*/

#ifndef PROJLOGICMODULEMENU_H_
#define PROJLOGICMODULEMENU_H_

#include	"Menus/Menu.h"

class ProjLogicModuleMenu : public Menu
{
public:
	ProjLogicModuleMenu();
	~ProjLogicModuleMenu();

	virtual void	Initialize(int offset);

	virtual void	Update(GameObject* currentGameObject);

	virtual void	Clear();

	virtual void InitGameObject(GameObject* object)const;

protected:
	Fl_Check_Button*		_enable;
};

#endif
