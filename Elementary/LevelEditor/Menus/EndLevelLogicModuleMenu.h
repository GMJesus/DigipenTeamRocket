/**
* \file      EndLevelLogicModuleMenu
* \author    Alexy DURAND
* \brief     Module Menu of the EndLevelLogicModule
*
*/

#ifndef ENDLEVELLOGICMODULEMENU_H_
#define ENDLEVELLOGICMODULEMENU_H_

#include	"Menus/Menu.h"

class EndLevelLogicModuleMenu : public Menu
{
public:
	EndLevelLogicModuleMenu();
	~EndLevelLogicModuleMenu();

	virtual void	Initialize(int offset);

	virtual void	Update(GameObject* currentGameObject);

	virtual void	Clear();

	virtual void InitGameObject(GameObject* object)const;

protected:
	Fl_Check_Button*		_enable;
};

#endif
