/**
* \file      SpriteSheetAnimationModuleMenu
* \author    Alexy DURAND
* \brief     Module Menu of the SpriteSheetAnimationModule
*
*/

#ifndef SPRITESHEETANIMATIONMODULEMENU_H_
#define SPRITESHEETANIMATIONMODULEMENU_H_

#include	"Menus/Menu.h"

class SpriteSheetAnimationModuleMenu : public Menu
{
public:
	SpriteSheetAnimationModuleMenu();
	~SpriteSheetAnimationModuleMenu();

	virtual void	Initialize(int offset);

	virtual void	Update(GameObject* currentGameObject);

	virtual void	Clear();

	virtual void InitGameObject(GameObject* object)const;

	static void			bitmap_sprite_cb(Fl_Widget* widget, void* data);
	void				SetBitmapSpriteName();

protected:
	Fl_Check_Button*		_enable;
	Fl_Check_Button*		_isVisible;
	Fl_Int_Input*			_layer;
	Fl_Int_Input*			_x;
	Fl_Int_Input*			_y;
	Fl_Int_Input*			_totalRaw;
	Fl_Int_Input*			_totalColumn;
	Fl_Button*				_spriteSheetPathButton;
	Fl_Box*					_spriteSheetPathName;
	Fl_Int_Input*			_startingPosX;
	Fl_Int_Input*			_startingPosY;
	Fl_Int_Input*			_spriteWidth;
	Fl_Int_Input*			_spriteHeight;
	Fl_Int_Input*			_frameNumber;
	Fl_Float_Input*			_duration;
	bool					_textureChanged;
};

#endif
