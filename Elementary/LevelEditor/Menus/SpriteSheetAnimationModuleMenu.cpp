#include	"Menus/SpriteSheetAnimationModuleMenu.h"
#include	"../EditorEngine/EditorWindow.h"

SpriteSheetAnimationModuleMenu::SpriteSheetAnimationModuleMenu()
{
	_box = NULL;
	_x = NULL;
	_y = NULL;
	_layer = NULL;
	_totalRaw = NULL;
	_totalColumn = NULL;
	_startingPosX = NULL;
	_startingPosY = NULL;
	_spriteWidth = NULL;
	_spriteHeight = NULL;
	_duration = NULL;
	_frameNumber = NULL;
	_spriteSheetPathButton = NULL;
	_spriteSheetPathName = NULL;

	_menuHeight = 235;
	_isActive = false;
	_textureChanged = false;
}

SpriteSheetAnimationModuleMenu::~SpriteSheetAnimationModuleMenu()
{

}

void	SpriteSheetAnimationModuleMenu::Initialize(int offset)
{
	_box = new Fl_Box(00, offset + 5, 300, 25, "SpriteSheet Module");
	_box->box(FL_UP_BOX);
	_box->color(FL_DARK_YELLOW);
	_box->labelfont(FL_BOLD);
	_x = new Fl_Int_Input(60, offset + 35, 80, 25, "X :");
	_y = new Fl_Int_Input(200, offset + 35, 80, 25, "Y :");

	_layer = new Fl_Int_Input(60, offset + 65, 80, 25, "Layer :");

	_totalRaw = new Fl_Int_Input(80, offset + 95, 60, 25, "Raws :");
	_totalColumn = new Fl_Int_Input(220, offset + 95, 60, 25, "Columns :");

	_startingPosX = new Fl_Int_Input(60, offset + 125, 80, 25, "PosX :");
	_startingPosY = new Fl_Int_Input(200, offset + 125, 80, 25, "PosY :");

	_spriteWidth = new Fl_Int_Input(80, offset + 155, 60, 25, "Width :");
	_spriteHeight = new Fl_Int_Input(220, offset + 155, 60, 25, "Height :");

	_duration = new Fl_Float_Input(80, offset + 185, 60, 25, "Duration :");
	_frameNumber = new Fl_Int_Input(220, offset + 185, 60, 25, "FrameNb :");

	_spriteSheetPathButton = new Fl_Button(10, offset + 215, 50, 20, "Bitmap");
	_spriteSheetPathButton->callback(bitmap_sprite_cb, this);
	_spriteSheetPathName = new Fl_Box(35, offset + 215, 230, 20, "");

	_isActive = true;

}

void	SpriteSheetAnimationModuleMenu::bitmap_sprite_cb(Fl_Widget* widget, void* data)
{
	((SpriteSheetAnimationModuleMenu*)data)->SetBitmapSpriteName();
}

void	SpriteSheetAnimationModuleMenu::SetBitmapSpriteName()
{
	_textureChanged = true;
	_spriteSheetPathName->label(EditorWindow::OpenBitmap());
}

void	SpriteSheetAnimationModuleMenu::InitGameObject(GameObject* object)const
{
	if (_isActive)
	{
		SpriteSheetAnimationModule* mod = static_cast<SpriteSheetAnimationModule *>(object->displayModule);
		_x->static_value(strdup(std::to_string(mod->GetX()).c_str()));
		_y->static_value(strdup(std::to_string(mod->GetY()).c_str()));
		_layer->static_value(strdup(std::to_string(mod->GetLayer()).c_str()));
		_totalRaw->static_value(strdup(std::to_string(mod->GetTotalRaw()).c_str()));
		_totalColumn->static_value(strdup(std::to_string(mod->GetTotalColumn()).c_str()));
		_startingPosX->static_value(strdup(std::to_string((int)mod->GetStartingPos().x).c_str()));
		_startingPosY->static_value(strdup(std::to_string((int)mod->GetStartingPos().y).c_str()));
		_spriteWidth->static_value(strdup(std::to_string(mod->GetSpriteWidth()).c_str()));
		_spriteHeight->static_value(strdup(std::to_string(mod->GetSpriteHeight()).c_str()));
		_frameNumber->static_value(strdup(std::to_string(mod->GetFrameNumber()).c_str()));
		std::stringstream duration;
		duration << fixed << std::setprecision(2) << mod->GetDuration();
		_duration->static_value(strdup(duration.str().c_str()));
		_spriteSheetPathName->label(mod->GetSpriteSheetPath());
	}
}

void	SpriteSheetAnimationModuleMenu::Update(GameObject* currentGameObject)
{
	if (_isActive)
	{
		SpriteSheetAnimationModule* mod = static_cast<SpriteSheetAnimationModule *>(currentGameObject->displayModule);
		mod->SetX(atoi(_x->value()));
		mod->SetY(atoi(_y->value()));
		mod->SetLayer(atoi(_layer->value()));
		mod->SetTotalRaw(atoi(_totalRaw->value()));
		mod->SetTotalColumn(atoi(_totalColumn->value()));

		mod->SetStartingPos(Vec2(atof(_startingPosX->value()), atof(_startingPosY->value())));
		mod->SetSpriteWidth(atoi(_spriteWidth->value()));
		mod->SetSpriteHeight(atoi(_spriteHeight->value()));

		mod->SetDuration(atof(_duration->value()));

		mod->SetFrameNumber(atoi(_frameNumber->value()));

		if (_textureChanged)
		{
			std::string newLabel(std::string(RESOURCES_PATH) + _spriteSheetPathName->label());
			mod->SetSpriteSheetPath(const_cast<char*>(newLabel.c_str()));
			_textureChanged = false;
		}
	}
}

void	SpriteSheetAnimationModuleMenu::Clear()
{
	if (_isActive == true)
	{
		_x->hide();
		_y->hide();
		_layer->hide();
		_totalRaw->hide();
		_totalColumn->hide();
		_startingPosX->hide();
		_startingPosY->hide();
		_spriteWidth->hide();
		_spriteHeight->hide();
		_duration->hide();
		_frameNumber->hide();
		_spriteSheetPathButton->hide();
		_spriteSheetPathName->hide();
		_box->hide();
		_isActive = false;
	}
}
