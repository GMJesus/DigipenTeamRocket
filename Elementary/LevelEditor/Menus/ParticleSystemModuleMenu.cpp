#include "ParticleSystemModuleMenu.h"
#include "../EditorEngine/EditorWindow.h"

ParticleSystemModuleMenu::ParticleSystemModuleMenu() : Menu()
{
	_box = NULL;
	_angle = NULL;
	_texturePlaneButton = NULL;
	_texturePlaneName = NULL;
	_lifeTime = NULL;
	_velocityX = NULL;
	_velocityY = NULL;
	_width = NULL;
	_height = NULL;
	_layer = NULL;
	_spawningRate = NULL;
	_gravityEnable = NULL;
	_playButton = NULL;
	_menuHeight = 210;
	_textureChanged = false;
	_isActive = false;
}

ParticleSystemModuleMenu::~ParticleSystemModuleMenu()
{

}


void	ParticleSystemModuleMenu::Initialize(int offset)
{
	_box = new Fl_Box(00, offset + 5, 300, 25, "Particle System Module");
	_box->box(FL_UP_BOX);
	_box->color(FL_DARK_YELLOW);
	_box->labelfont(FL_BOLD);
	_width = new Fl_Float_Input(60, offset + 35, 80, 25, "Width :");
	_height = new Fl_Float_Input(200, offset + 35, 80, 25, "Height :");
	_velocityX = new Fl_Float_Input(60, offset + 65, 80, 25, "Velocity X :");
	_velocityY = new Fl_Float_Input(200, offset + 65, 80, 25, "Velocity Y :");
	_angle = new Fl_Float_Input(60, offset + 95, 80, 25, "Angle :");
	_lifeTime = new Fl_Float_Input(200, offset + 95, 80, 25, "LifeTime :");
	_spawningRate = new Fl_Float_Input(60, offset + 125, 80, 25, "Spawning Frequency :");
	_layer = new Fl_Float_Input(200, offset + 125, 80, 25, "Layer :");
	_texturePlaneButton = new Fl_Button(10, offset + 155, 50, 20, "Image");
	_texturePlaneButton->callback(bitmap_particle_cb, this);
	_texturePlaneName = new Fl_Box(35, offset + 155, 230, 20, "");
	_gravityEnable = new Fl_Check_Button(60, offset + 185, 60, 25, " Gravity");
	_playButton = new Fl_Check_Button(200, offset + 185, 60, 25, "Play");
	_isActive = true;
}

void	ParticleSystemModuleMenu::InitGameObject(GameObject* object)const
{
	if (_isActive)
	{
		_width->static_value(strdup(std::to_string(static_cast<ParticleSystemModule*>(object->displayModule)->GetParticlesWidth()).c_str()));
		_height->static_value(strdup(std::to_string(static_cast<ParticleSystemModule*>(object->displayModule)->GetParticlesHeight()).c_str()));
		_velocityX->static_value(strdup(std::to_string(static_cast<ParticleSystemModule*>(object->displayModule)->GetInitialVelocity().x).c_str()));
		_velocityY->static_value(strdup(std::to_string(static_cast<ParticleSystemModule*>(object->displayModule)->GetInitialVelocity().y).c_str()));
		_angle->static_value(strdup(std::to_string(static_cast<ParticleSystemModule*>(object->displayModule)->GetAngle()).c_str()));
		_lifeTime->static_value(strdup(std::to_string(static_cast<ParticleSystemModule*>(object->displayModule)->GetLifeTime()).c_str()));
		_spawningRate->static_value(strdup(std::to_string(static_cast<ParticleSystemModule*>(object->displayModule)->GetSpawningRate()).c_str()));
		_gravityEnable->value(static_cast<ParticleSystemModule*>(object->displayModule)->IsGravityEnabled());
		_playButton->value(static_cast<ParticleSystemModule*>(object->displayModule)->IsPlaying());
		if (static_cast<ParticleSystemModule*>(object->displayModule)->GetTexture())
			_texturePlaneName->label(static_cast<ParticleSystemModule*>(object->displayModule)->GetTexture()->GetTexturePath());
		_layer->static_value(strdup(std::to_string(object->displayModule->GetLayer()).c_str()));
	}
}


void	ParticleSystemModuleMenu::Update(GameObject* currentObject)
{
	if (!_isActive)
		return;
	static_cast<ParticleSystemModule*>(currentObject->displayModule)->SetParticlesWidth(atof(_width->value()));
	static_cast<ParticleSystemModule*>(currentObject->displayModule)->SetParticlesHeight(atof(_height->value()));
	static_cast<ParticleSystemModule*>(currentObject->displayModule)->SetInitialVelocity(Vec2(atof(_velocityX->value()), atof(_velocityY->value())));
	static_cast<ParticleSystemModule*>(currentObject->displayModule)->SetAngle(atof(_angle->value()));
	static_cast<ParticleSystemModule*>(currentObject->displayModule)->SetLifeTime(atof(_lifeTime->value()));
	static_cast<ParticleSystemModule*>(currentObject->displayModule)->SetSpawningRate(atof(_spawningRate->value()));
	static_cast<ParticleSystemModule*>(currentObject->displayModule)->SetGravityEnabled(_gravityEnable->value());
	static_cast<ParticleSystemModule*>(currentObject->displayModule)->SetPlaying(_playButton->value());
	currentObject->displayModule->SetLayer(atof(_layer->value()));
	if (_textureChanged)
	{
		std::string newLabel(std::string("../Engine/Ressources/") + _texturePlaneName->label());
		static_cast<ParticleSystemModule*>(currentObject->displayModule)->SetParticlesTexture(Graphic::TextureLoader::Instance()->GetTexture(&std::wstring(newLabel.begin(), newLabel.end())[0]));
		_textureChanged = false;
	}
}

void	ParticleSystemModuleMenu::Clear()
{
	if (_isActive == true)
	{
		_isActive = false;
		_box->hide();
		_width->hide();
		_height->hide();
		_velocityX->hide();
		_velocityY->hide();
		_angle->hide();
		_lifeTime->hide();
		_spawningRate->hide();
		_gravityEnable->hide();
		_layer->hide();
		_texturePlaneButton->hide();
		_texturePlaneName->hide();
	}
}

void			ParticleSystemModuleMenu::bitmap_particle_cb(Fl_Widget* widget, void* data)
{

	((ParticleSystemModuleMenu*)data)->SetBitmapSpriteName();
}

void				ParticleSystemModuleMenu::SetBitmapSpriteName()
{
	_textureChanged = true;
	_texturePlaneName->label(EditorWindow::OpenBitmap());
}
