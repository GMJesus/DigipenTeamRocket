/**
* \file      BoxCollisionModuleMenu
* \author    Alexy DURAND
* \brief     Module Menu of the BoxCollisionModule
*
*/


#ifndef BOXCOLLISIONMODULEMENU_H_
# define BOXCOLLISIONMODULEMENU_H_

#include "Menu.h"

class BoxCollisionModuleMenu : public Menu
{
public:
	BoxCollisionModuleMenu();
	~BoxCollisionModuleMenu();

	virtual void		Update(GameObject* currentObject);
	virtual void		Initialize(int offset);
	virtual void		Clear();
	virtual void		InitGameObject(GameObject* object)const;

protected:
	Fl_Box*					_box;
	Fl_Choice*				_type_group;
	Fl_Menu_Item*			_type_group_items;
	Fl_Float_Input*			_boxWidth;
	Fl_Float_Input*			_boxHeight;
	Fl_Check_Button*		_generateHitEvents;
	Fl_Check_Button*		_collisionEnabled;
};

#endif