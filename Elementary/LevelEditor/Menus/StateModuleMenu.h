/**
* \file      StateModuleMenu
* \author    Baptiste DUMAS
* \brief     Module Menu of the StateModule
*
*/

#ifndef STATEMODULEMENU_H_
#define STATEMODULEMENU_H_

#include	"Menus/Menu.h"

class StateModuleMenu : public Menu
{
public:
	StateModuleMenu();
	~StateModuleMenu();

	virtual void	Initialize(int offset);

	virtual void	Update(GameObject* currentGameObject);

	virtual void	Clear();

	virtual void InitGameObject(GameObject* object)const;

protected:
	Fl_Check_Button*	_enable;
	Fl_Int_Input*		_state;
	Fl_Float_Input*		_propagationTime;
	Fl_Float_Input*		_destroyTime;
	Fl_Choice*			_type_group;
	Fl_Menu_Item*		_type_group_items;
	Fl_Float_Input*		_fireStartX;
	Fl_Float_Input*		_fireStartY;
	Fl_Int_Input*		_fireLength;
	Fl_Float_Input*		_waterStartX;
	Fl_Float_Input*		_waterStartY;
	Fl_Int_Input*		_waterLength;
	Fl_Float_Input*		_ElectricStartX;
	Fl_Float_Input*		_ElectricStartY;
	Fl_Int_Input*		_ElectricLength;
};

#endif
