#include	"Menus/InteractivePlatformLogicModuleMenu.h"
#include	"../EditorEngine/EditorWindow.h"

InteractivePlatformLogicModuleMenu::InteractivePlatformLogicModuleMenu()
{

	_box = NULL;
	_enable = NULL;
	_isActivate = NULL;
	_posStartX = NULL;
	_posStartY = NULL;
	_posEndX = NULL;
	_posEndY = NULL;
	_posPassByX = NULL;
	_posPassByY = NULL;
	_type_group = NULL;
	_type_group_items = NULL;

	_menuHeight = 150;
	_isActive = false;
}

InteractivePlatformLogicModuleMenu::~InteractivePlatformLogicModuleMenu()
{

}

void	InteractivePlatformLogicModuleMenu::Initialize(int offset)
{
	_box = new Fl_Box(00, offset + 5, 300, 25, "Interactive Platform Logic");
	_box->box(FL_UP_BOX);
	_box->color(FL_DARK_YELLOW);
	_box->labelfont(FL_BOLD);

	_isActivate = new Fl_Check_Button(200, offset + 35, 60, 25, " Is Activate");

	_type_group = new Fl_Choice(60, offset + 35, 80, 25);;
	_type_group_items = new Fl_Menu_Item[4];;
	for (int i = 0; i < 4; i++)
		_type_group_items[i].text = NULL;
	_type_group_items->add("Start", 0, NULL, "Start", 0);
	_type_group_items->add("Mid", 0, NULL, "Mid", 0);
	_type_group_items->add("End", 0, NULL, "End", 0);
	_type_group->menu(_type_group_items);

	_posStartX = new Fl_Float_Input(60, offset + 65, 80, 25, "Start X");
	_posStartY = new Fl_Float_Input(200, offset + 65, 80, 25, "Start Y");
	_posPassByX = new Fl_Float_Input(60, offset + 65, 80, 25, "Start X");
	_posPassByY = new Fl_Float_Input(200, offset + 65, 80, 25, "Start Y");
	_posEndX = new Fl_Float_Input(60, offset + 65, 80, 25, "Start X");
	_posEndY = new Fl_Float_Input(200, offset + 65, 80, 25, "Start Y");

	_posEndX->hide();
	_posEndY->hide();
	_posPassByX->hide();
	_posPassByY->hide();

	_isActive = true;
}

void	InteractivePlatformLogicModuleMenu::InitGameObject(GameObject* object)const
{
	if (_isActive)
	{
		InteractivePlatformLogicModule* mod = static_cast<InteractivePlatformLogicModule *>(object->logicModule);
		_isActivate->value(mod->IsIsActivate());
		std::stringstream posStartX;
		posStartX << fixed << std::setprecision(2) << mod->GetPosStart().x;
		_posStartX->static_value(strdup(posStartX.str().c_str()));
		std::stringstream posStartY;
		posStartY << fixed << std::setprecision(2) << mod->GetPosStart().y;
		_posStartY->static_value(strdup(posStartY.str().c_str()));
		std::stringstream posMidX;
		posMidX << fixed << std::setprecision(2) << mod->GetPosPassBy().x;
		_posPassByX->static_value(strdup(posMidX.str().c_str()));
		std::stringstream posMidY;
		posMidY << fixed << std::setprecision(2) << mod->GetPosPassBy().y;
		_posPassByY->static_value(strdup(posMidY.str().c_str()));
		std::stringstream posEndX;
		posEndX << fixed << std::setprecision(2) << mod->GetPosEnd().x;
		_posEndX->static_value(strdup(posEndX.str().c_str()));
		std::stringstream posEndY;
		posEndY << fixed << std::setprecision(2) << mod->GetPosEnd().y;
		_posEndY->static_value(strdup(posEndY.str().c_str()));
	}
}

void	InteractivePlatformLogicModuleMenu::Update(GameObject* currentGameObject)
{
	if (_isActive)
	{
		InteractivePlatformLogicModule* mod = static_cast<InteractivePlatformLogicModule *>(currentGameObject->logicModule);

		mod->IsActivate((int)_isActivate->value());
		if (_type_group->find_item("Start")->label() == _type_group->mvalue()->label())
		{
			if (_posStartX->visible() == 0)
			{
				_posStartX->show();
				_posStartY->show();
			}
			if (_posEndX->visible() == 1)
			{
				_posEndX->hide();
				_posEndY->hide();
				_posPassByX->hide();
				_posPassByY->hide();
			}
			mod->SetPosStart(Vec2(atoi(_posStartX->value()), atoi(_posStartY->value())));
		}
		else if (_type_group->find_item("Mid")->label() == _type_group->mvalue()->label())
		{
			if (_posPassByX->visible() == 0)
			{
				_posPassByX->show();
				_posPassByY->show();
			}
			if (_posEndX->visible() == 1)
			{
				_posEndX->hide();
				_posEndY->hide();
				_posStartX->hide();
				_posStartY->hide();
			}
			mod->SetPosPassBy(Vec2(atoi(_posPassByX->value()), atoi(_posPassByY->value())));
		}
		else if (_type_group->find_item("End")->label() == _type_group->mvalue()->label())
		{
			if (_posEndX->visible() == 0)
			{
				_posEndX->show();
				_posEndY->show();
			}
			if (_posStartX->visible() == 1)
			{
				_posStartX->hide();
				_posStartY->hide();
				_posPassByX->hide();
				_posPassByY->hide();
			}
			mod->SetPosEnd(Vec2(atoi(_posEndX->value()), atoi(_posEndY->value())));
		}
	}
}

void	InteractivePlatformLogicModuleMenu::Clear()
{
	if (_isActive == true)
	{
		_isActive = false;
		_box->hide();
		_type_group->hide();
		_type_group_items->hide();
		_isActivate->hide();
		_posStartX->hide();
		_posStartY->hide();
		_posEndX->hide();
		_posEndY->hide();
		_posPassByX->hide();
		_posPassByY->hide();
	}
}

