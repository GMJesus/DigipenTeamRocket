#ifndef PARTICLESYSTEMMODULEMENU_H_
# define PARTICLESYSTEMMODULEMENU_H_

#include "Menu.h"

/**
* \brief     Module Menu of the ParticleSystemModule
*
*/
class ParticleSystemModuleMenu : public Menu
{
public:
	ParticleSystemModuleMenu();
	~ParticleSystemModuleMenu();

	virtual void		Update(GameObject* currentObject);
	virtual void		Initialize(int offset);
	virtual void		Clear();
	virtual void		InitGameObject(GameObject* object)const;

	static void			bitmap_particle_cb(Fl_Widget* widget, void* data);
	void				SetBitmapSpriteName();

	bool					_textureChanged;
	Fl_Box*					_box;
	Fl_Button*				_texturePlaneButton;
	Fl_Box*					_texturePlaneName;
	Fl_Float_Input*			_angle;
	Fl_Float_Input*			_lifeTime;
	Fl_Float_Input*			_velocityX;
	Fl_Float_Input*			_velocityY;
	Fl_Float_Input*			_width;
	Fl_Float_Input*			_height;
	Fl_Float_Input*			_layer;
	Fl_Float_Input*			_spawningRate;
	Fl_Check_Button*		_playButton;
	Fl_Check_Button*		_gravityEnable;
};

#endif