#include "LifeModuleMenu.h"
#include	"../EditorEngine/EditorWindow.h"

LifeModuleMenu::LifeModuleMenu() : Menu()
{
	_box = NULL;
	_health = NULL;
	_maxHealth = NULL;
	_isImmortal = NULL;
	_menuHeight = 90;
	_isActive = false;
}

LifeModuleMenu::~LifeModuleMenu()
{

}

void	LifeModuleMenu::Initialize(int offset)
{
	_box = new Fl_Box(00, offset + 5, 300, 25, "Life Module");
	_box->box(FL_UP_BOX);
	_box->color(FL_DARK_YELLOW);
	_box->labelfont(FL_BOLD);
	_health = new Fl_Int_Input(60, offset + 35, 70, 25, "Health :");
	_maxHealth = new Fl_Int_Input(230, offset + 35, 50, 25, "Max Health :");
	_isImmortal = new Fl_Check_Button(30, offset + 65, 60, 25, " Immortal");
	_isActive = true;
}

void	LifeModuleMenu::InitGameObject(GameObject* object)const
{
	if (_isActive)
	{
		_health->static_value(strdup(std::to_string(object->lifeModule->GetHealth()).c_str()));
		_maxHealth->static_value(strdup(std::to_string(object->lifeModule->GetMaxHealth()).c_str()));
		_isImmortal->value(object->lifeModule->IsImmortal());
	}
}

void	LifeModuleMenu::Update(GameObject* currentObject)
{
	if (!_isActive)
		return;
	currentObject->lifeModule->SetHealth(atoi(_health->value()));
	currentObject->lifeModule->SetMaxHealth(atoi(_maxHealth->value()));
	currentObject->lifeModule->SetImmortal((int)_isImmortal->value());
}

void	LifeModuleMenu::Clear()
{
	if (_isActive == true)
	{
		_isActive = false;
		_box->hide();
		_health->hide();
		_maxHealth->hide();
		_isImmortal->hide();
	}
}