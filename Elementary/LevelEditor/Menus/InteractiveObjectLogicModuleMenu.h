/**
* \file      InteractiveObjectLogicModuleMenu
* \author    Baptiste DUMAS
* \brief     Module Menu of the InteractiveObjectLogicModule
*
*/

#ifndef INTERACTIVEOBJECTLOGICMODULEMENU_H_
#define INTERACTIVEOBJECTLOGICMODULEMENU_H_

#include	"Menus/Menu.h"

class InteractiveObjectLogicModuleMenu : public Menu
{
public:
	InteractiveObjectLogicModuleMenu();
	~InteractiveObjectLogicModuleMenu();

	virtual void	Initialize(int offset);

	virtual void	Update(GameObject* currentGameObject);

	virtual void	Clear();

	virtual void InitGameObject(GameObject* object)const;

protected:
	Fl_Check_Button*		_enable;
	Fl_Check_Button*		_SwitchActive;
};

#endif
