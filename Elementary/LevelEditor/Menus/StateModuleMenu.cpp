#include	"Menus/StateModuleMenu.h"
#include	"../EditorEngine/EditorWindow.h"

StateModuleMenu::StateModuleMenu()
{
	_type_group = NULL;
	_type_group_items = NULL;
	_enable = NULL;
	_state = NULL;
	_propagationTime = NULL;
	_destroyTime = NULL;
	_fireStartX = NULL;
	_fireStartY = NULL;
	_fireLength = NULL;
	_waterStartX = NULL;
	_waterStartY = NULL;
	_waterLength = NULL;
	_ElectricStartX = NULL;
	_ElectricStartY = NULL;
	_ElectricLength = NULL;

	_menuHeight = 150;
	_isActive = false;
}

StateModuleMenu::~StateModuleMenu()
{

}

void	StateModuleMenu::Initialize(int offset)
{
	_box = new Fl_Box(00, offset + 5, 300, 25, "State Module");
	_box->box(FL_UP_BOX);
	_box->color(FL_DARK_YELLOW);
	_box->labelfont(FL_BOLD);

	_state = new Fl_Int_Input(60, offset + 35, 80, 25, "State");
	_propagationTime = new Fl_Float_Input(90, offset + 65, 50, 25, "Propagation");
	_destroyTime = new Fl_Float_Input(220, offset + 65, 60, 25, "Destroy");
	
	_type_group = new Fl_Choice(60, offset + 95, 80, 25);;
	_type_group_items = new Fl_Menu_Item[4];;
	for (int i = 0; i < 4; i++)
		_type_group_items[i].text = NULL;
	_type_group_items->add("Fire", 0, NULL, "Fire", 0);
	_type_group_items->add("Water", 0, NULL, "Water", 0);
	_type_group_items->add("Electricity", 0, NULL, "Electricity", 0);
	_type_group->menu(_type_group_items);

	_fireStartX = new Fl_Float_Input(60, offset + 125, 80, 25, "Start X");
	_fireStartY = new Fl_Float_Input(200, offset + 125, 80, 25, "Start Y");
	_fireLength = new Fl_Int_Input(200, offset + 95, 80, 25, "Length");

	_waterStartX = new Fl_Float_Input(60, offset + 125, 80, 25, "Start X");
	_waterStartY = new Fl_Float_Input(200, offset + 125, 80, 25, "Start Y");
	_waterLength = new Fl_Int_Input(200, offset + 95, 80, 25, "Length");

	_ElectricStartX = new Fl_Float_Input(60, offset + 125, 80, 25, "Start X");
	_ElectricStartY = new Fl_Float_Input(200, offset + 125, 80, 25, "Start Y");
	_ElectricLength = new Fl_Int_Input(200, offset + 95, 80, 25, "Length");
	_waterStartX->hide();
	_waterStartY->hide();
	_waterLength->hide();
	_ElectricStartX->hide();
	_ElectricStartY->hide();
	_ElectricLength->hide();
	_isActive = true;
}

void	StateModuleMenu::InitGameObject(GameObject* object)const
{
	if (_isActive)
	{
		StateModule* mod = object->stateModule;
		_state->static_value(strdup(std::to_string(mod->GetState()).c_str()));
		std::stringstream propagation;
		propagation << fixed << std::setprecision(2) << mod->GetPropagationTime();
		_propagationTime->static_value(strdup(propagation.str().c_str()));
		std::stringstream destroyTime;
		destroyTime << fixed << std::setprecision(2) << mod->GetDestroyTime();
		_destroyTime->static_value(strdup(destroyTime.str().c_str()));


			std::stringstream fireStartX;
			fireStartX << fixed << std::setprecision(2) << mod->GetFireStart().x;
			_fireStartX->static_value(strdup(fireStartX.str().c_str()));
			std::stringstream fireStartY;
			fireStartY << fixed << std::setprecision(2) << mod->GetFireStart().y;
			_fireStartY->static_value(strdup(fireStartY.str().c_str()));
			_fireLength->static_value(strdup(std::to_string(mod->GetFireLength()).c_str()));

			std::stringstream waterStartX;
			waterStartX << fixed << std::setprecision(2) << mod->GetWaterStart().x;
			_waterStartX->static_value(strdup(waterStartX.str().c_str()));
			std::stringstream waterStartY;
			waterStartY << fixed << std::setprecision(2) << mod->GetWaterStart().y;
			_waterStartY->static_value(strdup(waterStartY.str().c_str()));
			_waterLength->static_value(strdup(std::to_string(mod->GetWaterLength()).c_str()));

			std::stringstream ElectricStartX;
			ElectricStartX << fixed << std::setprecision(2) << mod->GetElectricStart().x;
			_ElectricStartX->static_value(strdup(ElectricStartX.str().c_str()));
			std::stringstream ElectricStartY;
			ElectricStartY << fixed << std::setprecision(2) << mod->GetElectricStart().y;
			_ElectricStartY->static_value(strdup(ElectricStartY.str().c_str()));
			_ElectricLength->static_value(strdup(std::to_string(mod->GetElectricLength()).c_str()));
	}
}

void	StateModuleMenu::Update(GameObject* currentGameObject)
{
	if (_isActive)
	{
		StateModule* mod = currentGameObject->stateModule;
		mod->SetState(atoi(_state->value()));
		mod->SetPropagationTime(atof(_propagationTime->value()));
		mod->SetDestroyTime(atof(_destroyTime->value()));
		if (_type_group->find_item("Fire")->label() == _type_group->mvalue()->label())
		{
			if (_fireStartX->visible() == 0)
			{
				_fireStartX->show();
				_fireStartY->show();
				_fireLength->show();
			}
			if (_waterStartX->visible() == 1)
			{
				_waterStartX->hide();
				_waterStartY->hide();
				_waterLength->hide();
				_ElectricStartX->hide();
				_ElectricStartY->hide();
				_ElectricLength->hide();
			}
			mod->SetFireStart(Vec2(atof(_fireStartX->value()), atof(_fireStartY->value())));
			mod->SetFireLength(atoi(_fireLength->value()));
		}
		else if (_type_group->find_item("Water")->label() == _type_group->mvalue()->label())
		{
			if (_waterStartX->visible() == 0)
			{
				_waterStartX->show();
				_waterStartY->show();
				_waterLength->show();
			}
			if (_fireStartX->visible() == 1)
			{
				_fireStartX->hide();
				_fireStartY->hide();
				_fireLength->hide();
				_ElectricStartX->hide();
				_ElectricStartY->hide();
				_ElectricLength->hide();
			}
			mod->SetWaterStart(Vec2(atof(_waterStartX->value()), atof(_waterStartY->value())));
			mod->SetWaterLength(atoi(_waterLength->value()));
		}
		else if (_type_group->find_item("Electricity")->label() == _type_group->mvalue()->label())
		{
			if (_ElectricStartX->visible() == 0)
			{
				_ElectricStartX->show();
				_ElectricStartY->show();
				_ElectricLength->show();
			}
			if (_fireStartX->visible() == 1)
			{
				_fireStartX->hide();
				_fireStartY->hide();
				_fireLength->hide();
				_waterStartX->hide();
				_waterStartY->hide();
				_waterLength->hide();
			}
			mod->SetElectricStart(Vec2(atof(_ElectricStartX->value()), atof(_ElectricStartY->value())));
			mod->SetElectricLength(atoi(_ElectricLength->value()));
		}
	}
}

void	StateModuleMenu::Clear()
{
	if (_isActive)
	{
		_isActive = false;
		_box->hide();
		_state->hide();
		_propagationTime->hide();
		_destroyTime->hide();
		_type_group->hide();
		_type_group_items->hide();
		_fireStartX->hide();
		_fireStartY->hide();
		_fireLength->hide();
		_waterStartX->hide();
		_waterStartY->hide();
		_waterLength->hide();
		_ElectricStartX->hide();
		_ElectricStartY->hide();
		_ElectricLength->hide();
	}
}