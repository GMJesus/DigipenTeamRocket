#include "TransformModuleMenu.h"
#include	"../EditorEngine/EditorWindow.h"

TransformModuleMenu::TransformModuleMenu() : Menu()
{
	_box = NULL;
	_xPosition = NULL;
	_yPosition = NULL;
	_angle = NULL;
	_menuHeight = 120;
	_isActive = false;
}

TransformModuleMenu::~TransformModuleMenu()
{

}

void	TransformModuleMenu::Initialize(int offset)
{
	_isActive = true;
	_box = new Fl_Box(00, offset + 5, 300, 25, "Transform Module");
	_box->box(FL_UP_BOX);
	_box->color(FL_DARK_YELLOW);
	_box->labelfont(FL_BOLD);
	_xPosition = new Fl_Float_Input(80, offset + 35, 80, 25, "Position X :");
	_yPosition = new Fl_Float_Input(190, offset + 35, 80, 25, "Y :");
	_angle = new Fl_Float_Input(80, offset + 65, 60, 25, "Angle :");
	_scale = new Fl_Float_Input(190, offset + 65, 80, 25, "Scale :");
	_scale->value(strdup("1.0"));
	_parent = new Fl_Input(80, offset + 95, 80, 25, "Parent :");
	_isActive = true;
}

void	TransformModuleMenu::InitGameObject(GameObject* object)const
{
	if (_isActive)
	{
		std::stringstream xPosition;
		xPosition << fixed << std::setprecision(2) << object->transformModule->Position().x;
		_xPosition->static_value(strdup(xPosition.str().c_str()));
		std::stringstream yPosition;
		yPosition << fixed << std::setprecision(2) << object->transformModule->Position().y;
		_yPosition->static_value(strdup(yPosition.str().c_str()));
		std::stringstream angle;
		angle << fixed << std::setprecision(2) << object->transformModule->LocalRotation();
		_angle->static_value(strdup(angle.str().c_str()));
		std::stringstream scale;
		scale << fixed << std::setprecision(2) << object->transformModule->LocalScale();
		_scale->static_value(strdup(scale.str().c_str()));
		if (object->transformModule->GetParent())
			_parent->static_value(strdup(object->transformModule->GetParent()->GetName().c_str()));
	}
}

void	TransformModuleMenu::Update(GameObject* currentObject)
{
	if (!_isActive)
		return;
	Vec2 vec;
	vec.x = atoi(_xPosition->value());
	vec.y = atoi(_yPosition->value());
	currentObject->transformModule->SetPosition(vec);
	currentObject->transformModule->SetRotation(atof(_angle->value()));
	currentObject->transformModule->Scale(atof(_scale->value()));

	//In order to have value between 0 and 360
	std::stringstream angle;
	angle << fixed << std::setprecision(2) << currentObject->transformModule->LocalRotation();
	_angle->static_value(strdup(angle.str().c_str()));

	GameObject* newParent;
	if ((newParent = GameObjectFactory::Inst()->GetObjectByName(_parent->value())))
	{
		GameObject* currentParent = currentObject->transformModule->GetParent();
		if (currentParent != newParent)
		{
			if (currentParent)
				currentParent->transformModule->RemoveChild(currentObject);
			currentObject->transformModule->ParentTo(newParent);
		}
	}
}

void	TransformModuleMenu::Clear()
{
	if (_isActive == true)
	{
		_isActive = false;
		_box->hide();
		_xPosition->hide();
		_yPosition->hide();
		_angle->hide();
		_parent->hide();
		_scale->hide();
	}
}