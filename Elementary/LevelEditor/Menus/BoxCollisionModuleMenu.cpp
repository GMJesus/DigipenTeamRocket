#include "BoxCollisionModuleMenu.h"
#include	"../EditorEngine/EditorWindow.h"

BoxCollisionModuleMenu::BoxCollisionModuleMenu() : Menu()
{
	_box = NULL;
	_type_group = NULL;
	_type_group_items = NULL;
	_boxWidth = NULL;
	_boxHeight = NULL;
	_generateHitEvents = NULL;
	_collisionEnabled = NULL;
	_menuHeight = 120;
	_isActive = false;
}

BoxCollisionModuleMenu::~BoxCollisionModuleMenu()
{

}

void	BoxCollisionModuleMenu::Initialize(int offset)
{
	_box = new Fl_Box(00, offset + 5, 300, 25, "Box Collision Module");
	_box->box(FL_UP_BOX);
	_box->color(FL_DARK_YELLOW);
	_box->labelfont(FL_BOLD);
	_type_group = new Fl_Choice(20, offset + 35, 80, 25);;
	_type_group_items = new Fl_Menu_Item[4];;
	for (int i = 0; i < 4; i++)
		_type_group_items[i].text = NULL;
	_type_group_items->add("AABB", 0, NULL, "AABB", 0);
	_type_group_items->add("OOBB", 0, NULL, "OOBB", 0);
	_type_group_items->add("Circle", 0, NULL, "Circle", 0);
	_type_group->menu(_type_group_items);
	_boxWidth = new Fl_Float_Input(90, offset + 65, 60, 25, "Box Width :");
	_boxHeight = new Fl_Float_Input(220, offset + 65, 60, 25, "Height :");
	_generateHitEvents = new Fl_Check_Button(30, offset + 95, 60, 25, " Hit Events");
	_collisionEnabled = new Fl_Check_Button(180, offset + 95, 60, 25, " Collision");
	_isActive = true;
}

void	BoxCollisionModuleMenu::InitGameObject(GameObject* object)const
{
	if (_isActive)
	{
		_type_group->value(object->boxCollisionModule->GetType());
		std::stringstream boxWidth;
		boxWidth << fixed << std::setprecision(2) << object->boxCollisionModule->GetBoxWidth();
		_boxWidth->static_value(strdup(boxWidth.str().c_str()));
		std::stringstream boxHeight;
		boxHeight << fixed << std::setprecision(2) << object->boxCollisionModule->GetBoxHeight();
		_boxHeight->static_value(strdup(boxHeight.str().c_str()));
		_generateHitEvents->value(object->boxCollisionModule->GenerateHitEvents());
		_collisionEnabled->value(object->boxCollisionModule->CollisionEnabled());
	}
}

void	BoxCollisionModuleMenu::Update(GameObject* currentObject)
{
	if (!_isActive)
		return;
	currentObject->boxCollisionModule->SetType((E_CollisionType)_type_group->value());
	currentObject->boxCollisionModule->SetBoxWidth(atoi(_boxWidth->value()));
	currentObject->boxCollisionModule->SetBoxHeight(atoi(_boxHeight->value()));
	currentObject->boxCollisionModule->SetGenerateHitEvents((int)_generateHitEvents->value());
	currentObject->boxCollisionModule->SetCollisionEnabled((int)_collisionEnabled->value());
}

void	BoxCollisionModuleMenu::Clear()
{
	if (_isActive == true)
	{
		_isActive = false;
		_box->hide();
		_type_group->hide();
		_type_group_items->hide();
		_boxWidth->hide();
		_boxHeight->hide();
		_generateHitEvents->hide();
		_collisionEnabled->hide();
	}
}