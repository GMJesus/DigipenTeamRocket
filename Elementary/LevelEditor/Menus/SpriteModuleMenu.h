/**
* \file      SpriteModuleMenu
* \author    Alexy DURAND
* \brief     Module Menu of the SpriteModule
*
*/

#ifndef SPRITEMODULEMENU_H_
# define SPRITEMODULEMENU_H_

#include "Menu.h"

class SpriteModuleMenu : public Menu
{
public:
	SpriteModuleMenu();
	~SpriteModuleMenu();

	virtual void		Update(GameObject* currentObject);
	virtual void		Initialize(int offset);
	virtual void		Clear();
	virtual void		InitGameObject(GameObject* object)const;

	static void			bitmap_sprite_cb(Fl_Widget* widget, void* data);
	void				SetBitmapSpriteName();

	Fl_Int_Input*		_x;
	Fl_Int_Input*		_y;
	Fl_Float_Input*		_angle;
	Fl_Int_Input*		_width;
	Fl_Int_Input*		_height;
	Fl_Button*			_texturePlaneButton;
	Fl_Box*				_texturePlaneName;
	Fl_Int_Input*		_layer;
	bool				_textureChanged;
};

#endif