#ifndef ENDOFLEVELLOGICMODULEMENU_H_
#define ENDOFLEVELLOGICMODULEMENU_H_

#include	"Menus/Menu.h"

class EndOfLevelLogicModuleMenu : public Menu
{
public:
	EndOfLevelLogicModuleMenu();
	~EndOfLevelLogicModuleMenu();

	virtual void	Initialize(int offset);

	virtual void	Update(GameObject* currentGameObject);

	virtual void	Clear();

	virtual void InitGameObject(GameObject* object)const;

protected:
	Fl_Check_Button*		_enable;
	Fl_String_Input*			LevelToLoad;
};

#endif
