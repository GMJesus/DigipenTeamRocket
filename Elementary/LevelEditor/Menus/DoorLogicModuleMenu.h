/**
* \file      DoorLogicModuleMenu
* \author    Baptiste DUMAS
* \brief     Module Menu of the DoorLogicModule
*
*/


#ifndef DOORLOGICMODULEMENU_H_
#define DOORLOGICMODULEMENU_H_

#include	"Menus/Menu.h"
#include	"Modules\DoorLogicModule.h"

class DoorLogicModuleMenu : public Menu
{
public:
	DoorLogicModuleMenu();
	~DoorLogicModuleMenu();

	virtual void	Initialize(int offset);

	virtual void	Update(GameObject* currentGameObject);

	virtual void	Clear();

	virtual void InitGameObject(GameObject* object)const;

protected:
	Fl_Check_Button*		_enable;
	Fl_Check_Button*		_isOpen;
};

#endif
