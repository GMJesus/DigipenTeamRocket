#include "SoundModuleMenu.h"
#include	"../EditorEngine/EditorWindow.h"

SoundModuleMenu::SoundModuleMenu() : Menu()
{
	_box = NULL;
	_soundButton = NULL;
	_soundName = NULL;
	_loop = NULL;
	_volume = NULL;
	_soundChanged = false;
	_menuHeight = 90;
	_isActive = false;
}

SoundModuleMenu::~SoundModuleMenu()
{

}

void	SoundModuleMenu::Initialize(int offset)
{
	_box = new Fl_Box(00, offset + 5, 300, 25, "Sound Module");
	_box->box(FL_UP_BOX);
	_box->color(FL_DARK_YELLOW);
	_box->labelfont(FL_BOLD);
	_soundButton = new Fl_Button(10, offset + 35, 50, 20, "Sound");
	_soundButton->callback(sound_cb, this);
	_soundName = new Fl_Box(35, offset + 35, 180, 20, "");
	_play = new Fl_Button(200, offset + 35, 50, 20, "Play");
	_play->callback(play_cb, this);
	_loop = new Fl_Check_Button(30, offset + 65, 60, 25, " Loop");
	_volume = new Fl_Float_Input(200, offset + 65, 80, 25, "Volume :");
	_isActive = true;
}

void	SoundModuleMenu::InitGameObject(GameObject* object)const
{
	if (_isActive)
	{
		_soundName->label(object->soundModule->GetPath().c_str());
		_loop->value(object->soundModule->IsLoopSound());
		std::stringstream volume;
		volume << fixed << std::setprecision(2) << object->soundModule->Getvolume();
		_volume->static_value(strdup(volume.str().c_str()));
	}
}

void	SoundModuleMenu::Update(GameObject* currentObject)
{
	if (!_isActive)
		return;
	currentObject->soundModule->SetLoop((bool)_loop->value());
	currentObject->soundModule->SetVolume(atof(_volume->value()));
	if (_soundChanged)
	{
		std::string newLabel(std::string(RESOURCES_PATH) + _soundName->label());
		currentObject->soundModule->SetPath(newLabel.c_str());
		_soundChanged = !_soundChanged;
	}
}

void	SoundModuleMenu::Clear()
{
	if (_isActive == true)
	{
		_isActive = false;
		_box->hide();
		_play->hide();
		_soundButton->hide();
		_soundName->hide();
		_loop->hide();
		_volume->hide();
	}
}

void	SoundModuleMenu::play_cb(Fl_Widget* widget, void* data)
{
	((SoundModuleMenu*)data)->Play();
}

void		SoundModuleMenu::Play()
{
	SoundManager &p_Sound = SoundManager::Instance();
	SoundClass soundSample;
#ifdef NDEBUG
	std::string newLabel(std::string("./Ressources/Sounds/") + _soundName->label());
#else
	std::string newLabel(std::string("./Ressources/Sounds/") + _soundName->label());
#endif
	p_Sound.createSound(&soundSample, newLabel.c_str());
	p_Sound.playSound(soundSample, false);
}

void	SoundModuleMenu::sound_cb(Fl_Widget* widget, void* data)
{
	((SoundModuleMenu*)data)->SetSoundName();
}

void	SoundModuleMenu::SetSoundName()
{
	_soundChanged = true;
	_soundName->label(EditorWindow::OpenSound());
}