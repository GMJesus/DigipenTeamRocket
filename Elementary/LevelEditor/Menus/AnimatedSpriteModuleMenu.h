/**
* \file      AnimatedSpriteModuleMenu
* \author    Alexy DURAND
* \brief     Module Menu of the AnimatedSpriteModule
*
*/

#ifndef ANIMATEDSPRITEMODULEMENU_H_
# define ANIMATEDSPRITEMODULEMENU_H_

#include "Menu.h"

class AnimatedSpriteModuleMenu : public Menu
{
public:
	AnimatedSpriteModuleMenu();
	~AnimatedSpriteModuleMenu();

	virtual void		Update(GameObject* currentObject);
	virtual void		Initialize(int offset);
	virtual void		Clear();
	virtual void		InitGameObject(GameObject* object)const;

	static void			bitmap_sprite_cb(Fl_Widget* widget, void* data);
	void				SetBitmapSpriteName();

protected:
	Fl_Box*					_box;
	Fl_Int_Input*			_x;
	Fl_Int_Input*			_y;
	Fl_Float_Input*			_angle;
	Fl_Int_Input*			_initialWidth;
	Fl_Int_Input*			_initialHeight;
	Fl_Int_Input*			_width;
	Fl_Int_Input*			_height;
	Fl_Button*				_texturePlaneButton;
	Fl_Box*					_texturePlaneName;
	bool					_textureChanged;
};

#endif