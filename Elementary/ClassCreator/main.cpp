/**
* \author	Aymeric Lambolez
*/

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <list>

#include "../Engine/Game/GameObject.h"
#include "../Engine/Modules/Module.h"

#define XmlWriterSourcePath "../LevelEditor/XML/XmlWriter.cpp"
#define XmlParserSourcePath "../Engine/XML/XmlParser.cpp"
#define XmlWriterHeaderPath "../LevelEditor/XML/XmlWriter.h"
#define XmlParserHeaderPath "../Engine/XML/XmlParser.h"
#define GameObjectFactorySource "../Engine/Game/GameObjectFactory.cpp"
#define GameObjectFactoryHeader "../Engine/Game/GameObjectFactory.h"
#define GameObjectInclude "../Engine/Game/GameObjectInclude.h"
#define GameObjectHeader "../Engine/Game/GameObject.h"
#define GameObjectPath "../Engine/Game/"
#define LevelEditorHeader "../LevelEditor/FLTK_UI/EditorWindow.h"
#define LevelEditorSource "../LevelEditor/FLTK_UI/EditorWindow.cpp"

#define GameObjectTypePrefix "GameObject::Type::E_"

#define COMMENT_FORMAT "//@ClassCreator "

void	CreateHeaderFile(const std::string& className)
{
	std::ofstream	file(GameObjectPath + className + ".h");
	if (!file)
		throw std::exception((std::string("Cannot open file ") + GameObjectPath + className + ".h").c_str());

	std::string upperName = className;
	std::transform(upperName.begin(), upperName.end(), upperName.begin(), ::toupper);
	file << "#ifndef " << upperName << "_H_" << std::endl;
	file << "#define " << upperName << "_H_" << std::endl << std::endl;
	file << "#include	\"Game/GameObject.h\"" << std::endl << std::endl;

	file << "class " << className << " : public GameObject" << std::endl;
	file << "{" << std::endl;

	file << "public:" << std::endl;
	file << "\t" << className << "(const std::string& name, unsigned int id);" << std::endl;
	file << "\t~" << className << "();" << std::endl << std::endl;
	file << "\tvoid	Initialize();" << std::endl << std::endl;
	file << "\tGameObject::Type	GetType()const;" << std::endl << std::endl;

	file << "};" << std::endl << std::endl;
	file << "#endif" << std::endl;
	file.close();
}

void	CreateSourceFile(const std::string& className)
{
	std::ofstream	file(GameObjectPath + className + ".cpp");
	if (!file)
		throw std::exception((std::string("Cannot open file ") + GameObjectPath + className + ".cpp").c_str());

	file << "#include \"Game/" << className << ".h\"" << std::endl << std::endl;
	file << className << "::" << className << "(const std::string& name, unsigned int id) : GameObject(name, id)" << std::endl;
	file << "{" << std::endl << std::endl << "}" << std::endl << std::endl;

	file << className << "::~" << className << "()" << std::endl;
	file << "{" << std::endl << std::endl << "}" << std::endl << std::endl;

	file << "void\t\t\t\t" << className << "::Initialize()" << std::endl;
	file << "{" << std::endl << std::endl << "}" << std::endl << std::endl;

	file << "GameObject::Type\t" << className << "::GetType()const{ return " << GameObjectTypePrefix << className << "; }" << std::endl;
	file.close();
}

void	CreateFiles(const std::string& className)
{
	CreateHeaderFile(className);
	CreateSourceFile(className);
}

void	InsertHeaderFileInIncludeFile(const std::string& className)
{
	std::ifstream ifile(GameObjectInclude);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + GameObjectInclude).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		if (buf.find(std::string(COMMENT_FORMAT) + "ObjectIncludeStart") != std::string::npos)
		{
			std::string toAdd("#include\t\"Game\\");
			toAdd += className + ".h\"";
			lines.push_back(toAdd);
		}
	}
	ifile.close();

	std::ofstream ofile(GameObjectInclude);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + GameObjectInclude).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}
	ofile.close();
	lines.clear();
}

void	CreateGameObjectType(const std::string& className)
{
	std::ifstream ifile(GameObjectHeader);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + GameObjectHeader).c_str());
	std::list<std::string>	lines;
	std::string buf;
	bool	count = false;
	int		classNb = 0;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		if (buf.find(std::string(COMMENT_FORMAT) + "ObjectTypeStart") != std::string::npos)
		{
			count = true;
		}
		else if (buf.find(std::string(COMMENT_FORMAT) + "ObjectTypeEnd") != std::string::npos)
		{
			count = false;
			lines.remove(lines.back());
			lines.back().append(",");
			lines.push_back("\t\tE_" + className + " = " + std::to_string(classNb - 1));
			lines.push_back("\t\t//@ClassCreator ObjectTypeEnd");
		}
		else if (buf.find(std::string(COMMENT_FORMAT) + "ObjectClassNumber") != std::string::npos)
		{
			lines.push_back("\tstatic const int\t\tGAMEOBJECT_CLASS_NUMBER = " + std::to_string(classNb) + ";");
			std::getline(ifile, buf);
		}
		if (count)
			++classNb;
	}
	ifile.close();

	std::ofstream ofile(GameObjectHeader);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + GameObjectHeader).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}
	ofile.close();
	lines.clear();
}

void	RegisterInGameObjectFactoryHeader(const std::string& className, bool *modules)
{
	std::ifstream ifile(GameObjectFactoryHeader);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + GameObjectFactoryHeader).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		if (buf.find(std::string(COMMENT_FORMAT) + "ObjectFunctionCreationStart") != std::string::npos)
		{
			std::string toAdd("\tGameObject*\t\t\t\t\tCreate");
			toAdd += className + "(unsigned int objectId, const std::string& objectName);";
			lines.push_back(toAdd);
		}
	}
	ifile.close();

	std::ofstream ofile(GameObjectFactoryHeader);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + GameObjectFactoryHeader).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}
	ofile.close();
	lines.clear();
}

void	RegisterInGameObjectFactorySource(const std::string& className, bool *modules, std::string* modulesNames)
{
	std::ifstream ifile(GameObjectFactorySource);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + GameObjectFactorySource).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		if (buf.find(std::string(COMMENT_FORMAT) + "FuncArrayObjectCreationStart") != std::string::npos)
		{
			std::string toAdd("\t_objectClassArray[GameObject::Type::E_");
			toAdd += className + "] = &GameObjectFactory::Create" + className + ";";
			lines.push_back(toAdd);
		}
	}
	ifile.close();

	std::ofstream ofile(GameObjectFactorySource);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + GameObjectFactorySource).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}
	ofile << std::endl;
	ofile << "GameObject*\t\t\tGameObjectFactory::Create" + className + "(unsigned int objectId, const std::string& objectName)" << std::endl;
	ofile << "{" << std::endl;
	ofile << "\tLogWriter::Instance().addLogMessage(\"Creating " + className + " object\");" << std::endl;
	ofile << "\tGameObject* newObject = new " + className + "(objectName, objectId);" << std::endl;
	for (int i = 0; i < Module::MODULE_CLASS_NUMBER; ++i)
	{
		if (modules[i])
			ofile << "\tModuleFactory::Inst()->CreateModule(Module::E_" + modulesNames[i] + ", newObject);" << std::endl;
	}
	ofile << "\treturn newObject;" << std::endl;
	ofile << "}" << std::endl;

	ofile.close();
	lines.clear();
}

void	RegisterInXmlParserSource(const std::string& className)
{
	std::ifstream ifile(XmlParserSourcePath);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + XmlParserSourcePath).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		if (buf.find(std::string(COMMENT_FORMAT) + "FuncArrayObjectParsingStart") != std::string::npos)
		{
			std::string toAdd("\t_objectCreationFunc[GameObject::Type::E_");
			toAdd += className + "] = &XmlParser::Parse" + className + ";";
			lines.push_back(toAdd);
		}
	}
	ifile.close();

	std::ofstream ofile(XmlParserSourcePath);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + XmlParserSourcePath).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}

	ofile << std::endl;
	ofile << "bool XmlParser::Parse" + className + "(TiXmlElement* elem)" << std::endl;
	ofile << "{" << std::endl;
	ofile << "\tbool ret = true;" << std::endl;
	ofile << "\tconst char*	name = elem->Attribute(\"name\");" << std::endl;
	ofile << "\tGameObject* object = GameObjectFactory::Inst()->CreateGameObject(GameObject::Type::E_" << className << ", name);" << std::endl << std::endl;
	ofile << "\tif (!ParseModule(elem, object))" << std::endl;
	ofile << "\t{" << std::endl;
	ofile << "\t\tGameObjectFactory::Inst()->DeleteObject(object->Id(), true);" << std::endl;
	ofile << "\t\tret = false;" << std::endl;
	ofile << "\t}" << std::endl << std::endl;
	ofile << "\treturn ret;" << std::endl;
	ofile << "}" << std::endl;

	ofile.close();
	lines.clear();
}

void	RegisterInXmlParserHeader(const std::string& className)
{
	std::ifstream ifile(XmlParserHeaderPath);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + XmlParserHeaderPath).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);

		if (buf.find(std::string(COMMENT_FORMAT) + "ObjectFunctionParsingStart") != std::string::npos)
		{
			std::string toAdd("\t\tbool Parse");
			toAdd += className + "(TiXmlElement* elem);";
			lines.push_back(toAdd);
		}
	}
	ifile.close();

	std::ofstream ofile(XmlParserHeaderPath);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + XmlParserHeaderPath).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}
	ofile.close();
	lines.clear();
}

void	RegisterInXmlWriterSource(const std::string& className, bool* modules)
{
	std::string modulesNames[Module::MODULE_CLASS_NUMBER];
	//@ModuleCreator ModuleVariableNameArrayStart
	modulesNames[Module::Type::E_InteractivePlatformLogicModule] = "logicModule";
	modulesNames[Module::Type::E_EndLevelLogicModule] = "logicModule";
	modulesNames[Module::Type::E_CheckpointLogicModule] = "logicModule";
	modulesNames[Module::Type::E_DoorLogicModule] = "logicModule";
	modulesNames[Module::Type::E_SwitchLogicModule] = "logicModule";
	modulesNames[Module::Type::E_InteractiveObjectLogicModule] = "logicModule";
	modulesNames[Module::Type::E_BackAndForthBehaviourModule] = "aIModule";
	modulesNames[Module::Type::E_StateModule] = "stateModule";
	modulesNames[Module::Type::E_GunLogicModule] = "logicModule";
	modulesNames[Module::Type::E_ProjLogicModule] = "logicModule";
	modulesNames[Module::Type::E_SpriteSheetAnimationModule] = "displayModule";
	modulesNames[Module::Type::E_AIModule] = "AIModule";
	modulesNames[Module::Type::E_AnimatedSpriteModule] = "displayModule";
	modulesNames[Module::Type::E_BoxCollisionModule] = "boxCollisionModule";
	modulesNames[Module::Type::E_LifeModule] = "lifeModule";
	modulesNames[Module::Type::E_LogicModule] = "logicModule";
	modulesNames[Module::Type::E_ParticleSystemModule] = "displayModule";
	modulesNames[Module::Type::E_PhysicModule] = "physicModule";
	modulesNames[Module::Type::E_SoundModule] = "soundModule";
	modulesNames[Module::Type::E_SpriteModule] = "displayModule";
	modulesNames[Module::Type::E_TransformModule] = "transformModule";

	std::ifstream ifile(XmlWriterSourcePath);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + XmlWriterSourcePath).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		if (buf.find(std::string(COMMENT_FORMAT) + "FuncArrayObjectSerializationStart") != std::string::npos)
		{
			std::string toAdd("\t\t_objectSerializationFunc[GameObject::Type::E_");
			toAdd += className + "] = &XmlWriter::Serialize" + className + ";";
			lines.push_back(toAdd);
		}
	}
	ifile.close();
	lines.remove(lines.back());
	std::ofstream ofile(XmlWriterSourcePath);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + XmlWriterSourcePath).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}

	ofile << std::endl;
	ofile << "\tbool\t\tXmlWriter::Serialize" + className + "(GameObject* obj, TiXmlElement* node)" << std::endl;
	ofile << "\t{" << std::endl;
	ofile << "\t\tif (!obj)" << std::endl;
	ofile << "\t\t\treturn false;" << std::endl;
	for (int i = 0; i < Module::MODULE_CLASS_NUMBER; ++i)
	{
		if (modules[i])
			ofile << "\t\tnode->LinkEndChild(SerializeModule(obj->" << modulesNames[i] << "));" << std::endl;
	}
	ofile << "\treturn true;" << std::endl;
	ofile << "\t}" << std::endl;
	ofile << "}" << std::endl;

	ofile.close();
	lines.clear();
}

void	RegisterInXmlWriterHeader(const std::string& className)
{
	std::ifstream ifile(XmlWriterHeaderPath);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + XmlWriterHeaderPath).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		if (buf.find(std::string(COMMENT_FORMAT) + "FuncArrayObjectSerializationStart") != std::string::npos)
		{
			std::string toAdd("\t\tbool\tSerialize");
			toAdd += className + "(GameObject* obj, TiXmlElement* node);";
			lines.push_back(toAdd);
		}
	}
	ifile.close();

	std::ofstream ofile(XmlWriterHeaderPath);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + XmlWriterHeaderPath).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}
	ofile.close();
	lines.clear();
}

void	RegisterInLevelEditorHeader(const std::string& className)
{
	std::ifstream ifile(LevelEditorHeader);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + LevelEditorHeader).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		if (buf.find(std::string(COMMENT_FORMAT) + "ObjectCreationFuncStart") != std::string::npos)
		{
			std::string toAdd("\tvoid\t\t\tCreate");
			toAdd += className + "Menu();";
			lines.push_back(toAdd);
		}
	}
	ifile.close();

	std::ofstream ofile(LevelEditorHeader);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + LevelEditorHeader).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}
	ofile.close();
	lines.clear();
}

void	RegisterInLevelEditorSource(const std::string& className, bool* modules, std::string* modulesNames)
{
	std::ifstream ifile(LevelEditorSource);
	if (!ifile)
		throw std::exception((std::string("Cannot open file ") + LevelEditorSource).c_str());
	std::list<std::string>	lines;
	std::string buf;

	while (std::getline(ifile, buf))
	{
		lines.push_back(buf);
		if (buf.find(std::string(COMMENT_FORMAT) + "ObjectCreationFuncStart") != std::string::npos)
		{
			std::string toAdd("\t_menuCreationFunc[GameObject::Type::E_");
			toAdd += className + "] = &EditorWindow::Create" + className + "Menu;";
			lines.push_back(toAdd);
		}
		else if (buf.find(std::string(COMMENT_FORMAT) + "ObjectGroupItemGenerationEnd") != std::string::npos)
		{
			std::string toAdd("\tobject_group_items->add(\"");
			toAdd += className + "\", 0, cb_object, strdup(const_cast<char *>(std::to_string(GameObject::E_" + className + ").c_str())), 0);";

			lines.remove(lines.back());
			lines.push_back(toAdd);
			lines.push_back("\t//@ClassCreator ObjectGroupItemGenerationEnd");
		}
	}
	ifile.close();
	std::ofstream ofile(LevelEditorSource);
	if (!ofile)
		throw std::exception((std::string("Cannot open file ") + LevelEditorSource).c_str());
	for (std::list<std::string>::const_iterator it = lines.begin(); it != lines.end(); ++it)
	{
		ofile << *it << std::endl;
	}

	ofile << std::endl;
	ofile << "void EditorWindow::Create" << className << "Menu()" << std::endl;
	ofile << "{" << std::endl;
	for (int i = 0; i != Module::MODULE_CLASS_NUMBER; ++i)
	{
		if (modules[i])
		{
			std::string var = modulesNames[i] + "Menu";
			var[0] = tolower(var[0]);
			ofile << "\t_" << var << "->Initialize(_offset);"<< std::endl;
			ofile << "\t_offset += _" << var << "->GetHeight();" << std::endl;
		}
	}
	ofile << "}" << std::endl;

	ofile.close();
	lines.clear();
}

int main()
{
	bool modules[Module::MODULE_CLASS_NUMBER]{false};
	std::string modulesNames[Module::MODULE_CLASS_NUMBER];
	//@ModuleCreator ModuleNamesArrayStart
	modulesNames[Module::Type::E_InteractivePlatformLogicModule] = "InteractivePlatformLogicModule";
	modulesNames[Module::Type::E_EndLevelLogicModule] = "EndLevelLogicModule";
	modulesNames[Module::Type::E_CheckpointLogicModule] = "CheckpointLogicModule";
	modulesNames[Module::Type::E_DoorLogicModule] = "DoorLogicModule";
	modulesNames[Module::Type::E_SwitchLogicModule] = "SwitchLogicModule";
	modulesNames[Module::Type::E_InteractiveObjectLogicModule] = "InteractiveObjectLogicModule";
	modulesNames[Module::Type::E_BackAndForthBehaviourModule] = "BackAndForthBehaviourModule";
	modulesNames[Module::Type::E_StateModule] = "StateModule";
	modulesNames[Module::Type::E_GunLogicModule] = "GunLogicModule";
	modulesNames[Module::Type::E_ProjLogicModule] = "ProjLogicModule";
	modulesNames[Module::Type::E_SpriteSheetAnimationModule] = "SpriteSheetAnimationModule";
	modulesNames[Module::Type::E_AIModule] = "AIModule";
	modulesNames[Module::Type::E_AnimatedSpriteModule] = "AnimatedSpriteModule";
	modulesNames[Module::Type::E_BoxCollisionModule] = "BoxCollisionModule";
	modulesNames[Module::Type::E_LifeModule] = "LifeModule";
	modulesNames[Module::Type::E_LogicModule] = "LogicModule";
	modulesNames[Module::Type::E_ParticleSystemModule] = "ParticleSystemModule";
	modulesNames[Module::Type::E_PhysicModule] = "PhysicModule";
	modulesNames[Module::Type::E_SoundModule] = "SoundModule";
	modulesNames[Module::Type::E_SpriteModule] = "SpriteModule";
	modulesNames[Module::Type::E_TransformModule] = "TransformModule";

	std::string strInput;
	std::string className;

	std::cout << "Class Name : ";
	std::cin >> className;
	std::cout << std::endl;
	std::cout << "Generating .cpp and .h files..." << std::endl;
	CreateFiles(className);
	std::cout << "Done" << std::endl;
	std::cout << "Registering file in GameObjectInclude.h..." << std::endl;
	InsertHeaderFileInIncludeFile(className);
	std::cout << "Done" << std::endl;
	std::cout << "Generating GameObject::Type element..." << std::endl;
	CreateGameObjectType(className);
	std::cout << "Done" << std::endl << std::endl;

	//@ModuleCreator ModuleRequestsStart
	std::cout << "Adding InteractivePlatformLogicModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_InteractivePlatformLogicModule] = strInput[0] == 'y';

	std::cout << "Adding EndLevelLogicModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_EndLevelLogicModule] = strInput[0] == 'y';

	std::cout << "Adding CheckpointLogicModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_CheckpointLogicModule] = strInput[0] == 'y';

	std::cout << "Adding DoorLogicModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_DoorLogicModule] = strInput[0] == 'y';

	std::cout << "Adding SwitchLogicModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_SwitchLogicModule] = strInput[0] == 'y';

	std::cout << "Adding InteractiveObjectLogicModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_InteractiveObjectLogicModule] = strInput[0] == 'y';

	std::cout << "Adding BackAndForthBehaviourModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_BackAndForthBehaviourModule] = strInput[0] == 'y';

	std::cout << "Adding StateModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_StateModule] = strInput[0] == 'y';

	std::cout << "Adding GunLogicModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_GunLogicModule] = strInput[0] == 'y';

	std::cout << "Adding ProjLogicModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_ProjLogicModule] = strInput[0] == 'y';

	std::cout << "Adding SpriteSheetAnimationModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_SpriteSheetAnimationModule] = strInput[0] == 'y';

	std::cout << "Adding SpriteModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_SpriteModule] = strInput[0] == 'y';

	std::cout << "Adding AnimatedSpriteModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_AnimatedSpriteModule] = strInput[0] == 'y';

	std::cout << "Adding AIModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_AIModule] = strInput[0] == 'y';

	std::cout << "Adding BoxCollisionModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_BoxCollisionModule] = strInput[0] == 'y';

	std::cout << "Adding LifeModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_LifeModule] = strInput[0] == 'y';

	std::cout << "Adding LogicModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_LogicModule] = strInput[0] == 'y';

	std::cout << "Adding ParticleSystem ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_ParticleSystemModule] = strInput[0] == 'y';

	std::cout << "Adding PhysicModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_PhysicModule] = strInput[0] == 'y';

	std::cout << "Adding SoundModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_SoundModule] = strInput[0] == 'y';

	std::cout << "Adding TransformModule ? [y/n] : ";
	std::cin >> strInput;
	std::cout << std::endl;
	modules[Module::Type::E_TransformModule] = strInput[0] == 'y';

	std::cout << "Registering class in GameObjectFactory..." << std::endl;
	RegisterInGameObjectFactoryHeader(className, modules);
	RegisterInGameObjectFactorySource(className, modules, modulesNames);
	std::cout << "Done" << std::endl;

	std::cout << "Generating class Parsing..." << std::endl;
	RegisterInXmlParserHeader(className);
	RegisterInXmlParserSource(className);
	std::cout << "Done" << std::endl;

	std::cout << "Generating class Serializing..." << std::endl;
	RegisterInXmlWriterHeader(className);
	RegisterInXmlWriterSource(className, modules);
	std::cout << "Done" << std::endl;

	std::cout << "Registering class in LevelEditor..." << std::endl;
	RegisterInLevelEditorHeader(className);
	RegisterInLevelEditorSource(className, modules, modulesNames);
	std::cout << "Done" << std::endl;
}
