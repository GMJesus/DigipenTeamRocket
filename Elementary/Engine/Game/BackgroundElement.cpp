#include "Game\BackgroundElement.h"

BackgroundElement::BackgroundElement(const std::string& name, unsigned int id) : GameObject(name, id)
{

}

void	BackgroundElement::Initialize()
{
}

GameObject::Type	BackgroundElement::GetType()const{ return GameObject::Type::E_BackgroundElement; }
