#include "Game/Character.h"

Character::Character(const std::string& name, unsigned int id) : GameObject(name, id)
{

}

Character::~Character()
{

}

void				Character::Initialize()
{

}

GameObject::Type	Character::GetType()const{ return GameObject::Type::E_Character; }
