#ifndef		GAMEOBJECTINCLUDE_H_
# define	GAMEOBJECTINCLUDE_H_

//@ClassCreator ObjectIncludeStart
#include	"Game\InteractivePlatform.h"
#include	"Game\EndOfLevel.h"
#include	"Game\Checkpoint.h"
#include	"Game\Door.h"
#include	"Game\Switch.h"
#include	"Game\BackAndForthObject.h"
#include	"Game\ElementalInteractiveObject.h"
#include	"Game\AnimatedElement.h"
#include	"Game\Gun.h"
#include	"Game\Projectile.h"
#include	"Game\StaticObject.h"
#include	"Game\Character.h"
#include	"Game\ParticleSystem.h"
#include	"Game\Player.h"
#include	"Game\GameObject.h"
#include	"Game\BackgroundElement.h"
#include	"Game\DecorativeElement.h"
#include	"Game\InvisibleWall.h"
#include	"Game\Actor.h"

#endif		/* GAMEOBJECTINCLUDE_H_ */
