#ifndef		DECORATIVEELEMENT_H_
# define	DECORATIVEELEMENT_H_

#include "Game/GameObject.h"
#include "Modules\ModuleFactory.h"
#include "Modules\ModuleImport.h"
#include "Graphics\TextureLoader.h"

/**
* \brief	Class for decorative elements, without impact on Gameplay
*			Contains the following modules :
*				- SpriteModule
*				- TransformModule
*/
class DecorativeElement : public GameObject
{
public:
	DecorativeElement(const std::string& name, unsigned int id);
	~DecorativeElement();

	void	Initialize();

	GameObject::Type	GetType()const;
};

#endif		// !DECORATIVEELEMENT_H_

