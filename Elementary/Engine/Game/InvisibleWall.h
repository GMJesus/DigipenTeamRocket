#ifndef		INVISIBLEWALL_H_
# define	INVISIBLEWALL_H_

#include	"Game\GameObject.h"

/**
* \brief	Class for invisible walls
*			Contains the following modules :
*				- TransformModule
*				- BoxCollisionModule
*/
class InvisibleWall : public GameObject
{
public:
	InvisibleWall(const std::string& name, unsigned int id);
	~InvisibleWall();

	void Initialize();

	GameObject::Type	GetType()const;
};

#endif		// !INVISIBLEWALL_H_
