#ifndef SWITCH_H_
#define SWITCH_H_

#include	"Game/GameObject.h"

/**
* \brief	Class for switches, that can trigger other objects
*			Contains the following modules :
*				- SpritSheetAnimationModule
*				- TransformModule
*				- BoxCollisionModule
*				- PhysicModule
*				- SwitchLogicModule
*				- SoundModule
*/
class Switch : public GameObject
{
public:
	Switch(const std::string& name, unsigned int id);
	~Switch();

	void	Initialize();

	GameObject::Type	GetType()const;

};

#endif
