#ifndef DOOR_H_
#define DOOR_H_

#include	"Game/GameObject.h"

/**
* \brief	Class for doors
*			Doors are blocking elements that can be opened with a trigger event
*			Contains the following modules :
*				- SpriteSheetAnimationModule
*				- TransformModule
*				- BoxCollisionModule
*				- PhysicModule
*				- DoorLogicModule
*				- SoundModule
*/
class Door : public GameObject
{
public:
	Door(const std::string& name, unsigned int id);
	~Door();

	void	Initialize();

	GameObject::Type	GetType()const;

};

#endif
