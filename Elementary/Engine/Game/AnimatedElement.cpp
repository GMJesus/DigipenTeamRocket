#include "Game/AnimatedElement.h"

AnimatedElement::AnimatedElement(const std::string& name, unsigned int id) : GameObject(name, id)
{

}

AnimatedElement::~AnimatedElement()
{

}

void				AnimatedElement::Initialize()
{

}

GameObject::Type	AnimatedElement::GetType()const{ return GameObject::Type::E_AnimatedElement; }
