#include "Game/InteractivePlatform.h"

InteractivePlatform::InteractivePlatform(const std::string& name, unsigned int id) : GameObject(name, id)
{

}

InteractivePlatform::~InteractivePlatform()
{

}

void				InteractivePlatform::Initialize()
{

}

GameObject::Type	InteractivePlatform::GetType()const{ return GameObject::Type::E_InteractivePlatform; }
