#ifndef INTERACTIVEPLATFORM_H_
#define INTERACTIVEPLATFORM_H_

#include	"Game/GameObject.h"

/**
* \brief	Class for Platform that can will have an interactive behaviour
*			Contains the following modules:
*				- BoxCollisionModule
*				- SoundModule
*				- TransformModule
*				- SpriteSheetAnimationModule
*				- StateModule
*				- InteractivePlatformLogicModule
*/
class InteractivePlatform : public GameObject
{
public:
	InteractivePlatform(const std::string& name, unsigned int id);
	~InteractivePlatform();

	void	Initialize();

	GameObject::Type	GetType()const;

};

#endif
