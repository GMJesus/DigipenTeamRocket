#include "GameObject.h"
#include "Modules\ModuleFactory.h"
#include "Graphics/TexturePlane.h"
#include "Graphics/GraphicEngine.h"
#include "Graphics/Texture.h"
#include "Modules/BoxCollisionModule.h"
#include "Modules/TransformModule.h"

GameObject::GameObject(const std::string& name, unsigned int id)
{
	_name = name;

	//@ModuleCreator ModuleInitialization
	stateModule = NULL;
	aIModule = NULL;
	boxCollisionModule = NULL;
	displayModule = NULL;
	lifeModule = NULL;
	logicModule = NULL;
	physicModule = NULL;
	soundModule = NULL;
	stateModule = NULL;
	transformModule = NULL;
	_id = id;
	_isDead = false;
	_dontDestroyOnLoad = false;
	_horizontalDebug = NULL;
	_verticalDebug = NULL;
}

GameObject::~GameObject()
{

}

void				GameObject::SetDontDestroyOnLoad(bool b){ _dontDestroyOnLoad = b; }
void				GameObject::SetIsDead(bool b){ _isDead = b; }
void				GameObject::SetName(std::string name) { _name = name; }

unsigned int		GameObject::Id(){ return _id; }
bool				GameObject::IsDead()const{ return _isDead; }
bool				GameObject::DontDestroyOnLoad()const{ return _dontDestroyOnLoad; }
const std::string&	GameObject::GetName()const{ return _name; }

void				GameObject::UpdateDebugHeight()
{
	if (!_horizontalDebug || !_verticalDebug)
	{
		_horizontalDebug = new Graphic::TexturePlane;
		_verticalDebug = new Graphic::TexturePlane;
#ifdef USEDIRECT3D
		_horizontalDebug->Initialize(DirectXInterface::Instance().GetDevice());
		_verticalDebug->Initialize(DirectXInterface::Instance().GetDevice());
#endif
	}
	int height = boxCollisionModule ? boxCollisionModule->GetBoxHeight() * PPU * (transformModule ? transformModule->Scale() : 1) : 10;
	_verticalDebug->SetWidth(1);
	_verticalDebug->SetHeight(height);
	Graphic::Texture* tex = new Graphic::Texture();
#ifdef USEDIRECT3D
	tex->Initialize(DirectXInterface::Instance().GetDevice(), Graphic::CreateBorder(DirectXInterface::Instance().GetDevice(), 1, height));
#endif
	_verticalDebug->SetTexture(tex);
}

void				GameObject::UpdateDebugWidth()
{
	if (!_horizontalDebug || !_verticalDebug)
	{
		_horizontalDebug = new Graphic::TexturePlane;
		_verticalDebug = new Graphic::TexturePlane;
#ifdef USEDIRECT3D
		_horizontalDebug->Initialize(DirectXInterface::Instance().GetDevice());
		_verticalDebug->Initialize(DirectXInterface::Instance().GetDevice());
#endif
	}
	int width = boxCollisionModule ? boxCollisionModule->GetBoxWidth() * PPU * (transformModule ? transformModule->Scale() : 1) : 10;
	_horizontalDebug->SetWidth(width);
	_horizontalDebug->SetHeight(1);
	Graphic::Texture* tex = new Graphic::Texture();
#ifdef USEDIRECT3D
	tex->Initialize(DirectXInterface::Instance().GetDevice(), Graphic::CreateBorder(DirectXInterface::Instance().GetDevice(), width, 1));
#endif
	_horizontalDebug->SetTexture(tex);
}

void				GameObject::DrawDebug()
{
	if (!transformModule)
		return;
	if (!_horizontalDebug || !_verticalDebug)
	{
		_horizontalDebug = new Graphic::TexturePlane;
		_verticalDebug = new Graphic::TexturePlane;
#ifdef USEDIRECT3D
		_horizontalDebug->Initialize(DirectXInterface::Instance().GetDevice());
		_verticalDebug->Initialize(DirectXInterface::Instance().GetDevice());
#endif
		UpdateDebugWidth();
		UpdateDebugHeight();
	}
	float posX = transformModule->Position().x;
	float posY = transformModule->Position().y;
#ifdef USEDIRECT3D
	_horizontalDebug->Render(DirectXInterface::Instance().GetDeviceContext(), posX, posY - (_verticalDebug->GetHeight() / PPU) / 2.f, 0.f);
	_horizontalDebug->Render(DirectXInterface::Instance().GetDeviceContext(), posX, posY + (_verticalDebug->GetHeight() / PPU) / 2.f, 0.f);
	_verticalDebug->Render(DirectXInterface::Instance().GetDeviceContext(), posX - (_horizontalDebug->GetWidth() / PPU) / 2.f, posY, 0.f);
	_verticalDebug->Render(DirectXInterface::Instance().GetDeviceContext(), posX + (_horizontalDebug->GetWidth() / PPU) / 2.f, posY, 0.f);
#endif
}
