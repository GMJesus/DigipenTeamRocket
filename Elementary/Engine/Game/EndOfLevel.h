#ifndef ENDOFLEVEL_H_
#define ENDOFLEVEL_H_

#include	"Game/GameObject.h"

/**
* \brief	Class for object that will activate the end of the level once triggered
*			Contains the following modules :
*				- TransformModule
*				- BoxCollisionModule
*				- EndLevelLogicModule
*/
class EndOfLevel : public GameObject
{
public:
	EndOfLevel(const std::string& name, unsigned int id);
	~EndOfLevel();

	void	Initialize();

	GameObject::Type	GetType()const;

};

#endif
