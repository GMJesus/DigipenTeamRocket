/**
* \author	Aymeric Lambolez
*/

#include	"Game\GameObjectFactory.h"
#ifdef GAME
#include	"GameEngine\GameEngine.h"
#else
#include	"EditorEngine\EditorEngine.h"
#endif

GameObjectFactory*	GameObjectFactory::p_inst = NULL;

GameObjectFactory::GameObjectFactory()
{
	_totalGameObjectCount = 0;
	_currentGameObjectCount = 0;
	_deletedGameObjectCount = 0;
	_nextId = 0;

	//GameObjectName Initialization
	_objectClassArray = new GOCreationFPtr[GameObject::GAMEOBJECT_CLASS_NUMBER];
	//@ClassCreator FuncArrayObjectCreationStart
	_objectClassArray[GameObject::Type::E_InteractivePlatform] = &GameObjectFactory::CreateInteractivePlatform;
	_objectClassArray[GameObject::Type::E_EndOfLevel] = &GameObjectFactory::CreateEndOfLevel;
	_objectClassArray[GameObject::Type::E_Checkpoint] = &GameObjectFactory::CreateCheckpoint;
	_objectClassArray[GameObject::Type::E_Door] = &GameObjectFactory::CreateDoor;
	_objectClassArray[GameObject::Type::E_Switch] = &GameObjectFactory::CreateSwitch;
	_objectClassArray[GameObject::Type::E_BackAndForthObject] = &GameObjectFactory::CreateBackAndForthObject;
	_objectClassArray[GameObject::Type::E_ElementalInteractiveObject] = &GameObjectFactory::CreateElementalInteractiveObject;
	_objectClassArray[GameObject::Type::E_AnimatedElement] = &GameObjectFactory::CreateAnimatedElement;
	_objectClassArray[GameObject::Type::E_Gun] = &GameObjectFactory::CreateGun;
	_objectClassArray[GameObject::Type::E_Projectile] = &GameObjectFactory::CreateProjectile;
	_objectClassArray[GameObject::Type::E_StaticObject] = &GameObjectFactory::CreateStaticObject;
	_objectClassArray[GameObject::Type::E_Character] = &GameObjectFactory::CreateCharacter;
	_objectClassArray[GameObject::Type::E_ParticleSystem] = &GameObjectFactory::CreateParticleSystem;
	_objectClassArray[GameObject::Type::E_Player] = &GameObjectFactory::CreatePlayer;
	_objectClassArray[GameObject::Type::E_Actor] = &GameObjectFactory::CreateActor;
	_objectClassArray[GameObject::Type::E_BackgroundElement] = &GameObjectFactory::CreateBackgroundElement;
	_objectClassArray[GameObject::Type::E_Decorative_Element] = &GameObjectFactory::CreateDecorativeElement;
	_objectClassArray[GameObject::Type::E_InvisibleWall] = &GameObjectFactory::CreateInvisibleWall;
	_player = NULL;
}

GameObjectFactory::~GameObjectFactory()
{

}

GameObjectFactory*	GameObjectFactory::Inst()
{
	if (!p_inst)
		p_inst = new GameObjectFactory();
	return p_inst;
}


void				GameObjectFactory::ResetMap(bool force)
{
	//Delete all objects that were stored to delete
	while (_toDelete.size())
	{
		ClearObject(_toDelete.front());
		delete(_toDelete.front());
		++_deletedGameObjectCount;
		--_currentGameObjectCount;
		_toDelete.pop_front();
	}

	std::map<unsigned int, GameObject*>::iterator it = _objectMap.begin();
	//Delete all objects remaining
	while (it != _objectMap.end())
	{
 		if (!force && it->second->DontDestroyOnLoad()) //Skip this object if it should not be deleted
			++it;
		else
		{
			it = DeleteObject(it, force);
		}
	}
	//Clear this map, because the object in it has been destroyed
	_objectNoTransformMap.clear();
}

void											GameObjectFactory::ClearObject(GameObject* object)const
{
	if (object->aIModule)
	{
		object->aIModule->Destroy();
		delete(object->aIModule);
	}
	if (object->boxCollisionModule)
	{
		object->boxCollisionModule->Destroy();
		delete(object->boxCollisionModule);
	}
	if (object->lifeModule)
	{
		object->lifeModule->Destroy();
		delete(object->lifeModule);
	}
	if (object->logicModule)
	{
		object->logicModule->Destroy();
		delete(object->logicModule);
	}
	if (object->physicModule)
	{
		object->physicModule->Destroy();
		delete(object->physicModule);
	}
	if (object->soundModule)
	{
		object->soundModule->Destroy();
		delete(object->soundModule);
	}
	if (object->displayModule)
	{
		object->displayModule->Destroy();
		delete(object->displayModule);
	}
	if (object->transformModule)
	{
		object->transformModule->Destroy();
		delete(object->transformModule);
	}
}

std::map<unsigned int, GameObject*>::iterator	GameObjectFactory::DeleteObject(std::map<unsigned int, GameObject*>::iterator it, bool deleteNow)
{
	std::map<unsigned int, GameObject*>::iterator toDelete = it;
	++it;

	if (toDelete->second->transformModule)
	{
		//Delete children of this object if any
		std::list<GameObject*> children = toDelete->second->transformModule->GetChilds();
		for (std::list<GameObject*>::iterator it = children.begin(); it != children.end(); ++it)
		{
			(*it)->transformModule->ClearParent();
			if (!deleteNow)
				DeleteObject((*it)->Id());
		}

		//Remove the deleted object from the children list of the parent, if any
		GameObject* parent;
		if ((parent = toDelete->second->transformModule->GetParent()))
			parent->transformModule->RemoveChild(toDelete->second);
	}

	if (_player == toDelete->second)
		_player = NULL;
	if (deleteNow) //Delete the object directly
	{
		ClearObject(toDelete->second);
		delete(toDelete->second);
		++_deletedGameObjectCount;
		--_currentGameObjectCount;
		_objectMap.erase(toDelete);
	}
	else //Push the object in the list of object to delete at the end of the frame
	{
		toDelete->second->SetIsDead(true);
		_toDelete.push_back(toDelete->second);
		_objectMap.erase(toDelete);
	}

	return it;
}

void			GameObjectFactory::DeleteObject(unsigned int objectId, bool deleteNow)
{
	std::map<unsigned int, GameObject*>::iterator it = _objectMap.find(objectId);
	if (it != _objectMap.end())
	{
		LogWriter::Instance().addLogMessage("Deleting " + it->second->GetName() + " of Id : " + std::to_string(objectId), E_MsgType::Log);
		DeleteObject(it, deleteNow);
	}
	else
	{
		LogWriter::Instance().addLogMessage("Trying to delete non-existing object (given ID : " + std::to_string(objectId) + ")", E_MsgType::Warning);
	}
}

void			GameObjectFactory::Update()
{
	//Delete all objects pushed in the list of object to delete this frame
	while (_toDelete.size())
	{
		ClearObject(_toDelete.front());
		delete(_toDelete.front());
		++_deletedGameObjectCount;
		--_currentGameObjectCount;
		_toDelete.pop_front();
	}
}

const std::map<unsigned int, GameObject*>&	GameObjectFactory::GetObjectMap()const
{
	return _objectMap;
}

const std::map<unsigned int, GameObject*>&	GameObjectFactory::GetNoTransformObjectMap()const
{
	return _objectNoTransformMap;
}

GameObject*									GameObjectFactory::GetObjectById(unsigned int id)const
{
	std::map<unsigned int, GameObject*>::const_iterator it = _objectMap.find(id);
	if (it != _objectMap.end())
		return it->second;
	return NULL;
}

GameObject*									GameObjectFactory::GetObjectByName(const std::string &name)const
{
	std::map<unsigned int, GameObject*>::const_iterator it;

	for (it = _objectMap.begin(); it != _objectMap.end(); ++it)
	{
		if (it->second->GetName() == name)
			return it->second;
	}
	return NULL;
}


Character*									GameObjectFactory::GetPlayer()const
{
	return _player;
}

GameObject*			GameObjectFactory::CreateGameObject(GameObject::Type type, const std::string& objectName)
{
	GameObject*		newObject = NULL;
	try
	{
		int objectId = _nextId;
		++_nextId; //Increase the next ID now in case of this object creating a new one in his modules initialization
		newObject = (this->*_objectClassArray[type])(objectId, objectName == "Name" ? "Object" + std::to_string(objectId) : objectName);
		_objectMap[objectId] = newObject;
		if (!newObject->transformModule) //If this object does not have a transformModule, push it into the appropriate map so we can get it without Quadtrees
			_objectNoTransformMap[objectId] = newObject;
		newObject->Initialize();
		++_currentGameObjectCount;
		++_totalGameObjectCount;
	}
	catch (std::out_of_range &e)
	{
		LogWriter::Instance().addLogMessage("Trying to create non-existing class instance", E_MsgType::Warning);
	}
	return newObject;
}

GameObject*		GameObjectFactory::SpawnGameObject(GameObject::Type type, const std::string& objectName, const Vec2& pos, float width, float height)
{
	Collision::AABB*	aabb = new Collision::AABB();
	aabb->Initialize(pos, width, height);
#ifdef GAME
	if (!GameEngine::GetActualQuadtree()->QueryRange(aabb, true)) //If there is no object at that position
	{
		return CreateGameObject(type, objectName);
	}
#else
	if (!EditorEngine::Engine::GetQuadtree()->QueryRange(aabb, true)) //If there is no object at that position
	{
		return CreateGameObject(type, objectName);
	}
#endif
	delete aabb;
	return NULL;
}

void			GameObjectFactory::Shutdown()
{
	LogWriter::Instance().addLogMessage("[SUMMARY] Objects Remaining : " + std::to_string(_currentGameObjectCount), E_MsgType::Log);
	ResetMap(true);
	LogWriter::Instance().addLogMessage("[SUMMARY] Total Object Created : " + std::to_string(_totalGameObjectCount), E_MsgType::Log);
	LogWriter::Instance().addLogMessage("[SUMMARY] Total Object Deleted : " + std::to_string(_deletedGameObjectCount), E_MsgType::Log);
}

GameObject*			GameObjectFactory::CreateBackgroundElement(unsigned int objectId, const std::string& objectName)
{
	LogWriter::Instance().addLogMessage("Creating Background Element object");
	GameObject* newObject = new BackgroundElement(objectName, objectId);
	ModuleFactory::Inst()->CreateModule(Module::E_SpriteModule, newObject);
	return newObject;
}

GameObject*			GameObjectFactory::CreateDecorativeElement(unsigned int objectId, const std::string& objectName)
{
	LogWriter::Instance().addLogMessage("Creating Decorative Element object");
	GameObject* newObject = new DecorativeElement(objectName, objectId);
	ModuleFactory::Inst()->CreateModule(Module::E_SpriteModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_TransformModule, newObject);
	return newObject;
}

GameObject*			GameObjectFactory::CreateInvisibleWall(unsigned int objectId, const std::string& objectName)
{
	LogWriter::Instance().addLogMessage("Creating Invisible Wall object");
	GameObject* newObject = new InvisibleWall(objectName, objectId);
	ModuleFactory::Inst()->CreateModule(Module::E_TransformModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_BoxCollisionModule, newObject);
	return newObject;
}

GameObject*			GameObjectFactory::CreateActor(unsigned int objectId, const std::string& objectName)
{
	LogWriter::Instance().addLogMessage("Creating Actor object " + objectName);
	GameObject* newObject = new Actor(objectName, objectId);
	ModuleFactory::Inst()->CreateModule(Module::E_SpriteModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_TransformModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_BoxCollisionModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_PhysicModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_LogicModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_StateModule, newObject);
	return newObject;
}

GameObject*			GameObjectFactory::CreatePlayer(unsigned int objectId, const std::string& objectName)
{
	LogWriter::Instance().addLogMessage("Creating Player object");
	GameObject* newObject = new Player(objectName, objectId);
	ModuleFactory::Inst()->CreateModule(Module::E_AnimatedSpriteModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_BoxCollisionModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_LifeModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_LogicModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_PhysicModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_TransformModule, newObject);
	return newObject;
}

GameObject*			GameObjectFactory::CreateParticleSystem(unsigned int objectId, const std::string& objectName)
{
	LogWriter::Instance().addLogMessage("Creating ParticleSystem object");
	GameObject* newObject = new ParticleSystem(objectName, objectId);
	ModuleFactory::Inst()->CreateModule(Module::E_TransformModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_ParticleSystemModule, newObject);	
	return newObject;
}

GameObject*			GameObjectFactory::CreateCharacter(unsigned int objectId, const std::string& objectName)
{
	LogWriter::Instance().addLogMessage("Creating Character object");
	GameObject* newObject = new Character(objectName, objectId);
	ModuleFactory::Inst()->CreateModule(Module::E_SpriteSheetAnimationModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_BoxCollisionModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_LifeModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_PhysicModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_TransformModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_CharacterLogicModule, newObject);
	if (!_player)
		_player = static_cast<Character*>(newObject);
	else
		LogWriter::Instance().addLogMessage("Trying to create a new player while another was existing", E_MsgType::Warning);
	newObject->SetDontDestroyOnLoad(true);
	return newObject;
}

GameObject*			GameObjectFactory::CreateStaticObject(unsigned int objectId, const std::string& objectName)
{
	LogWriter::Instance().addLogMessage("Creating StaticObject object");
	GameObject* newObject = new StaticObject(objectName, objectId);
	ModuleFactory::Inst()->CreateModule(Module::E_SpriteModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_BoxCollisionModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_PhysicModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_TransformModule, newObject);
	return newObject;
}

GameObject*			GameObjectFactory::CreateProjectile(unsigned int objectId, const std::string& objectName)
{
	LogWriter::Instance().addLogMessage("Creating Projectile object");
	GameObject* newObject = new Projectile(objectName, objectId);
	ModuleFactory::Inst()->CreateModule(Module::E_SoundModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_BoxCollisionModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_TransformModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_SpriteSheetAnimationModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_ProjLogicModule, newObject);
	return newObject;
}

GameObject*			GameObjectFactory::CreateGun(unsigned int objectId, const std::string& objectName)
{
	LogWriter::Instance().addLogMessage("Creating Gun object");
	GameObject* newObject = new Gun(objectName, objectId);
	ModuleFactory::Inst()->CreateModule(Module::E_SpriteModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_SoundModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_TransformModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_GunLogicModule, newObject);
	return newObject;
}

	GameObject*			GameObjectFactory::CreateElementalInteractiveObject(unsigned int objectId, const std::string& objectName)
{
		LogWriter::Instance().addLogMessage("Creating ElementalInteractiveObject object");
		GameObject* newObject = new ElementalInteractiveObject(objectName, objectId);
		ModuleFactory::Inst()->CreateModule(Module::E_BoxCollisionModule, newObject);
		ModuleFactory::Inst()->CreateModule(Module::E_LogicModule, newObject);
		ModuleFactory::Inst()->CreateModule(Module::E_PhysicModule, newObject);
		ModuleFactory::Inst()->CreateModule(Module::E_SoundModule, newObject);
		ModuleFactory::Inst()->CreateModule(Module::E_TransformModule, newObject);
		ModuleFactory::Inst()->CreateModule(Module::E_SpriteSheetAnimationModule, newObject);
		ModuleFactory::Inst()->CreateModule(Module::E_StateModule, newObject);
		return newObject;
	}

GameObject*			GameObjectFactory::CreateAnimatedElement(unsigned int objectId, const std::string& objectName)
{
	LogWriter::Instance().addLogMessage("Creating AnimatedElement object");
	GameObject* newObject = new AnimatedElement(objectName, objectId);
	ModuleFactory::Inst()->CreateModule(Module::E_AnimatedSpriteModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_TransformModule, newObject);
	return newObject;
}

GameObject*			GameObjectFactory::CreateBackAndForthObject(unsigned int objectId, const std::string& objectName)
{
	LogWriter::Instance().addLogMessage("Creating BackAndForthObject object");
	GameObject* newObject = new BackAndForthObject(objectName, objectId);
	ModuleFactory::Inst()->CreateModule(Module::E_BoxCollisionModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_PhysicModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_SoundModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_TransformModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_SpriteSheetAnimationModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_BackAndForthBehaviourModule, newObject);
	return newObject;
}

GameObject*			GameObjectFactory::CreateSwitch(unsigned int objectId, const std::string& objectName)
{
	LogWriter::Instance().addLogMessage("Creating Switch object");
	GameObject* newObject = new Switch(objectName, objectId);
	ModuleFactory::Inst()->CreateModule(Module::E_BoxCollisionModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_PhysicModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_SoundModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_TransformModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_SpriteSheetAnimationModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_SwitchLogicModule, newObject);
	return newObject;
}

GameObject*			GameObjectFactory::CreateDoor(unsigned int objectId, const std::string& objectName)
{
	LogWriter::Instance().addLogMessage("Creating Door object");
	GameObject* newObject = new Door(objectName, objectId);
	ModuleFactory::Inst()->CreateModule(Module::E_BoxCollisionModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_PhysicModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_SoundModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_TransformModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_SpriteSheetAnimationModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_DoorLogicModule, newObject);
	return newObject;
}

GameObject*			GameObjectFactory::CreateCheckpoint(unsigned int objectId, const std::string& objectName)
{
	LogWriter::Instance().addLogMessage("Creating Checkpoint object");
	GameObject* newObject = new Checkpoint(objectName, objectId);
	ModuleFactory::Inst()->CreateModule(Module::E_BoxCollisionModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_SoundModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_TransformModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_SpriteSheetAnimationModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_CheckpointLogicModule, newObject);
	return newObject;
}

GameObject*			GameObjectFactory::CreateEndOfLevel(unsigned int objectId, const std::string& objectName)
{
	LogWriter::Instance().addLogMessage("Creating EndOfLevel object");
	GameObject* newObject = new EndOfLevel(objectName, objectId);
	ModuleFactory::Inst()->CreateModule(Module::E_BoxCollisionModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_TransformModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_EndLevelLogicModule, newObject);
	return newObject;
}

GameObject*			GameObjectFactory::CreateInteractivePlatform(unsigned int objectId, const std::string& objectName)
{
	LogWriter::Instance().addLogMessage("Creating InteractivePlatform object");
	GameObject* newObject = new InteractivePlatform(objectName, objectId);
	ModuleFactory::Inst()->CreateModule(Module::E_BoxCollisionModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_SoundModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_TransformModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_SpriteSheetAnimationModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_StateModule, newObject);
	ModuleFactory::Inst()->CreateModule(Module::E_InteractivePlatformLogicModule, newObject);
	return newObject;
}
