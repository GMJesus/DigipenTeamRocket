#ifndef STATICOBJECT_H_
#define STATICOBJECT_H_

#include	"Game/GameObject.h"

/**
* \brief	Class for static objects, such as walls and floor
*			Contains the following modules :
*				- SpriteModule
*				- TransformModule
*				- BoxCollisionModule
*				- PhysicModule
*/
class StaticObject : public GameObject
{
public:
	StaticObject(const std::string& name, unsigned int id);
	~StaticObject();

	void	Initialize();

	GameObject::Type	GetType()const;

};

#endif
