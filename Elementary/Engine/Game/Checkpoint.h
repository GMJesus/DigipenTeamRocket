#ifndef CHECKPOINT_H_
#define CHECKPOINT_H_

#include	"Game/GameObject.h"

/**
* \brief	Class for checkpoints
*			Checkpoints allow the player to respawn at their location
*			Contains the following modules :
*				- SpriteSheetAnimationModule
*				- TransformModule
*				- BoxCollisionModule
*				- CheckpointLogicModule
*				- SoundModule
*/
class Checkpoint : public GameObject
{
public:
	Checkpoint(const std::string& name, unsigned int id);
	~Checkpoint();

	void	Initialize();

	GameObject::Type	GetType()const;

};

#endif
