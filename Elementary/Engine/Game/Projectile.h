#ifndef PROJECTILE_H_
#define PROJECTILE_H_

#include	"Game/GameObject.h"

/**
* \brief	Class for projectiles
*			Contains the following modules :
*				- SpriteSheetAnimationModule
*				- TransformModule
*				- BoxCollisionModule
*				- SoundModule
*				- ProjLogicModule
*/
class Projectile : public GameObject
{
public:
	Projectile(const std::string& name, unsigned int id);
	~Projectile();

	void	Initialize();

	GameObject::Type	GetType()const;

};

#endif
