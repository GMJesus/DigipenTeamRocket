#include "Game/ElementalInteractiveObject.h"

ElementalInteractiveObject::ElementalInteractiveObject(const std::string& name, unsigned int id) : GameObject(name, id)
{

}

ElementalInteractiveObject::~ElementalInteractiveObject()
{

}

void				ElementalInteractiveObject::Initialize()
{

}

GameObject::Type	ElementalInteractiveObject::GetType()const{ return GameObject::Type::E_ElementalInteractiveObject; }
