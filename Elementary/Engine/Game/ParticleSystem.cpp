#include "Game/ParticleSystem.h"

ParticleSystem::ParticleSystem(const std::string& name, unsigned int id) : GameObject(name, id)
{

}

ParticleSystem::~ParticleSystem()
{

}

void				ParticleSystem::Initialize()
{

}

GameObject::Type	ParticleSystem::GetType()const{ return GameObject::Type::E_ParticleSystem; }
