#ifndef PARTICLESYSTEM_H_
#define PARTICLESYSTEM_H_

#include	"Game/GameObject.h"

/**
* \brief	Class for particle emitter
*			Contains the following modules :
*				- ParticleSystemModule
*				- TransformModule
*/
class ParticleSystem : public GameObject
{
public:
	ParticleSystem(const std::string& name, unsigned int id);
	~ParticleSystem();

	void	Initialize();

	GameObject::Type	GetType()const;

};

#endif
