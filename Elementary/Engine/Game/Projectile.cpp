#include "Game/Projectile.h"

Projectile::Projectile(const std::string& name, unsigned int id) : GameObject(name, id)
{

}

Projectile::~Projectile()
{

}

void				Projectile::Initialize()
{

}

GameObject::Type	Projectile::GetType()const{ return GameObject::Type::E_Projectile; }
