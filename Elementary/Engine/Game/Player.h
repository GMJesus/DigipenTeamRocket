#ifndef PLAYER_H_
#define PLAYER_H_

#include	"Game/GameObject.h"

/**
* \brief	to delete
*/
class Player : public GameObject
{
public:
	Player(const std::string& name, unsigned int id);
	~Player();

	void	Initialize();

	GameObject::Type	GetType()const;

};

#endif
