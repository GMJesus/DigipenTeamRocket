#ifndef ANIMATEDELEMENT_H_
#define ANIMATEDELEMENT_H_

#include	"Game/GameObject.h"

/**
* \brief	Animated GameObject
*				Contains :
*					- AnimatedSpriteModule
*					- TransformModule
*/
class AnimatedElement : public GameObject
{
public:
	AnimatedElement(const std::string& name, unsigned int id);
	~AnimatedElement();

	void	Initialize();

	GameObject::Type	GetType()const;

};

#endif
