#include "Game/BackAndForthObject.h"

BackAndForthObject::BackAndForthObject(const std::string& name, unsigned int id) : GameObject(name, id)
{

}

BackAndForthObject::~BackAndForthObject()
{

}

void				BackAndForthObject::Initialize()
{

}

GameObject::Type	BackAndForthObject::GetType()const{ return GameObject::Type::E_BackAndForthObject; }
