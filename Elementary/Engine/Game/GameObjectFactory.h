/**
* \author	Aymeric Lambolez
*/

#ifndef		GAMEOBJECTFACTORY_H_
# define	GAMEOBJECTFACTORY_H_

#include	<map>
#include	<list>
#include	"Game\GameObjectInclude.h"
#include	"Graphics\TextureLoader.h"
#include	"Modules\ModuleImport.h"
#include	"Utils\LogWriter.h"

/**
* \brief	Class reponsible of the management of GameObjects
*			It encapsulate GameObject creation, deletion, and access
*/
class GameObjectFactory
{
private:
	typedef GameObject*	(GameObjectFactory::*GOCreationFPtr)(unsigned int, const std::string &);

	GameObjectFactory();
	~GameObjectFactory();

	/**
	* \brief	Pointer to an instance of this class
	*/
	static GameObjectFactory*	p_inst;

	unsigned int				_totalGameObjectCount;
	unsigned int				_deletedGameObjectCount;
	unsigned int				_currentGameObjectCount;
	unsigned int				_nextId;

	/**
	* \brief	The map of objects in the current level
	*/
	std::map<unsigned int, GameObject*>		_objectMap;
	/**
	* \brief	The map of objects without TransformModule in the current level
	*/
	std::map<unsigned int, GameObject*>		_objectNoTransformMap;
	/**
	* \brief	Array of pointer to member function for class instancing
	*/
	GOCreationFPtr*							_objectClassArray;
	/**
	* \brief	The list of objects to delete at the end of the frame
	*/
	std::list<GameObject*>					_toDelete;
	/**
	* \brief	The current player
	*/
	Character*								_player;

	/**
	* \brief	Manage the internal object deletion
	* \param	it			Iterator to the element to delete
	* \param	deleteNow	Should this object be deleted now ? If false, will be deleted at the end of the frame
	* \return	An iterator to the next element of the map
	*/
	std::map<unsigned int, GameObject*>::iterator	DeleteObject(std::map<unsigned int, GameObject*>::iterator it, bool deleteNow);


	/**
	* \brief	Clear an object, shutting down every modules in it
	* \param	object The object to clear
	*/
	void											ClearObject(GameObject* object)const;

	//GameObject Creation Function
	//@ClassCreator ObjectFunctionCreationStart
	GameObject*					CreateInteractivePlatform(unsigned int objectId, const std::string& objectName);
	GameObject*					CreateEndOfLevel(unsigned int objectId, const std::string& objectName);
	GameObject*					CreateCheckpoint(unsigned int objectId, const std::string& objectName);
	GameObject*					CreateDoor(unsigned int objectId, const std::string& objectName);
	GameObject*					CreateSwitch(unsigned int objectId, const std::string& objectName);
	GameObject*					CreateBackAndForthObject(unsigned int objectId, const std::string& objectName);
	GameObject*					CreateElementalInteractiveObject(unsigned int objectId, const std::string& objectName);
	GameObject*					CreateAnimatedElement(unsigned int objectId, const std::string& objectName);
	GameObject*					CreateGun(unsigned int objectId, const std::string& objectName);
	GameObject*					CreateProjectile(unsigned int objectId, const std::string& objectName);
	GameObject*					CreateStaticObject(unsigned int objectId, const std::string& objectName);
	GameObject*					CreateCharacter(unsigned int objectId, const std::string& objectName);
	GameObject*					CreateParticleSystem(unsigned int objectId, const std::string& objectName);
	GameObject*					CreatePlayer(unsigned int objectId, const std::string& objectName);
	GameObject*					CreateBackgroundElement(unsigned int objectId, const std::string& objectName);
	GameObject*					CreateDecorativeElement(unsigned int objectId, const std::string& objectName);
	GameObject*					CreateActor(unsigned int objectId, const std::string& objectName);
	GameObject*					CreateInvisibleWall(unsigned int objectId, const std::string& objectName);
	//@ClassCreator ObjectFunctionCreationEnd

public:
	/**
	* \brief	Opere the deletion of objects that where asked to be deleted this frame
	*			This function should the last one call in the GameLoop !
	*/
	void						Update();

	/**
	* \brief	Shutdown this object, clearing all variable and releasing everything needed
	*/
	void						Shutdown();

	/**
	* \brief	Returns a pointer to an instance of that class
	*/
	static GameObjectFactory*	Inst();

	/**
	* \brief	Creates and returns an object of type "type", with the name "objectName"
	* \param	type		The type of the object to create
	* \param	objectName	The name of the object to create
	* \return	A pointer to the newly created GameObject
	*/
	GameObject*					CreateGameObject(GameObject::Type type, const std::string& objectName);

	/**
	* \brief	Checks if the object can be created at the given position.
	*			If so, creates and returns an object of type "type", with the name "objectName".
	*			Also sets the transformModule and BoxCollisionModule to value passed as parameters
	* \param	type		The type of the object to spawn
	* \param	objectName	The name of the object to spawn
	* \param	pos			The position where to spawn the object
	* \param	width		The width of the box collision of this object
	* \param	height		The height of the box collision of this object
	* \return	A pointer to the newly created GameObject
	*/
	GameObject*					SpawnGameObject(GameObject::Type type, const std::string& objectName, const Vec2& pos, float width, float height);

	/**
	* \brief	Resets the objects map, destroying all objects in the level except objects with dontDestroyOnLoad to true
	* 			If force is true, even objects with dontDestroyOnLoad to true will be destroyed
	* 			This function should be use only when changing level, or closing game
	* \param	force	Does this function also destroy objects that shouldn't be destroy ? (objects with dontDestroyOnLoad at true)
	*/
	void						ResetMap(bool force);

	/**
	* \brief	Delete the given object at the end of the frame
	* 			If deleteNow is true, then the object will be directly deleted
	* 			This can lead to unexpected behaviour
	*			Be aware that children objects will also be destroyed
	* \param	objectId	The id of the object to destroy
	* \param	deleteNow	Should this object be deleted now, or at the end of the frame ?
	*/
	void						DeleteObject(unsigned int objectId, bool deleteNow = false);

	/**
	* \brief	Returns all objects in the level, stored in a map of <Id, Object>
	* \return	The current map of object
	*/
	const std::map<unsigned int, GameObject*>&	GetObjectMap()const;

	/**
	* \brief	Returns a map containing all objects that doesn't have a transformModule, in a form <Id, Object>
	* \return	The current map of object without TransformModule
	*/
	const std::map<unsigned int, GameObject*>&	GetNoTransformObjectMap()const;
	
	/**
	* \brief	Gets the object of the given ID
	* \param	id	The ID of the object to return
	* \return	The object corresponding to the ID given in description. Returns NULLif no object correspond to the ID
	*/
	GameObject*									GetObjectById(unsigned int id)const;

	/**
	* \brief	Gets the first object corresponding to the given name
	*			Be Careful when using this function : multiple objects can have the same name
	* \param	name	The name of the object to return
	* \return	The object found. If no object found, returns NULL
	*/
	GameObject*									GetObjectByName(const std::string &name)const;

	/**
	* \brief	Gets the current player
	* \return	A pointer to the current player
	*/
	Character*									GetPlayer()const;
};

#endif		// !GAMEOBJECTFACTORY_H_
