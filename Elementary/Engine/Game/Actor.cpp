#include	"Game\Actor.h"

Actor::Actor(const std::string& name, unsigned int id) : GameObject(name, id)
{

}

Actor::~Actor()
{

}

void			Actor::Initialize()
{

}

GameObject::Type	Actor::GetType()const{ return GameObject::Type::E_Actor; }
