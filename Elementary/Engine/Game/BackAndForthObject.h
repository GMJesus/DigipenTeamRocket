#ifndef BACKANDFORTHOBJECT_H_
#define BACKANDFORTHOBJECT_H_

#include	"Game/GameObject.h"

/**
* \brief	Class for basic AI behaviour.
*			Contains the following modules :
*				- SpriteSheetAnimationModule
*				- TransformModule
*				- BoxCollisionModule
*				- PhysicModule
*				- BackAndForthBehaviourModule
*				- SoundModule
*/
class BackAndForthObject : public GameObject
{
public:
	BackAndForthObject(const std::string& name, unsigned int id);
	~BackAndForthObject();

	void	Initialize();

	GameObject::Type	GetType()const;

};

#endif
