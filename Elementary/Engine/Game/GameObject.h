#ifndef GAMEOBJECT_H_
#define GAMEOBJECT_H_

#include	<string>

//@ModuleCreator ClassDeclaration
class StateModule;
class DisplayModule;
class AIModule;
class BoxCollisionModule;
class PhysicModule;
class TransformModule;
class LifeModule;
class LogicModule;
class SoundModule;
class StateModule;

namespace Graphic{
	class TexturePlane;
}

/**
* \brief	Base classe for GameObjects
*			GameObject contain modules, that contain all the logic needed for the game
*			To implement a specific behaviour for a specific object, create a dedicated LogicModule
*/
class GameObject
{
public:
	GameObject(const std::string& name, unsigned int id);
	~GameObject();

	//@ModuleCreator ModuleVariableDeclaration
	StateModule*			stateModule;
	DisplayModule*			displayModule;
	AIModule*				aIModule;
	BoxCollisionModule*		boxCollisionModule;
	PhysicModule*			physicModule;
	SoundModule*			soundModule;
	TransformModule*		transformModule;
	LifeModule*				lifeModule;
	LogicModule*			logicModule;

	/**
	* \brief	The different types of GameObjects
	*/
	enum Type
	{
		//@ClassCreator ObjectTypeStart
		E_Actor = 0,
		E_BackgroundElement = 1,
		E_Decorative_Element = 2,
		E_InvisibleWall = 3,
		E_Player = 4,
		E_ParticleSystem = 5,
		E_Character = 6,
		E_StaticObject = 7,
		E_Projectile = 8,
		E_Gun = 9,
		E_ElementalInteractiveObject = 10,
		E_AnimatedElement = 11,
		E_BackAndForthObject = 12,
		E_Switch = 13,
		E_Door = 14,
		E_Checkpoint = 15,
		E_EndOfLevel = 16,
		E_InteractivePlatform = 17
		//@ClassCreator ObjectTypeEnd
	};

	//@ClassCreator ObjectClassNumber
	static const int		GAMEOBJECT_CLASS_NUMBER = 18;

	/**
	* \brief	Gets the unique ID of this object
	* \return	The ID of this object
	*/
	unsigned int			Id();
	bool					DontDestroyOnLoad()const;
	bool					IsDead()const;
	const std::string&		GetName()const;
	void					SetDontDestroyOnLoad(bool b);
	void					SetIsDead(bool b);
	void					SetName(std::string name);

	/**
	* \brief	Display a box around the object if this one has a BoxCollisionModule
	*			If the object does not have a BoxCollisionModule, and in the Level Editor project, will display tiny square at the center of the object
	*/
	void					DrawDebug();
	/**
	* \brief	Update the vertical lines of the debug box of this object
	*/
	void					UpdateDebugHeight();
	/**
	* \brief	Update the horizontal lines of the debug box of this object
	*/
	void					UpdateDebugWidth();

	/**
	* \brief	Initialize the object
	*			This function is automatically called by the GameObjectFactory
	*/
	virtual void			Initialize() = 0;
	virtual GameObject::Type	GetType()const = 0;
protected:
	/**
	* \brief	The name of this object
	*			Multiple objects can have the same name, don't use it as identificator
	*/
	std::string				_name;
	/**
	* \brief	Should this object be destroyed on the load of a new level ?
	*/
	bool					_dontDestroyOnLoad;
	/**
	* \brief	Is this object dead ?
	*			An object is considered as dead once his deletion is programmed
	*/
	bool					_isDead;
	/**
	* \brief	The unique id of this object
	*/
	unsigned int			_id;

	/**
	* \brief	Horizontal line used for debug purpose only
	*/
	Graphic::TexturePlane*	_horizontalDebug;
	/**
	* \brief	Vertical line used for debug purpose only
	*/
	Graphic::TexturePlane*	_verticalDebug;
};

#endif // !GAMEOBJECT_H_
