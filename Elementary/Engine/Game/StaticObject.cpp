#include "Game/StaticObject.h"

StaticObject::StaticObject(const std::string& name, unsigned int id) : GameObject(name, id)
{

}

StaticObject::~StaticObject()
{

}

void				StaticObject::Initialize()
{

}

GameObject::Type	StaticObject::GetType()const{ return GameObject::Type::E_StaticObject; }
