#ifndef ELEMENTALINTERACTIVEOBJECT_H_
#define ELEMENTALINTERACTIVEOBJECT_H_

#include	"Game/GameObject.h"

/**
* \brief	Class for element that have logic based on elemental interaction
*			Contains the following modules :
*				- SpriteSheetAnimationModule
*				- TransformModule
*				- BoxCollisionModule
*				- PhysicModule
*				- LogicModule
*				- StateModule
*				- SoundModule
*/
class ElementalInteractiveObject : public GameObject
{
public:
	ElementalInteractiveObject(const std::string& name, unsigned int id);
	~ElementalInteractiveObject();

	void	Initialize();

	GameObject::Type	GetType()const;

};

#endif
