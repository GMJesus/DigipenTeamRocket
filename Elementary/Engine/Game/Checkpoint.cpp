#include "Game/Checkpoint.h"

Checkpoint::Checkpoint(const std::string& name, unsigned int id) : GameObject(name, id)
{

}

Checkpoint::~Checkpoint()
{

}

void				Checkpoint::Initialize()
{

}

GameObject::Type	Checkpoint::GetType()const{ return GameObject::Type::E_Checkpoint; }
