#ifndef CHARACTER_H_
#define CHARACTER_H_

#include	"Game/GameObject.h"

/**
* \brief	Class for the player
*			Contains the following modules :
*				- AnimatedSpriteModule
*				- TransformModule
*				- BoxCollisionModule
*				- PhysicModule
*				- CharacterLogicModule
*				- LifeModule
*/
class Character : public GameObject
{
public:
	Character(const std::string& name, unsigned int id);
	~Character();

	void	Initialize();

	GameObject::Type	GetType()const;

};

#endif
