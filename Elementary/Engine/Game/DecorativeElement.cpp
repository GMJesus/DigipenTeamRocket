#include "Game\DecorativeElement.h"

DecorativeElement::DecorativeElement(const std::string& name, unsigned int id) : GameObject(name, id)
{

}

void	DecorativeElement::Initialize()
{
}

GameObject::Type	DecorativeElement::GetType()const{ return GameObject::Type::E_Decorative_Element; }
