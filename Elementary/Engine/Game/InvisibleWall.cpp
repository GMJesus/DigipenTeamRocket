#include "Game/InvisibleWall.h"

InvisibleWall::InvisibleWall(const std::string& name, unsigned int id) : GameObject(name, id)
{

}

InvisibleWall::~InvisibleWall()
{

}

void			InvisibleWall::Initialize()
{

}

GameObject::Type	InvisibleWall::GetType()const{ return GameObject::Type::E_InvisibleWall; }
