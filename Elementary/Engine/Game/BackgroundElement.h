#ifndef		BACKGROUNDELEMENT_H
# define	BACKGROUNDELEMENT_H

#include "Game/GameObject.h"
#include "Modules\ModuleFactory.h"
#include "Modules\ModuleImport.h"
#include "Graphics\TextureLoader.h"

/**
* \brief	Class Background Element, that does not have any position
*			Contains the following modules :
*				- SpriteModule
*/
class BackgroundElement : public GameObject
{
public:
	BackgroundElement(const std::string& name, unsigned int id);
	~BackgroundElement();

	void	Initialize();

	GameObject::Type	GetType()const;
};

#endif		// !BACKGROUNDELEMENT_H

