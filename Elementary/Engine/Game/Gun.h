#ifndef GUN_H_
#define GUN_H_

#include	"Game/GameObject.h"

/**
* \brief	Class for the player gun
*			Contains the following modules :
*				- SpriteModule
*				- TransformModule
*				- GunLogicModule
*				- SoundModule
*/
class Gun : public GameObject
{
public:
	Gun(const std::string& name, unsigned int id);
	~Gun();

	void	Initialize();

	GameObject::Type	GetType()const;

};

#endif
