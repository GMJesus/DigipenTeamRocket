#ifndef		ACTOR_H_
# define	ACTOR_H_

#include	"Game/GameObject.h"

/**
* \brief	Class for basic actors.
*			Contains the following modules :
*				- SpriteModule
*				- TransformModule
*				- BoxCollisionModule
*				- PhysicModule
*				- LogicModule
*				- StateModule
*/
class Actor : public GameObject
{
public:
	Actor(const std::string& name, unsigned int id);
	~Actor();

	void	Initialize();

	GameObject::Type	GetType()const;
};

#endif		// !ACTOR_H_
