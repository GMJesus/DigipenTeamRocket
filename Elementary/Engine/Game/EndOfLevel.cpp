#include "Game/EndOfLevel.h"

EndOfLevel::EndOfLevel(const std::string& name, unsigned int id) : GameObject(name, id)
{

}

EndOfLevel::~EndOfLevel()
{

}

void				EndOfLevel::Initialize()
{

}

GameObject::Type	EndOfLevel::GetType()const{ return GameObject::Type::E_EndOfLevel; }
