/**
* \author	Aymeric Lambolez
*/
#include	"Modules/SpriteSheetAnimationModule.h"

SpriteSheetAnimationModule::SpriteSheetAnimationModule(GameObject* owner) : DisplayModule(owner)
{

}

SpriteSheetAnimationModule::~SpriteSheetAnimationModule()
{

}

void				SpriteSheetAnimationModule::Initialize()
{
#ifdef USEDIRECT3D
	_texturePlane.Initialize(DirectXInterface::Instance().GetDevice());
#endif
	_spriteWidth = 0;
	_spriteHeight = 0;
	_x = 0;
	_y = 0;
	_totalColumn = 1;
	_totalRaw = 1;
	_duration = 0.f;
	_frameNumber = 0;
	_spriteSheetHeight = 0;
	_spriteSheetWidth = 0;
	_currentFrameTime = 0;
	_currentFrame = 0;
	_previousFrame = -1;
	_timeBetweenFrame = 0.f;
	_location = D3DXMATRIX(
		1.f, 0.f, 0.f, 0.f,
		0.f, 1.f, 0.f, 0.f,
		0.f, 0.f, 1.f, 0.f,
		0.f, 0.f, 0.f, 1.f
		);
	ChangeFrame(0);
	_spriteSheetPath = NULL;
}

void				SpriteSheetAnimationModule::Update(float deltaTime)
{
	DisplayModule::Update(deltaTime);
	if (_duration > 0.f && _frameNumber > 1)
	{
		_currentFrameTime += deltaTime;
		if (_currentFrameTime >= _timeBetweenFrame)
		{
			ChangeFrame(_currentFrame == _frameNumber - 1 ? 0 : _currentFrame + 1);
			_currentFrameTime = 0;
		}
	}
	else if (_frameNumber > 0)
	{
		ChangeFrame(0);
	}
}

void				SpriteSheetAnimationModule::ChangeFrame(int newFrame)
{
	Vec2 framePos = _startingPos;
	framePos.x += newFrame != 0 ? newFrame % _totalColumn : 0;
	framePos.y += newFrame != 0 ? newFrame / _totalColumn : 0;
	float frameWidthPercent = 1.f / (float)_totalColumn;
	float frameHeightPercent = 1.f / (float)_totalRaw;
	Vec2 TL = Vec2(frameWidthPercent * framePos.x, frameHeightPercent * framePos.y);
	Vec2 TR = Vec2(frameWidthPercent * framePos.x + frameWidthPercent, frameHeightPercent * framePos.y);
	Vec2 BR = Vec2(frameWidthPercent * framePos.x + frameWidthPercent, frameHeightPercent * framePos.y + frameHeightPercent);
	Vec2 BL = Vec2(frameWidthPercent * framePos.x, frameHeightPercent * framePos.y + frameHeightPercent);
	_texturePlane.SetUVs(TL, TR, BR, BL);
	_currentFrame = newFrame;
}

void				SpriteSheetAnimationModule::Destroy()
{
	_texturePlane.Shutdown();
	delete(_spriteSheetPath);
}

void				SpriteSheetAnimationModule::Draw()
{
	
#ifdef USEDIRECT3D
	if (!_texturePlane.Render(DirectXInterface::Instance().GetDeviceContext(), _location))
		LogWriter::Instance().addLogMessage("Cannot draw", E_MsgType::Warning);
#endif
}

void				SpriteSheetAnimationModule::Flip()
{
	_texturePlane.Flip();
}

Module::Type	SpriteSheetAnimationModule::GetType()const{ return Module::Type::E_SpriteSheetAnimationModule; }

void				SpriteSheetAnimationModule::SetTotalRaw(int totalRaw){ _totalRaw = totalRaw > 1 ? totalRaw : 1; }
void				SpriteSheetAnimationModule::SetTotalColumn(int totalColumn){ _totalColumn = totalColumn > 1 ? totalColumn : 1; }
void				SpriteSheetAnimationModule::SetStartingPos(const Vec2& startingPos){ _startingPos = startingPos; }
void				SpriteSheetAnimationModule::SetSpriteSheetWidth(int spriteSheetWidth){ _spriteSheetWidth = spriteSheetWidth; }
void				SpriteSheetAnimationModule::SetSpriteSheetHeight(int spriteSheetHeight){ _spriteSheetHeight = spriteSheetHeight; }
void				SpriteSheetAnimationModule::SetCurrentFrame(int frame){ ChangeFrame(frame); }
void				SpriteSheetAnimationModule::SetDuration(float duration)
{
	_duration = duration;
	if (_frameNumber > 0 && _duration > 0)
		_timeBetweenFrame = _duration / _frameNumber;
}
void				SpriteSheetAnimationModule::SetFrameNumber(int frameNumber)
{ 
	_frameNumber = frameNumber;
	if (_frameNumber > 0 && _duration > 0)
		_timeBetweenFrame = _duration / _frameNumber;
}
void				SpriteSheetAnimationModule::SetSpriteWidth(int SpriteWidth)
{
	_spriteWidth = SpriteWidth;
	_texturePlane.SetWidth(SpriteWidth);
}
void				SpriteSheetAnimationModule::SetSpriteSheetPath(char* spriteSheetPath)
{
	_spriteSheetPath = strdup(spriteSheetPath);
	std::string str = spriteSheetPath;
	_texturePlane.SetTexture(Graphic::TextureLoader::Instance()->GetTexture(&std::wstring(str.begin(), str.end())[0]));
}
void				SpriteSheetAnimationModule::SetSpriteHeight(int SpriteHeight)
{
	_spriteHeight = SpriteHeight; 
	_texturePlane.SetHeight(SpriteHeight);
}

int					SpriteSheetAnimationModule::GetTotalRaw()const { return _totalRaw; }
int					SpriteSheetAnimationModule::GetTotalColumn()const { return _totalColumn; }
char*				SpriteSheetAnimationModule::GetSpriteSheetPath()const { return _spriteSheetPath; }
const Vec2&			SpriteSheetAnimationModule::GetStartingPos()const { return _startingPos; }
int					SpriteSheetAnimationModule::GetSpriteSheetWidth()const { return _spriteSheetWidth; }
int					SpriteSheetAnimationModule::GetSpriteSheetHeight()const { return _spriteSheetHeight; }
int					SpriteSheetAnimationModule::GetSpriteWidth()const { return _spriteWidth; }
int					SpriteSheetAnimationModule::GetSpriteHeight()const { return _spriteHeight; }
int					SpriteSheetAnimationModule::GetFrameNumber()const { return _frameNumber; }
float				SpriteSheetAnimationModule::GetDuration()const { return _duration; }