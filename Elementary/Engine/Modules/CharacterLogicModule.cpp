#include	"Modules/CharacterLogicModule.h"
#include    "Inputs/InputManager.h"
#include	"Modules\ModuleImport.h"
#include    "Game/Gun.h"
#include	"GameEngine/GameEngine.h"

bool keyWasPressed = false;

CharacterLogicModule::CharacterLogicModule(GameObject* owner) :
    LogicModule(owner),
    _speed(1),
    _isJumping(false), _currentJumpTime(0.0F), _maxJumpTime(0.3F), _jumpPower(Vec2(0.0F, 500.0F)), // jump variable
    _gun(NULL), _clickHold(false) // gun
{
	_goRight = true;
}

CharacterLogicModule::~CharacterLogicModule()
{

}

void				CharacterLogicModule::Initialize()
{
#ifdef GAME
    _gun = static_cast<Gun*>(GameObjectFactory::Inst()->CreateGameObject(GameObject::E_Gun, "Gun"));
	static_cast<SpriteModule*>(_gun->displayModule)->SetHeight(10);
	static_cast<SpriteModule*>(_gun->displayModule)->SetWidth(10);
	static_cast<SpriteModule*>(_gun->displayModule)->SetLayer(2);
	_gun->transformModule->ParentTo(_owner);
	_gun->SetDontDestroyOnLoad(true);
	static_cast<GunLogicModule*>(_gun->logicModule)->SetPlayerOwner(_owner);
#endif
}

void				CharacterLogicModule::Update(float deltaTime)
{
    if (!_owner->transformModule || !deltaTime)
        return;
	if (_owner->transformModule->Position().y <= -100)
		Die();
	bool keyPressed = false;
    // Jump
    if (_isJumping && InputManager::Instance().IsReleased(E_Key::Input_Jump))
        resetJump();
	if (InputManager::Instance().IsPressed(E_Key::Input_Jump))
		keyPressed = Jump(deltaTime);

    // Movement
    if (InputManager::Instance().IsPressed(E_Key::Input_Left))
        keyPressed = GoLeft();
    if (InputManager::Instance().IsPressed(E_Key::Input_Right))
		keyPressed = GoRight();
    if (InputManager::Instance().IsPressed(E_Key::Input_Up))
		keyPressed = GoUp();
    if (InputManager::Instance().IsPressed(E_Key::Input_Down))
		keyPressed = GoDown();

	if (!keyPressed)
	{
		if (_owner->displayModule)
		{
			SpriteSheetAnimationModule* mod = static_cast<SpriteSheetAnimationModule*>(_owner->displayModule);
			if (mod)
			{
				mod->SetStartingPos(Vec2(_goRight ? 1 : 0, 0));
				mod->SetCurrentFrame(0);
				mod->SetDuration(0.f);
				mod->SetFrameNumber(1);
			}
		}
	}
	keyWasPressed = keyPressed;

	// Jump
    if (_isJumping && InputManager::Instance().IsReleased(E_Key::Input_Jump))
        resetJump();
    if (InputManager::Instance().IsPressed(E_Key::Input_Jump))
		keyPressed = Jump(deltaTime);

	// Switch Element
	if (InputManager::Instance().SinglePressed(E_Key::Input_Element_1))
		static_cast<GunLogicModule*>(_gun->logicModule)->SetElem(Fire);
	if (InputManager::Instance().SinglePressed(E_Key::Input_Element_2))
		static_cast<GunLogicModule*>(_gun->logicModule)->SetElem(Water);
	if (InputManager::Instance().SinglePressed(E_Key::Input_Element_3))
		static_cast<GunLogicModule*>(_gun->logicModule)->SetElem(Earth);
	if (InputManager::Instance().SinglePressed(E_Key::Input_Element_4))
		static_cast<GunLogicModule*>(_gun->logicModule)->SetElem(Electricity);

    // gun
    if (!_clickHold && InputManager::Instance().IsMousePressed(E_Key::Input_Fire))
    {
        _clickHold = true;
        Shoot();
    }
    else if (_clickHold && InputManager::Instance().IsMouseReleased(E_Key::Input_Fire))
        _clickHold = false;

    // friction
	if ((!keyPressed && !_owner->physicModule->IsInAir()) || !_owner->physicModule->GravityEnabled())
		_owner->physicModule->AddVelocity(_owner->physicModule->GetVelocity() / -20);
}

void				CharacterLogicModule::OnHit(const GameObject* other)
{
	if (other->boxCollisionModule->CollisionEnabled())
		resetJump();
	if (other->stateModule)
	{
		if (other->stateModule->IsBurning() || other->stateModule->IsElec())
			_owner->lifeModule->SetHealth(0);
	}
}

void				CharacterLogicModule::Destroy()
{
    
}

Module::Type	CharacterLogicModule::GetType()const{ return Module::Type::E_CharacterLogicModule; }

bool               CharacterLogicModule::Jump(float deltaTime)
{
    if (!_owner->physicModule ||
        (_owner->physicModule->IsInAir() && !_isJumping))
        return false;
#ifdef GAME
	if (_owner->boxCollisionModule)
	{
		Collision::AABB* box = new Collision::AABB();
		box->Initialize(_owner->transformModule->Position(), _owner->boxCollisionModule->GetBoxWidth() + 1, _owner->boxCollisionModule->GetBoxHeight() + 1);
		std::list<GameObject*>* list = GameEngine::GetActualQuadtree()->QueryRange(box);
		delete box;
		std::list<std::tuple<const GameObject*, Contact*>>* collidingObject = _owner->boxCollisionModule->GetCollidingObject(*list);
		if (collidingObject != NULL)
		{
			
			for (std::list<std::tuple<const GameObject*, Contact*>>::const_iterator it = collidingObject->begin(); it != collidingObject->end(); ++it)
			{
				if (std::get<1>(*it)->normal == Vec2(0, 1))
					_owner->transformModule->Translate(Vec2(0, 1));
				delete(std::get<1>(*it));
			}
			delete collidingObject;
			collidingObject = NULL;
		}
		delete list;
	}
#endif
    if (_currentJumpTime + deltaTime > _maxJumpTime)
    {
        _owner->physicModule->AddVelocity(_jumpPower * (_maxJumpTime - _currentJumpTime));
        resetJump();
    }
    else
    {
        _owner->physicModule->AddVelocity(_jumpPower * deltaTime);
        _currentJumpTime += deltaTime;
        _isJumping = true;
    }
	return true;
}

void                CharacterLogicModule::resetJump()
{
    _currentJumpTime = 0.0F;
    _isJumping = false;
}

bool                CharacterLogicModule::GoUp()
{
    if (!_owner->physicModule)
        return false;
	if (!_owner->physicModule->GravityEnabled())
		_owner->physicModule->AddVelocity(Vec2(0.0F, _owner->physicModule->GetVelocity().y > 0 ? 4.0f : 1.0F) * _speed);
	return true;
}

bool                CharacterLogicModule::GoDown()
{
    if (!_owner->physicModule)
        return false;
	if (!_owner->physicModule->GravityEnabled())
		_owner->physicModule->AddVelocity(Vec2(0.0F, _owner->physicModule->GetVelocity().y < 0 ? -4.0f : -1.0F) * _speed);
	return true;
}

bool                CharacterLogicModule::GoLeft()
{
    if (!_owner->physicModule)
        return false;

	if (_goRight)
	{
		_goRight = false;
		PutLeftAnimation();
	}
	if (!keyWasPressed)
		PutLeftAnimation();
	_owner->physicModule->AddVelocity(Vec2(_owner->physicModule->GetVelocity().x > 0 ? -4.0f : -1.0F, 0.0F) * _speed);
	return true;
}

bool                CharacterLogicModule::GoRight()
{
    if (!_owner->physicModule)
        return false;

	if (!_goRight)
	{
		_goRight = true;
		PutRightAnimation();
	}
	if (!keyWasPressed)
		PutRightAnimation();

	_owner->physicModule->AddVelocity(Vec2(_owner->physicModule->GetVelocity().x < 0 ? 4.0f : 1.0F, 0.0F) * _speed);
	return true;
}

void				CharacterLogicModule::PutLeftAnimation()
{
	if (_owner->displayModule)
	{
		SpriteSheetAnimationModule* mod = static_cast<SpriteSheetAnimationModule*>(_owner->displayModule);
		if (mod)
		{
			mod->SetStartingPos(Vec2(0, 4));
			mod->SetCurrentFrame(0);
			mod->SetDuration(0.75f);
			mod->SetFrameNumber(16);
		}
	}
}

void				CharacterLogicModule::PutRightAnimation()
{
	if (_owner->displayModule)
	{
		SpriteSheetAnimationModule* mod = static_cast<SpriteSheetAnimationModule*>(_owner->displayModule);
		if (mod)
		{
			mod->SetStartingPos(Vec2(1, 2));
			mod->SetCurrentFrame(0);
			mod->SetDuration(0.75f);
			mod->SetFrameNumber(16);
		}
	}
}

float               CharacterLogicModule::GetSpeed() const
{
    return _speed;
}

void                CharacterLogicModule::SetSpeed(float val)
{
    _speed = val;
}

void				CharacterLogicModule::Die()
{
#ifdef GAME
	GameEngine::Engine::Instance()->ResetPlayer();
#endif
}

void                CharacterLogicModule::Shoot()
{
    GunLogicModule* logicModule = static_cast<GunLogicModule*>(_gun->logicModule);
    if (!logicModule)
        return;
    Vec2* position = Graphic::Camera::Instance()->GetMouseWorldPosition();
    logicModule->Shoot(*position - _owner->transformModule->Position());
    delete position;
}