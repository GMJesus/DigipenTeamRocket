#ifndef		TRANSFORMMODULE_H_
# define	TRANSFORMMODULE_H_

#include	<list>

#include	"Modules\Module.h"

#ifdef USEDIRECT3D
#include	"Graphics/DirectXInterface.h"
#endif

/**
* \brief	Class for storing Transform information
*			Object without Transform Module will be sent into a specific map in the Game Object Factory, so we can access to them easily
*/
class TransformModule : public Module
{
public:
	TransformModule(GameObject* owner);
	~TransformModule();

	//Inherited methods
	void			Initialize();
	void			Update(float deltaTime);
	void			Destroy();
	Module::Type	GetType()const;

	//Setters
	void			Translate(const Vec2& pos);
	void			SetRotation(double angle);
	void			SetPosition(const Vec2& pos);
	void			Scale(float scale);

	//Getters
	Vec2			Position()const;
	double			Rotation()const;
	float			Scale()const;

	//Local Getters
	const Vec2&		LocalPosition()const;
	double			LocalRotation()const;
	float			LocalScale()const;

	//Parenting management
	/**
	* \brief	Parent the current object to another one
	* \param	parentName	The name of the parent object.
	*			Be careful, because names are not unique identifiers, this may result in wrong parenting
	*/
	void			ParentTo(const std::string& parentName);
	/**
	* \brief	Parent the current object to another one
	* \param	parent		The parent object
	*/
	void			ParentTo(GameObject* parent);
	/**
	* \brief	Add the given object to the list of children of the current object
	* \param	child	The child object to store
	*/
	void			AddChild(GameObject* child);
	/**
	* \brief	Remove the given object from the children list of the current object
	*			Also calls the ClearParent() function from the child
	* \param	toRemove	The child to remove from the list
	*/
	void			RemoveChild(GameObject* toRemove);
	/**
	* \brief	Returns the parent object
	* \return	The parent object if there is one, NULL overwise
	*/
	GameObject*		GetParent()const;
	/**
	* \brief	Clear the parent variable of this object
	*			Also calls the RemoveChild() function of the parent, with the current object as parameter
	*/
	void			ClearParent();
	/**
	* \brief	Returns the children list of this object
	* \return	The children list of this object
	*/
	const			std::list<GameObject*>&	GetChilds()const;

#ifdef USEDIRECT3D
	D3DXMATRIX		GetWorldMatrix()const;
	D3DXMATRIX		GetLocalMatrix()const;
#endif

private:
	//@ModuleCreator BindableProperty
	float	_scale;
	//@ModuleCreator BindableProperty
	Vec2	_position;
	//@ModuleCreator BindableProperty
	double	_angle;
	std::list<GameObject*>	_childs;
	GameObject*				_parent;
};

#endif		// !TRANSFORMMODULE_H_
