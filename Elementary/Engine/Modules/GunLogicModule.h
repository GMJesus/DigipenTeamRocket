/**
* \file		 GunLogicModule
* \author    Baptiste DUMAS
* \brief     File with the GunLogicModule that manage the Gun Class
*/

#ifndef GUNLOGICMODULE_H_
#define GUNLOGICMODULE_H_

#include	"Modules/LogicModule.h"
#include	"Modules/ProjLogicModule.h"
#include	"Modules/StateModule.h"
#include	"Modules/TransformModule.h"
#include	"Game/Projectile.h"
#include	"Game/GameObjectFactory.h"

/**
* \brief	LogicModule of the Gun
*/
class GunLogicModule : public LogicModule
{
public:
	GunLogicModule(GameObject* owner);
	~GunLogicModule();

	void	Initialize();
	void	Update(float deltaTime);
	void	Destroy();

	/**
	* \brief Instanciate a Projectile Object
	* \param direction	Spawning direction of the instanciated projectile
	*/
	void	Shoot(const Vec2& direction);
	/**
	* \brief	Set Gun's current element
	*/
	void	SetElem(E_Element elem);
	void	SetPlayerOwner(GameObject* player);

	E_Element	GetElem()const;
	Module::Type	GetType()const;

protected:
	E_Element		_elem; /// \brief	Current gun's element, can be Fire, Water, Electricity or Earth 
	Projectile*		_proj;
	StaticObject*	_ground;
	unsigned int	_projId;
	GameObject*		_playerOwner;

private:
	/**
	* \brief	Initialize Projectile of Earth element
	* \param direction	Spawning position of the projectile
	*/
	void	InitGround(const Vec2& direction);
	void	InitProj(const Vec2& direction);
};

#endif
