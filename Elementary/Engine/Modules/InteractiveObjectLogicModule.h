/**
* \file		 InteractiveLogicModule
* \author    Baptiste DUMAS
* \brief     File with LogicModule for InteractiveObject.
*/

#ifndef INTERACTIVEOBJECTLOGICMODULE_H_
#define INTERACTIVEOBJECTLOGICMODULE_H_

#include	"Modules/LogicModule.h"

/**
* \brief	Base Class for InteractiveObject Logic
*/
class InteractiveObjectLogicModule : public LogicModule
{
public:
	InteractiveObjectLogicModule(GameObject* owner);
	~InteractiveObjectLogicModule();

	virtual void	Initialize();
	virtual void	Update(float deltaTime);
	virtual void	Destroy();
	virtual void	OnHit(const GameObject* other);

	/**
	* \brief	Function called when a switch change position
	*/
	virtual void	Interact();

	Module::Type	GetType()const;

	bool				IsSwitchActive()const;
	void				SwitchActive(bool SwitchActive);
protected:
	bool			_SwitchActive;
};

#endif
