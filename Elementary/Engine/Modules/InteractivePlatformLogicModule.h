/**
* \file		 InteractivePlatformLogicModule
* \author    Baptiste DUMAS
* \brief     File with the InteractiveObjectLogicModule of the Platform Class
*/

#ifndef INTERACTIVEPLATFORMLOGICMODULE_H_
#define INTERACTIVEPLATFORMLOGICMODULE_H_

#include	"Modules/InteractiveObjectLogicModule.h"
#include	"Modules/TransformModule.h"
#include	"Modules/BoxCollisionModule.h"

/**
* \brief LogicModule for Platform
*/
class InteractivePlatformLogicModule : public InteractiveObjectLogicModule
{
public:
	InteractivePlatformLogicModule(GameObject* owner);
	~InteractivePlatformLogicModule();

	void	Initialize();
	void	Update(float deltaTime);
	void	Destroy();
	Module::Type	GetType()const;

	void		Interact();
	void		OnHit(const GameObject* other);

	void		IsActivate(bool isActivate);
	void		SetPosStart(const Vec2& posStart);
	void		SetPosEnd(const Vec2& posEnd);
	void		SetPosPassBy(const Vec2& posPassBy);

	bool		IsIsActivate()const;
	const Vec2&	GetPosStart()const;
	const Vec2&	GetPosEnd()const;
	const Vec2&	GetPosPassBy()const;

protected:
	bool		_isActivate; /// \brief Platform move only if it's true
	Vec2		_posStart; /// \brief	Starting position
	Vec2		_posEnd; /// \brief	Ending position
	Vec2		_posPassBy; /// \brief	Pass by this pos if position != 0
	Vec2		_velocity;

	bool		_passedByStart;
	bool		_passedByMid;
	bool		_passedByEnd;

	void		GoToStart(float dt);
	void		GoToMid(float dt);
	void		GoToEnd(float dt);
};

#endif
