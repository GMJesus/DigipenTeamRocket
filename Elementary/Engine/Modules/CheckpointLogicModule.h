/**
* \author	Aymeric Lambolez
*/

#ifndef CHECKPOINTLOGICMODULE_H_
#define CHECKPOINTLOGICMODULE_H_

#include	"Modules/LogicModule.h"

/**
* \brief	Module for Checkpoint Behaviour
*			Once hit by the player, this one will respawn at this checkpoint location
*/
class CheckpointLogicModule : public LogicModule
{
public:
	CheckpointLogicModule(GameObject* owner);
	~CheckpointLogicModule();

	void	Initialize();

	void	Update(float deltaTime);

	void	Destroy();

	Module::Type	GetType()const;

	virtual void	OnHit(const GameObject* other);

protected:
};

#endif
