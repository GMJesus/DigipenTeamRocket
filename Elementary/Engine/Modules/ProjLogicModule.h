/**
* \file		 ProjLogicModule
* \author    Baptiste DUMAS
* \brief     File with LogicModule of the Projectile Class
*/

#ifndef PROJLOGICMODULE_H_
#define PROJLOGICMODULE_H_

#include	"Modules/LogicModule.h"
#include	"Modules/StateModule.h"
#include	"Modules/SpriteSheetAnimationModule.h"
#include	"Modules/TransformModule.h"
#include	"Game/GameObjectFactory.h"
#include	"Utils/Vec2.h"

#define DESTROYTIMER 2.f;


/**
* \brief	LogicModule of the Projectile Class
*/
class ProjLogicModule : public LogicModule
{
public:
	ProjLogicModule(GameObject* owner);
	~ProjLogicModule();

	void			Initialize();

	void			Update(float deltaTime);

	void			Destroy();

	void			OnHit(const GameObject* other);
	void			Die();

	void			SetElem(E_Element elem);
	void			SetDir(const Vec2& direction);

	Module::Type	GetType()const;
	E_Element		GetElem()const;
	Vec2			GetDir()const;

protected:
	float		_timer;
	Vec2		_dir;
	E_Element	_elem;
};

#endif
