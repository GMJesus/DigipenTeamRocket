/**
* \author	Aymeric Lambolez
*/

#ifndef		MODULEFACTORY_H_
# define	MODULEFACTORY_H_

#include	"Modules\Module.h"

class ModuleFactory
{
private:
	ModuleFactory();
	~ModuleFactory();
	static	ModuleFactory*	p_instance;

	unsigned int			_nbModules;
public:
	/**
	* \brief	Gets an instance of that class
	*			Creates a new instance the first time it is called
	* \return	The instance of that class
	*/
	static ModuleFactory*	Inst();

	/**
	* \brief	Create a module of the given type, inserts it into its parent, and returns it
	* \param	type	The type of the module to create
	* \param	parent	The GameObject that will be the parent of this Module
	*					The module will be automatically stored in this GameObject
	* \return	The newly created Module
	*/
	Module*					CreateModule(Module::Type type, GameObject* parent);
};

#endif		/* !MODULEFACTORY_H_ */