/**
* \file		 SwitchLogicModule
* \author    Baptiste DUMAS
* \brief     File with InteractiveObjectLogicModule of the Switch
*/

#ifndef SWITCHLOGICMODULE_H_
#define SWITCHLOGICMODULE_H_

#include	"Modules/InteractiveObjectLogicModule.h"
#include	"Game\GameObjectFactory.h"

/**
* \brief	InteractiveObjectLogicModule of the switch
*/
class SwitchLogicModule : public InteractiveObjectLogicModule
{
public:
	SwitchLogicModule(GameObject* owner);
	~SwitchLogicModule();

	void	Initialize();
	void	Update(float deltaTime);
	void	Destroy();
	void	OnHit(const GameObject* other);

	void	Interact();

	Module::Type	GetType()const;

	const std::string&		GetObjectToInteract()const;
	void					SetObjectToInteract(const std::string& objectToInteractName);
protected:
	std::string		_objectToInteract; /// \brief Name of the object the switch will interact
	float			_timeToRespond;
	float			_actualTimer;
};

#endif
