/**
* \file		 BoxCollisionModule
* \author    Baptiste DUMAS
* \brief     Module that handle all collision detection of the game
*/

#ifndef BOXCOLLISIONMODULE_H_
# define	BOXCOLLISIONMODULE_H_

#include	"Modules\ModuleImport.h"
#include	"Game/GameObject.h"
#include	"Physics/Collision.h"
#include	"Graphics\TextureLoader.h"
#include	"Physics/RayCast.h"
#include	<list>

enum E_CollisionType
{
	AABB,
	OBB,
	Circle
};

/**
* \brief Contact structure, contain contact information
*/
struct Contact
{
	Vec2				normal;
	float				penetration;
};

/**
* \brief BoxCollisionModule Class
*/
class BoxCollisionModule : public Module
{
public:
	BoxCollisionModule(GameObject* owner);
	~BoxCollisionModule();

	void	Initialize();
	void	Update(float deltaTime);
	void	Destroy();
	void	Shutdown();
	Module::Type	GetType()const;

	/**
	* \brief IsColliding Detect if owner is colliding with another object
	* \param Objects	List of GameObject* arround this object
	*/
	void				IsColliding(const std::list<GameObject*>& Objects);
	/**
	* \brief Resolve the collision of this if it has PhysicsModule
	* \param other	GameObject colliding with this object
	*/
	void				ResolveColliding(const GameObject* other);

	/**
	* \brief Calculus normal between two position
	* \param pos1	First position
	* \param pos2	Second position
	* \return Vector2 normal betwwen the two positions
	*/
	Vec2				CalcNormal(const Vec2& pos1, const Vec2& pos2);
	/**
	* \brief Check if there collision between two position
	* \param start	Start postion
	* \param end	End postion
	* \return True if there is collision
	*/
	bool				CollideVersusLine(const Vec2& start, const Vec2& end);

	/**
	* \brief Add an object to the ignore list of this object
	* \param objectID	Id of the object that will be ignored
	* \param forceOtherToIgnore	true by default, if true the object this object ignore will ignore this object also
	*/
	void				AddObjectToIgnore(unsigned int objectID, bool forceOtherToIgnore = true);
	/**
	* \brief Remove an object of the ignore list of this object
	* \param objectID	Id of the object that will be remove
	* \param forceOtherToIgnore	true by default, if true the object this object don't ignore anymore will ignore this object
	*/
	void				RemoveObjectToIgnore(unsigned int objectID, bool forceOtherToIgnore = true);

	void				SetType(E_CollisionType type);
	void				SetBoxWidth(float width);
	void				SetBoxHeight(float height);
	void				SetGenerateHitEvents(bool generateHitEvents);
	void				SetCollisionEnabled(bool collisionEnabled);

	E_CollisionType		GetType();
	float				GetBoxWidth();
	float				GetBoxHeight();
	const Vec2			GetBoxCorner(int nb)const;
	bool				GenerateHitEvents();
	bool				CollisionEnabled();
	/**
	* \brief Check all colliding GameObject to stock all colliding object information
	* \param Objects	List of GameObject arround this object
	* \return	List of tuple that contain colliding GameObject and Contact information between it and the owner
	*/
	std::list<std::tuple<const GameObject*, Contact*>>*	GetCollidingObject(const std::list<GameObject*>& Objects);

protected:
	/**
	* \brief Check if an object is ignore
	* \param id	Id of GameObject
	* \return True if the GameObject is ignore
	*/
	bool				ContainObjectToIgnore(unsigned int id)const;
	/**
	* \brief True if this is colliding with another object
	*/
	bool				_isColliding;

	Collision::AABB		*aabb;
	Collision::OBB		*obb;
	E_CollisionType		_type;
	//@ModuleCreator BindableProperty
	float				_boxWidth; /// \brief Width of the bounding box
	//@ModuleCreator BindableProperty
	float				_boxHeight; /// \brief Height of the bounding box
	//@ModuleCreator BindableProperty
	bool				_generateHitEvents; /// \brief True will call funtion OnHit() of the LogicModule, only if the colliding object _generatedHitEvent is also true
	//@ModuleCreator BindableProperty
	bool				_collisionEnabled;
	Contact				*_contact;
	std::list<unsigned int>							_toIgnore;
};

#endif // !BOXCOLLISIONMODULE_H_
