#ifndef		MODULE_H_
# define	MODULE_H_

#include	<string>

#ifndef		CLASSCREATOR
#include	"Game\GameObject.h"
#include	"Utils\LogWriter.h"
#include	"Utils\Vec2.h"
#endif


/**
* \brief	Base class for Modules
*			Modules are class that provide a specific behaviour to a GameObject when attached to it
*/
class Module
{
public:
	/**
	* \brief	Constructor
	* \param	owner	The owner of this Module
	*/
	Module(GameObject* owner);
	~Module();

	/**
	* \brief	Defines the types of a Module
	*/
	enum Type
	{
		//@ModuleCreator ModuleTypeStart
		E_SpriteModule = 0,
		E_AnimatedSpriteModule = 1,
		E_BoxCollisionModule = 2,
		E_LifeModule = 3,
		E_LogicModule = 4,
		E_PhysicModule = 5,
		E_SoundModule = 6,
		E_TransformModule = 7,
		E_AIModule = 8,
		E_ParticleSystemModule = 9,
		E_SpriteSheetAnimationModule = 10,
		E_CharacterLogicModule = 11,
		E_ProjLogicModule = 12,
		E_GunLogicModule = 13,
		E_StateModule = 14,
		E_BackAndForthBehaviourModule = 15,
		E_InteractiveObjectLogicModule = 16,
		E_SwitchLogicModule = 17,
		E_DoorLogicModule = 18,
		E_CheckpointLogicModule = 19,
		E_EndLevelLogicModule = 20,
		E_InteractivePlatformLogicModule = 21
		//@ModuleCreator ModuleTypeEnd
	};

	//@ModuleCreator ModuleClassNumber
	static const int		MODULE_CLASS_NUMBER = 22;

	bool				IsEnable();
	void				Enable(bool enable);
	GameObject*			GetOwner();

	virtual Module::Type		GetType()const = 0;
	/**
	* \brief	Intialize the module
	*/
	virtual void		Initialize() = 0;
	/**
	* \brief	Update the module, proceding to the logic of it
	* \param	deltaTime	The time since last frame
	*/
	virtual void		Update(float deltaTime) = 0;
	/**
	* \brief	Release all datas of the Module
	*/
	virtual void		Destroy() = 0;


protected:
	//@ModuleCreator BindableProperty
	bool				_enable;
	GameObject*			_owner;
};

#endif		/* MODULE_H_ */
