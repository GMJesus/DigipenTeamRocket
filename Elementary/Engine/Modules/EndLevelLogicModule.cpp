/**
* \author	Emeric Caramanna
*/

#include	"Modules/EndLevelLogicModule.h"
#include	"GameEngine/GameEngine.h"

EndLevelLogicModule::EndLevelLogicModule(GameObject* owner) : LogicModule(owner)
{

}

EndLevelLogicModule::~EndLevelLogicModule()
{

}

void				EndLevelLogicModule::Initialize()
{

}

void				EndLevelLogicModule::Update(float deltaTime)
{

}

void				EndLevelLogicModule::Destroy()
{

}

Module::Type	EndLevelLogicModule::GetType()const{ return Module::Type::E_EndLevelLogicModule; }

void				EndLevelLogicModule::OnHit(const GameObject* other)
{
	if (_first && other->GetType() == GameObject::Type::E_Character)
	{
		_first = false;
 		GameObjectFactory::Inst()->ResetMap(false);
#ifdef GAME
		GameEngine::Engine::Instance()->setLoad(true);
#endif
	}
}