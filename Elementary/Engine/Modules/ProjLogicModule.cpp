#include	"Modules/ProjLogicModule.h"

ProjLogicModule::ProjLogicModule(GameObject* owner) : LogicModule(owner)
{
	_elem = Fire;
	_dir = Vec2(0, 0);
}

ProjLogicModule::~ProjLogicModule()
{

}

void				ProjLogicModule::Initialize()
{
	_timer = DESTROYTIMER;
	_owner->boxCollisionModule->SetBoxWidth(2);
	_owner->boxCollisionModule->SetBoxHeight(2);
	_owner->boxCollisionModule->SetType(OBB);
}

void				ProjLogicModule::Update(float deltaTime)
{
	if (_elem != Earth)
		_owner->transformModule->SetPosition(_owner->transformModule->Position() + (_dir * 100 * deltaTime));
	if (_timer <= 0)
		Die();
	_timer -= deltaTime;
}

void				ProjLogicModule::Destroy()
{
	GameObjectFactory::Inst()->DeleteObject(_owner->Id(), false);
}

void				ProjLogicModule::OnHit(const GameObject* other)
{
	if (other->boxCollisionModule && other->boxCollisionModule->CollisionEnabled())
	{
		if (other->stateModule)
			other->stateModule->OnHit(_elem);
		if (other->logicModule)
			other->logicModule->OnHit(_owner);
		Die();
	}
}

void				ProjLogicModule::Die()
{
	Destroy();
}

void			ProjLogicModule::SetElem(E_Element elem)
{
	_elem = elem;
	if (static_cast<SpriteSheetAnimationModule*>(_owner->displayModule))
	{
		static_cast<SpriteSheetAnimationModule*>(_owner->displayModule)->SetSpriteHeight(20);
		static_cast<SpriteSheetAnimationModule*>(_owner->displayModule)->SetSpriteWidth(20);
		if (_elem == Earth)
			;
#ifdef GAME
		else if (_elem == Water)
			static_cast<SpriteSheetAnimationModule*>(_owner->displayModule)->SetSpriteSheetPath(strdup("./Ressources/water.png"));
		else if (_elem == Fire)
			static_cast<SpriteSheetAnimationModule*>(_owner->displayModule)->SetSpriteSheetPath(strdup("Ressources/Fire.jpg"));
		else if (_elem == Electricity)
			static_cast<SpriteSheetAnimationModule*>(_owner->displayModule)->SetSpriteSheetPath(strdup("./Ressources/Electric.jpg"));
		else
			static_cast<SpriteSheetAnimationModule*>(_owner->displayModule)->SetSpriteSheetPath(strdup("./Ressources/Fire.jpg"));
#else
		else if (_elem == Water)
			static_cast<SpriteSheetAnimationModule*>(_owner->displayModule)->SetSpriteSheetPath(strdup("../Engine/Ressources/water.png"));
		else if (_elem == Fire)
			static_cast<SpriteSheetAnimationModule*>(_owner->displayModule)->SetSpriteSheetPath(strdup("../Engine/Ressources/Fire.jpg"));
		else if (_elem == Electricity)
			static_cast<SpriteSheetAnimationModule*>(_owner->displayModule)->SetSpriteSheetPath(strdup("../Engine/Ressources/Electric.jpg"));
		else
			static_cast<SpriteSheetAnimationModule*>(_owner->displayModule)->SetSpriteSheetPath(strdup("../Engine/Ressources/Fire.jpg"));
#endif
	}
}

void			ProjLogicModule::SetDir(const Vec2& direction)
{
	_dir = direction.Normalize();
}

Module::Type	ProjLogicModule::GetType()const{ return Module::Type::E_ProjLogicModule; }
E_Element		ProjLogicModule::GetElem()const{ return _elem; }
Vec2			ProjLogicModule::GetDir()const{ return _dir; }