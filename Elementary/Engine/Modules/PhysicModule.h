/**
* \file		 PhysicModule
* \author    Baptiste DUMAS
* \brief     File with physics implematation of the game
*/

#ifndef PHYSICMODULE_H_
# define	PHYSICMODULE_H_

#include	"Modules\ModuleImport.h"
#include	"Physics\Physics.h"
#include	"Utils/Vec2.h"
#include	"Utils\LogWriter.h"
#include	<vector>

/**
* \brief PhusicModule Class, handle all physic of the game
*/
class PhysicModule : public Module
{
public:
	PhysicModule(GameObject* owner);
	~PhysicModule();

	void	Initialize();
	void	Update(float deltaTime);
	void	Destroy();
	void	Shutdown();
	Module::Type	GetType()const;

	Vec2	GetVelocity()const;
	float	GetInvMass()const;
	float	GetAbsorption()const;
	/**
	* \brief	Summed all forces of this object
	* \return	Vector2 force
	*/
	Vec2*	GetSumForces()const;
	bool	GravityEnabled()const;
	std::vector<Vec2>* GetForces()const;
	float	GetMaxVelocity()const;

	/**
	* \brief	Add force to the current forces that affected this object
	* \param force	Vector2 force to add
	*/
	void	AddForce(const Vec2& force);
	void	SetVelocity(const Vec2& velocity);
	/**
	* \brief	Add velocity to the current velocity of this object
	* \param force	Vector2 velocity to add
	*/
	void	AddVelocity(const Vec2& toAdd);
	void	SetMass(float mass);
	void	SetAbsorption(float absorption);
	void	SetForces(std::vector<Vec2> *forces);
	void	EnableGravity(bool enable);
	void	SetMaxVelocity(float vel);

	bool		IsInAir()const;
	void		InAir(bool val);

protected:
	bool	CheckIsInAir() const;
	bool	_inAir;
	//@ModuleCreator BindableProperty
	float	_maxVelocity;
	//@ModuleCreator BindableProperty
	Vec2	_velocity;
	//@ModuleCreator BindableProperty
	float	_invMass;
	//@ModuleCreator BindableProperty
	float	_absorption;
	//@ModuleCreator BindableProperty
	bool	_gravityEnabled;
	std::vector<Vec2> *_forces;
};

#endif // !PHYSICMODULE_H_
