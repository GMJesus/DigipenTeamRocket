#include	"Modules/GunLogicModule.h"
#include	"GameEngine\GameEngine.h"
#include <UI/Menu/MenuFactory.h>


GunLogicModule::GunLogicModule(GameObject* owner) : LogicModule(owner)
{
	_proj = NULL;
	_ground = NULL;
	_playerOwner = NULL;
	_projId = 0;
	_elem = Fire;
}

GunLogicModule::~GunLogicModule()
{
	if (_proj)
		GameObjectFactory::Inst()->DeleteObject(_proj->Id(), false);
	if (_ground)
		GameObjectFactory::Inst()->DeleteObject(_ground->Id(), false);

}

void				GunLogicModule::Initialize()
{

}

void				GunLogicModule::Update(float deltaTime)
{

}

void				GunLogicModule::Destroy()
{
	
}

void			GunLogicModule::Shoot(const Vec2& direction)
{
	if (_elem == Earth)
	{
		if (_ground == NULL)
			InitGround(direction);
		else if (_ground)
		{
			GameObjectFactory::Inst()->DeleteObject(_ground->Id(), false);
			_ground = NULL;
		}
	}
	else
	{
		if (_proj == NULL)
			InitProj(direction);
		else if (_proj)
		{
			if (GameObjectFactory::Inst()->GetObjectById(_projId) == NULL)
				InitProj(direction);
		}
	}
}

void			GunLogicModule::InitGround(const Vec2& direction)
{
	if (_playerOwner != NULL)
	{
		if (_playerOwner->physicModule)
		{
			if (_playerOwner->physicModule->IsInAir() == true)
				return;
		}
	}
	_ground = (StaticObject*)GameObjectFactory::Inst()->SpawnGameObject(GameObject::E_StaticObject, "PlayerProjectile", direction + _owner->transformModule->Position(), 15, 5);
	if (_ground != NULL)
	{
		_ground->transformModule->SetPosition(direction + _owner->transformModule->Position());
		_ground->boxCollisionModule->SetBoxWidth(15);
		_ground->boxCollisionModule->SetBoxHeight(5);
		_ground->boxCollisionModule->SetType(OBB);
		_ground->physicModule->SetMass(0);
		_ground->physicModule->SetAbsorption(0);
		_ground->physicModule->EnableGravity(false);
		static_cast<SpriteModule*>(_ground->displayModule)->SetWidth(150);
		static_cast<SpriteModule*>(_ground->displayModule)->SetHeight(50);
#ifdef GAME
		static_cast<SpriteModule*>(_ground->displayModule)->SetTexture(Graphic::TextureLoader::Instance()->GetTexture(L"./Ressources/DarkDirt.jpg"));
#else
		static_cast<SpriteModule*>(_ground->displayModule)->SetTexture(Graphic::TextureLoader::Instance()->GetTexture(L"../Engine/Ressources/DarkDirt.jpg"));
#endif
		if (_owner->soundModule)
		{
			_owner->soundModule->SetPath("./Ressources/Sounds/WoodBlock.wav");
			_owner->soundModule->Play();
		}
	}
}

void		GunLogicModule::InitProj(const Vec2& direction)
{
	_proj = static_cast<Projectile*>(GameObjectFactory::Inst()->CreateGameObject(GameObject::E_Projectile, "PlayerProjectile"));
	if (_proj != NULL)
	{
		_proj->transformModule->SetPosition(_owner->transformModule->Position() + direction.Normalize() * 2);
		static_cast<ProjLogicModule*>(_proj->logicModule)->SetDir(direction);
		static_cast<ProjLogicModule*>(_proj->logicModule)->SetElem(_elem);
		static_cast<ProjLogicModule*>(_proj->logicModule)->Initialize();
		_proj->boxCollisionModule->AddObjectToIgnore(GameObjectFactory::Inst()->GetPlayer()->Id());
		_projId = _proj->Id();
		if (_owner->soundModule)
		{
			switch (_elem)
			{
			case Fire:
				_owner->soundModule->SetPath("./Ressources/Sounds/FireShoot.wav");
				break;
			case Water:
				_owner->soundModule->SetPath("./Ressources/Sounds/WaterShoot.wav");
				break;
			case Electricity:
				_owner->soundModule->SetPath("./Ressources/Sounds/ElectricShoot.wav");
				break;
			default:
				break;
			}
			_owner->soundModule->Play();
		}
	}
}

void			GunLogicModule::SetElem(E_Element elem)
{
#ifdef GAME
#include "UI/Menu/MenuFactory.h"
    UIEngine::MenuFactory::Instance()->GetMenu(0)->GetSwitch(_elem)->SwitchState(false);
    UIEngine::MenuFactory::Instance()->GetMenu(0)->GetSwitch(elem)->SwitchState(true);
#endif
    
    _elem = elem;
	if (_elem == Fire)
		UIEngine::UI::Instance().PrintMessage("Fire");
	if (_elem == Water)
		UIEngine::UI::Instance().PrintMessage("Water");
	if (_elem == Earth)
		UIEngine::UI::Instance().PrintMessage("Earth");
	if (_elem == Electricity)
		UIEngine::UI::Instance().PrintMessage("Electricity");
}

void			GunLogicModule::SetPlayerOwner(GameObject* player)
{
	_playerOwner = player;
}

Module::Type	GunLogicModule::GetType()const{ return Module::Type::E_GunLogicModule; }
E_Element		GunLogicModule::GetElem()const{ return _elem; }