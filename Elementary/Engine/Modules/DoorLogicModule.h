/**
* \file		 DoorLogicModule
* \author    Baptiste DUMAS
* \brief     File with the LogicModule of the Door Class
*/

#ifndef DOORLOGICMODULE_H_
#define DOORLOGICMODULE_H_

#include	"Modules/InteractiveObjectLogicModule.h"
#include	"Modules\BoxCollisionModule.h"

/**
* \brief LogicModule of the Door Class, child of InteractiveObjectLogicModule
*/
class DoorLogicModule : public InteractiveObjectLogicModule
{
public:
	DoorLogicModule(GameObject* owner);
	~DoorLogicModule();

	void	Initialize();
	void	Update(float deltaTime);
	void	Destroy();
	void	OnHit(const GameObject* other);
	void	Interact();

	Module::Type	GetType()const;

	bool		IsIsOpen()const;
	void		IsOpen(bool isOpen);
protected:
	bool		_isOpen; /// \brief True if the door is open
};

#endif
