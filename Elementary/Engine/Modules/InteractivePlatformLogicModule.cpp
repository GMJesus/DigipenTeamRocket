#include	"Modules/InteractivePlatformLogicModule.h"
#include	"UI\UIEngine.h"
#include	"Physics\RayCast.h"
#include	"GameEngine\GameEngine.h"

InteractivePlatformLogicModule::InteractivePlatformLogicModule(GameObject* owner) : InteractiveObjectLogicModule(owner)
{
	_isActivate = false;
	_posStart = 0;
	_posPassBy = 0;
	_posEnd = 0;
	_passedByStart = true;
	_passedByMid = false;
	_passedByEnd = false;
	_velocity = Vec2(2, 2);
}

InteractivePlatformLogicModule::~InteractivePlatformLogicModule()
{

}

void			InteractivePlatformLogicModule::Initialize()
{

}

void			InteractivePlatformLogicModule::Update(float deltaTime)
{
	if (_isActivate && _owner->transformModule)
	{
		Vec2	traveledDistance = _owner->transformModule->Position();
		if (_posPassBy.x != 0 && _posPassBy.y != 0)
		{
			if (_passedByMid == false)
				GoToMid(deltaTime);
			else if (_passedByEnd == false)
				GoToEnd(deltaTime);
			else
				GoToStart(deltaTime);
		}
		else
		{
			if (_passedByEnd == false)
				GoToEnd(deltaTime);
			else if (_passedByStart == false)
				GoToStart(deltaTime);
		}
		traveledDistance -= _owner->transformModule->Position();
#ifdef GAME
		/*Collision::AABB* box = new Collision::AABB();
		box->Initialize(_owner->transformModule->Position(), _owner->boxCollisionModule->GetBoxWidth() + 1, _owner->boxCollisionModule->GetBoxHeight() + 1);
		std::list<GameObject*>* list = GameEngine::GetActualQuadtree()->QueryRange(box);
		delete box;
		std::list<std::tuple<const GameObject*, Contact*>>* collidingObject = _owner->boxCollisionModule->GetCollidingObject(*list);
		if (collidingObject != NULL)
		{
			LogWriter::Instance().addLogMessage(std::to_string(collidingObject->size()));
			for (std::list<std::tuple<const GameObject*, Contact*>>::const_iterator it = collidingObject->begin(); it != collidingObject->end(); ++it)
			{
				if (std::get<1>(*it)->normal == Vec2(0, -1))
				{
					if (std::get<0>(*it)->transformModule)
						std::get<0>(*it)->transformModule->Translate(traveledDistance * -1);
				}
				delete(std::get<1>(*it));
				LogWriter::Instance().addLogMessage(std::get<1>(*it)->normal.ToString());

			}
			delete collidingObject;
			collidingObject = NULL;
		}
		delete list;*/
#endif
	}
}

void			InteractivePlatformLogicModule::Destroy()
{

}

void			InteractivePlatformLogicModule::Interact()
{
	if (_isActivate)
		_isActivate = false;
	else
		_isActivate = true;
	UIEngine::UI::Instance().PrintMessage(to_string(_isActivate));
}

void				InteractivePlatformLogicModule::OnHit(const GameObject* other)
{

}

void			InteractivePlatformLogicModule::GoToStart(float dt)
{
	_owner->transformModule->Translate((_posStart - _owner->transformModule->Position()).Normalize() * dt * 10);
	if ((int)_owner->transformModule->Position().x == _posStart.x && (int)_owner->transformModule->Position().y == _posStart.y)
	{
		_passedByEnd = false;
		_passedByMid = false;
		_passedByStart = true;
	}
}

void			InteractivePlatformLogicModule::GoToMid(float dt)
{
	_owner->transformModule->Translate((_posPassBy - _owner->transformModule->Position()).Normalize() * dt * 10);
	if ((int)_owner->transformModule->Position().x == _posPassBy.x && (int)_owner->transformModule->Position().y == _posPassBy.y)
	{
		_passedByMid = true;
	}
}

void			InteractivePlatformLogicModule::GoToEnd(float dt)
{
	_owner->transformModule->Translate((_posEnd - _owner->transformModule->Position()).Normalize() * dt * 10);
	if ((int)_owner->transformModule->Position().x == _posEnd.x && (int)_owner->transformModule->Position().y == _posEnd.y)
	{
		_passedByEnd = true;
		_passedByMid = false;
		_passedByStart = false;
	}
}

void			InteractivePlatformLogicModule::IsActivate(bool isActivate){ _isActivate = isActivate; }
void			InteractivePlatformLogicModule::SetPosStart(const Vec2& posStart){ _posStart = posStart; }
void			InteractivePlatformLogicModule::SetPosEnd(const Vec2& posEnd){ _posEnd = posEnd; }
void			InteractivePlatformLogicModule::SetPosPassBy(const Vec2& posPassBy){ _posPassBy = posPassBy; }

bool			InteractivePlatformLogicModule::IsIsActivate()const { return _isActivate; }
const Vec2&		InteractivePlatformLogicModule::GetPosStart()const { return _posStart; }
const Vec2&		InteractivePlatformLogicModule::GetPosEnd()const { return _posEnd; }
const Vec2&		InteractivePlatformLogicModule::GetPosPassBy()const { return _posPassBy; }

Module::Type	InteractivePlatformLogicModule::GetType()const{ return Module::Type::E_InteractivePlatformLogicModule; }
