/**
* \author	Aymeric Lambolez
*/

#include	"Modules\ModuleFactory.h"
#include	"Modules\ModuleImport.h"

ModuleFactory* ModuleFactory::p_instance = NULL;

ModuleFactory::ModuleFactory()
{

}

ModuleFactory::~ModuleFactory()
{

}

ModuleFactory*		ModuleFactory::Inst()
{
	if (!ModuleFactory::p_instance)
		ModuleFactory::p_instance = new ModuleFactory();
	return ModuleFactory::p_instance;
}

Module*				ModuleFactory::CreateModule(Module::Type type, GameObject* parent)
{
	switch (type)
	{
		//@ModuleCreator SwitchCreationStart
	case Module::E_InteractivePlatformLogicModule:
		parent->logicModule = new InteractivePlatformLogicModule(parent);
		parent->logicModule->Initialize();
		break;
	case Module::E_EndLevelLogicModule:
		parent->logicModule = new EndLevelLogicModule(parent);
		parent->logicModule->Initialize();
		break;
	case Module::E_CheckpointLogicModule:
		parent->logicModule = new CheckpointLogicModule(parent);
		parent->logicModule->Initialize();
		break;
	case Module::E_DoorLogicModule:
		parent->logicModule = new DoorLogicModule(parent);
		parent->logicModule->Initialize();
		break;
	case Module::E_SwitchLogicModule:
		parent->logicModule = new SwitchLogicModule(parent);
		parent->logicModule->Initialize();
		break;
	case Module::E_InteractiveObjectLogicModule:
		parent->logicModule = new InteractiveObjectLogicModule(parent);
		parent->logicModule->Initialize();
		break;
	case Module::E_BackAndForthBehaviourModule:
		parent->aIModule = new BackAndForthBehaviourModule(parent);
		parent->aIModule->Initialize();
		break;
	case Module::E_StateModule:
		parent->stateModule = new StateModule(parent);
		parent->stateModule->Initialize();
		break;
	case Module::E_GunLogicModule:
		parent->logicModule = new GunLogicModule(parent);
		parent->logicModule->Initialize();
		break;
	case Module::E_ProjLogicModule:
		parent->logicModule = new ProjLogicModule(parent);
		parent->logicModule->Initialize();
		break;
	case Module::E_SpriteSheetAnimationModule:
		parent->displayModule = new SpriteSheetAnimationModule(parent);
		parent->displayModule->Initialize();
		break;
	case Module::E_CharacterLogicModule:
        parent->logicModule = new CharacterLogicModule(parent);
		parent->logicModule->Initialize();
		break;
	case Module::E_SpriteModule:
		parent->displayModule = new SpriteModule(parent);
		parent->displayModule->Initialize();
		break;
	case Module::E_AnimatedSpriteModule:
		parent->displayModule = new AnimatedSpriteModule(parent);
		parent->displayModule->Initialize();
		break;
	case Module::E_BoxCollisionModule:
		parent->boxCollisionModule = new BoxCollisionModule(parent);
		parent->boxCollisionModule->Initialize();
		break;
	case Module::E_LifeModule:
		parent->lifeModule = new LifeModule(parent);
		parent->lifeModule->Initialize();
		break;
	case Module::E_LogicModule:
        parent->logicModule = new LogicModule(parent);
		parent->logicModule->Initialize();
		break;
	case Module::E_PhysicModule:
		parent->physicModule = new PhysicModule(parent);
		parent->physicModule->Initialize();
		break;
	case Module::E_SoundModule:
		parent->soundModule = new SoundModule(parent);
		parent->soundModule->Initialize();
		break;
	case Module::E_TransformModule:
		parent->transformModule = new TransformModule(parent);
		parent->transformModule->Initialize();
		break;
	case Module::E_AIModule:
		parent->aIModule = new AIModule(parent);
		parent->aIModule->Initialize();
		break;
	case Module::E_ParticleSystemModule:
		parent->displayModule = new ParticleSystemModule(parent);
		parent->displayModule->Initialize();
		break;
	default:
		break;
	}
	return NULL;
}
