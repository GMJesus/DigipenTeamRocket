#include	"Modules\PhysicModule.h"

#define ABS(x) (x > 0.f ? x : x * -1)

PhysicModule::PhysicModule(GameObject* owner) : Module(owner)
{
	_forces = NULL;
	_maxVelocity = 50.f;
}

PhysicModule::~PhysicModule()
{
}

void	PhysicModule::Initialize()
{
	_forces = new std::vector<Vec2>(); 
}

void	PhysicModule::Update(float deltaTime)
{
	InAir(CheckIsInAir());
	Vec2* sumForces = GetSumForces();
	_velocity +=  *sumForces* deltaTime;
	delete (sumForces);
	if (_maxVelocity > 0.f)
	{
		if (ABS(_velocity.x) > _maxVelocity)
			_velocity.x = _maxVelocity * (_velocity.x > 0.f ? 1 : -1);
		if (ABS(_velocity.y) > _maxVelocity)
			_velocity.y = _maxVelocity * (_velocity.y > 0.f ? 1 : -1);
	}
	_owner->transformModule->Translate(_velocity * deltaTime * 0.99f);
}

void		PhysicModule::AddForce(const Vec2& force)
{
	if (_forces != NULL)
	{
		_forces->push_back(force);
	}
}

void		PhysicModule::Destroy()
{
	if (_forces)
		delete(_forces);
}

Vec2*	PhysicModule::GetSumForces()const { return PhysicEngine::SumForces(_forces, _gravityEnabled && _inAir); }
Vec2	PhysicModule::GetVelocity()const { return _velocity; }
float	PhysicModule::GetMaxVelocity()const { return _maxVelocity; }
bool	PhysicModule::GravityEnabled()const { return _gravityEnabled; }
float	PhysicModule::GetInvMass()const { return _invMass; }
float	PhysicModule::GetAbsorption()const { return _absorption; }
std::vector<Vec2>*	PhysicModule::GetForces()const { return _forces; }
Module::Type		PhysicModule::GetType()const{ return Module::E_PhysicModule; }

void	PhysicModule::SetForces(std::vector<Vec2> *forces)
{
	if (_forces)
	{
		if (forces == NULL)
			_forces->clear();
		else
		{
			delete(_forces);
			_forces = forces;
		}
	}
}
void	PhysicModule::SetVelocity(const Vec2& velocity){ _velocity = velocity; }
void	PhysicModule::AddVelocity(const Vec2& toAdd){ _velocity += toAdd; }
void	PhysicModule::EnableGravity(bool enable){ _gravityEnabled = enable; }
void	PhysicModule::SetMass(float mass) 
{
	if (mass == 0)
		_invMass = 0;
	else
		_invMass = 1.f / mass; 
}
void	PhysicModule::SetAbsorption(float absorption) { _absorption = absorption; }
void	PhysicModule::SetMaxVelocity(float vel) { _maxVelocity = vel; }

bool				PhysicModule::IsInAir()const
{
	return _inAir;
}

bool			PhysicModule::CheckIsInAir()const
{
#ifdef GAME
	if (_owner->boxCollisionModule && _owner->transformModule)
	{
		bool ret;

		Vec2 pos = _owner->transformModule->Position();
		pos.y -= ((_owner->boxCollisionModule->GetBoxHeight() / 2) + 0.01f);
		ret = !RayCast(pos, pos + (Vec2(0, -0.1f)));
		pos.x += (_owner->boxCollisionModule->GetBoxWidth() / 2 - 0.02f);
		ret = ret && !RayCast(pos, pos + (Vec2(0, -0.1f)));
		pos.x -= (_owner->boxCollisionModule->GetBoxWidth() - 0.04f);
		ret = ret && !RayCast(pos, pos + (Vec2(0, -0.1f)));
		return ret;
	}
#endif
	return true;
}


void				PhysicModule::InAir(bool val)
{
	_inAir = val;
}

