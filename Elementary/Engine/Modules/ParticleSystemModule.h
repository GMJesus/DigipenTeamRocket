/**
* \author	Emeric Caramanna
*/

#pragma once

#include "Graphics/Particle.h"
#include "Modules/ModuleImport.h"
#include "Utils/Vec2.h"


class ParticleSystemModule : public DisplayModule
{
public:
	ParticleSystemModule(GameObject* owner);
	~ParticleSystemModule();
	void	Initialize();
	void	Update(float deltaTime);
	void	Draw();
	void	Destroy();
	Module::Type	GetType()const;

	void	SetSpawningRate(float spawningRate);
	void	SetLifeTime(float lifeTime);
	void	SetParticlesWidth(int pWidth);
	void	SetParticlesHeight(int pHeight);
	void	SetParticlesTexture(Graphic::Texture* texture);
	void	SetInitialVelocity(Vec2 initialVelocity);
	void	SetGravityEnabled(bool gravity);
	void	SetAngle(float angle);

	float	GetSpawningRate();
	float	GetLifeTime();
	int		GetParticlesWidth();
	int		GetParticlesHeight();
	Graphic::TexturePlane*	GetTexture();
	Vec2	GetInitialVelocity();
	std::vector<Particle*> GetCurrentParticles();
	bool	IsGravityEnabled();
	float	GetAngle();

	/**
	* \brief Launch all the particles at once based on the spawning rate
	*/
	void	Explode();

	/**
	* \brief Start the generation of particles
	*/
	void	Play();
	/**
	* \brief Pause the generation of particles
	*		 The existing particles finish their life time
	*/
	void	Pause();
	/**
	* \brief Stop the generation of particles
	*		 The existing particles instantly die
	*/
	void	Stop();
	/**
	* \brief Launch particles for a given time in seconds
	*/
	void	PlayFor(float duration);

	void	SetPlaying(bool play);
	bool	IsPlaying();


private:
	/**
	* \brief Get a random float between the min and max value
	*/
	float				RandomF(float min, float max);
	/**
	* \brief Get the result of the Vec2 rotation (counterclockwise)
	* \param vector Initial vector before the rotation
	* \param angle Angle to rotate the vector in deg
	*/
	Vec2				RotateVec(Vec2 vector, float angle);
	Particle			*GetDeadParticle();

	bool				_isPlaying;
	bool				_gravityEnabled;
	float				_spawningTime;
	float				_delta;
	float				_spawningRate;
	float				_lifeTime;
	float				_angle;
	float				_duration;
	int 				_pWidth;
	int 				_pHeight;
	Graphic::TexturePlane*	_texture;
	Vec2				_initialVelocity;
	Vec2				_position;
	Vec2				_rVelocity;
	std::vector<Particle*> _currentParticles;
};

