#include	"Modules\AIModule.h"

AIModule::AIModule(GameObject* owner) : Module(owner)
{

}

AIModule::~AIModule()
{

}

void	AIModule::Initialize()
{

}

void	AIModule::Update(float deltaTime)
{

}

void	AIModule::Destroy()
{

}

Module::Type		AIModule::GetType()const{ return Module::E_AIModule; }
