/**
* \author	Aymeric Lambolez
*/

#ifndef SPRITESHEETANIMATIONMODULE_H_
#define SPRITESHEETANIMATIONMODULE_H_

#include	"Modules/Module.h"
#include	"Modules/DisplayModule.h"
#include	"Modules/TransformModule.h"
#include	"Graphics/TexturePlane.h"
#include	"Graphics\GraphicEngine.h"
#include	"Graphics/TextureLoader.h"

/**
* \brief	Class for drawing animations by Sprite Sheets
*/
class SpriteSheetAnimationModule : public DisplayModule
{
public:
	SpriteSheetAnimationModule(GameObject* owner);
	~SpriteSheetAnimationModule();

	void	Initialize();

	void	Update(float deltaTime);

	void	Destroy();
	void	Draw();
	void	Flip();

	Module::Type	GetType()const;

	int			GetTotalRaw()const;
	void		SetTotalRaw(int totalRaw);
	int			GetTotalColumn()const;
	void		SetTotalColumn(int totalColumn);
	char*		GetSpriteSheetPath()const;
	void		SetSpriteSheetPath(char* spriteSheetPath);
	const Vec2&	GetStartingPos()const;
	void		SetStartingPos(const Vec2& startingPos);
	int			GetSpriteSheetWidth()const;
	void		SetSpriteSheetWidth(int spriteSheetWidth);
	int			GetSpriteSheetHeight()const;
	void		SetSpriteSheetHeight(int spriteSheetHeight);
	int			GetSpriteWidth()const;
	void		SetSpriteWidth(int SpriteWidth);
	int			GetSpriteHeight()const;
	void		SetSpriteHeight(int SpriteHeight);
	int			GetFrameNumber()const;
	void		SetFrameNumber(int frameNumber);
	float		GetDuration()const;
	void		SetDuration(float duration);
	void		SetCurrentFrame(int frame);
protected:
	int			_totalRaw;
	int			_totalColumn;
	char*		_spriteSheetPath;
	Vec2		_startingPos;
	int			_spriteSheetWidth;
	int			_spriteSheetHeight;
	int			_spriteWidth;
	int			_spriteHeight;
	int			_frameNumber;
	float		_duration;
	int			_currentFrame;
	int			_previousFrame;
	float		_timeBetweenFrame;
	float		_currentFrameTime;
	Graphic::TexturePlane _texturePlane;

	void		ChangeFrame(int newFrame);
};

#endif
