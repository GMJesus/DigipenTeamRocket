/**
* \author	Emeric Caramanna
*/
#ifndef		ANIMATEDSPRITEMODULE_H_
# define	ANIMATEDSPRITEMODULE_H_

#include	<list>
#include	"Modules\DisplayModule.h"
#include	"Modules\TransformModule.h"
#include	"Graphics\GraphicEngine.h"
#include	"Utils\EngineParameters.h"
#include	"Graphics\SpriteAnimation.h"
#include	"Graphics\TexturePlane.h"
#include	"Graphics\DirectXInterface.h"

class AnimatedSpriteModule : public DisplayModule
{
public:
	AnimatedSpriteModule(GameObject* owner);
	~AnimatedSpriteModule();


	void	Initialize();
	void	Update(float deltaTime);
	void	Draw();
	void	Destroy();
	void	Flip();
	Module::Type	GetType()const;

	void	Animation(std::vector<SpriteAnimation*>::iterator it, float deltaTime);

	const Graphic::TexturePlane&	GetTexturePlane();

	int					GetCurrentWidth();
	int					GetCurrentHeight();
	int					GetInitialWidth();
	int					GetInitialHeight();
	int					GetWidth();
	int					GetHeight();
	const char*			GetTexturePath();

	void				SetCurrentWidth(int width);
	void				SetCurrentHeight(int height);
	void				SetInitialWidth(int width);
	void				SetInitialHeight(int height);
	void				SetTexture(Graphic::Texture* tex);

	void				PlayAll();
	void				PauseAll();
	void				StopAll();
	void				PlayType(SpriteAnimation::E_Animated type);
	void				PauseType(SpriteAnimation::E_Animated type);
	void				StopType(SpriteAnimation::E_Animated type);

	vector<SpriteAnimation*> _storyboard;


protected:
	int							_initialWidth;
	int							_initialHeight;
	int							_width;
	int							_height;
	Graphic::TexturePlane		_texturePlane;
};

#endif // !ANIMATEDSPRITEMODULE_H_
