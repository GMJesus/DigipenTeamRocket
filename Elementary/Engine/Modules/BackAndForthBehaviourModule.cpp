/**
* \author	Aymeric Lambolez
*/

#include	"Modules/BackAndForthBehaviourModule.h"
#include	"Modules/PhysicModule.h"

BackAndForthBehaviourModule::BackAndForthBehaviourModule(GameObject* owner) : AIModule(owner)
{
	_goRight = true;
}

BackAndForthBehaviourModule::~BackAndForthBehaviourModule()
{

}

void				BackAndForthBehaviourModule::Initialize()
{
	if (_owner->physicModule)
	{
		Vec2 vel = _owner->physicModule->GetVelocity();
		vel.x = _owner->physicModule->GetMaxVelocity() * (_goRight ? 1 : -1);
		_owner->physicModule->SetVelocity(vel);
		_goRight = !_goRight;
	}
}

void				BackAndForthBehaviourModule::Update(float deltaTime)
{
#ifdef GAME
	if (_owner->boxCollisionModule && _owner->transformModule && _owner->physicModule && !_owner->physicModule->IsInAir())
	{
		GameObject* touched;
		Vec2 pos = _owner->transformModule->Position();
		pos.x += ((_owner->boxCollisionModule->GetBoxWidth() / 2.f) + 0.01f) * (_goRight ? -1 : 1);
		if ((touched = RayCast(pos, pos + (Vec2(0.1f * (_goRight ? -1 : 1), 0.f)))) && touched->boxCollisionModule && touched->boxCollisionModule->CollisionEnabled())
		{
			if (touched->boxCollisionModule && touched->boxCollisionModule->GenerateHitEvents())
			{
				UIEngine::UI::Instance().PrintMessage("Change because of wall");
				if (touched->logicModule)
					touched->logicModule->OnHit(_owner);
			}
			ChangeDirection();
		}
		else if (!_ignoreGroundModification)
		{
			pos.y -= ((_owner->boxCollisionModule->GetBoxHeight() / 2) + 0.01f);
			if (!RayCast(pos, pos + (Vec2(0, -0.1f))))
			{
				UIEngine::UI::Instance().PrintMessage("Change because of floor");
				ChangeDirection();
			}
		}
		if (_owner->physicModule->GetVelocity().x != (_owner->physicModule->GetMaxVelocity() * (_goRight ? 1 : -1)))
		{
			Vec2 vel = _owner->physicModule->GetVelocity();
			vel.x = _owner->physicModule->GetMaxVelocity() * (_goRight ? -1 : 1);
			_owner->physicModule->SetVelocity(vel);
		}
	}
#endif
}

void				BackAndForthBehaviourModule::ChangeDirection()
{
	if (_owner->physicModule)
	{
		Vec2 vel = _owner->physicModule->GetVelocity();
		vel.x = _owner->physicModule->GetMaxVelocity() * (_goRight ? 1 : -1);
		_owner->physicModule->SetVelocity(vel);
		_goRight = !_goRight;
		if (_owner->displayModule)
			_owner->displayModule->Flip();
	}
}

void				BackAndForthBehaviourModule::Destroy()
{

}

Module::Type	BackAndForthBehaviourModule::GetType()const{ return Module::Type::E_BackAndForthBehaviourModule; }
void				BackAndForthBehaviourModule::SetSpeed(float speed)
{ 
	_speed = speed;
	if (_owner->physicModule)
		_owner->physicModule->SetMaxVelocity(speed);
}
float	BackAndForthBehaviourModule::GetSpeed()const { return _speed; }

void				BackAndForthBehaviourModule::IgnoreGroundModification(bool ignoreGroundModification){ _ignoreGroundModification = ignoreGroundModification; }
bool	BackAndForthBehaviourModule::IsIgnoreGroundModification()const { return _ignoreGroundModification; }

