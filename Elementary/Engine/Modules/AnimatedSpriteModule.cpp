/**
* \author	Emeric Caramanna
*/

#include "Modules\AnimatedSpriteModule.h"

AnimatedSpriteModule::AnimatedSpriteModule(GameObject* owner) : DisplayModule(owner)
{

}

AnimatedSpriteModule::~AnimatedSpriteModule()
{

}

void		AnimatedSpriteModule::Initialize()
{
#ifdef USEDIRECT3D
	_texturePlane.Initialize(DirectXInterface::Instance().GetDevice());
#endif

	_width = 100;
	_height = 100;
	_initialWidth = 100;
	_initialHeight = 100;
}

void				AnimatedSpriteModule::PlayAll()
{
	if (_storyboard.size() > 0)
	{
		for (std::vector<SpriteAnimation*>::iterator it = _storyboard.begin(); it != _storyboard.end();
			++it)
		{
			(*it)->Play();
		}
	}

}

void				AnimatedSpriteModule::PauseAll()
{
	if (_storyboard.size() > 0)
	{
		for (std::vector<SpriteAnimation*>::iterator it = _storyboard.begin(); it != _storyboard.end();
			++it)
		{
			(*it)->Pause();
		}
	}
}

void				AnimatedSpriteModule::StopAll()
{
	if (_storyboard.size() > 0)
	{
		for (std::vector<SpriteAnimation*>::iterator it = _storyboard.begin(); it != _storyboard.end();
			++it)
		{
			(*it)->Stop();
		}
	}
}

void				AnimatedSpriteModule::PlayType(SpriteAnimation::E_Animated type)
{
	if (_storyboard.size() > 0)
	{
		for (std::vector<SpriteAnimation*>::iterator it = _storyboard.begin(); it != _storyboard.end();
			++it)
		{
			if ((*it)->GetAnimated() == type)
				(*it)->Play();
		}
	}
}

void				AnimatedSpriteModule::PauseType(SpriteAnimation::E_Animated type)
{
	if (_storyboard.size() > 0)
	{
		for (std::vector<SpriteAnimation*>::iterator it = _storyboard.begin(); it != _storyboard.end();
			++it)
		{
			if ((*it)->GetAnimated() == type)
				(*it)->Pause();
		}
	}
}

void				AnimatedSpriteModule::StopType(SpriteAnimation::E_Animated type)
{
	if (_storyboard.size() > 0)
	{
		for (std::vector<SpriteAnimation*>::iterator it = _storyboard.begin(); it != _storyboard.end();
			++it)
		{
			if ((*it)->GetAnimated() == type)
				(*it)->Stop();
		}
	}
}

void		AnimatedSpriteModule::Update(float deltaTime)
{
	DisplayModule::Update(deltaTime);

	if (_storyboard.size() > 0)
	{
		for (std::vector<SpriteAnimation*>::iterator it = _storyboard.begin(); it != _storyboard.end();
			++it)
		{
			if ((*it)->isPlaying())
			{
				this->Animation(it, deltaTime);
			}
		}
	}
}

void		AnimatedSpriteModule::Draw()
{
#ifdef USEDIRECT3D
	if (!_texturePlane.Render(DirectXInterface::Instance().GetDeviceContext(), _location))
		LogWriter::Instance().addLogMessage("Cannot draw", E_MsgType::Warning);
#endif
}

void		AnimatedSpriteModule::Destroy()
{
	_texturePlane.Shutdown();
}

Module::Type		AnimatedSpriteModule::GetType()const{ return Module::E_AnimatedSpriteModule; }

void		AnimatedSpriteModule::Animation(std::vector<SpriteAnimation*>::iterator it, float deltaTime)
{
	switch ((*it)->GetAnimated())
	{
	case SpriteAnimation::E_Animated::TranslateX:
		_x += (int)(*it)->Update(deltaTime);
		break;
	case SpriteAnimation::E_Animated::TranslateY:
		_y += (int)(*it)->Update(deltaTime);
		break;
	case SpriteAnimation::E_Animated::Rotation:
		_angle += (*it)->Update(deltaTime);
		break;
	case SpriteAnimation::E_Animated::Width:
		SetCurrentWidth(_initialWidth + (int)(*it)->Update(deltaTime));
		break;
	case SpriteAnimation::E_Animated::Height:
		SetCurrentHeight(_initialHeight + (int)(*it)->Update(deltaTime));
		break;
	}
}

void				AnimatedSpriteModule::Flip()
{
	_texturePlane.Flip();
}

void				AnimatedSpriteModule::SetTexture(Graphic::Texture* tex)
{
	if (!tex)
		LogWriter::Instance().addLogMessage("Sprite Module : Empty Texture setted", E_MsgType::Warning);
	_texturePlane.SetTexture(tex);
}

const Graphic::TexturePlane&		AnimatedSpriteModule::GetTexturePlane()
{
	return _texturePlane;
}

int					AnimatedSpriteModule::GetCurrentWidth()
{
	return _width;
}

int					AnimatedSpriteModule::GetCurrentHeight()
{
	return _height;
}

int					AnimatedSpriteModule::GetInitialWidth()
{
	return _initialWidth;
}

int					AnimatedSpriteModule::GetInitialHeight()
{
	return _initialHeight;
}

int				AnimatedSpriteModule::GetWidth()
{
	return(_width);
}

int				AnimatedSpriteModule::GetHeight()
{
	return(_height);
}

const char*			AnimatedSpriteModule::GetTexturePath()
{
	return(_texturePlane.GetTexturePath());
}

void				AnimatedSpriteModule::SetCurrentWidth(int width)
{
	_texturePlane.SetWidth(width);
	_width = width;
}

void				AnimatedSpriteModule::SetCurrentHeight(int height)
{
	_texturePlane.SetHeight(height);
	_height = height;
}

void				AnimatedSpriteModule::SetInitialWidth(int width)
{
	_initialWidth = width;
	_texturePlane.SetWidth(width);
	_width = width;
}

void				AnimatedSpriteModule::SetInitialHeight(int height)
{
	_initialHeight = height;
	_texturePlane.SetHeight(height);
	_height = height;
}
