#include	"Modules/InteractiveObjectLogicModule.h"
#include	"UI\UIEngine.h"

InteractiveObjectLogicModule::InteractiveObjectLogicModule(GameObject* owner) : LogicModule(owner)
{

}

InteractiveObjectLogicModule::~InteractiveObjectLogicModule()
{

}

void				InteractiveObjectLogicModule::Initialize()
{

}

void				InteractiveObjectLogicModule::Update(float deltaTime)
{

}

void				InteractiveObjectLogicModule::Destroy()
{

}

void				InteractiveObjectLogicModule::OnHit(const GameObject* other)
{

}

void				InteractiveObjectLogicModule::Interact()
{

}

Module::Type		InteractiveObjectLogicModule::GetType()const{ return Module::Type::E_InteractiveObjectLogicModule; }
void				InteractiveObjectLogicModule::SwitchActive(bool SwitchActive){ _SwitchActive = SwitchActive; }
bool				InteractiveObjectLogicModule::IsSwitchActive()const { return _SwitchActive; }

