#include	"Modules\LogicModule.h"
#include	"Modules\ModuleImport.h"

LogicModule::LogicModule(GameObject* owner) : Module(owner)
{
	_hitten = false;
}

LogicModule::~LogicModule()
{

}

void		LogicModule::Initialize()
{

}

void		LogicModule::Update(float deltaTime)
{
}

void		LogicModule::OnHit(const GameObject* other)
{

}

void		LogicModule::Die()
{

}

void		LogicModule::Destroy()
{

}

Module::Type		LogicModule::GetType()const{ return Module::E_LogicModule; }
