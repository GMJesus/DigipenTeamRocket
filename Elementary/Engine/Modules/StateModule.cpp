#include	"Modules/StateModule.h"
#include	"UI/UIEngine.h"
#include	"Game/GameObjectFactory.h"

StateModule::StateModule(GameObject* owner) : Module(owner)
{
	_timerP = -1.f;
	_timerD = -1.f;
	_fireStart = Vec2(0, 0);
	_fireLength = 0;
	_waterStart = Vec2(0, 0);
	_waterLength = 0;
	_ElectricStart = Vec2(0, 0);
	_ElectricLength = 0;
	_state = 2;
	_propagationTime = 0;
	_destroyTime = 0;
	_elecDuration = 0;
}

StateModule::~StateModule()
{

}

void				StateModule::Initialize()
{
	_isPropagate = false;
	_onPropagation = false;
	_elecDuration = 0.2f;
}

void				StateModule::Update(float deltaTime)
{
	if (_timerP > 0.f)
	{
		_timerP -= deltaTime;
		if (_timerP <= 0.f)
		{
			if (_state == 49 || _state == 113 || _state == 62 || _state == 54 || _state == 50 || _state == 114)
			{
				_state -= 32;
				SetState(_state);
				UpdateTexture(_state);
			}
			_isPropagate = true;
		}
	}
	if (_timerD > 0)
	{
		_timerD -= deltaTime;
		if (_timerD <= 0)
			Destroy();
	}
}

void				StateModule::Destroy()
{
	GameObjectFactory::Inst()->DeleteObject(_owner->Id(), false);
}

void	StateModule::OnHit(E_Element elem)
{
	SetState(GetNewState(elem));
	_onPropagation = true;
}

void	StateModule::OnHit(const char state)
{
	SetState(GetNewState(state));
	_onPropagation = true;
}

char	StateModule::GetNewState(const char state)
{
	char newState = _state;

	if (state == 14 || state == 30 || state == 62)
	{
		if (_state == 113 || _state == 81 || _state == 49 || _state == 17)
			newState += 1;
		else if (_state != 14 || _state != 30 || _state != 62 || _state != 82 || _state != 114 || _state != 66)
		{
			_timerD = _destroyTime;
			newState += 8;
		}
	}
	if (state == 49 || state == 113 || state == 62 || state == 54 || state == 50 || state == 114)
	{
		if (_state != 49 || _state != 113 || _state != 62 || _state != 54 || _state != 50
			|| _state != 114 || _state != 2 || _state != 6 || _state != 14 || _state != 66 || _state == 17)
			newState += 32;
	}
	if (newState != _state)
		UpdateTexture(newState);
	return newState;
}

char	StateModule::GetNewState(E_Element element)
{
	char state = _state;

	if (element == Fire)
		state = FireGetNewState();
	if (element == Water)
		state = WaterGetNewState();
	if (element == Electricity)
		state = ElectricityGetNewState();
	if (state != _state)
		UpdateTexture(state);
	return state;
}

void	StateModule::UpdateTexture(char state)
{
	if (_owner->displayModule)
	{
		SpriteSheetAnimationModule* mod = static_cast<SpriteSheetAnimationModule*>(_owner->displayModule);
		if (state == 14 || state == 30 || state == 62)
			mod->SetStartingPos(Vec2(1, 0));
		else if (state == 49 || state == 113 || state == 62 || state == 54 || state == 50 || state == 114 || state == 48)
			mod->SetStartingPos(Vec2(3, 0));
		else if (state % 2 == 1)
			mod->SetStartingPos(Vec2(2, 0));
		else
			mod->SetStartingPos(Vec2(0, 0));
	}
}

char	StateModule::FireGetNewState()const
{
	char state = _state;

	if (state % 2 == 1)
		state += 1;
	else if (state == 14 || state == 30 || state == 62 || state == 81 || state == 82 || state == 113 || state == 114 || state == 66)
		; // Do Nothing
	else if (state == 6 || state == 22 || state == 54)
		state += 8;
	return state;
}

char	StateModule::WaterGetNewState()const
{
	char state = _state;

	if (state % 2 == 1)
		return state;
	else if (state == 14 || state == 30 || state == 62)
		state -= 8;
	else if (state == 2 || state == 6 || state == 18 || state == 22 || state == 54 || state == 50 || state == 82 || state == 114 || state == 66)
		state -= 1;
	return state;
}

char	StateModule::ElectricityGetNewState()const
{
	char state = _state;

	if (state == 30 || state == 18 || state == 22 || state == 82 || state == 17 || state == 81 || state == 16)
		state += 32;
	else if (state % 2 == 1)
		state += 32;
	else
		return state;
	return state;
}

void			StateModule::SetState(const char state)
{
	if (state == 14 || state == 30 || state == 62)
	{
		_isBurning = true;
		if (!_onPropagation)
		{
			_timerP = _propagationTime;
			_timerD = _destroyTime;
		}
	}
	else if (state == 17 || state == 49 || state == 113 || state == 81 || state == 2 || state == 6 || state == 18
		|| state == 22 || state == 54 || state == 50 || state == 82 || state == 114 || state == 66 || state == 48 || state == 0)
	{
		_isBurning = false;
		_timerP = -1.f;
		_timerD = -1.f;
	}
	if (state == 49 || state == 113 || state == 62 || state == 54 || state == 50 || state == 114 || state == 48)
	{
		_isElec = true;
		_timerP = _elecDuration;
	}
	else if (state == 17 || state == 81 || state == 2 || state == 6 || state == 14
		|| state == 30 || state == 18 || state == 22 || state == 82 || state == 66 || state == 16 || state == 0)
	{
		_isElec = false;
	}
	//UIEngine::UI::Instance().PrintMessage(to_string(_isElec));
	_state = state;
}

bool			StateModule::IsBurning()const { return _isBurning; }
bool			StateModule::IsElec()const { return _isElec; }
bool			StateModule::IsPropagate()const { return _isPropagate; }
bool			StateModule::OnPropagation()const { return _onPropagation; }

char			StateModule::GetState()const { return _state; }

void			StateModule::SetPropagationTime(float propagationTime){ _propagationTime = propagationTime; }
float			StateModule::GetPropagationTime()const { return _propagationTime; }

void			StateModule::SetDestroyTime(float propagationTime){ _destroyTime = propagationTime; }
float			StateModule::GetDestroyTime()const { return _destroyTime; }

void			StateModule::SetFireStart(const Vec2& fireStart){ _fireStart = fireStart; }
const Vec2&		StateModule::GetFireStart()const { return _fireStart; }

void			StateModule::SetFireLength(int fireLength){ _fireLength = fireLength; }
int				StateModule::GetFireLength()const { return _fireLength; }

void			StateModule::SetWaterStart(const Vec2& waterStart){ _waterStart = waterStart; }
const Vec2&		StateModule::GetWaterStart()const { return _waterStart; }

void			StateModule::SetWaterLength(int waterLength){ _waterLength = waterLength; }
int				StateModule::GetWaterLength()const { return _waterLength; }

void			StateModule::SetElectricStart(const Vec2& ElectricStart){ _ElectricStart = ElectricStart; }
const			Vec2&	StateModule::GetElectricStart()const { return _ElectricStart; }

void			StateModule::SetElectricLength(int ElectricLength){ _ElectricLength = ElectricLength; }
int				StateModule::GetElectricLength()const { return _ElectricLength; }

Module::Type	StateModule::GetType()const{ return Module::Type::E_StateModule; }
