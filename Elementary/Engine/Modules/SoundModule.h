/**
* \author	Alexy Durand
*/
#ifndef SOUNDMODULE_H_
# define	SOUNDMODULE_H_

#include	"Modules\Module.h"
#include	"Sounds\SoundManager.h"

class SoundModule : public Module
{
public:
	SoundModule(GameObject* owner);
	~SoundModule();

	void	Initialize();
	void	Update(float deltaTime);
	void	Destroy();
	Module::Type	GetType()const;

	const std::string&	GetPath();
	float				Getvolume();
	bool				IsLoopSound();

	void				SetPath(const std::string& path);
	void				SetVolume(float volume);
	void				SetLoop(bool loop);

	void				Play();
	void				Pause();
	void				Resume();

protected:
	//@ModuleCreator BindableProperty
	std::string		_path;
	//@ModuleCreator BindableProperty
	bool			_loop;
	//@ModuleCreator BindableProperty
	float			_volume;

	SoundClass		_sound;
	FMOD::Channel	*_channel;
};

#endif // !SOUNDMODULE_H_
