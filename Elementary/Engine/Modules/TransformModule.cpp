#include	"Modules\TransformModule.h"
#include	"Game/GameObjectFactory.h"

TransformModule::TransformModule(GameObject* owner) : Module(owner),
_parent(NULL)
{
	_position.x = 0.f;
	_position.y = 0.f;
	_angle = 0.f;
	_scale = 1.f;
}

TransformModule::~TransformModule(){}

void	TransformModule::Initialize(){}

void	TransformModule::Update(float deltaTime){}

void	TransformModule::Destroy()
{
	_childs.clear();
}

void	TransformModule::Translate(const Vec2& translation)
{
	_position += translation;
}

void	TransformModule::SetPosition(const Vec2& pos)
{
	_position = pos;
}

void	TransformModule::SetRotation(double angle)
{
	//Ensure that the value is between 0 and 360
	if (angle >= 360.0)
	{
		while (angle >= 360.0)
			angle -= 360.0;
	}
	if (angle < 0.0)
	{
		while (angle < 0.0)
			angle += 360.0;
	}

	_angle = angle;
}

Vec2	TransformModule::Position()const
{
	Vec2 ret = _position;
	if (_parent)
		ret += _parent->transformModule->Position();
	return ret;
}

const Vec2&	TransformModule::LocalPosition()const
{
	return _position;
}

double	TransformModule::Rotation()const
{
	double ret = _angle;
	if (_parent)
		ret += _parent->transformModule->Rotation();
	return ret;
}

double	TransformModule::LocalRotation()const
{
	return _angle;
}

float	TransformModule::Scale()const
{
	float ret = _scale;
	if (_parent)
		ret += _parent->transformModule->Scale();
	return ret;
}

float	TransformModule::LocalScale()const
{
	return _scale;
}

void	TransformModule::Scale(float scale)
{
	_scale = scale;
}

void	TransformModule::ParentTo(const std::string& parentName)
{
	_parent = GameObjectFactory::Inst()->GetObjectByName(parentName);
	if (_parent)
		_parent->transformModule->AddChild(_owner);
}

void	TransformModule::ParentTo(GameObject* parent)
{
	_parent = parent;
	if (_parent && _parent->transformModule)
		_parent->transformModule->AddChild(_owner);
}

void	TransformModule::AddChild(GameObject* child)
{
	_childs.push_back(child);
}

void	TransformModule::RemoveChild(GameObject* toRemove)
{
	std::list<GameObject*>::iterator it;
	for (it = _childs.begin(); it != _childs.end(); ++it)
	{
		if (*it == toRemove)
			break;
	}
	if (it != _childs.end())
	{
		_childs.erase(it);
		toRemove->transformModule->ClearParent();
	}
}

void	TransformModule::ClearParent()
{
	if (_parent)
		_parent->transformModule->RemoveChild(_owner);
	_parent = NULL;
}

const std::list<GameObject*>&	TransformModule::GetChilds()const
{
	return _childs;
}

GameObject*	TransformModule::GetParent()const
{
	return _parent;
}

D3DXMATRIX	TransformModule::GetLocalMatrix()const
{
	double radAngle = M_PI * _angle / 180.f;
	D3DXMATRIX scaleMatrix = D3DXMATRIX(
		_scale, 0.f, 0.f, 0.f,
		0.f, _scale, 0.f, 0.f,
		0.f, 0.f, 1.f, 0.f,
		0.f, 0.f, 0.f, 1.f
		);

	//Rotate around the Z-axys
	D3DXMATRIX rotationMatrix = D3DXMATRIX(
		cos(radAngle),	sin(radAngle),	0.f, 0.f,
		sin(radAngle) * -1, cos(radAngle),	0.f, 0.f,
		0.f,			0.f,			1.f, 0.f,
		0.f,			0.f,			0.f, 1.f
		);

	//Rotate around the Y-axys
	//D3DXMATRIX rotationMatrix = D3DXMATRIX(
	//	cos(radAngle),	0.f, asin(radAngle),	0.f,
	//	0.f,			1.f, 0.f,				0.f,
	//	sin(radAngle),	0.f, cos(radAngle),		0.f,
	//	0.f,			0.f, 0.f,				1.f
	//	);

	//Rotate around the X-axys
	//D3DXMATRIX rotationMatrix = D3DXMATRIX(
	//	1.f, 0.f,				0.f,			0.f,
	//	0.f, cos(radAngle),		sin(radAngle),	0.f,
	//	0.f, asin(radAngle),	cos(radAngle),	0.f,
	//	0.f, 0.f,				0.f,			1.f
	//	);
	
	D3DXMATRIX translationMatrix = D3DXMATRIX(
		1.f, 0.f, 0.f, 0.f,
		0.f, 1.f, 0.f, 0.f,
		0.f, 0.f, 1.f, 0.f,
		_position.x, _position.y, 0.f, 1.f
		);
	D3DXMATRIX result;
	result = scaleMatrix;
	result *= rotationMatrix;
	result *= translationMatrix;
	return result;
}

D3DXMATRIX	TransformModule::GetWorldMatrix()const
{
	if (_parent)
		return _parent->transformModule->GetWorldMatrix() * GetLocalMatrix();
	else
		return GetLocalMatrix();
}

Module::Type		TransformModule::GetType()const{ return Module::E_TransformModule; }
