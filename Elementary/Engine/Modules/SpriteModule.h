#ifndef		SPRITEMODULE_H_
# define	SPRITEMODULE_H_

#include	"Modules\ModuleImport.h"
#include	"Graphics\DirectXInterface.h"
#include	"Graphics\TexturePlane.h"
#include	"Graphics\GraphicEngine.h"
#include	"Graphics\Camera.h"
#include	"Utils\EngineParameters.h"

class SpriteModule : public DisplayModule
{
public:
	SpriteModule(GameObject* owner);
	~SpriteModule();

	void	Initialize();
	void	Update(float deltaTime);
	void	Draw();
	void	Destroy();
	void	Flip();
	Module::Type	GetType()const;

	void	Shutdown();

	const Graphic::TexturePlane&	GetTexturePlane();

	int					GetWidth();
	int					GetHeight();

	void				SetWidth(int width);
	void				SumWidth(int width);
	void				SetHeight(int height);
	void				SumHeight(int height);
	void				SetTexture(Graphic::Texture* tex);
	const char*			GetTexturePath();

protected:
	//@ModuleCreator BindableProperty
	int					_width;
	//@ModuleCreator BindableProperty
	int					_height;
	Graphic::TexturePlane		_texturePlane;
};
#endif		/* !SPRITEMODULE_H_ */