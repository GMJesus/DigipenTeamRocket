/**
* \author	Alexy Durand
*/

#include	"Modules\SoundModule.h"

SoundModule::SoundModule(GameObject* owner) : Module(owner)
{
	_sound = NULL;
	_channel = NULL;
	_loop = false;
}

SoundModule::~SoundModule()
{
	
}

void	SoundModule::Initialize()
{

}

void	SoundModule::Update(float deltaTime)
{

}

void	SoundModule::Destroy()
{
	SoundManager::Instance().releaseSound(_sound);
	SoundManager::Instance().releaseChannel(_channel);
}

void	SoundModule::SetPath(const std::string& path)
{
	_path = path;
	if (_sound)
		SoundManager::Instance().releaseSound(_sound);
	SoundManager::Instance().createSound(&_sound, _path.c_str());
}

void	SoundModule::Play()
{
	SoundManager::Instance().playSound(_sound, _channel, _loop);
}

void	SoundModule::Pause()
{
	SoundManager::Instance().pauseChannel(_channel);
}

void	SoundModule::Resume()
{
	SoundManager::Instance().resumeChannel(_channel);
}

void				SoundModule::SetVolume(float volume)
{ 
	_volume = volume; 
	_channel->setVolume(_volume);
}

const std::string&	SoundModule::GetPath(){ return _path; }
float				SoundModule::Getvolume(){ return _volume; }
bool				SoundModule::IsLoopSound(){ return _loop; }
Module::Type		SoundModule::GetType()const{ return Module::E_SoundModule; }


void				SoundModule::SetLoop(bool loop){ _loop = loop; }
