#ifndef DISPLAYMODULE_H_
# define DISPLAYMODULE_H_

#define	_USE_MATH_DEFINES

#include <math.h>

#include	"Modules\Module.h"
#include	"Graphics/DirectXInterface.h"


/**
* \brief	Abstract class for Module that will display Graphic stuff
*/
class DisplayModule : public Module
{
public:
	DisplayModule(GameObject* owner);
	~DisplayModule();

	/**
	* \brief	Draw the object
	*/
	virtual void	Draw() = 0;

	void			Update(float deltaTime);
	bool			IsVisible();
	void			SetVisibility(bool visible);
	void			SetLayer(int layer);
	int				GetLayer() const;
	virtual Module::Type	GetType()const = 0;
	/**
	* \brief	Flip the stored texture along the X axys
	*/
	virtual	void	Flip();

	void			SetX(int x);
	void			SetY(int y);
	void			SetAngle(double angle);
	int				GetX()const;
	int				GetY()const;
	double			GetAngle()const;

protected:
	//@ModuleCreator BindableProperty
	bool			_isVisible;
	//@ModuleCreator BindableProperty
	int				_layer;
	//@ModuleCreator BindableProperty
	int				_x;
	//@ModuleCreator BindableProperty
	int				_y;
	//@ModuleCreator BindableProperty
	double			_angle;

#ifdef USEDIRECT3D
	D3DXMATRIX		_location;
#endif
};

#endif // !DISPLAYMODULE_H_
