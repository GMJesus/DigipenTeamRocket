#include	"Modules/DoorLogicModule.h"
#include	"UI\UIEngine.h"

DoorLogicModule::DoorLogicModule(GameObject* owner) : InteractiveObjectLogicModule(owner)
{
	_isOpen = false;
}

DoorLogicModule::~DoorLogicModule()
{

}

void				DoorLogicModule::Initialize()
{

}

void				DoorLogicModule::Update(float deltaTime)
{

}

void				DoorLogicModule::Destroy()
{

}

void				DoorLogicModule::OnHit(const GameObject* other)
{

}

void				DoorLogicModule::Interact()
{
	_isOpen = !_isOpen;
	_owner->boxCollisionModule->SetCollisionEnabled(!_isOpen);
	if (_owner->displayModule)
	{
		SpriteSheetAnimationModule* mod = static_cast<SpriteSheetAnimationModule*>(_owner->displayModule);
		if (mod)
			mod->SetStartingPos(Vec2(!_isOpen, 0));
	}

}

Module::Type	DoorLogicModule::GetType()const{ return Module::Type::E_DoorLogicModule; }
void			DoorLogicModule::IsOpen(bool isOpen){ _isOpen = isOpen; }
bool			DoorLogicModule::IsIsOpen()const { return _isOpen; }

