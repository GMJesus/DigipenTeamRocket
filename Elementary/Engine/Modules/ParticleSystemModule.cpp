/**
* \author	Emeric Caramanna
*/

#include "ParticleSystemModule.h"


ParticleSystemModule::ParticleSystemModule(GameObject* owner) : DisplayModule(owner)
{
	_duration = -1;
	_isPlaying = false;
	_gravityEnabled = false;
	_spawningTime = 1;
	_delta = 0;
	_spawningRate = 1;
	_lifeTime = 1;
	_angle = 0;
	_pWidth = 10;
	_pHeight = 10;
	_initialVelocity.x = 0;
	_initialVelocity.y = 1;
	_texture = NULL;
}


ParticleSystemModule::~ParticleSystemModule()
{
}

void	ParticleSystemModule::Initialize(){}

void	ParticleSystemModule::Explode()
{
	int i = _spawningRate - _currentParticles.size();
	while (i < 0)
	{
		_currentParticles[_currentParticles.size() - 1]->Destroy();
		_currentParticles.pop_back();
		i = _spawningRate - _currentParticles.size();
	}
	if (_owner->transformModule)
	{
		_position.x = _location._41;
		_position.y = _location._42;
	}
	i = _spawningRate - _currentParticles.size();
	while (i > 0)
	{
		_currentParticles.push_back(new Particle(_lifeTime, _texture, _rVelocity, _position, _pWidth, _pHeight, _gravityEnabled));
		i = _spawningRate - _currentParticles.size();
	}
	i = 0;
	float rAngle;
	while (i < _currentParticles.size())
	{
		if (_angle != 0)
		{
			rAngle = RandomF(_angle / 2, -1 * _angle / 2);
			_rVelocity = RotateVec(_initialVelocity, rAngle);
		}
		_currentParticles[i]->Launch(_lifeTime, _position, Vec2(1,1));
		i++;
	}
}

void	ParticleSystemModule::Update(float deltaTime)
{
	DisplayModule::Update(deltaTime);
	if (_texture)
	{
		if (_isPlaying)
		{
			_delta += deltaTime;
			if (_delta >= _spawningTime)
			{

				_position.x = _location._41;
				_position.y = _location._42;
				UIEngine::UI::Instance().PrintMessage(std::to_string(_position.x));
				UIEngine::UI::Instance().PrintMessage(std::to_string(_position.y));
				while (_delta >= _spawningTime)
				{
					_rVelocity = _initialVelocity;
					if (_angle != 0)
					{
						float rAngle = RandomF(_angle / 2, -1 * _angle / 2);
						_rVelocity = RotateVec(_rVelocity, rAngle);
					}
					if (_currentParticles.size() < (_spawningRate * _lifeTime) + 1)
					{
						_currentParticles.push_back(new Particle(_lifeTime, _texture, _rVelocity, _position, _pWidth, _pHeight, _gravityEnabled));
					}
					else
					{
						Particle * dead = GetDeadParticle();
						if (dead)
						{
							dead->Launch(_lifeTime, _position, _rVelocity);
						}
					}
					_delta -= _spawningTime;
				}
				_delta = 0;
			}
		}

		for (std::vector<Particle*>::iterator it = _currentParticles.begin(); it != _currentParticles.end(); ++it)
		{
			(*it)->Update(deltaTime);
		}
		if (_duration > 0)
		{
			_duration -= deltaTime;
			if (_duration <= 0)
			{
				_isPlaying = false;
				GameObjectFactory::Inst()->DeleteObject(_owner->Id());
			}
		}
	}
}

Particle	*ParticleSystemModule::GetDeadParticle()
{
	for (int i = 0; i < _currentParticles.size(); i++)
	{
		if (!_currentParticles[i]->IsAlive())
			return _currentParticles[i];
	}
	return NULL;
}

Vec2	ParticleSystemModule::RotateVec(Vec2 vector, float angle)
{
	Vec2 ret;

	angle = angle * M_PI / 180.0f;
	float sina = sin((double)angle);
	float cosa = cos((double)angle);

	ret.x = (cosa * vector.x) - (sina * vector.y);
	ret.y = (sina * vector.x) + (cosa * vector.y);
	return ret;
}

float	ParticleSystemModule::RandomF(float min, float max)
{
	float random = ((float)rand()) / (float)RAND_MAX;
	float diff = max - min;
	float r = random * diff;
	return min + r;
}

void	ParticleSystemModule::Draw()
{

		for (int i = 0; i < _currentParticles.size(); i++)
		{
			//UIEngine::UI::Instance().PrintMessage("Draw");
			_currentParticles[i]->Draw();
		}
}

void	ParticleSystemModule::Destroy()
{
	for (int i = 0; i < _currentParticles.size(); i++)
	{
		_currentParticles[i]->Destroy();
	}
	_currentParticles.clear();
	if (_texture)
		_texture->Shutdown();
}


Module::Type	ParticleSystemModule::GetType()const
{ 
	return Module::Type::E_ParticleSystemModule; 
}

float	ParticleSystemModule::GetSpawningRate()
{ 
	return _spawningRate; 
}

float	ParticleSystemModule::GetLifeTime()
{ 
	return _lifeTime; 
}

int		ParticleSystemModule::GetParticlesWidth()
{ 
	return _pWidth; 
}

int		ParticleSystemModule::GetParticlesHeight()
{ 
	return _pHeight; 
}

Graphic::TexturePlane*	ParticleSystemModule::GetTexture()
{ 
	if (_texture)
		return _texture; 
	return NULL;
}

Vec2	ParticleSystemModule::GetInitialVelocity()
{ 
	return _initialVelocity; 
}

std::vector<Particle*> ParticleSystemModule::GetCurrentParticles()
{ 
	return _currentParticles; 
}

bool	ParticleSystemModule::IsGravityEnabled()
{ 
	return _gravityEnabled; 
}

void	ParticleSystemModule::SetGravityEnabled(bool gravity)
{
	_gravityEnabled = gravity;
}

void	ParticleSystemModule::SetSpawningRate(float spawningRate)
{
	_spawningRate = spawningRate;
	_spawningTime = 1 / _spawningRate;
}

void	ParticleSystemModule::SetLifeTime(float lifeTime)
{
	_lifeTime = lifeTime;
}

void	ParticleSystemModule::SetParticlesWidth(int pWidth)
{
	_pWidth = pWidth;
}

void	ParticleSystemModule::SetParticlesHeight(int pHeight)
{
	_pHeight = pHeight;
}

void	ParticleSystemModule::SetParticlesTexture(Graphic::Texture* texture)
{

	_texture = new Graphic::TexturePlane();
#ifdef USEDIRECT3D
	_texture->Initialize(DirectXInterface::Instance().GetDevice());
#endif
	if (!texture)
		LogWriter::Instance().addLogMessage("Particle System Module : Empty Texture setted", E_MsgType::Warning);
	_texture->SetTexture(texture);
}

float	ParticleSystemModule::GetAngle()
{
	return _angle;
}

void	ParticleSystemModule::SetAngle(float angle)
{
	_angle = angle;
}

void	ParticleSystemModule::SetInitialVelocity(Vec2 initialVelocity)
{
	_initialVelocity = initialVelocity;
}

void	ParticleSystemModule::Play()
{
	_delta = 0;
	_isPlaying = true;
}

void	ParticleSystemModule::Pause()
{
	_delta = 0;
	_isPlaying = false;
}

void	ParticleSystemModule::Stop()
{
	_delta = 0;
	_isPlaying = false;
	for (int i = 0; i < _currentParticles.size(); i++)
	{
		_currentParticles[i]->Destroy();
	}
	_currentParticles.clear();
}

void	ParticleSystemModule::PlayFor(float duration)
{
	_isPlaying = true;
	_duration = duration;
}

void	ParticleSystemModule::SetPlaying(bool play)
{
	_isPlaying = play;
}

bool	ParticleSystemModule::IsPlaying()
{
	return _isPlaying;
}