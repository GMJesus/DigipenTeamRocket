/**
* \author	Aymeric Lambolez
*/

#include "Modules\LifeModule.h"
#include "Modules\LogicModule.h"
#include "UI\UIEngine.h"

LifeModule::LifeModule(GameObject* owner) : Module(owner)
{
	_health = 0;
	_maxHealth = 0;
	_isImmortal = false;
}

LifeModule::~LifeModule()
{

}

void		LifeModule::Initialize()
{

}

void		LifeModule::Update(float deltaTime)
{

}

void		LifeModule::Destroy()
{

}

int			LifeModule::GetHealth(){ return _health; }
int			LifeModule::GetMaxHealth(){ return _maxHealth; }
bool		LifeModule::IsImmortal(){ return _isImmortal; }
Module::Type		LifeModule::GetType()const{ return Module::E_LifeModule; }

void		LifeModule::SetHealth(int health)
{
	_health = health;
	if (_health <= 0 && _owner->logicModule && !_isImmortal)
		_owner->logicModule->Die();
}
void		LifeModule::SetMaxHealth(int maxHealth){ _maxHealth = maxHealth <= 1 ? 1 : maxHealth; }
void		LifeModule::SetImmortal(bool immortal){ _isImmortal = immortal; }
