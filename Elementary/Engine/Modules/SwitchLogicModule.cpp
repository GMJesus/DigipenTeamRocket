#include	"Modules/SwitchLogicModule.h"

SwitchLogicModule::SwitchLogicModule(GameObject* owner) : InteractiveObjectLogicModule(owner)
{
	_actualTimer = -1.f;
	_timeToRespond = 0.5f;
}

SwitchLogicModule::~SwitchLogicModule()
{

}

void				SwitchLogicModule::Initialize()
{

}

void				SwitchLogicModule::Update(float deltaTime)
{
	if (_actualTimer >= 0 && _actualTimer <= _timeToRespond)
		_actualTimer += deltaTime;
	else if (_actualTimer > _timeToRespond)
		_actualTimer = -1.f;
}

void				SwitchLogicModule::Destroy()
{

}

void				SwitchLogicModule::OnHit(const GameObject* other)
{
	if (other->stateModule)
	{
		if (other->stateModule->IsElec() == true)
			Interact();
	}
	else if (static_cast<ProjLogicModule*>(other->logicModule) && other->IsDead() == false)
	{
		UIEngine::UI::Instance().PrintMessage(_objectToInteract);
		if (static_cast<ProjLogicModule*>(other->logicModule)->GetElem() == Electricity && other->IsDead() == false)
			Interact();
	}
}

void				SwitchLogicModule::Interact()
{
	if (_actualTimer >= 0.f)
		return;
	_actualTimer = 0.f;
	GameObject* obj = GameObjectFactory::Inst()->GetObjectByName(_objectToInteract);
	if (obj && static_cast<InteractiveObjectLogicModule*>(obj->logicModule))
	{
		static_cast<InteractiveObjectLogicModule*>(obj->logicModule)->Interact();
	}
	if (_owner->soundModule)
	{
		_owner->soundModule->Play();
	}
	if (_owner->displayModule && static_cast<SpriteSheetAnimationModule*>(_owner->displayModule))
	{
		static_cast<SpriteSheetAnimationModule*>(_owner->displayModule)->SetStartingPos(Vec2(!_SwitchActive, 0));
	}
	_SwitchActive = !_SwitchActive;
}

Module::Type	SwitchLogicModule::GetType()const{ return Module::Type::E_SwitchLogicModule; }
void				SwitchLogicModule::SetObjectToInteract(const std::string& objectToInteract){ _objectToInteract = objectToInteract; }
const std::string&	SwitchLogicModule::GetObjectToInteract()const { return _objectToInteract; }

