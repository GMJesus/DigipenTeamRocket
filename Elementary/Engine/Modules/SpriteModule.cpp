#include "Modules\SpriteModule.h"

SpriteModule::SpriteModule(GameObject* owner) : DisplayModule(owner)
{
}

SpriteModule::~SpriteModule()
{

}

void		SpriteModule::Initialize()
{
#ifdef USEDIRECT3D
	_texturePlane.Initialize(DirectXInterface::Instance().GetDevice());
#endif
	_angle = 0.f;
	_x = _y = 0;
	_location = D3DXMATRIX(
		1.f, 0.f, 0.f, 0.f,
		0.f, 1.f, 0.f, 0.f,
		0.f, 0.f, 1.f, 0.f,
		0.f, 0.f, 0.f, 1.f
		);
	_width = 100;
	_height = 100;
}

void		SpriteModule::Update(float deltaTime)
{
	DisplayModule::Update(deltaTime);
}

void		SpriteModule::Destroy()
{
	Shutdown();
}

void		SpriteModule::Shutdown()
{
	_texturePlane.Shutdown();
}

void		SpriteModule::Draw()
{
#ifdef USEDIRECT3D
	if (!_texturePlane.Render(DirectXInterface::Instance().GetDeviceContext(), _location))
		LogWriter::Instance().addLogMessage("Cannot draw " + _owner->GetName(), E_MsgType::Warning);
#endif
}

void				SpriteModule::SetTexture(Graphic::Texture* tex)
{
	if (!tex)
		LogWriter::Instance().addLogMessage("Sprite Module : Empty Texture setted", E_MsgType::Warning);
	_texturePlane.SetTexture(tex);
}

const char*				SpriteModule::GetTexturePath()
{
	return(_texturePlane.GetTexturePath());
}

const Graphic::TexturePlane&		SpriteModule::GetTexturePlane()
{
	return _texturePlane;
}

int					SpriteModule::GetWidth()
{
	return _width;
}

int					SpriteModule::GetHeight()
{
	return _height;
}

void				SpriteModule::Flip()
{
	_texturePlane.Flip();
}

Module::Type		SpriteModule::GetType()const{ return Module::E_SpriteModule; }

void				SpriteModule::SetWidth(int width)
{
	_texturePlane.SetWidth(width);
	_width = width;
	_owner->UpdateDebugWidth();
}

void				SpriteModule::SetHeight(int height)
{
	_texturePlane.SetHeight(height);
	_height = height;
	_owner->UpdateDebugHeight();
}

void				SpriteModule::SumHeight(int height)
{
	_texturePlane.SetHeight(GetHeight() + height);
	_height += height;
}

void				SpriteModule::SumWidth(int width)
{
	_texturePlane.SetHeight(GetWidth() + width);
	_width += width;
}
