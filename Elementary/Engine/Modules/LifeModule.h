#ifndef LIFEMODULE_H_
#define LIFEMODULE_H_

#include "Modules\Module.h"

/**
* \brief	Module for Life datas
*			When life is equal or less than 0, will call the Die function of the Logic Module of the object, if there is one
*/
class LifeModule : public Module
{
public:
	LifeModule(GameObject* owner);
	~LifeModule();

	void	Initialize();
	void	Update(float deltaTime);
	void	Destroy();
	Module::Type	GetType()const;

	int		GetHealth();
	int		GetMaxHealth();
	bool	IsImmortal();

	void	SetHealth(int health);
	void	SetMaxHealth(int maxHealth);
	void	SetImmortal(bool immortal);

protected:
	//@ModuleCreator BindableProperty
	int		_health;
	//@ModuleCreator BindableProperty
	int		_maxHealth;
	//@ModuleCreator BindableProperty
	bool	_isImmortal;
};

#endif // !LIFEMODULE_H_
