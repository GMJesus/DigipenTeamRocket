#include	"Modules\BoxCollisionModule.h"
#include	"Utils\EngineParameters.h"


BoxCollisionModule::BoxCollisionModule(GameObject* owner) : Module(owner)
{
	aabb = NULL;
	obb = NULL;
	_contact = NULL;
	_isColliding = false;
	_generateHitEvents = true;
	_collisionEnabled = true;
}

BoxCollisionModule::~BoxCollisionModule()
{

}

void		BoxCollisionModule::Initialize()
{
	_contact = new Contact();
}

void		BoxCollisionModule::AddObjectToIgnore(unsigned int id, bool forceOtherToIgnore)
{
	_toIgnore.push_back(id);
	GameObject* other = GameObjectFactory::Inst()->GetObjectById(id);
	if (forceOtherToIgnore && other->boxCollisionModule)
		other->boxCollisionModule->AddObjectToIgnore(_owner->Id(), false);
}

void		BoxCollisionModule::RemoveObjectToIgnore(unsigned int id, bool forceOtherToIgnore)
{
	_toIgnore.remove(id);
	GameObject* other = GameObjectFactory::Inst()->GetObjectById(id);
	if (forceOtherToIgnore && other->boxCollisionModule)
		other->boxCollisionModule->RemoveObjectToIgnore(_owner->Id(), false);
}

void		BoxCollisionModule::Update(float deltaTime)
{
	if (_owner->transformModule)
	{
		if (_type == AABB && aabb)
		{
			aabb->MoveTo(_owner->transformModule->Position());
			aabb->SetPosition(_owner->transformModule->Position());
		}
		else if (_type == OBB && obb)
		{
			obb->MoveTo(_owner->transformModule->Position());
			if (_owner->transformModule)
			{
				obb->SetWidth(_boxWidth * _owner->transformModule->Scale());
				obb->SetHeight(_boxHeight * _owner->transformModule->Scale());
			}
		}
	}
}

bool		BoxCollisionModule::ContainObjectToIgnore(unsigned int id)const
{
	for (auto it = _toIgnore.begin(); it != _toIgnore.end(); ++it)
		if (id == *it)
			return true;
	return false;
}

void		BoxCollisionModule::IsColliding(const std::list<GameObject*>& Objects)
{
	for (std::list<GameObject*>::const_iterator it = Objects.begin(); it != Objects.end(); ++it)
	{
		if (!(*it)->boxCollisionModule || ContainObjectToIgnore((*it)->Id()))
			continue;
		if ((*it)->Id() != _owner->Id())
		{
			_contact->normal = Vec2(0, 0);
			_contact->penetration = 0;
			if ((*it)->boxCollisionModule->obb->VsOBB(obb, _contact))
			{
				_isColliding = true;
				if (_collisionEnabled && (*it)->boxCollisionModule->CollisionEnabled())
				{
					ResolveColliding((*it));
				}
				if (_generateHitEvents || (*it)->boxCollisionModule->GenerateHitEvents())
				{
					if (_owner->logicModule)
						_owner->logicModule->OnHit((*it));
				}
			}
			else
				_isColliding = false;
		}
	}
	
}

void		BoxCollisionModule::ResolveColliding(const GameObject* other)
{
	if (_owner->physicModule)
	{
		PhysicModule* spm = _owner->physicModule;
		PhysicModule* opm = other->physicModule;

		Vec2 rv = (opm ? opm->GetVelocity() : 0.f) - spm->GetVelocity();
		Vec2 normal = _contact->normal;
		// Calculate relative velocity in terms of the normal direction
		float velAlongNormal = rv.x * normal.x;
		velAlongNormal += rv.y * normal.y;

		// Do not resolve if velocities are separating

		// Calculate restitution
		float e = max(spm->GetAbsorption(), opm ? opm->GetAbsorption() : 0.f);

		// Calculate impulse scalar
		float j = e * velAlongNormal;


		// Apply impulse
		Vec2 impulse = normal;
		impulse *= j;
		//PRINT_ON_SCREEN((impulse * spm->GetInvMass()).ToString());
		Vec2 tmp;
		if (!normal.x)
			tmp = Vec2(spm->GetVelocity().x, 0.f);
		else if (!normal.y)
		{
			tmp = Vec2(0.f, spm->GetVelocity().y);
		}
		spm->SetVelocity(tmp);
		if (spm->GetInvMass())
			spm->AddVelocity(impulse);
		if (spm->GetInvMass() != 0)
			_owner->transformModule->SetPosition(_owner->transformModule->Position() + _contact->normal * (_contact->penetration));
	}

	if (other->stateModule && _owner->stateModule)
	{
		if (other->stateModule->OnPropagation() == false)
		{
			if (_owner->stateModule->IsPropagate() == true)
			{
				other->stateModule->OnHit(_owner->stateModule->GetState());
			}
		}
	}
}

void		BoxCollisionModule::Destroy()
{
	for (auto it = _toIgnore.begin(); it != _toIgnore.end(); ++it)
	{
		unsigned int id = *it;
		GameObject* other = GameObjectFactory::Inst()->GetObjectById(id);
		if (other->boxCollisionModule)
			other->boxCollisionModule->RemoveObjectToIgnore(_owner->Id(), false);
	}
	if (aabb)
		delete(aabb);
	if (obb)
		delete(obb);
}

Vec2				BoxCollisionModule::CalcNormal(const Vec2& pos1, const Vec2& pos2)
{
	float angle = atan2((pos1.x - pos2.x), (pos1.y - pos2.y));
	return Vec2(sin(angle), cos(angle)) * -1;
}


E_CollisionType		BoxCollisionModule::GetType(){ return _type; }
float				BoxCollisionModule::GetBoxWidth(){ return _boxWidth; }
float				BoxCollisionModule::GetBoxHeight(){ return _boxHeight; }
const Vec2			BoxCollisionModule::GetBoxCorner(int nb)const { return obb->GetCorner(nb); }
bool				BoxCollisionModule::GenerateHitEvents(){ return _generateHitEvents; }
bool				BoxCollisionModule::CollisionEnabled(){ return _collisionEnabled; }
Module::Type		BoxCollisionModule::GetType()const{ return Module::E_BoxCollisionModule; }

void				BoxCollisionModule::SetType(E_CollisionType type)
{
	_type = type; 
	switch (_type)
	{
	case AABB:
		if (aabb)
			delete (aabb);
		aabb = new Collision::AABB();
		aabb->Initialize(_owner->transformModule->Position(), _boxWidth, _boxHeight);
	case OBB:
		if (obb)
			delete(obb);
		obb = Collision::CreateOBB(_owner->transformModule->Position(), _boxWidth, _boxHeight, _owner->transformModule->Rotation());
	}
}

bool				BoxCollisionModule::CollideVersusLine(const Vec2& start, const Vec2& end)
{
	if (obb)
		return obb->VsLine(start, end);
	else
		return (false);
}

std::list<std::tuple<const GameObject*, Contact*>>*	BoxCollisionModule::GetCollidingObject(const std::list<GameObject*>& Objects)
{
	std::list<std::tuple<const GameObject*, Contact*>>* collidingObject = new std::list<std::tuple<const GameObject*, Contact*>>;
	for (std::list<GameObject*>::const_iterator it = Objects.begin(); it != Objects.end(); ++it)
	{

		if (!(*it)->boxCollisionModule || ContainObjectToIgnore((*it)->Id()))
			continue;
		if ((*it)->Id() != _owner->Id())
		{
			Contact* test = new Contact();
			test->normal = Vec2(0, 0);
			test->penetration = 0;
			Collision::OBB* tmpObb = new Collision::OBB();
			tmpObb->SetAngle(0);
			tmpObb->SetPos(_owner->transformModule->Position());
			tmpObb->SetWidth(GetBoxWidth() + 1);
			tmpObb->SetHeight(GetBoxHeight() + 1);
			if ((*it)->boxCollisionModule->obb->VsOBB(tmpObb, test))
			{
				if (_collisionEnabled && (*it)->boxCollisionModule->CollisionEnabled())
				{
					collidingObject->push_back(std::make_tuple((*it), test));
				}
			}
			//delete test;
			delete (tmpObb);
		}
	}
	if (collidingObject->size() > 0)
		return collidingObject;
	else
	{
		delete (collidingObject);
		return NULL;
	}
}

void				BoxCollisionModule::SetBoxWidth(float width)
{
	if (_type == AABB && aabb)
		aabb->SetWidth(width);
	else if (_type == OBB && obb)
		obb->SetWidth(width);
	_boxWidth = width; 
#ifdef GAME
	if (GameEngine::engineParameters.debugCollisionBox)
#endif
		_owner->UpdateDebugWidth();
}

void				BoxCollisionModule::SetBoxHeight(float height)
{
	if (_type == AABB && aabb)
		aabb->SetHeight(height);
	else if (_type == OBB && obb)
		obb->SetHeight(height);
	_boxHeight = height;

#ifdef GAME
	if (GameEngine::engineParameters.debugCollisionBox)
#endif
		_owner->UpdateDebugHeight();
}

void				BoxCollisionModule::SetGenerateHitEvents(bool generateHitEvents){ _generateHitEvents = generateHitEvents; }

void				BoxCollisionModule::SetCollisionEnabled(bool collisionEnabled){ _collisionEnabled = collisionEnabled; }
