#include "Module.h"

Module::Module(GameObject* owner)
{
	_owner = owner;
}

Module::~Module()
{

}

//Getters
bool				Module::IsEnable(){return _enable;}
GameObject*			Module::GetOwner(){return _owner;}

//Setters
void				Module::Enable(bool enable){_enable = enable;}

void				Module::Initialize()
{

}

void				Module::Update(float deltaTime)
{

}