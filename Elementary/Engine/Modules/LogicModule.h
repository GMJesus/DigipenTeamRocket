#ifndef LOGICMODULE_H_
#define	LOGICMODULE_H_

#include	"Modules\Module.h"

/**
* \brief	Base class for implementing custom behaviour
*/
class LogicModule : public Module
{
public:
	LogicModule(GameObject* owner);
    virtual ~LogicModule();

    virtual void	Initialize();
    virtual void	Update(float deltaTime);
    virtual void	Destroy();
    virtual Module::Type	GetType()const;

	virtual void	OnHit(const GameObject* other);
	virtual void	Die();

private:
	bool _hitten;
};

#endif // !LOGICMODULE_H_
