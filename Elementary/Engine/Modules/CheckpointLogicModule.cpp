/**
* \author	Aymeric Lambolez
*/

#include	"Modules/CheckpointLogicModule.h"
#include	"Game/Character.h"
#include	"GameEngine/GameEngine.h"

CheckpointLogicModule::CheckpointLogicModule(GameObject* owner) : LogicModule(owner)
{

}

CheckpointLogicModule::~CheckpointLogicModule()
{

}

void				CheckpointLogicModule::Initialize()
{

}

void				CheckpointLogicModule::Update(float deltaTime)
{

}

void				CheckpointLogicModule::Destroy()
{

}

void				CheckpointLogicModule::OnHit(const GameObject* other)
{
#ifdef GAME
	if (other == GameObjectFactory::Inst()->GetPlayer())
	{
		GameEngine::Engine::Instance()->SetSpawningPosition(_owner->transformModule->Position());
	}
#endif
}

Module::Type	CheckpointLogicModule::GetType()const{ return Module::Type::E_CheckpointLogicModule; }
