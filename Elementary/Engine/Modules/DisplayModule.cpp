#include "Modules/DisplayModule.h"
#include "Modules/TransformModule.h"
#include "Graphics/Camera.h"
#include "UI/UIEngine.h"



DisplayModule::DisplayModule(GameObject* owner) : Module(owner)
{
	_layer = 0;
	_location = D3DXMATRIX(
		1.f, 0.f, 0.f, 0.f,
		0.f, 1.f, 0.f, 0.f,
		0.f, 0.f, 1.f, 0.f,
		0.f, 0.f, 0.f, 1.f
		);
	_x = _y = 0;
	_angle = 0;
}

DisplayModule::~DisplayModule()
{

}

void		DisplayModule::Update(float deltaTime)
{
	if (_owner->transformModule)
	{
		_location = _owner->transformModule->GetWorldMatrix();
	}
	else
	{
		Vec2 cameraPosition = Graphic::Camera::Instance()->Get2DPosition();
		_location._41 = cameraPosition.x;
		_location._42 = cameraPosition.y;
	}
	if (_angle)
	{
		float	radAngle = _angle * M_PI / 180.f;
		D3DXMATRIX mat = D3DXMATRIX(
			cos(radAngle), sin(radAngle), 0.f, 0.f,
			sin(radAngle) * -1, cos(radAngle), 0.f, 0.f,
			0.f, 0.f, 1.f, 0.f,
			0.f, 0.f, 0.f, 1.f
			);
		mat *= _location;
		_location = mat;
	}
	_location._41 += (float)(_x / PPU);
	_location._42 += (float)(_y / PPU);
}

bool		DisplayModule::IsVisible(){ return _isVisible; }
void		DisplayModule::SetVisibility(bool visible){ _isVisible = visible; }
int			DisplayModule::GetLayer() const { return _layer; }
void		DisplayModule::SetLayer(int layer){ _layer = layer; }
void		DisplayModule::Flip(){}
void		DisplayModule::SetX(int x){ _x = x; }
void		DisplayModule::SetY(int y){ _y = y; }
void		DisplayModule::SetAngle(double angle){ _angle = angle; }
int			DisplayModule::GetX()const{ return _x; }
int			DisplayModule::GetY()const{ return _y; }
double		DisplayModule::GetAngle()const{ return _angle; }