/**
* \file		 StateModule
* \author    Baptiste DUMAS
* \brief     File with the StateModule that handle interaction between ElementalObject
*/

#ifndef STATEMODULE_H_
#define STATEMODULE_H_

#include	"Modules/Module.h"
//#include	"Game/GameObjectFactory.h"

class GameObjectFactory;

enum E_Element
{
	Fire,
	Water,
	Earth,
	Electricity
};

/**
* \brief	StateModule Class, handle the elemental state of owner
*/
class StateModule : public Module
{
public:
	StateModule(GameObject* owner);
	~StateModule();

	void	Initialize();
	void	Update(float deltaTime);
	void	Destroy();

	/**
	* \brief	Called by Projectile
	* \param elem	Projectile's Element
	*/
	void		OnHit(E_Element elem);
	/**
	* \brief	Called when colliding with another GameObject with a StateModule
	* \param state	Current state of the colliding object
	*/
	void		OnHit(const char state);
	void		Burn();
	void		Conduct();

	/**
	* \brief	return the state's value if an object with a specific state is colliding
	* \param state	State of the supposing object
	* \return	a state
	*/
	char		GetNewState(const char state);
	/**
	* \brief	return the state's value if a projectile with a specific elem is colliding
	* \param elem	Elem of the supposing object
	* \return	a state
	*/
	char		GetNewState(E_Element element);

	char		GetState()const;
	void		SetState(char state);

	float		GetPropagationTime()const;
	void		SetPropagationTime(float propagationTime);

	float		GetDestroyTime()const;
	void		SetDestroyTime(float destroyTime);

	bool		IsBurning()const;
	bool		IsElec()const;
	bool		IsPropagate()const;
	bool		OnPropagation()const;

	const Vec2&	GetFireStart()const;
	void		SetFireStart(const Vec2& fireStart);
	int			GetFireLength()const;
	void		SetFireLength(int fireLength);

	const Vec2&	GetWaterStart()const;
	void		SetWaterStart(const Vec2& waterStart);
	int			GetWaterLength()const;
	void		SetWaterLength(int waterLength);

	const Vec2&	GetElectricStart()const;
	void		SetElectricStart(const Vec2& ElectricStart);
	int			GetElectricLength()const;
	void		SetElectricLength(int ElectricLength);

	Module::Type	GetType()const;

protected:
	//@ModuleCreator BindableProperty
	char		_state;
	//@ModuleCreator BindableProperty
	float		_propagationTime; /// \brief	Time before this object will propagate fire
	//@ModuleCreator BindableProperty
	float		_destroyTime; /// \brief	Time before this object will be destroy by fire
	float		_elecDuration;
	float		_timerP;
	float		_timerD;

	char*		_normalState;

	bool		_isBurning;
	bool		_isElec;
	bool		_isPropagate;
	bool		_onPropagation;

	//@ModuleCreator BindableProperty
	Vec2		_fireStart;
	//@ModuleCreator BindableProperty
	int			_fireLength;
	//@ModuleCreator BindableProperty
	Vec2		_waterStart;
	//@ModuleCreator BindableProperty
	int			_waterLength;
	//@ModuleCreator BindableProperty
	Vec2		_ElectricStart;
	//@ModuleCreator BindableProperty
	int			_ElectricLength;
	//@ModuleCreator BindableProperty

	char		FireGetNewState()const;
	char		WaterGetNewState()const;
	char		ElectricityGetNewState()const;
	void		UpdateTexture(char state);
};

#endif
