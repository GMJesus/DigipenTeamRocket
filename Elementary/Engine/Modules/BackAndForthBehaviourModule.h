/**
* \author	Aymeric Lambolez
*/

#ifndef BACKANDFORTHBEHAVIOURMODULE_H_
#define BACKANDFORTHBEHAVIOURMODULE_H_

#include	"Modules/AIModule.h"

/**
* \brief	Basic AI for object that go back and forward
*			The AI will switch direction once blocked by a wall, or by a hole
*/
class BackAndForthBehaviourModule : public AIModule
{
public:
	BackAndForthBehaviourModule(GameObject* owner);
	~BackAndForthBehaviourModule();

	void	Initialize();

	void	Update(float deltaTime);

	void	Destroy();

	Module::Type	GetType()const;

	float		GetSpeed()const;
	void		SetSpeed(float speed);
	bool		IsIgnoreGroundModification()const;
	void		IgnoreGroundModification(bool ignoreGroundModification);
protected:
	void		ChangeDirection();

	float		_speed;
	/**
	* \brief	If true, will not switch direction once arrived in front of a hole
	*/
	bool		_ignoreGroundModification;
	bool		_goRight;
};

#endif
