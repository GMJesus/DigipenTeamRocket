/**
* \author	Thomas Hannot
*/

#ifndef CHARACTERLOGICMODULE_H_
#define CHARACTERLOGICMODULE_H_

#include	"Modules/Module.h"
#include    "Modules/LogicModule.h"
#include	"Game/GameObjectFactory.h"
#include	"Game/Gun.h"

class Gun;

class CharacterLogicModule : public LogicModule
{
public:
	CharacterLogicModule(GameObject* owner);
    virtual ~CharacterLogicModule();

	virtual void	Initialize();
    virtual void	Update(float deltaTime);
    virtual void	Destroy();
	void			OnHit(const GameObject* other);
    virtual Module::Type	GetType()const;
	void			Die();

    float       GetSpeed() const;
    void        SetSpeed(float val);

protected:
    float   _speed;
	bool	_goRight;


    // jump
    bool    _isJumping;
    float   _currentJumpTime;
    float   _maxJumpTime;
    Vec2    _jumpPower;

    bool    Jump(float deltaTime);
    void    resetJump();

    // movement
	bool    GoUp();
    bool    GoDown();
    bool    GoLeft();
    bool    GoRight();

	void	PutLeftAnimation();
	void	PutRightAnimation();

    // Gun
    Gun*    _gun;
    bool    _clickHold;
    void    Shoot();
};

#endif
