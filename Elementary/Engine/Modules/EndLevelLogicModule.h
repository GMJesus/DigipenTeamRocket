/**
* \author	Emeric Caramanna
*/

#ifndef ENDLEVELLOGICMODULE_H_
#define ENDLEVELLOGICMODULE_H_

#include	"Game/GameObjectFactory.h"
#include	"Modules/LogicModule.h"

class EndLevelLogicModule : public LogicModule
{
public:
	EndLevelLogicModule(GameObject* owner);
	~EndLevelLogicModule();

	void	Initialize();

	void	Update(float deltaTime);

	void	Destroy();

	Module::Type	GetType()const;

	void	OnHit(const GameObject* other);

protected:
	bool _first = true;
};

#endif
