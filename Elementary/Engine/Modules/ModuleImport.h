#ifndef		MODULEIMPORT_H_
# define	MODULEIMPORT_H_

//@ModuleCreator IncludeStart
#include	"InteractivePlatformLogicModule.h"
#include	"EndLevelLogicModule.h"
#include	"CheckpointLogicModule.h"
#include	"DoorLogicModule.h"
#include	"SwitchLogicModule.h"
#include	"InteractiveObjectLogicModule.h"
#include	"BackAndForthBehaviourModule.h"
#include	"StateModule.h"
#include	"GunLogicModule.h"
#include	"ProjLogicModule.h"
#include	"SpriteSheetAnimationModule.h"
#include	"CharacterLogicModule.h"
#include	"AIModule.h"
#include	"AnimatedSpriteModule.h"
#include	"BoxCollisionModule.h"
#include	"LifeModule.h"
#include	"LogicModule.h"
#include	"PhysicModule.h"
#include	"SoundModule.h"
#include	"SpriteModule.h"
#include	"TransformModule.h"
#include	"StateModule.h"
#include	"ParticleSystemModule.h"

#endif		// !MODULEIMPORT_H_
