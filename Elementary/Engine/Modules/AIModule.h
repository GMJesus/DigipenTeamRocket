#ifndef		AIMODULE_H_
# define	AIMODULE_H_

#include	"Modules\Module.h"

/**
* \brief	Base class for AI Module
*/
class AIModule : public Module
{
public:
	AIModule(GameObject* owner);
	~AIModule();

	virtual void	Initialize();
	virtual void	Update(float deltaTime);
	virtual void	Destroy();
	virtual Module::Type	GetType()const;
};

#endif // !AIMODULE_H_
