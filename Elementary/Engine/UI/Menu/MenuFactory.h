/**
* \file      MenuFactory
* \author    Thomas HANNOT
* \brief     Singleton. Handle every menus, their components and their usage.
*
*/

#ifndef __MENUFACTORY_H__
#define __MENUFACTORY_H__

///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include <vector>
#include "UI/Menu/Menu.h"
#include "UI/Text.h"

namespace UIEngine
{
    class MenuFactory
    {
    public:
        /**
        * \brief    Initialize depending class.
        * \param	text	A Text instance to enable the printing.
        * \return   A boolean that let the user knows if the function ended with an error.
        */
        bool Initialize(Text* text);
        /**
        * \brief    Update the menus.
        */
        void Update();

        /**
        * \brief    Add a Menu to manage.
        * \param	menu	The Menu to add.
        * \return   The id of the added Menu.
        */
        int AddMenu(Menu* menu);
        /**
        * \brief    Add an Image to the corresponding Menu.
        * \param	image	The Image to add.
        * \param	menuId	The id of which Menu to add it.
        */
        void AddImage(Image* image, int menuId);
        /**
        * \brief    Add a Button to the corresponding Menu and set its callback to the corresponding id.
        * \param	button	    The Button to add.
        * \param	menuId	    The id of which Menu to add it.
        * \param	callbackId	The id of which callback to set.
        */
        void AddButton(Button* button, int menuId, int callbackId);
        /**
        * \brief    Add a DropDown to the corresponding Menu and set its callback to the corresponding id.
        * \param	dropDown	The DropDown to add.
        * \param	menuId	    The id of which Menu to add it.
        * \param	callbackId	The id of which callback to set.
        */
        void AddDropDown(DropDown* dropDown, int menuId, int callbackId);
        /**
        * \brief    Add an Image to the corresponding Menu.
        * \param	image	The Image to add.
        * \param	menuId	The id of which Menu to add it.
        */
        void AddSwitch(UISwitch* uiswitch, int menuId);

        /**
        * \brief    Enable the corresponding Menu, desactivate the precedent.
        * \param	menuId	The id of which Menu to enable.
        */
        void ActivateMenu(int menuId);
        /**
        * \brief    Disable the current Menu, reactivate the last one
        */
        void GoLastMenu();
        /**
        * \brief    Disable all of the Menu in the history list.
        */
        void QuitMenu();

        /**
        * \brief    Access to the class form any files.
        * \return   Instance on the singleton of the class.
        */
        Menu* GetMenu(int position);

        /**
        * \brief    Access to the class form any files.
        * \return   Instance on the singleton of the class.
        */
        static MenuFactory* Instance();

        /// \brief List of usable callbacks for button (function needed: Initialize())
        vector<void(*)(vector<int>)> callbackButtons;
        /// \brief List of usable callbacks for dropdown (function needed: Initialize())
        vector<void(*)(char*)> callbackDropDowns;
    protected:
        /**
        * \brief    Constructor.
        */
        MenuFactory();
        /**
        * \brief    Destructor.
        */
        ~MenuFactory();

        /// \brief Contains every menus
        vector<Menu*> menus;
        /// \brief Stack from the currently used menu to the first one to have appeared.
        vector<int> historyMenu;
        /// \brief Instance of Text class to use in menus
        Text* text;

        /// \brief Class instance
        static MenuFactory* inst;
    };
}

#endif