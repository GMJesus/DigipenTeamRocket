#include "UI/Menu/MenuFactory.h"
#include "GameEngine/GameEngine.h"

namespace UIEngine
{
    MenuFactory* MenuFactory::inst = nullptr;

    MenuFactory::MenuFactory() :
        text(nullptr)
    {
    }

    MenuFactory::~MenuFactory()
    {
    }

    MenuFactory* MenuFactory::Instance()
    {
        if (!inst)
            inst = new MenuFactory();
        return inst;
    }

    bool MenuFactory::Initialize(Text* text)
    {
        this->text = text;

        /*
        ** Callback buttons
        */
        // 0: Quit
        callbackButtons.push_back([](vector<int> values)
        {
            GameEngine::Engine::Instance()->Stop();
        });
        // 1: Resume
        callbackButtons.push_back([](vector<int> values)
        {
            GameEngine::Engine::Instance()->Resume();
        });
        // 2: Activate Menu
        callbackButtons.push_back([](vector<int> values)
        {
            if (values.size() >= 1)
                MenuFactory::Instance()->ActivateMenu(values.at(0));
        });
        // 3: Go Last Menu
        callbackButtons.push_back([](vector<int> values)
        {
            MenuFactory::Instance()->GoLastMenu();
        });
        // 4: Quit Menu
        callbackButtons.push_back([](vector<int> values)
        {
            MenuFactory::Instance()->QuitMenu();
        });
        // 5: Confirm Menu
        callbackButtons.push_back([](vector<int> values)
        {
            if (values.size() >= 1)
                MenuFactory::Instance()->callbackButtons[values.at(0)](values);
        });
        // 6: Launch Confirm Quit Menu
        callbackButtons.push_back([](vector<int> values)
        {
            if (values.size() >= 3)
            {
                MenuFactory::Instance()
                    ->GetMenu(values.at(0))
                    ->GetButton(values.at(1))
                    ->addValue(values.at(2));
                MenuFactory::Instance()->ActivateMenu(values.at(0));
            }
        });
        // 7: Retry (After GameOVer)
        callbackButtons.push_back([](vector<int> values)
        {
            GameEngine::Engine::Instance()->ResetLevels();
            MenuFactory::Instance()->QuitMenu();
            GameEngine::Engine::Instance()->Resume();
        });

        /*
        ** Callback DropDown
        */
        callbackDropDowns.push_back([](char* value)
        {
            /*
            Change screen resolution
            */
        });

        return true;
    }

    void MenuFactory::Update()
    {
        for (auto menu : menus)
            menu->Update(text);
    }

    int MenuFactory::AddMenu(Menu* menu)
    {
        menu->setEnable(false);
        menus.push_back(menu);

        return menus.size() - 1;
    }

    void MenuFactory::AddImage(Image* image, int menuId)
    {
        if (menuId < 0 || menuId >= menus.size())
            return;

        menus.at(menuId)->AddImage(image);
    }

    void MenuFactory::AddButton(Button* button, int menuId, int callbackId)
    {
        if (menuId < 0 || menuId >= menus.size() ||
            callbackId < 0 || callbackId >= callbackButtons.size())
            return;

        button->setCallBack(callbackButtons.at(callbackId));
        menus.at(menuId)->AddButton(button);
    }

    void MenuFactory::AddDropDown(DropDown* dropDown, int menuId, int callbackId)
    {
        if (menuId < 0 || menuId >= menus.size() ||
            callbackId < 0 || callbackId >= callbackDropDowns.size())
            return;

        dropDown->setCallback(callbackDropDowns.at(callbackId));
        menus.at(menuId)->AddDropDown(dropDown);
    }

    void MenuFactory::AddSwitch(UISwitch* uiswitch, int menuId)
    {
        if (menuId < 0 || menuId >= menus.size())
            return;

        menus.at(menuId)->AddSwitch(uiswitch);
    }

    void MenuFactory::ActivateMenu(int menuId)
    {
        if (menuId < 0 || menuId >= menus.size())
            return;

        if (historyMenu.size() > 0)
        {
            auto lastMenuId = historyMenu.back();
            menus.at(lastMenuId)->setActivate(false);
        }

        menus.at(menuId)->setEnable(true);
        menus.at(menuId)->setActivate(true);

        historyMenu.push_back(menuId);
    }

    void MenuFactory::GoLastMenu()
    {
        if (historyMenu.size() > 1)
        {
            auto menuId = historyMenu.back();
            menus.at(menuId)->setActivate(false);
            menus.at(menuId)->setEnable(false);

            historyMenu.pop_back();
            menuId = historyMenu.back();
            menus.at(menuId)->setActivate(true);
        }
    }

    void MenuFactory::QuitMenu()
    {
        if (historyMenu.size() > 0)
        {
            auto menuId = historyMenu.back();
            menus.at(menuId)->setActivate(false);
            for (auto id : historyMenu)
                menus.at(id)->setEnable(false);
            historyMenu.clear();
        }
    }

    Menu* MenuFactory::GetMenu(int position)
    {
        if (position >= menus.size())
            return nullptr;
        return menus.at(position);
    }
}