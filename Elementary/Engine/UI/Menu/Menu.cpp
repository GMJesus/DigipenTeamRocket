#include "Menu.h"

using namespace UIEngine;

Menu::Menu() :
enable(false),
activate(false),
background(nullptr)
{
}

Menu::~Menu()
{
}

bool Menu::Initialize(WCHAR* texturePath, float positionX, float positionY, float width, float height)
{
    background = new Image;
    return background->Initialize(texturePath, positionX, positionY, width, height);
}

void Menu::Update(Text* text)
{
    if (!enable)
        return;

    // show everything
    background->Update();
    for (auto image : images)
        image->Update();
    for (auto dropDown : dropDowns)
    {
        if (!dropDown->isTextSet())
            dropDown->SetText(text);
        dropDown->Update();
    }
    for (auto uiswitch : switches)
        uiswitch->Update();
    for (auto button : buttons)
        button->Update();
}

void Menu::AddImage(Image* image)
{
    if (image)
        images.push_back(image);
}

void Menu::AddButton(Button* button)
{
    if (button)
        buttons.push_back(button);
}

void Menu::AddDropDown(DropDown* dropDown)
{
    dropDowns.push_back(dropDown);
}

void Menu::AddSwitch(UISwitch* s)
{
    switches.push_back(s);
}

Image* Menu::GetImage(int position)
{
    if (position >= images.size())
        return nullptr;
    return images.at(position);
}

Button* Menu::GetButton(int position)
{
    if (position >= buttons.size())
        return nullptr;
    return buttons.at(position);
}

DropDown* Menu::GetDropDown(int position)
{
    if (position >= dropDowns.size())
        return nullptr;
    return dropDowns.at(position);
}

UISwitch* Menu::GetSwitch(int position)
{
    if (position >= switches.size())
        return nullptr;
    return switches.at(position);
}

bool Menu::Enable() const
{
    return enable;
}

void Menu::setEnable(bool enable)
{
    this->enable = enable;
}

bool Menu::Activate() const
{
    return activate;
}

void Menu::setActivate(bool enable)
{
    this->activate = enable;
    for (auto button : buttons)
        button->setEnable(enable);
    for (auto dropDown : dropDowns)
        dropDown->setEnable(enable);
}

float Menu::BackgroundPositionX() const
{
    return background->PositionX();
}

void Menu::setBackgroundPositionX(float backgroundPositionX)
{
    background->setPositionX(backgroundPositionX);
}

float Menu::BackgroundPositionY() const
{
    return background->PositionY();
}

void Menu::setBackgroundPositionY(float backgroundPositionY)
{
    background->setPositionY(backgroundPositionY);
}

float Menu::BackgroundWidth() const
{
    return background->Width();
}

void Menu::setBackgroundWidth(float backgroundWidth)
{
    background->setWidth(backgroundWidth);
}

float Menu::BackgroundHeight() const
{
    return background->Height();
}

void Menu::setBackgroundHeight(float backgroundHeight)
{
    background->setHeight(backgroundHeight);
}