/**
* \file      Menu
* \author    Thomas HANNOT
* \brief     Container of every Menu's Item.
*
*/


#ifndef __MENU_H__
#define __MENU_H__

///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include <vector>
#include "Graphics/TexturePlane.h"
#include "UI/Button.h"
#include "UI/Image.h"
#include "UI/DropDown.h"
#include "UI/UISwitch.h"

namespace UIEngine
{
    class Menu
    {
        /* Table enable / activate
        **
        ** enable | activate | Menu items with action | Menu display
        ** ---------------------------------------------------------
        **   true |   true   |          work          |   display  
        **   true |  false   |      don't work        | not display
        **  false |   true   |      don't work        | not display
        **  false |  false   |      don't work        | not display
        */

        /// \brief boolean which enable the update of the Menu
        bool enable;
        /// \brief boolean which activate its items which contains an action (like button, dropdown ...)
        bool activate;

        // Background
        Image* background;

        /*
        ** Items
        */
        /// \brief List of the images in the Menu
        std::vector<Image*> images;
        /// \brief List of the buttons in the Menu
        std::vector<Button*> buttons;
        /// \brief List of the dropdowns in the Menu
        std::vector<DropDown*> dropDowns;
        /// \brief List of the switches in the Menu
        std::vector<UISwitch*> switches;

    public:
        /**
        * \brief    Constructor.
        */
        Menu();
        /**
        * \brief    Destructor.
        */
        ~Menu();

        /**
        * \brief    Initialize depending class.
        * \param	texturePath Path to the file of the image to display.
        * \param	positionX   position on the X axis. /!\ Warning: this value is to set in percentage of the screen
        * \param	positionY   position on the Y axis. /!\ Warning: this value is to set in percentage of the screen
        * \param	width       width of the image. /!\ Warning: this value is to set in percentage of the screen
        * \param	height      height of the image. /!\ Warning: this value is to set in percentage of the screen
        * \return   A boolean that let the user knows if the function ended with an error.
        */
        bool Initialize(WCHAR* texturePath, float positionX, float positionY, float width, float height);
        void Update(Text* text);

        void AddButton(Button*);
        void AddImage(Image*);
        void AddDropDown(DropDown*);
        void AddSwitch(UISwitch*);

        Image* GetImage(int position);
        Button* GetButton(int position);
        DropDown* GetDropDown(int position);
        UISwitch* GetSwitch(int position);

        bool Enable() const;
        void setEnable(bool enable);
        bool Activate() const;
        void setActivate(bool enable);

        float BackgroundPositionX() const;
        void setBackgroundPositionX(float backgroundPositionX);
        float BackgroundPositionY() const;
        void setBackgroundPositionY(float backgroundPositionY);
        float BackgroundWidth() const;
        void setBackgroundWidth(float backgroundWidth);
        float BackgroundHeight() const;
        void setBackgroundHeight(float backgroundHeight);
    };
}

#endif // __MENU_H__