/**
* \file      Image
* \author    Thomas HANNOT
* \brief     Item from Menu.
*            To use to show an Image on the screen.
*
*/

#ifndef __IMAGE_H__
#define __IMAGE_H__

///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include "Graphics/TexturePlane.h"

namespace UIEngine
{
    class Image
    {
        /// \brief texture which contains the image to display
        Graphic::TexturePlane* texture;

        /// \brief position on the X axis. /!\ Warning: this value is to set in percentage of the screen
        float positionX;
        /// \brief position on the Y axis. /!\ Warning: this value is to set in percentage of the screen
        float positionY;
        /// \brief width of the image. /!\ Warning: this value is to set in percentage of the screen
        float width;
        /// \brief height of the image. /!\ Warning: this value is to set in percentage of the screen
        float height;

    public:
        /**
        * \brief    Constructor.
        */
        Image();
        /**
        * \brief    Destructor.
        */
        ~Image();

        /**
        * \brief    Initialize depending class.
        * \param	texturePath Path to the file of the image to display.
        * \param	positionX   position on the X axis. /!\ Warning: this value is to set in percentage of the screen
        * \param	positionY   position on the Y axis. /!\ Warning: this value is to set in percentage of the screen
        * \param	width       width of the image. /!\ Warning: this value is to set in percentage of the screen
        * \param	height      height of the image. /!\ Warning: this value is to set in percentage of the screen
        * \return   A boolean that let the user knows if the function ended with an error.
        */
        bool Initialize(WCHAR* texturePath, float positionX, float positionY, float width, float height);
        /**
        * \brief    Display the image.
        */
        void Update();

        float PositionX() const;
        void setPositionX(const float positionX);
        float PositionY() const;
        void setPositionY(const float positionY);
        float Width() const;
        void setWidth(const float width);
        float Height() const;
        void setHeight(const float height);
    };
}

#endif
