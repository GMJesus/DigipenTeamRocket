////////////////////////////////////////////////////////////////////////////////
// Filename: UIEngine.cpp
////////////////////////////////////////////////////////////////////////////////
#include "UIEngine.h"
#include "GameEngine/GameEngine.h"
#include "UI/Menu/MenuFactory.h"

using namespace UIEngine;

UI UI::m_instance = UI();

UI::UI()
{
    m_Fps = nullptr;
    m_Text = nullptr;
	_messages.clear();

    m_sentence = nullptr;
    device = nullptr;
    deviceContext = nullptr;
}

UI::~UI()
{
}

UI &UI::Instance()
{
	return m_instance;
}

#ifdef USEDIRECT3D
bool UI::Initialize(D3DXMATRIX m_worldMatrix, D3DXMATRIX m_orthoMatrix, D3DXMATRIX m_baseViewMatrix, s_WindowDatas &datas)
{
	bool result;

	worldMatrix = m_worldMatrix;
	orthoMatrix = m_orthoMatrix;
	baseViewMatrix = m_baseViewMatrix;

	// Create the text object.
	m_Text = new Text;
	if (!m_Text)
	{
		LogWriter::Instance().addLogMessage("Could not create TextObject.", Error);
		return false;
	}

	// Initialize the text object.
	result = m_Text->Initialize(m_DXInterface.m_device, m_DXInterface.m_deviceContext, datas.m_hwnd, datas.screenWidth, datas.screenHeight, baseViewMatrix);
	if (!result)
	{
		LogWriter::Instance().addLogMessage("Could not initialize text object.", Error);
		MessageBoxW(datas.m_hwnd, L"Could not initialize the text object.", L"Error", MB_OK);
		return false;
	}

	// Create the fps object.
	m_Fps = new FpsClass;
	if (!m_Fps)
	{
		LogWriter::Instance().addLogMessage("Could not create FPS Class.", Error);
		return false;
	}

	// Initialize the fps object.
	m_Fps->Initialize();

	// Create the cpu object.
	m_Cpu = new CpuClass;
	if (!m_Cpu)
	{
		LogWriter::Instance().addLogMessage("Could not create CPU Class.", Error);
		return false;
	}

	// Initialize the cpu object.
	m_Cpu->Initialize();
	LogWriter::Instance().addLogMessage("UI Engine Initialized");

#ifdef GAME
    MenuFactory::Instance()->Initialize(m_Text);
#endif

    return true;
}
#endif

void UI::Shutdown()
{

	// Release the fps object.
	if (m_Fps)
	{
		delete m_Fps;
        m_Fps = nullptr;
	}
	// Release the text object.
	if (m_Text)
	{
		m_Text->Shutdown();
		delete m_Text;
        m_Text = nullptr;
	}


}

bool UI::Update(float deltaTime)
{
#ifdef GAME
	if (GameEngine::engineParameters.drawFPS)
	{
		// Update the system stats.
		m_Fps->Frame();
		//m_Cpu->Frame();

		PrintFPS(m_Fps->GetFps());
		//PrintCPU(m_Cpu->GetCpuPercentage());
	}
	if (GameEngine::engineParameters.debugActive)
		UpdateMessages(deltaTime);

    MenuFactory::Instance()->Update();
#elif EDITOR
	UpdateMessages(deltaTime);
#endif

	// Turn on the alpha blending before rendering the text.
	m_DXInterface.TurnOnAlphaBlending();

	Print();

	// Turn off alpha blending after rendering the text.
	m_DXInterface.TurnOffAlphaBlending();

	return true;
}

bool UI::PrintFPS(int fps)
{
	char tempString[16];
	char fpsString[16];
	float red, green, blue;
	bool result;

	// Truncate the fps to below 10,000.
	if (fps > 9999)
		fps = 9999;

	// Convert the fps integer to string format.
	_itoa_s(fps, tempString, 10);

	// Setup the fps string.
	strcpy_s(fpsString, "FPS: ");
	strcat_s(fpsString, tempString);

	// If fps is 60 or above set the fps color to green.
	if (fps >= 60)
	{
		red = 0.0f;
		green = 1.0f;
		blue = 0.0f;
	}

	// If fps is below 60 set the fps color to yellow.
	else if (fps < 60)
	{
		red = 1.0f;
		green = 1.0f;
		blue = 0.0f;
	}

	// If fps is below 30 set the fps color to red.
    else
	{
		red = 1.0f;
		green = 0.0f;
		blue = 0.0f;
	}

	result = StackText(fpsString, 16, 50, 20, red, green, blue);
	return result;
}

bool UI::StackText(char *sentence, int maxLength, int positionX, int positionY, float red, float green, float blue)
{
	bool result;

#ifdef USEDIRECT3D
	// Initialize the first sentence.
	result = m_Text->InitializeSentence(&m_sentence, maxLength, m_DXInterface.m_device);
	if (!result)
		return false;

	// Now update the sentence vertex buffer with the new string information.
	result = m_Text->UpdateSentence(m_sentence, sentence, positionX, positionY, red, green, blue, m_DXInterface.m_deviceContext);
	if (!result)
		return false;
	worldMatrix = m_DXInterface.GetWorldMatrix();
	orthoMatrix = m_DXInterface.GetOrthoMatrix();
	sentences.push_back(m_sentence);
#endif


	return true;
}

std::list<UI::s_TempMessage*>::iterator&	UI::DeleteMessage(std::list<s_TempMessage*>::iterator& it)
{
	std::list<UI::s_TempMessage*>::iterator temp = it;
	++it;

	ReleaseSentence(&(*temp)->message);
	delete (*temp);
	_messages.erase(temp);
	return it;
}

bool	UI::UpdateMessages(float deltaTime)
{
	bool	dirtyState = false;
	bool	ret = true;

	for (std::list<UI::s_TempMessage*>::iterator it = _messages.begin(); it != _messages.end(); ++it)
	{
		(*it)->remainingTime -= deltaTime;
		if ((*it)->remainingTime <= 0)
		{
			dirtyState = true;
			it = DeleteMessage(it);
		}
		if (it == _messages.end())
			break;
	}
	if (dirtyState && !_messages.empty())
	{
		std::list<s_TempMessage*> templist;
		while (!_messages.empty() && ret)
		{
			s_TempMessage* mes = _messages.front();
			ret = RecreateMessage(mes->text, mes->remainingTime, mes->message->red, mes->message->green, mes->message->blue, templist);
			ReleaseSentence(&mes->message);
			delete (mes);
			_messages.pop_front();
		}
		if (ret)
			_messages = templist;
	}
	return ret;
}

bool	UI::RecreateMessage(char* sentence, float duration, float red, float green, float blue, std::list<s_TempMessage*>& list)
{
	bool	result;

#ifdef USEDIRECT3D
	// Initialize the first sentence.
	result = m_Text->InitializeSentence(&m_sentence, strlen(sentence), m_DXInterface.m_device);
	if (!result)
		return false;


	// Now update the sentence vertex buffer with the new string information.
	result = m_Text->UpdateSentence(m_sentence, sentence, xPosMessage, windowDatas.screenHeight + yPosMessage + (yOffsetMessage * list.size()), red, green, blue, m_DXInterface.m_deviceContext);
	if (!result)
		return false;
	worldMatrix = m_DXInterface.GetWorldMatrix();
	orthoMatrix = m_DXInterface.GetOrthoMatrix();
#endif

	s_TempMessage* elem = new s_TempMessage;
	elem->message = m_sentence;
	elem->remainingTime = duration;
	elem->text = sentence;

	list.push_back(elem);


	return true;
}

bool	UI::PrintMessage(const std::string &str, float duration, float red, float green, float blue)
{
	return PrintMessage(strdup(const_cast<char*>(str.c_str())), duration, red, green, blue);
}

bool	UI::PrintMessage(const char* sentence, float duration, float red, float green, float blue)
{
	return PrintMessage(const_cast<char*>(sentence), duration, red, green, blue);
}

void	UI::ReleaseSentence(SentenceType** sentence)
{
	if (*sentence)
	{
		// Release the sentence vertex buffer.
#ifdef USEDIRECT3D
		if ((*sentence)->vertexBuffer)
		{
			(*sentence)->vertexBuffer->Release();
            (*sentence)->vertexBuffer = nullptr;
		}

		// Release the sentence index buffer.
		if ((*sentence)->indexBuffer)
		{
			(*sentence)->indexBuffer->Release();
            (*sentence)->indexBuffer = nullptr;
		}
#endif
		// Release the sentence.
		delete *sentence;
        *sentence = nullptr;
	}

	return;
}
bool	UI::PrintMessage(char* sentence, float duration, float red, float green, float blue)
{
	bool	result;

#ifdef GAME
	if (!GameEngine::engineParameters.debugActive)
		return false;
#endif // GAME

#ifdef USEDIRECT3D
	// Initialize the first sentence.
	result = m_Text->InitializeSentence(&m_sentence, strlen(sentence), m_DXInterface.m_device);
	if (!result)
		return false;


	// Now update the sentence vertex buffer with the new string information.
	result = m_Text->UpdateSentence(m_sentence, sentence, xPosMessage, windowDatas.screenHeight +  yPosMessage + (yOffsetMessage * _messages.size()), red, green, blue, m_DXInterface.m_deviceContext);
	if (!result)
		return false;
	worldMatrix = m_DXInterface.GetWorldMatrix();
	orthoMatrix = m_DXInterface.GetOrthoMatrix();
#endif
	s_TempMessage* elem = new s_TempMessage;
	elem->message = m_sentence;
	elem->remainingTime = duration;
	elem->text = sentence;

	_messages.push_back(elem);


	return true;
}

bool UI::Print()
{
	bool result;

	for (std::size_t i = 0; i < sentences.size(); ++i)
	{

#ifdef USEDIRECT3D
		result = m_Text->RenderSentence(m_DXInterface.m_deviceContext, sentences[i], worldMatrix, orthoMatrix);
		if (!result)
			return false;
		ReleaseSentence(&sentences[i]);
#endif
	}
	sentences.clear();

	for (std::list<s_TempMessage*>::const_iterator it = _messages.begin(); it != _messages.end(); ++it)
	{
#ifdef USEDIRECT3D
		result = m_Text->RenderSentence(m_DXInterface.m_deviceContext, (*it)->message, worldMatrix, orthoMatrix);
		if (!result)
			return false;
#endif
	}
	return true;
}