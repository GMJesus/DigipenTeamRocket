/**
* \file      Button
* \author    Thomas HANNOT
* \brief     Item from Menu.
*            To use to display and use a Button on the screen.
*
*/

#ifndef __BUTTON_H__
#define __BUTTON_H__

///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include <vector>
#include "UI/Image.h"

namespace UIEngine
{
    class Button
    {
    protected:
        /// \brief enable/disable the button
        bool enable;

        /// \brief callback function called when a click is done on the button
        void(*callBack)(vector<int>);
        /// \brief Background of the button
        Image* image;
        /// \brief List of values to use with the callback
        vector<int> values;

    public:
        /**
        * \brief    Constructor.
        */
        Button();
        /**
        * \brief    Destructor.
        */
        ~Button();

        /**
        * \brief    Initialize depending class.
        * \param	texturePath Path to the file of the image to display.
        * \param	positionX   position on the X axis. /!\ Warning: this value is to set in percentage of the screen
        * \param	positionY   position on the Y axis. /!\ Warning: this value is to set in percentage of the screen
        * \param	width       width of the image. /!\ Warning: this value is to set in percentage of the screen
        * \param	height      height of the image. /!\ Warning: this value is to set in percentage of the screen
        * \return   A boolean that let the user knows if the function ended with an error.
        */
        bool Initialize(WCHAR* texturePath, float positionX, float positionY, float width, float height);
        /**
        * \brief    Display the Button. Check if click on it.
        */
        void Update();

        bool Enable() const;
        void setEnable(bool enable);

        void setCallBack(void(*callBack)(vector<int>));
        void addValue(const int value);
        int Value(int position) const;

        float PositionX() const;
        void setPositionX(float positionX);
        float PositionY() const;
        void setPositionY(float positionY);
        float Width() const;
        void setWidth(float width);
        float Height() const;
        void setHeight(float height);
    };
}

#endif // __BUTTON_H__
