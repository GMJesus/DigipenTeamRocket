/**
* \file      FontShader
* \author    Alexy DURAND
* \brief     Handle the printing of text with shaders.
*
*/
#ifndef _FONTSHADER_H_
#define _FONTSHADER_H_

#pragma warning (disable : 4005)

//////////////
// INCLUDES //
//////////////
#include "Graphics/DirectXInterface.h"
#include <fstream>
using namespace std;


////////////////////////////////////////////////////////////////////////////////
// Class name: FontShader
////////////////////////////////////////////////////////////////////////////////
class FontShader
{
private:
#ifdef USEDIRECT3D
	struct ConstantBufferType
	{
		D3DXMATRIX world;
		D3DXMATRIX view;
		D3DXMATRIX projection;
	};

	struct PixelBufferType
	{
		D3DXVECTOR4 pixelColor;
	};
#endif

public:
	/**
	* \brief    Constructor.
	*/
	FontShader();
	FontShader(const FontShader&);
	/**
	* \brief    Destructor.
	*/
	~FontShader();

#ifdef USEDIRECT3D
	/**
	* \brief    Call the initialization of the shader.
	* \param    device        Intern Parameter of D3D.
	* \param    hwnd		  Game Window
	* \return   A boolean that let the user knows if the function ended with an error.
	*/
	bool Initialize(ID3D11Device* device, HWND hwnd);

	/**
	* \brief    Call the update of the shader parameters and the printing of the shader.
	* \param    deviceContext        Intern Parameter of D3D.
	* \param    indexCount			 Index.
	* \param    worldMatrix			 Intern Parameter of D3D.
	* \param    viewMatrix			 Intern Parameter of D3D.
	* \param    projectionMatrix	 Intern Parameter of D3D.
	* \param    texture				 Intern Parameter of D3D.
	* \param    pixelColor			 Intern Parameter of D3D.
	* \return   A boolean that let the user knows if the function ended with an error.
	*/
	bool Render(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix,
		D3DXMATRIX projectionMatrix, ID3D11ShaderResourceView* texture, D3DXVECTOR4 pixelColor);
#endif
	/**
	* \brief    Free all depending classes form the memory.
	*/
	void Shutdown();

private:
#ifdef USEDIRECT3D
	/**
	* \brief    Shader initialization.
	* \param    device				 Intern Parameter of D3D.
	* \param    hwnd				 Game Window
	* \param    vsFilename			 Path of .vs file.
	* \param    psFilename			 Path of .ps file.
	* \return   A boolean that let the user knows if the function ended with an error..
	*/
	bool InitializeShader(ID3D11Device* device, HWND hwnd, WCHAR* vsFilename, WCHAR* psFilename);

	/**
	* \brief    Print an error message in a .txt file if the shader can't be loaded.
	* \param    errorMessage		 Error message
	* \param    hwnd				 Game Window
	* \param    shaderFilename		 Path of the shader in error
	*/
	void OutputShaderErrorMessage(ID3D10Blob* errorMessage, HWND hwnd, WCHAR* shaderFilename);

	/**
	* \brief    Change the parameters of the shader.
	* \param    deviceContext        Intern Parameter of D3D.
	* \param    worldMatrix			 Intern Parameter of D3D.
	* \param    viewMatrix			 Intern Parameter of D3D.
	* \param    projectionMatrix	 Intern Parameter of D3D.
	* \param    texture				 Intern Parameter of D3D.
	* \param    pixelColor			 Intern Parameter of D3D.
	* \return   A boolean that let the user knows if the function ended with an error.
	*/
	bool SetShaderParameters(ID3D11DeviceContext* deviceContext, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix,
		D3DXMATRIX projectionMatrix, ID3D11ShaderResourceView* texture, D3DXVECTOR4 pixelColor);

	/**
	* \brief    Print the shader.
	* \param    deviceContext        Intern Parameter of D3D.
	* \param    indexCount			 Index.
	*/
	void RenderShader(ID3D11DeviceContext* deviceContext, int indexCount);
#endif

	/**
	* \brief    Clear the parameters linked to the shader in the memory.
	*/
	void ShutdownShader();

private:
#ifdef USEDIRECT3D
	ID3D11VertexShader* m_vertexShader;
	ID3D11PixelShader* m_pixelShader;
	ID3D11InputLayout* m_layout;
	ID3D11Buffer* m_constantBuffer;
	ID3D11SamplerState* m_sampleState;
	ID3D11Buffer* m_pixelBuffer;
#endif
};

#endif