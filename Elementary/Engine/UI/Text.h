/**
* \file      Text
* \author    Alexy DURAND
* \brief     Handle the text printing.
*
*/
#ifndef _TEXT_H_
#define _TEXT_H_

#pragma warning (disable : 4005)

///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include "Font.h"
#include "FontShader.h"

class SentenceType
{
public:
#ifdef USEDIRECT3D
	ID3D11Buffer *vertexBuffer, *indexBuffer;
#endif
	int vertexCount, indexCount, maxLength;
	float red, green, blue;
};

////////////////////////////////////////////////////////////////////////////////
// Class name: Text
////////////////////////////////////////////////////////////////////////////////
class Text
{
private:


#ifdef USEDIRECT3D
	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
	};
#endif

public:

	/**
	* \brief    Constructor.
	*/
	Text();
	Text(const Text&);

	/**
	* \brief    Destructor.
	*/
	~Text();

#ifdef USEDIRECT3D
	/**
	* \brief    Intialize depending classes.
	* \param    device					 Intern Parameter of D3D.
	* \param    deviceContext		     Intern Parameter of D3D.
	* \param    hwnd					 Game Window
	* \param    screenWidth				 Window Width
	* \param    screenHeight			 Window Height
	* \param    baseViewMatrix			 Intern Parameter of D3D.
	* \return   A boolean that let the user knows if the function ended with an error.r.
	*/
	bool Initialize(ID3D11Device* device, ID3D11DeviceContext* deviceContext, HWND hwnd, int screenWidth, int screenHeight,
		D3DXMATRIX baseViewMatrix);

	/**
	* \brief    Initialize the text to print.
	* \param    sentence				 Pointer on the SentenceType structure.
	* \param    maxLength				 Maximum Length of the text.
	* \param    device					 Intern Parameter of D3D.
	* \return   A boolean that let the user knows if the function ended with an error.
	*/
	bool InitializeSentence(SentenceType** sentence, int maxLength, ID3D11Device* device);

	/**
	* \brief    Fill SentenceType with the text to print.
	* \param    sentence				 Filled structure
	* \param    text					 Text to print.
	* \param    positionX				 X Position.
	* \param    positionY				 Y Position.
	* \param    red						 Red Value.
	* \param    green					 Green Value.
	* \param    blue					 Blue Value.
	* \param    deviceContext		     Intern Parameter of D3D.
	* \return   A boolean that let the user knows if the function ended with an error.
	*/
	bool UpdateSentence(SentenceType* sentence, char* text, int positionX, int positionY, float red, float green, float blue,
		ID3D11DeviceContext* deviceContext);

	/**
	* \brief    Print the text.
	* \param    deviceContext		     Intern Parameter of D3D.
	* \param    sentence				 Structure containing the text to print.
	* \param    worldMatrix				 Intern Parameter of D3D.
	* \param    orthoMatrix				 Intern Parameter of D3D.
	* \return   A boolean that let the user knows if the function ended with an error.
	*/
	bool RenderSentence(ID3D11DeviceContext* deviceContext, SentenceType* sentence, D3DXMATRIX worldMatrix,
		D3DXMATRIX orthoMatrix);
#endif

	/**
	* \brief    Free the structure from memory.
	* \param    sentence				 Structure to free.
	*/
	void ReleaseSentence(SentenceType** sentence);

	/**
	* \brief    Free all depending classes form the memory.
	*/
	void Shutdown();

private:
	Font* m_Font;
	FontShader* m_FontShader;
	int m_screenWidth, m_screenHeight;
#ifdef USEDIRECT3D
	D3DXMATRIX m_baseViewMatrix;
#endif
};


#endif