/**
* \file      UISwitch
* \author    Thomas HANNOT
* \brief     Item from Menu.
*            To use to show an Image on the screen.
*
*/

#ifndef __UISwitch_H__
#define __UISwitch_H__

///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include "UI/Image.h"

namespace UIEngine
{
    class UISwitch
    {
        /// \brief Image to display when on
        Image* on;
        /// \brief Image to display when off
        Image* off;

        /// \brief boolean who decides which Image to display
        bool switchState;

        /// \brief position on the X axis. /!\ Warning: this value is to set in percentage of the screen
        float positionX;
        /// \brief position on the Y axis. /!\ Warning: this value is to set in percentage of the screen
        float positionY;
        /// \brief width of the image. /!\ Warning: this value is to set in percentage of the screen
        float width;
        /// \brief height of the image. /!\ Warning: this value is to set in percentage of the screen
        float height;

    public:
        /**
        * \brief    Constructor.
        */
        UISwitch();
        /**
        * \brief    Destructor.
        */
        ~UISwitch();

        /**
        * \brief    Initialize depending class.
        * \param	onTexture   Path to the file of the image to display.
        * \param	offTexture  Path to the file of the image to display.
        * \param	positionX   position on the X axis. /!\ Warning: this value is to set in percentage of the screen
        * \param	positionY   position on the Y axis. /!\ Warning: this value is to set in percentage of the screen
        * \param	width       width of the image. /!\ Warning: this value is to set in percentage of the screen
        * \param	height      height of the image. /!\ Warning: this value is to set in percentage of the screen
        * \return   A boolean that let the user knows if the function ended with an error.
        */
        bool Initialize(WCHAR* onTexture, WCHAR* offTexture, float positionX, float positionY, float width, float height);
        /**
        * \brief    Display the image.
        */
        void Update();

        void SwitchState(bool state);
        bool SwitchState();

        float PositionX() const;
        void setPositionX(const float positionX);
        float PositionY() const;
        void setPositionY(const float positionY);
        float Width() const;
        void setWidth(const float width);
        float Height() const;
        void setHeight(const float height);
    };
}

#endif
