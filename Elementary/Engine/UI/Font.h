/**
* \file      Font
* \author    Alexy DURAND
* \brief     Handle the police of the Text HUD.
*
*/
#ifndef FONT_H_
#define FONT_H_

#pragma warning (disable : 4005)

//////////////
// INCLUDES //
//////////////
#include "Graphics/DirectXInterface.h"
#include <fstream>
using namespace std;


///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include "Graphics/Texture.h"

////////////////////////////////////////////////////////////////////////////////
// Class name: Font
////////////////////////////////////////////////////////////////////////////////
class Font
{
private:

	struct FontType
	{
		float left, right;
		int size;
	};

#ifdef USEDIRECT3D
	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
	};
#endif

public:
	/**
	* \brief    Constructor.
	*/
	Font();
	Font(const Font&);
	/**
	* \brief    Destructor.
	*/
	~Font();

#ifdef USEDIRECT3D
	/**
	* \brief    Initialize the chosen police.
	* \param    device        Intern Parameter of D3D.
	* \param    fontFilename  Path of the file containing the chosen police.
	* \param    textureFilename        Path of the texture in which sets the police.
	* \return   A boolean that let the user knows if the function ended with an error.
	*/
	bool Initialize(ID3D11Device* device, char* fontFilename, WCHAR* textureFilename);
	ID3D11ShaderResourceView* GetTexture();

	/**
	* \brief    Create Vertex Array of the police.
	* \param    vertices      Pointers on vertices
	* \param    sentence	  Sentence to initialize
	* \param    drawX         X Position
	* \param    drawY		  Y Position
	* \return   A boolean that let the user knows if the function ended with an error.
	*/
	void BuildVertexArray(void* vertices, char* sentence, float drawX, float drawY);
#endif

	/**
	* \brief    Free all depending classes form the memory.
	*/
	void Shutdown();


private:
	/**
	* \brief    Load the parameters of the chosen police.
	* \param    filename      Path of the chosen police.
	* \return   A boolean that let the user knows if the function ended with an error.
	*/
	bool LoadFontData(char* filename);

	/**
	* \brief    Unload parameters of the chosen police.
	*/
	void ReleaseFontData();

#ifdef USEDIRECT3D
	/**
	* \brief    Load the texture in which stock the police.
	* \param    device        Intern parameter of D3D.
	* \param    filename      Path of the texture
	* \return   A boolean that let the user knows if the function ended with an error.
	*/
	bool LoadTexture(ID3D11Device* device, WCHAR* filename);
#endif

	/**
	* \brief    Unload textures of the chosen police.
	*/
	void ReleaseTexture();

private:
	FontType* m_Font;
	Graphic::Texture* m_Texture;
};

#endif
