/**
* \file      FPSClass
* \author    Alexy DURAND
* \brief     Handle the FPS Count.
*
*/
#ifndef _FPSCLASS_H_
#define _FPSCLASS_H_


/////////////
// LINKING //
/////////////
#pragma comment(lib, "winmm.lib")


//////////////
// INCLUDES //
//////////////
#include <windows.h>
#include <mmsystem.h>


////////////////////////////////////////////////////////////////////////////////
// Class name: FpsClass
////////////////////////////////////////////////////////////////////////////////
class FpsClass
{
public:
	/**
	* \brief    Constructor.
	*/
	FpsClass();
	FpsClass(const FpsClass&);
	/**
	* \brief    Destructor.
	*/
	~FpsClass();

	/**
	* \brief    Initialize the value needed to count the FPS.
	*/
	void Initialize();

	/**
	* \brief    Count the number of time that the function is called per seconds.
	*/
	void Frame();

	int  GetFps();

private:
	int m_fps, m_count;
	unsigned long m_startTime;
};

#endif