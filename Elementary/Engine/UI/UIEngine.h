/**
* \file      UIEngine
* \author    Alexy DURAND
* \brief     Singleton. Handle all the UI stuff of the engine.
*
*/


#ifndef _UI_H_
#define _UI_H_

#pragma warning (disable : 4005)

#include <iostream>
#include <vector>

#define	PRINT_ON_SCREEN(str) UIEngine::UI::Instance().PrintMessage(str)

#include "FpsClass.h"
#include "CpuClass.h"
#include "UI\Text.h"
#include "Utils\WindowDatas.h"
#include "Graphics/DirectXInterface.h"
#include "Utils\EngineParameters.h"
#include "Utils\LogWriter.h"

#ifdef GAME
#define PAUSE_BACKGROUND_FILE    L"./Ressources/pause_background.png"
#define PAUSE_BUTTON_RETURN_FILE L"./Ressources/pause_resume_button.jpg"
#define PAUSE_BUTTON_MENU_FILE   L"./Ressources/pause_exit_button.png"
#else
#define PAUSE_BACKGROUND_FILE    L"../Engine/Ressources/pause_background.png"
#define PAUSE_BUTTON_RETURN_FILE L"../Engine/Ressources/pause_resume_button.jpg"
#define PAUSE_BUTTON_MENU_FILE   L"../Engine/Ressources/pause_exit_button.png"
#endif

/**
* \brief	Namespace defining the User Interface datas
*/
namespace UIEngine{

	class UI
	{
    public:
		/**
		* \brief	Structure to store datas for temporary messages
		*			Temporary messages are messages that are printed out on the screen for a given duration
		*/
        struct s_TempMessage
		{
			/// \brief The display time remaining of this message
			float			remainingTime;
			/// \brief The text to display
			char*			text;
			/// \brief The sentence to draw
			SentenceType*	message;
		};

		/**
		* \brief    Access to the class form any files.
		* \return   Instance on the singleton of the class.
		*/
		static	UI &Instance();
#ifdef USEDIRECT3D
		/**
		* \brief    Initialize depending class.
		* \param    m_worldMatrix        World matrix
		* \param    m_orthoMatrix		 Orthographic matrix
		* \param    m_baseViewMatrix     Base matrix
		* \param    datas				 Parameters of the Game Window
		* \return   A boolean that let the user knows if the function ended with an error.
		*/
		bool	Initialize(D3DXMATRIX m_worldMatrix, D3DXMATRIX m_orthoMatrix, D3DXMATRIX m_baseViewMatrix, s_WindowDatas &datas);
#endif
		/**
		* \brief    Update the depending class and call the messages printing.
		* \param    deltaTime        Time elapsed since the last call of the function.
		* \return   A boolean that let the user knows if the function ended with an error.
		*/
		bool	Update(float deltaTime);

		/**
		* \brief    Free all depending classes form the memory.
		*/
		void	Shutdown();

		/**
		* \brief    Convertit la cha�ne de caract�res en Text et la stock dans une liste.
		* \param    sentence        String to stock.
		* \param    maxLength		Max Length of the string.
		* \param    positionX		X Position.
		* \param    positionY		Y Position.
		* \param    red				Red Value
		* \param    green			Green Value
		* \param    blue			Blue Value
		* \return   A boolean that let the user knows if the function ended with an error.
		*/
		bool	StackText(char *sentence, int maxLength, int positionX, int positionY, float red, float green, float blue);

		/**
		* \brief	Print the given string to the screen for the given duration
		* \param	sentence	The sentence to print
		* \param	duration	The duration of the display of the message, in seconds
		* \param	red			The red color value of the message
		* \param	green		The green color value of the message
		* \param	blue		The blue color value of the message
		* \return	True if everything went well, false overwise
		*/
		bool	PrintMessage(const char* sentence, float duration = 2, float red = 1, float green = 1, float blue = 0);
		/**
		* \brief	Print the given string to the screen for the given duration
		* \param	str			The string to print
		* \param	duration	The duration of the display of the message, in seconds
		* \param	red			The red color value of the message
		* \param	green		The green color value of the message
		* \param	blue		The blue color value of the message
		* \return	True if everything went well, false overwise
		*/
		bool	PrintMessage(const std::string& str, float duration = 2, float red = 1, float green = 1, float blue = 0);
		/**
		* \brief	Print the given string to the screen for the given duration
		* \param	sentence	The sentence to print
		* \param	duration	The duration of the display of the message, in seconds
		* \param	red			The red color value of the message
		* \param	green		The green color value of the message
		* \param	blue		The blue color value of the message
		* \return	True if everything went well, false overwise
		*/
		bool	PrintMessage(char* sentence, float duration = 2, float red = 1, float green = 1, float blue = 0);

		/**
		* \brief    Handle the FPS printing.
		* \param    fps        Number of FPS to print.
		* \return   A boolean that let the user knows if the function ended with an error.
		*/
		bool	PrintFPS(int fps);
		bool	Print();
#ifdef USEDIRECT3D
		void	setDevice(ID3D11Device *m_device);
		void	setDeviceContext(ID3D11DeviceContext *m_deviceContext);
#endif

	private:
		UI& operator= (const UI &) {}
		UI(const UI &) {}

		/**
		* \brief      Class Instance.
		*/
		static UI m_instance;

		/**
		* \brief    Constructor.
		*/
		UI();

		/**
		* \brief    Destructor.
		*/
		~UI();

		/**
		* \brief	Manage the temporary messages.
		* \param	deltaTime	The time since last frame
		* \return	True if everything went well, false overwise
		*/
		bool	UpdateMessages(float deltaTime);
		/**
		* \brief	Recreate the given message, so its position can be updated
		* \param	sentence	The sentence to print
		* \param	duration	The duration remaining of this message
		* \param	red			The red color value of the message
		* \param	green		The green color value of the message
		* \param	blue		The blue color value of the message
		* \param	list		The current list of temporary messages
		*/
		bool	RecreateMessage(char* sentence, float duration, float red, float green, float blue, std::list<s_TempMessage*>& list);
		/**
		* \brief	Delete the given message from the current list of temporary messages
		* \param	it	The iterator to the message to delete
		* \return	The iterator to the next element of the list
		*/
		std::list<s_TempMessage*>::iterator&	DeleteMessage(std::list<s_TempMessage*>::iterator& it);
		/**
		* \brief	Manage the release of the given sentence
		* \param	sentence	The sentence to release
		*/
		void	ReleaseSentence(SentenceType** sentence);

		/**
		* \brief      Instance of the DirectXInterface class
		*/
		DirectXInterface &m_DXInterface = DirectXInterface::Instance();
		FpsClass *m_Fps;
		CpuClass *m_Cpu;
		SentenceType *m_sentence;
		std::vector<SentenceType *> sentences;

		/**
		* \brief	The list of temporary messages
		*/
		std::list<s_TempMessage *>	_messages;
		Text* m_Text;

		const int xPosMessage = 100;
		const int yPosMessage = -100;
		const int yOffsetMessage = -20;

#ifdef USEDIRECT3D
		D3DXMATRIX worldMatrix, orthoMatrix, baseViewMatrix;
		ID3D11Device* device;
		ID3D11DeviceContext* deviceContext;
#endif
	};

}

#endif