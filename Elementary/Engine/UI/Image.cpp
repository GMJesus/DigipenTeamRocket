#include "Image.h"
#include "Graphics/Camera.h"
#include "UI/UIEngine.h"
#include "GameEngine/GameEngine.h"

using namespace UIEngine;

Image::Image() :
texture(nullptr),
positionX(0.0f), positionY(0.0f),
width(0.0f), height(0.0f)
{
}

Image::~Image()
{
}

bool Image::Initialize(WCHAR* texturePath, float positionX, float positionY, float width, float height)
{
    setPositionX(positionX);
    setPositionY(positionY);
    setWidth(width);
    setHeight(height);

    texture = new Graphic::TexturePlane;
#ifdef USEDIRECT3D
    return texture->Initialize(DirectXInterface::Instance().GetDevice(), texturePath);
#else
    return true;
#endif
}

void Image::Update()
{
#ifdef USEDIRECT3D
    auto width = windowDatas.screenWidth * this->width / 100.0f;
    auto height = windowDatas.screenHeight * this->height / 100.0f;

    auto posX = windowDatas.screenWidth * positionX / 100.0f;
    auto posY = windowDatas.screenHeight * positionY / 100.0f;

    auto worldPosition = Graphic::Camera::Instance()
        ->ConvertPixelPositionToWorldPosition(posX + width / 2, posY + height / 2);

    if (texture)
    {
        texture->SetWidth(width);
        texture->SetHeight(height);
        texture->Render(DirectXInterface::Instance().GetDeviceContext(),
        worldPosition->x, worldPosition->y, 0.0f);
    }

    delete worldPosition;
#endif
}

float Image::PositionX() const
{
    return positionX;
}

void Image::setPositionX(const float positionX)
{
    this->positionX = positionX;
}

float Image::PositionY() const
{
    return positionY;
}

void Image::setPositionY(const float positionY)
{
    this->positionY = positionY;
}

float Image::Width() const
{
    return width;
}

void Image::setWidth(const float width)
{
    this->width = width;
}

float Image::Height() const
{
    return height;
}

void Image::setHeight(const float height)
{
    this->height = height;
}