#include "UISwitch.h"
#include "UIEngine.h"

using namespace UIEngine;

UISwitch::UISwitch() :
on(nullptr), off(nullptr), switchState(false),
positionX(0.0f), positionY(0.0f),
width(0.0f), height(0.0f)
{
}

UISwitch::~UISwitch()
{
}

bool UISwitch::Initialize(WCHAR* onTexture, WCHAR* offTexture, float positionX, float positionY, float width, float height)
{
    setPositionX(positionX);
    setPositionY(positionY);
    setWidth(width);
    setHeight(height);

    on = new Image;
    off = new Image;

    if (on->Initialize(onTexture, positionX, positionY, width, height) &&
        off->Initialize(offTexture, positionX, positionY, width, height))
        return true;
    return false;
}

void UISwitch::Update()
{
    auto img = switchState ? on : off;
    img->Update();
}

void UISwitch::SwitchState(bool state)
{
    switchState = state;
}

bool UISwitch::SwitchState()
{
    return switchState;
}

float UISwitch::PositionX() const
{
    return positionX;
}

void UISwitch::setPositionX(const float positionX)
{
    this->positionX = positionX;
    if (on)
        on->setPositionX(positionX);
    if (off)
        off->setPositionX(positionX);
}

float UISwitch::PositionY() const
{
    return positionY;
}

void UISwitch::setPositionY(const float positionY)
{
    this->positionY = positionY;
    if (on)
        on->setPositionY(positionY);
    if (off)
        off->setPositionY(positionY);
}

float UISwitch::Width() const
{
    return width;
}

void UISwitch::setWidth(const float width)
{
    this->width = width;
    if (on)
        on->setWidth(width);
    if (off)
        off->setWidth(width);

}

float UISwitch::Height() const
{
    return height;
}

void UISwitch::setHeight(const float height)
{
    this->height = height;
    if (on)
        on->setHeight(height);
    if (off)
        off->setHeight(height);
}