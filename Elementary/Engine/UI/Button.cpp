#include "Button.h"
#include "Inputs/InputManager.h"
#include "GameEngine/GameEngine.h"

using namespace UIEngine;

Button::Button() :
enable(false),
callBack(nullptr),
image(nullptr)
{
}


Button::~Button()
{
}


bool Button::Initialize(WCHAR* texturePath, float positionX, float positionY, float width, float height)
{
    image = new Image;
    return image->Initialize(texturePath, positionX, positionY, width, height);
}

void Button::Update()
{
    if (enable && callBack && InputManager::Instance().IsMousePressed(E_Key::Input_Fire))
    {
        auto width = windowDatas.screenWidth * image->Width() / 100.0f;
        auto height = windowDatas.screenHeight * image->Height() / 100.0f;

        auto posX = windowDatas.screenWidth * image->PositionX() / 100.0f;
        auto posY = windowDatas.screenHeight * image->PositionY() / 100.0f;

        int x;
        int y;
        InputManager::Instance().GetMouseLocation(x, y);

        if (x >= posX && x < posX + width &&
            y >= posY && y < posY + height)
        {
            callBack(values);
        }
    }

    image->Update();
}

bool Button::Enable() const
{
    return enable;
}

void Button::setEnable(bool enable)
{
    this->enable = enable;
}

void Button::setCallBack(void (*callBack)(vector<int>))
{
    this->callBack = callBack;
}

void Button::addValue(const int value)
{
    this->values.push_back(value);
}

int Button::Value(int position) const
{
    if (position < 0 || position >= values.size())
        return 0;
    return values.at(position);
}

float Button::PositionX() const
{
    return image->PositionX();
}

void Button::setPositionX(float positionX)
{
    image->setPositionX(positionX);
}

float Button::PositionY() const
{
    return image->PositionY();
}

void Button::setPositionY(float positionY)
{
    image->setPositionY(positionY);
}

float Button::Width() const
{
    return image->Width();
}

void Button::setWidth(float width)
{
    image->setWidth(width);
}

float Button::Height() const
{
    return image->Height();
}

void Button::setHeight(float height)
{
    image->setHeight(height);
}