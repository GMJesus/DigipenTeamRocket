#include "DropDown.h"
#include "UIEngine.h"

using namespace UIEngine;

DropDown::DropDown() :
currentChoice(0), cursorOnChoice(0), isOpen(false), buttonPressed(false),
firstCase(nullptr), defaultCase(nullptr), onCoverCase(nullptr),
maxLength(0), maxBoxes(0),
posX(0.0f), posY(0.0f), width(0.0f), height(0.0f),
enable(false), textManager(nullptr),
callback(nullptr)
{
}

DropDown::~DropDown()
{
}

bool DropDown::Initialize(WCHAR* firstCase, WCHAR* defaultCase, WCHAR* onCoverCase,
    int maxLength, int maxBoxes,
    float posX, float posY, float width, float height)
{
    this->firstCase = new Image;
    if (!this->firstCase->Initialize(firstCase, posX, posY, width, height))
        return false;
    this->defaultCase = new Image;
    if (!this->defaultCase->Initialize(defaultCase, posX, posY, width, height))
        return false;
    this->onCoverCase = new Image;
    if (!this->onCoverCase->Initialize(onCoverCase, posX, posY, width, height))
        return false;

    setMaxLength(maxLength);
    setMaxBoxes(maxBoxes);

    setPosX(posX);
    setPosY(posY);
    setWidth(width);
    setHeight(height);

    return true;
}

void DropDown::Update()
{
    if (!enable)
        return;
    
    if (isOpen)
    {
        if (InputManager::Instance().IsPressed(E_Key::Input_Down))
        {
            cursorOnChoice += 1;
            if (cursorOnChoice >= choices.size() - maxBoxes)
                cursorOnChoice = choices.size() - maxBoxes - 1;
        }
        if (InputManager::Instance().IsPressed(E_Key::Input_Up))
            cursorOnChoice = !cursorOnChoice ? 0 : cursorOnChoice - 1;
    }

    auto clickHappen = false;
    if (!buttonPressed && InputManager::Instance().IsMousePressed(E_Key::Input_Fire))
    {
        clickHappen = true;
        buttonPressed = true;
    }
    if (InputManager::Instance().IsMouseReleased(E_Key::Input_Fire))
        buttonPressed = false;

    /*
    ** Image update 
    */
    firstCase->Update();

    int x;
    int y;
    InputManager::Instance().GetMouseLocation(x, y);

    for (auto i = -1; i < maxBoxes && cursorOnChoice + i < choices.size(); i++)
    {
        if (!isOpen && i > 0)
            break;

        auto width = windowDatas.screenWidth * this->width / 100.0f;
        auto height = windowDatas.screenHeight * this->height / 100.0f;

        auto posX = windowDatas.screenWidth * this->posX / 100.0f;
        auto posY = (windowDatas.screenHeight * this->posY / 100.0f) + height * (i + 1);

        Image* image;
        if (x >= posX && x < posX + width &&
            y >= posY && y < posY + height)
        {
            if (i < 0)
            {
                if (clickHappen)
                    isOpen = !isOpen;
                continue;
            }

            image = onCoverCase;

            if (clickHappen && currentChoice != i)
            {
                currentChoice = i;
                callback(choices.at(currentChoice));
            }
        }
        else
            image = defaultCase;

        image->setPositionX(this->posX);
        image->setPositionY(this->posY + height * (i + 1));
        image->Update();
    }

    /*
    ** Text update
    */
    if (!textManager)
        return;
    for (auto i = -1; i < maxBoxes && cursorOnChoice + i < choices.size(); i++)
    {
        if (!isOpen && i > 0)
            break;

        auto width = windowDatas.screenWidth * this->width / 100.0f;
        auto height = windowDatas.screenHeight * this->height / 100.0f;

        auto posX = windowDatas.screenWidth * this->posX / 100.0f;
        auto posY = (windowDatas.screenHeight * this->posY / 100.0f) + height * (i + 1);

        posX += width / 10.0f;
        posY += height / 10.0f;

        // Get the sentence to print
        auto str = choices.at(i < 0 ? currentChoice : cursorOnChoice + i);

        SentenceType* sentence;
        if (!textManager->InitializeSentence(&sentence, strlen(str), DirectXInterface::Instance().GetDevice()))
            return;

        // Now update the sentence vertex buffer with the new string information.
        if (textManager->UpdateSentence(sentence, str, posY, posX, 0, 0, 0, DirectXInterface::Instance().GetDeviceContext()))
            return;
    }
}

void DropDown::AddChoice(char* choice)
{
    if (choice)
        choices.push_back(choice);
}

const char* DropDown::GetChoice() const
{
    return choices.at(currentChoice);
}

void DropDown::setCallback(void (* callback)(char*))
{
    this->callback = callback;
}

int DropDown::CurrentChoice() const
{
    return currentChoice;
}

void DropDown::setCurrentChoice(const int currentChoice)
{
    this->currentChoice = currentChoice;
}

int DropDown::MaxLength() const
{
    return maxLength;
}

void DropDown::setMaxLength(const int maxLength)
{
    this->maxLength = maxLength;
}

int DropDown::MaxBoxes() const
{
    return maxBoxes;
}

void DropDown::setMaxBoxes(const int maxBoxes)
{
    this->maxBoxes = maxBoxes;
}

bool DropDown::IsOpen() const
{
    return isOpen;
}

void DropDown::setIsOpen(const bool isOpen)
{
    this->isOpen = isOpen;
}

float DropDown::PosX() const
{
    return posX;
}

void DropDown::setPosX(const float posX)
{
    this->posX = posX;
}

float DropDown::PosY() const
{
    return posY;
}

void DropDown::setPosY(const float posY)
{
    this->posY = posY;
}

float DropDown::Width() const
{
    return width;
}

void DropDown::setWidth(const float width)
{
    this->width = width;
}

float DropDown::Height() const
{
    return height;
}

void DropDown::setHeight(const float height)
{
    this->height = height;
}

bool DropDown::Enable() const
{
    return enable;
}

void DropDown::setEnable(const bool enable)
{
    this->enable = enable;
}

void DropDown::SetText(Text* text)
{
    textManager = text;
}

bool DropDown::isTextSet()
{
    return textManager;
}
