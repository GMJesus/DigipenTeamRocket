/**
* \file      MenuFactory
* \author    Thomas HANNOT
* \brief     Item from Menu.
*            To use to show an Image on the screen.
*
*/

#ifndef __DROPDOWN_H__
#define __DROPDOWN_H__

///////////////////////
// MY CLASS INCLUDES //
///////////////////////
#include <vector>
#include "UI/Text.h"
#include "UI/Image.h"

//////////////////////
// MY CLASS DEFINES //
//////////////////////
#define DROPDOWN_OPTIONSCREEN_DELIMITOR "x"

namespace UIEngine
{
    class DropDown
    {
        /// \brief list of the possible choices in the DropDown
        vector<char*> choices;
        /// \brief current choice of the user in the list.
        int currentChoice;
        /// \brief current position in the list of the display (change only the text).
        int cursorOnChoice;

        /// \brief used to check if it must display the list under the first case or not.
        bool isOpen;
        /// \brief used to certify the click once then release.
        bool buttonPressed;

        /// \brief Image of the first case (the one which display the choice).
        Image* firstCase;
        /// \brief Image of every other cases.
        Image* defaultCase;
        /// \brief Image of case when covered by the mouse.
        Image* onCoverCase;

        /// \brief Max length of the char* contains in the list.
        int maxLength;
        /// \brief Max number of boxes to display under the first one.
        int maxBoxes;

        /// \brief position on the X axis. /!\ Warning: this value is to set in percentage of the screen
        float posX;
        /// \brief position on the Y axis. /!\ Warning: this value is to set in percentage of the screen
        float posY;
        /// \brief width of the image. /!\ Warning: this value is to set in percentage of the screen
        float width;
        /// \brief height of the image. /!\ Warning: this value is to set in percentage of the screen
        float height;

        /// \brief enable/disable the dropdown
        bool enable;

        /// \brief Text instance to print the choices.
        Text* textManager;

        /// \brief callback function called when the current choice is changed.
        void (*callback)(char*);

    public:
        /**
        * \brief    Constructor.
        */
        DropDown();
        /**
        * \brief    Destructor.
        */
        ~DropDown();

        /**
        * \brief    Initialize depending class.
        * \param	firstCase   Image of the first case (the one which display the choice). Path to the file of the image to display.
        * \param	defaultCase Image of every other cases. Path to the file of the image to display.
        * \param	onCoverCase Image of case when covered by the mouse. Path to the file of the image to display.
        * \param	maxLength   Max length of the char* contains in the list.
        * \param	maxBoxes    Max number of boxes to display under the first one.
        * \param	posX        position on the X axis. /!\ Warning: this value is to set in percentage of the screen
        * \param	posY        position on the Y axis. /!\ Warning: this value is to set in percentage of the screen
        * \param	width       width of the image. /!\ Warning: this value is to set in percentage of the screen
        * \param	height      height of the image. /!\ Warning: this value is to set in percentage of the screen
        * \return   A boolean that let the user knows if the function ended with an error.
        */
        bool Initialize(WCHAR* firstCase, WCHAR* defaultCase, WCHAR* onCoverCase, int maxLength, int maxBoxes,
            float posX, float posY, float width, float height);
        /**
        * \brief    Display the DropDown. update the action done with it.
        */
        void Update();

        void AddChoice(char*);
        const char* GetChoice() const;

        void setCallback(void (*callback)(char*));

        int CurrentChoice() const;
        void setCurrentChoice(const int currentChoice);
        int MaxLength() const;
        void setMaxLength(const int maxLength);
        int MaxBoxes() const;
        void setMaxBoxes(const int maxBoxes);
        bool IsOpen() const;
        void setIsOpen(const bool isOpen);
        float PosX() const;
        void setPosX(const float posX);
        float PosY() const;
        void setPosY(const float posY);
        float Width() const;
        void setWidth(const float width);
        float Height() const;
        void setHeight(const float height);

        bool Enable() const;
        void setEnable(const bool enable);

        void SetText(Text*);
        bool isTextSet();
    };
}

#endif