/**
* \author	R�mi Colin
*/
#ifndef _InputManager_H_
# define _InputManager_H_

#define DIRECTINPUT_VERSION 0x0800

#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

#include <dinput.h>
#include <fstream>
#include <string>
#include <vector>

/**
* \brief	Enumeration of each input managed
*/
enum	E_Key
{
	Input_Escape,
	Input_Up,
	Input_Down,
	Input_Left,
	Input_Right,
	Input_Jump,
	Input_Fire,
	Input_Element_1,
	Input_Element_2,
	Input_Element_3,
	Input_Element_4,
	Input_X,
	Input_W,
	Input_C,
	Input_V,
};

/**
* \brief	Class responsible of the management of the inputs
*				It manage keyboard inputs, mouse inputs and mouse location
*/
class			InputManager
{
public:
	/*
	* \brief		Instance the class
	* \return		A pointer to an instance of that class
	*/
	static	InputManager	&Instance();

	/*
	* \brief		Initialize the InputManager and set the parameters of the class
	* \param		hinstance		A handle to the current instance of the application
	* \param		hwnd			A handle to the window of the application
	* \param		screenWidth		The width of the window
	* \param		screenHeight	The height of the window
	* \return		"false" if an error occurs otherwise "true"
	*/
	bool					Initialize(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight);

	/*
	* \brief		Shutdown this object, clearing all variable and releasing everything needed
	*/
	void					Shutdown();

	/*
	* \brief		Read the current state of the keyboard
	*					Check if the focus is losed
	*					Check if a cheatcode is entered
	*					Check if a keyboard input is released
	* \return		"false" if an error occurs when reading the keyboard otherwise "true"
	*/
	bool					Frame();

	/*
	* \brief		Get the location of mouse by filling the parameters
	* \param		mouseX		A reference to the X coordinate of the mouse
	* \param		mouseY		A reference to the Y coordinate of the mouse
	*/
	void					GetMouseLocation(int &mouseX, int &mouseY);

	/*
	* \brief		Check if a keyboard input is pressed
	* \param		input		The enumeration corresponding to the input
	* \return		"true" if the input is pressed, otherwise "false"
	*/
	bool					IsPressed(const E_Key input);

	/*
	* \brief		Check if a keyboard input is released
	* \param		input		The enumeration corresponding to the input
	* \return		"true" if the input is released, otherwise "false"
	*/
	bool					IsReleased(const E_Key input);

	/*
	* \brief		Check if a mouse input is pressed
	* \param		input		The enumeration corresponding to the input
	* \return		"true" if the input is pressed, otherwise "false"
	*/
	bool					IsMousePressed(const E_Key input);

	/*
	* \brief		Check if a mouse input is released
	* \param		input		The enumeration corresponding to the input
	* \return		"true" if the input is pressed, otherwise "false"
	*/
	bool					IsMouseReleased(const E_Key input);

	/*
	* \brief		Check if a keyboard input is pressed only once until the input is released
	* \param		input		The enumeration corresponding to the input
	* \return		"true" if the input is pressed and if it is the first the fonction is call, otherwise return "false"
	*/
	bool					SinglePressed(const E_Key input);

	/*
	* \brief		Open the conf file of the input and start the parsing of this file
	* \return		"false" if an error occurs when the file is opened, otherwise "true" 
	*/
	bool					InitializeInputConfig();

	/*
	* \brief		Pars the conf file of the inputs
	*/
	void					ParsInputConfig();


	/*
	* \brief		Fill each input depending of the input in the conf file
	* \param		name		The name of the input in the conf file
	* \param		control		The value of the input in the conf file
	*/
	void					FillInputList(std::string &name, std::string &control);

	/*
	* \brief		The fonction called when the cheat code "GOD" is entered
	*/
	void					CheatCodeGod();

	/*
	* \brief		The fonction called when the cheat code "NEXT" is entered
	*/
	void					CheatCodeNext();

	/*
	* \brief		The fonction called when the cheat code "END" is entered
	*/
	void					CheatCodeEnd();

	/*
	* \brief		The fonction called when the cheat code "FLY" is entered
	*/
	void					CheatCodeFly();

private:
	InputManager();
	InputManager(const InputManager&);
	~InputManager();

	/**
	* \brief		Pointer to an instance of this class
	*/
	static					InputManager m_instance;

	bool					ReadKeyboard();
	void					ProcessInput();
	void					CheckPressedRelease();
	void					CheckCheatCode();

	void					IntializeListCharacter();

	IDirectInput8			*m_directInput;
	IDirectInputDevice8		*m_keyboard;
	IDirectInputDevice8		*m_mouse;


	unsigned char			m_keyboardState[256];
	DIMOUSESTATE			m_mouseState;
	HWND					m_hwnd;

	int						m_screenWidth, m_screenHeight;
	int						m_mouseX, m_mouseY;
	
	char					listInput[16];
	bool					inputPressed[16];
	std::ifstream			*confFile;
	
	bool					IsInputBlocked;

	std::string				cheatcode;

	/*
	* \brief		Array of the input, indicate for each input if it is pressed
	*/
	bool					m_inputPressed[256];

	/*
	* \brief		Array of the keyboard character
	*					The array is used to convert from the directX macro for keyboard to a code ascii of a character
	*/
	char					m_listCharacter[256];

	/*
	* \brief		Indicate if the cheat code "GOD" is activated
	*/
	bool					m_isGodActivated;
};

#endif