/**
* \author	R�mi Colin
*/
#include "InputManager.h"
#include "../Game/GameObjectFactory.h"
#include "../GameEngine/GameEngine.h"
#include "../Utils/WindowDatas.h"
#include "../UI/UIEngine.h"

#include <windows.h>
#include <iostream>
#include <vector>

InputManager InputManager::m_instance = InputManager();

InputManager::InputManager()
{
	m_directInput = 0;
	m_keyboard = 0;
	m_mouse = 0;
	confFile = NULL;
	listInput[Input_Escape] = DIK_ESCAPE;
	listInput[Input_Up] = DIK_W;
	listInput[Input_Down] = DIK_S;
	listInput[Input_Left] = DIK_A;
	listInput[Input_Right] = DIK_D;	
	listInput[Input_Jump] = DIK_SPACE;
	listInput[Input_Fire] = VK_LBUTTON;
	listInput[Input_Element_1] = DIK_1;
	listInput[Input_Element_2] = DIK_2;
	listInput[Input_Element_3] = DIK_3;
	listInput[Input_Element_4] = DIK_4;
	listInput[Input_X] = DIK_X;
	listInput[Input_W] = DIK_Z;
	listInput[Input_C] = DIK_C;
	listInput[Input_V] = DIK_V;

	inputPressed[Input_Escape] = false;
	inputPressed[Input_Up] = false;
	inputPressed[Input_Down] = false;
	inputPressed[Input_Left] = false;
	inputPressed[Input_Right] = false;
	inputPressed[Input_Jump] = false;
	inputPressed[Input_Fire] = false;
	inputPressed[Input_Element_1] = false;
	inputPressed[Input_Element_2] = false;
	inputPressed[Input_Element_3] = false;
	inputPressed[Input_Element_4] = false;
	inputPressed[Input_X] = false;
	inputPressed[Input_W] = false;
	inputPressed[Input_C] = false;
	inputPressed[Input_V] = false;

	IntializeListCharacter();

	m_isGodActivated = false;
}

InputManager::InputManager(const InputManager& other)
{
}

InputManager &InputManager::Instance()
{
	return m_instance;
}

InputManager::~InputManager()
{
}

bool InputManager::Initialize(HINSTANCE hinstance, HWND hwnd, int screenWidth, int screenHeight)
{
	HRESULT result;

	m_hwnd = hwnd;
	
	// Store the screen size which will be used for positioning the mouse cursor.
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;

	// Initialize the location of the mouse on the screen.
	m_mouseX = screenWidth / 2;
	m_mouseY = screenHeight / 2;
	
	// Initialize the main direct input interface.
	result = DirectInput8Create(hinstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_directInput, NULL);
	if (FAILED(result))
		return false;

	// Initialize the direct input interface for the keyboard.
	result = m_directInput->CreateDevice(GUID_SysKeyboard, &m_keyboard, NULL);
	if (FAILED(result))
		return false;

	// Set the data format.  In this case since it is a keyboard we can use the predefined data format.
	result = m_keyboard->SetDataFormat(&c_dfDIKeyboard);
	if (FAILED(result))
		return false;

	// Set the cooperative level of the keyboard to not share with other programs.
	result = m_keyboard->SetCooperativeLevel(hwnd, DISCL_BACKGROUND | DISCL_NONEXCLUSIVE);
	if (FAILED(result))
		return false;
	
	// Now acquire the keyboard.

#ifdef GAME
	result = m_keyboard->Acquire();
	if (FAILED(result))
		return false;
#endif // GAME

	if (InitializeInputConfig() == false)
		return false;

	return true;
}

bool InputManager::InitializeInputConfig()
{
//#ifdef _DEBUG
//#ifdef GAME
//	this->confFile = new std::ifstream("Ressources/input_config.txt", std::ios::in);
//#else
//	this->confFile = new std::ifstream("../Engine/Ressources/input_config.txt", std::ios::in);
//#endif
//#else
#ifdef NDEBUG
	this->confFile = new std::ifstream("./Ressources/input_config.txt", std::ios::in);
#else
#ifdef GAME
	this->confFile = new std::ifstream("Ressources/input_config.txt", std::ios::in);
#else
	this->confFile = new std::ifstream("../Engine/Ressources/input_config.txt", std::ios::in);
#endif
#endif

//#endif
	if (this->confFile->bad())
		return false;

	if (this->confFile->is_open())
		ParsInputConfig();

	return true;
}

void	InputManager::IntializeListCharacter()
{
	for (int i = 0; i < 256; i++)
	{
		m_listCharacter[i] = NULL;
		m_inputPressed[i] = false;
	}
	m_listCharacter[DIK_D] = 'D';
	m_listCharacter[DIK_E] = 'E';
	m_listCharacter[DIK_G] = 'G';
	m_listCharacter[DIK_N] = 'N';
	m_listCharacter[DIK_O] = 'O';
	m_listCharacter[DIK_F] = 'F';
	m_listCharacter[DIK_L] = 'L';
	m_listCharacter[DIK_Y] = 'Y';
	m_listCharacter[DIK_X] = 'X';
	m_listCharacter[DIK_T] = 'T';
}

void InputManager::Shutdown()
{
	// Release the keyboard.
	if (m_keyboard)
	{
		m_keyboard->Unacquire();
		m_keyboard->Release();
		m_keyboard = 0;
	}

	// Release the main interface to direct input.
	if (m_directInput)
	{
		m_directInput->Release();
		m_directInput = 0;
	}

	return;
}

bool InputManager::Frame()
{
	bool result;
	
	if (GetFocus() == m_hwnd)
	{
		// Read the current state of the keyboard.
		result = ReadKeyboard();
		if (!result)
		{
			return false;
		}

		// Process the changes in the mouse and keyboard.
		ProcessInput();
		CheckCheatCode();
		CheckPressedRelease();
		IsInputBlocked = false;
	}
	else
		IsInputBlocked = true;

	return true;
}

bool InputManager::ReadKeyboard()
{
	HRESULT result;

	// Read the keyboard device.
	result = m_keyboard->GetDeviceState(sizeof(m_keyboardState), (LPVOID)&m_keyboardState);
	if (FAILED(result))
	{
		// If the keyboard lost focus or was not acquired then try to get control back.
		if ((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED))
			m_keyboard->Acquire();
		else
			return false;
	}
	return true;
}

void InputManager::CheckCheatCode()
{
	for (int i = 0; i < 256; i++)
	{
		if (m_keyboardState[DIK_LCONTROL] & 0x80)
		{
			if (m_keyboardState[i] & 0x80)
			{
				if (m_listCharacter[i] && m_inputPressed[i] == false)
					cheatcode = cheatcode + m_listCharacter[i];
				m_inputPressed[i] = true;
			}
		}
		else
		{
			for (int j = 0; j < 256; j++)
				m_inputPressed[i] = false;
			cheatcode = "";
		}
	}

	if (cheatcode == "GOD")
	{
		CheatCodeGod();
		cheatcode = "";
	}
	else if (cheatcode == "NEXT")
	{
		CheatCodeNext();
		cheatcode = "";
	}
	else if (cheatcode == "FLY")
	{
		CheatCodeFly();
		cheatcode = "";
	}
	else if (cheatcode == "END")
	{
		CheatCodeEnd();
		cheatcode = "";
	}
}

void InputManager::ProcessInput()
{
	RECT rect;
	POINT p;
	GetCursorPos(&p);
	ScreenToClient(m_hwnd, &p);

	// Update the location of the mouse cursor based on the change of the mouse location during the frame.
	m_mouseX = p.x;
	m_mouseY = p.y;

	// Ensure the mouse location doesn't exceed the screen width or height.
	if (m_mouseX < 0)  
		m_mouseX = 0;
	if (m_mouseY < 0)
		m_mouseY = 0;

	if (m_mouseX > m_screenWidth)
		m_mouseX = m_screenWidth;
	if (m_mouseY > m_screenHeight)
		m_mouseY = m_screenHeight;

	GetWindowRect(m_hwnd, &rect);
	return;
}

void InputManager::CheckPressedRelease()
{
	for (int i = 0; i < strlen(listInput); i++)
	{
		if (!(m_keyboardState[listInput[i]] & 0x80))
			inputPressed[i] = false;
		if (!(m_keyboardState[i]) & 0x80)
			m_inputPressed[i] = false;
	}
}

void InputManager::GetMouseLocation(int& mouseX, int& mouseY)
{
	mouseX = m_mouseX;
	if (windowDatas.fullScreen == false)
		mouseY = m_mouseY + 33;
	else
		mouseY = m_mouseY;
	return;
}

bool	InputManager::IsPressed(const E_Key input)
{
	return ((m_keyboardState[listInput[input]] & 0x80) ? true : false);
}

bool	InputManager::IsReleased(const E_Key input)
{
	return ((m_keyboardState[listInput[input]] & 0x80) ? false : true);
}

bool	InputManager::SinglePressed(const E_Key input)
{
	if (m_keyboardState[listInput[input]] & 0x80 && !inputPressed[input])
	{
		inputPressed[input] = true;
		return true;
	}
	return false;
}

bool	InputManager::IsMousePressed(const E_Key input)
{
	if (IsInputBlocked)
		return false;
	else
		return (GetAsyncKeyState(VK_LBUTTON) < 0 ? true : false);
}

bool	InputManager::IsMouseReleased(const E_Key input)
{
	if (IsInputBlocked)
		return false;
	else
		return (GetAsyncKeyState(VK_LBUTTON) == 0 ? true : false);
}

void	InputManager::ParsInputConfig()
{
	std::string line;

	if (confFile)
	{
		while (getline((*confFile), line))
		{
			std::string nameInput;
			std::string control;
			size_t cut = line.find_first_of(" : ");

			nameInput = line.substr(0, cut);
			control = line.substr(cut + 3, line.size());
			//FillInputList(nameInput, control);
		}
	}
}

void	InputManager::FillInputList(std::string &name, std::string &control)
{
	char c = NULL;
	if (control.size() == 1)
	{
		c = control[0];
		if (c >= 97 && c <= 122)
			c -= 32;
		if (c >= 65 && c <= 90)
		{
			if (name == "Up")
				listInput[Input_Up] = c;
			else if (name == "Down")
				listInput[Input_Down] = c;
			else if (name == "Right")
				listInput[Input_Right] = c;
			else if (name == "Left")
				listInput[Input_Left] = c;
			else if (name == "W")
				listInput[Input_W] = c;
			else if (name == "X")
				listInput[Input_X] = c;
			else if (name == "C")
				listInput[Input_C] = c;
			else if (name == "V")
				listInput[Input_V] = c;
		}
	}
}

void	InputManager::CheatCodeGod()
{
	if (m_isGodActivated == true)
	{
		GameObjectFactory::Inst()->GetPlayer()->lifeModule->SetImmortal(false);
		m_isGodActivated = false;
	}
	else
	{
		GameObjectFactory::Inst()->GetPlayer()->lifeModule->SetImmortal(true);
		m_isGodActivated = true;
	}
}

void	InputManager::CheatCodeNext()
{
	GameObjectFactory::Inst()->ResetMap(false);
#ifdef GAME
	GameEngine::Engine::Instance()->setLoad(true);
#endif
}

void	InputManager::CheatCodeFly()
{
	GameObjectFactory::Inst()->GetPlayer()->physicModule->EnableGravity(!GameObjectFactory::Inst()->GetPlayer()->physicModule->GravityEnabled());
}

void	InputManager::CheatCodeEnd()
{
#ifdef GAME
	GameEngine::Engine::Instance()->LastLevel();
#endif
}