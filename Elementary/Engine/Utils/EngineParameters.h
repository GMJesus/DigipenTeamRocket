/**
* \author	Aymeric Lambolez
*/

#ifndef _ENGINEPARAMETERS_H_
#define _ENGINEPARAMETERS_H_

#include <list> 

namespace GameEngine
{
	/**
	* \brief	Data structure for the parameters of the engine
	*/
	struct s_EngineParameters
	{
		bool debugCollisionBox; /// \brief	If true, collision box will be print on the screen
		bool drawFPS;			/// \brief	If true, draw the Frame rate on the screen
		bool debugActive;		/// \brief	If true, active the console message printing
	};

	extern s_EngineParameters engineParameters;
}

#endif