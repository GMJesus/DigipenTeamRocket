/**
* \file		 Vec2
* \author    Baptiste DUMAS
* \brief     File with Vec Class
*/

#ifndef VECTOR2_H_
# define VECTOR2_H_

#include <string>

/**
* \brief	Class Vec2 that allow to create 2d point
*/
class Vec2
{
public:
	Vec2(float x = 0, float y = 0);
	~Vec2();

	float	x;
	float	y;

	/**
	* \brief	Calculate the SquaredLength of this
	* \return	Float SquaredLendth of this
	*/
	float	SquaredLength() const;
	/**
	* \brief	Calculate the Dot product of this and the parameter
	* \param	second Is the parameter that will be calculate with this
	* \return	Float Dot product of this and second
	*/
	float	Dot(const Vec2& second) const;
	/**
	* \brief	Calculate the Distance of this and the parameter
	* \param	second Is the parameter that will be calculate with this
	* \return	Float Distance of this and second
	*/
	float	Distance(const Vec2& second)const;
	/**
	* \brief	Normalize this
	* \return	Vec2 Normalize of this
	*/
	Vec2	Normalize()const;
	/**
	* \brief	Make the absolute value of this this
	* \return	Vec2 absolute of this
	*/
	Vec2	Absolute()const;
	/**
	* \brief	Convert this to a string, usually use on debug purpose
	* \return	String PosX: x, PosY: y
	*/
	const	std::string ToString()const;

	Vec2& operator=(const Vec2& second);
	Vec2& operator+=(const Vec2& second);
	Vec2& operator-=(const Vec2& second);
	Vec2& operator/=(const Vec2& second);
	Vec2& operator/=(const float second);
	Vec2& operator*=(const Vec2& second);
	Vec2& operator*=(const float second);
};

Vec2 operator+(Vec2 const &first, Vec2 const &second);
Vec2 operator-(Vec2 const &first, Vec2 const &second);
Vec2 operator*(Vec2 const &first, Vec2 const &second);
Vec2 operator*(Vec2 const &first, const float second);
Vec2 operator/(Vec2 const &first, const float second);
bool operator>(Vec2 const &first, const float second);
bool operator<(Vec2 const &first, const float second);
bool operator>=(Vec2 const &first, const float second);
bool operator>=(Vec2 const &first, const float second);
bool operator==(Vec2 const &first, Vec2 const &second);
bool operator!=(Vec2 const &first, Vec2 const &second);

#endif /* VECTOR2_H_ */