/**
* \author	Aymeric Lambolez
*/

#ifndef WINDOWDATAS_H_
#define WINDOWDATAS_H_

#include <windows.h>

/**
* \brief	Data structure used to store datas about the current window
*/
struct s_WindowDatas
{
	/**
	* \brief	The width of the screen
	*/
	int		screenWidth;
	/**
	* \brief	The height of the screen
	*/
	int		screenHeight;
	/**
	* \brief	Should the window be in fullscreen ?
	*/
	bool	fullScreen;
	/**
	* \brief	Should the VSync be activated ?
	*/
	bool	vSync;
	LPCWSTR	m_applicationName;
	HINSTANCE m_hinstance;
	HWND m_hwnd;
};

extern s_WindowDatas		windowDatas;

#endif // !WINDOWDATAS_H_