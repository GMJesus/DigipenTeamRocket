////////////////////////////////////////////////////////////////////////////////
// Filename: LogWriter.h
////////////////////////////////////////////////////////////////////////////////

#ifndef _LOGWRITER_H_
#define _LOGWRITER_H_

#include <fstream>
#include <time.h>
#include <string>
#include <windows.h>
#include <algorithm>

enum E_MsgType
{
	Log,
	Warning,
	Error,
};

////////////////////////////////////////////////////////////////////////////////
// Class name: LogWriter
////////////////////////////////////////////////////////////////////////////////

class LogWriter
{
public:
	/**
	* \brief	Gets the instance of that class
	*/
	static	LogWriter &Instance();

	/**
	* \brief	Initialize this obejct, opening the file
	* \return	True if initializing went well, false overwise
	*/
	bool	Initialize();
	/**
	* \brief	Shutdown this object, closing the opened file
	*/
	void	Shutdown();
	/**
	* \brief	Add a message to the log file
	* \param	msg		The message to add to the file
	* \param	type	The type of the message to print
	*			If type is Log, the prefix [LOG] will be add
	*			If type is Warning, the prefix [WARNING] will be added
	*			If type is Error, the prefix [ERROR] will be added
	* \return	True if the message has successfully been added to the file, false overwise
	*/
	bool	addLogMessage(const std::string& msg, const E_MsgType type = E_MsgType::Log);

private:
	LogWriter& operator= (const LogWriter &) {}
	LogWriter(const LogWriter &) {}

	char*	GetDate();

	static LogWriter m_instance;
	LogWriter();
	~LogWriter();

	std::ofstream	*file;
};

#endif /* LOGWRITER */