////////////////////////////////////////////////////////////////////////////////
// Filename: EngineParameters.cpp
////////////////////////////////////////////////////////////////////////////////
#include "EngineParameters.h"

EngineParameters EngineParameters::m_instance = EngineParameters();

EngineParameters::EngineParameters()
{
	debugActive = true;
	debugCollisionBox = true;
	drawFPS = true;
}

EngineParameters::~EngineParameters()
{
}

EngineParameters &EngineParameters::Instance()
{
	return m_instance;
}