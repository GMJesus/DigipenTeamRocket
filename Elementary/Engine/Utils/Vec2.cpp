#include "Vec2.h"
#include <fstream>
#include <iostream>

Vec2::Vec2(float x, float y)
{
	this->x = x;
	this->y = y;
}

Vec2::~Vec2()
{

}

float	Vec2::SquaredLength() const
{
	return ((this->x * this->x) + (this->y * this->y));
}

float	Vec2::Dot(const Vec2 &second) const
{
	return (this->x * second.x + this->y * second.y);
}

float	Vec2::Distance(const Vec2& second)const
{
	return sqrt((x - second.x) * (x - second.x) + (y - second.y) * (y - second.y));
}

Vec2	Vec2::Normalize()const
{
	float	length;
	length = sqrt((x * x) + (y * y));
	return (Vec2(x / length, y / length));
}

Vec2	Vec2::Absolute()const
{
	return (Vec2(sqrt(x * x), sqrt(y * y)));
}

const std::string	Vec2::ToString() const
{
	std::string	ret;

	ret.append(" Vec2 Pos X = ");
	ret.append(std::to_string(x));
	ret.append(" Vec2 Pos Y = ");
	ret.append(std::to_string(y));
	return (ret);
}

Vec2&	Vec2::operator=(const Vec2& second)
{
	this->x = second.x;
	this->y = second.y;
	return *this;
}

Vec2&	Vec2::operator+=(const Vec2& second)
{
	this->x += second.x;
	this->y += second.y;
	return *this;
}

Vec2&	Vec2::operator-=(const Vec2& second)
{
	this->x -= second.x;
	this->y -= second.y;
	return *this;
}

Vec2&	Vec2::operator/=(const Vec2& second)
{
	this->x /= second.x;
	this->y /= second.y;
	return *this;
}

Vec2&	Vec2::operator/=(const float second)
{
	this->x /= second;
	this->y /= second;
	return *this;
}

Vec2&	Vec2::operator*=(const Vec2& second)
{
	this->x *= second.x;
	this->y *= second.y;
	return *this;
}

Vec2&	Vec2::operator*=(const float second)
{
	this->x *= second;
	this->y *= second;
	return *this;
}

Vec2	operator+(Vec2 const &first, Vec2 const &second)
{
	return (Vec2(first.x + second.x, first.y + second.y));	
}

Vec2	operator-(Vec2 const &first, Vec2 const &second)
{
	return (Vec2(first.x - second.x, first.y - second.y));
}

Vec2	operator*(Vec2 const &first, Vec2 const &second)
{
	return (Vec2(first.x * second.x, first.y * second.y));
}

Vec2	operator*(Vec2 const &first, float const second)
{
	return (Vec2(first.x * second, first.y * second));
}

Vec2	operator/(Vec2 const &first, float const second)
{
	return (Vec2(first.x / second, first.y / second));
}

bool	operator<(Vec2 const &first, float const second)
{
	return (sqrt(first.SquaredLength()) < second);
}

bool	operator==(Vec2 const &first, const Vec2& second)
{
	return ((first.x == second.x) && (first.y == second.y));
}

bool	operator!=(Vec2 const &first, const Vec2& second)
{
	return ((first.x != second.x) && (first.y != second.y));
}