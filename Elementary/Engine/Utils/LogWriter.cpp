////////////////////////////////////////////////////////////////////////////////
// Filename: LogWriter.cpp
////////////////////////////////////////////////////////////////////////////////

#include "LogWriter.h"
#include <Windows.h>
#include <ShellAPI.h>
#include <KnownFolders.h>
#include <ShlObj.h>

LogWriter LogWriter::m_instance = LogWriter();

LogWriter::LogWriter()
{
}

LogWriter::~LogWriter()
{
}

LogWriter &LogWriter::Instance()
{
	return m_instance;
}

bool	LogWriter::Initialize()
{
	this->file = new std::ofstream();
	TCHAR* path = 0;

	SHGetKnownFolderPath(FOLDERID_Documents, KF_FLAG_SIMPLE_IDLIST, NULL, &path);
	char documentsPath[100];
	wcstombs(documentsPath, path, wcslen(path) + 1);
	strcat(documentsPath, "\\Elementary\\");
	wchar_t documentsPathW[64];
	mbstowcs(documentsPathW, documentsPath, strlen(documentsPath) + 1);
	CreateDirectoryW(documentsPathW, NULL);
	strcat(documentsPath, "Logs\\");
	mbstowcs(documentsPathW, documentsPath, strlen(documentsPath) + 1);

	if (CreateDirectoryW(documentsPathW, NULL) ||
		ERROR_ALREADY_EXISTS == GetLastError())
	{
		char *ptr;

		ptr = GetDate();
		std::string date(ptr);
		std::replace(date.begin(), date.end(), ' ', '_');
		std::replace(date.begin(), date.end(), ':', '-');
		date.erase(date.find('\n'));
		this->file->open(documentsPath + date + ".log");
		if (this->file->bad())
			return false;
	}
	else
	{
		return false;
	}
	return true;
}

char*	LogWriter::GetDate()
{
	char* ret;
	time_t *now = new time_t();
	time(now);
	ret = ctime(now);
	delete (now);
	return ret;
}

void	LogWriter::Shutdown()
{
	this->file->close();
	delete(this->file);
}

bool	LogWriter::addLogMessage(const std::string& msg, const E_MsgType type)
{
	char*	date;

	bool tmp = this->file->bad();
	if (this->file->bad())
		return false;
	this->file->seekp(0, std::ios::end);
	date = GetDate();
	this->file->write((const char*)date, strlen(date) - 1);
	this->file->write(" -", 2);
	if (type == Warning)
		this->file->write((const char*)" [WARNING] ", 11);
	else if (type == Error)
		this->file->write((const char*)" [ERROR] ", 9);
	else if (type == Log)
		this->file->write((const char*)" [INFO] ", 8);
	this->file->write((const char*)msg.c_str(), msg.size());
	this->file->write((const char*)"\n", 1);
	return true;
}