/**
* \author	Alexy Durand
*/
#include "SoundManager.h"

SoundManager SoundManager::m_instance = SoundManager();

SoundManager::SoundManager()
{
	if (FMOD::System_Create(&_system) != FMOD_OK)
		return;
	_system->getVersion(&_version);
	if (_version < FMOD_VERSION)
		return;
	int driverCount = 0;
	_system->getNumDrivers(&driverCount);
	if (driverCount == 0)
		return;
	_system->init(36, FMOD_INIT_NORMAL, NULL);
}

SoundManager::~SoundManager()
{
}

SoundManager &SoundManager::Instance()
{
	return m_instance;
}

void SoundManager::createSound(SoundClass *pSound, const char *pFile)
{
	_system->createSound(pFile, FMOD_HARDWARE, 0, pSound);
}

void SoundManager::playSound(SoundClass pSound, FMOD::Channel *channel, bool bLoop)
{
	if (!bLoop)
		pSound->setMode(FMOD_LOOP_OFF);
	else
	{
		pSound->setMode(FMOD_LOOP_NORMAL);
		pSound->setLoopCount(-1);
	}
	_system->playSound(FMOD_CHANNEL_FREE, pSound, false, &channel);
}

void SoundManager::pauseChannel(FMOD::Channel *channel)
{
	channel->setPaused(true);
}

void SoundManager::resumeChannel(FMOD::Channel *channel)
{
	channel->setPaused(false);
}

void SoundManager::releaseSound(SoundClass pSound)
{
	pSound->release();
}

void SoundManager::releaseChannel(FMOD::Channel *channel)
{
	channel->stop();
}

