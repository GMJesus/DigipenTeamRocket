/**
* \file      SoundManager
* \author    Alexy DURAND
* \brief     Singleton. Handle all the sound stuff of the engine with FMOD.
*
*/

#ifndef _SOUNDMANAGER_H_
#define _SOUNDMANAGER_H_


//////////////
// INCLUDES //
//////////////
#pragma once
#pragma comment(lib,"fmodex_vc.lib") 
#include "External libraries\FMOD\fmod.hpp"
#include "External libraries\FMOD\fmod_errors.h"
#include <iostream>

typedef FMOD::Sound *SoundClass;

/**
* \brief     Singleton. Handle all the sound stuff of the engine with FMOD.
*/
class SoundManager
{
public:
	/**
	* \brief     Access to the class form any files.
	* \return    Instance on the singleton of the class.
	*/
	static SoundManager& Instance();

	/**
	* \brief    Initialize a SoundClass with a given path.
	* \param    pSound        SoundClass to initialize.
	* \param    pFile         Path to the chosen sound.
	*/
	void createSound(SoundClass *pSound, const char *pFile);

	/**
	* \brief    Play a sound.
	* \param    pSound        Sound to play.
	* \param    bLoop         Boolean to determine if the sound has to be played in loop or no.
	*/
	void playSound(SoundClass pSound, FMOD::Channel *channel, bool bLoop = false);

	/**
	* \brief    Pause the channel.
	*/
	void pauseChannel(FMOD::Channel *channel);

	/**
	* \brief    Resume the channel.
	*/
	void resumeChannel(FMOD::Channel *channel);

	/**
	* \brief    Delete the SoundClass form the memory.
	* \param    pSound        SoundClass to delete.
	*/
	void releaseSound(SoundClass pSound);

	void releaseChannel(FMOD::Channel *channel);

private:
	SoundManager& operator= (const SoundManager &) {}
	SoundManager(const SoundManager &) {}

	/**
	* \brief      Instance of the class.
	*/
	static SoundManager m_instance;

	/**
	* \brief    Constructor
	*/
	SoundManager();

	/**
	* \brief    Destructor.
	*/
	~SoundManager();

	/**
	* \brief      Pointer on FMOD System
	*/
	FMOD::System *_system;
	unsigned int _version;
};

#endif