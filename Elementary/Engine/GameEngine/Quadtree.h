/**
* \author	Aymeric Lambolez
*/

#ifndef		QUADTREE_H_
# define	QUADTREE_H_

#include	<list>

#include	"Physics\AABB.h"
#include	"Game\GameObject.h"
#include	"Modules\ModuleImport.h"

namespace GameEngine
{
	/**
	* \brief	Class for space partionning, using a quadtree algorithm
	*/
	class Quadtree
	{
	public:
		/**
		* \brief	Constructor
		* \param	position	The center of the first node of this Quadtree
		* \param	size		The size of the first node of this Quadtree
		*/
		Quadtree(const Vec2& position, const Vec2& size);
		/**
		* \brief	Constructor
		* \param	boundingBox	The datas to use as position and size of the first node of this Quadtree
		*/
		Quadtree(Collision::AABB* boundingBox);
		~Quadtree();

		/**
		* \brief	The maximum object capacity of each node
		*/
		const unsigned int MAX_CAPACITY = 10;

		/**
		* \brief	Insert an element in this Quadtree
		*			If the node is a MAX_CAPACITY, it will split and the element will be put in a child node
		* \param	object	The object to insert in the Quadtree
		* \return	True if the object has been inserted, false overwise
		*/
		bool					Insert(GameObject* object);
		/**
		* \brief	Returns the list of object contained in the box passed as parameter
		* \param	range	The box in which to find the objects to return
		* \param	precise	If false, this function will return all object containg by the Quadtree if range fits in it
		*			If true, will returns just the objects that are in collision with the box
		* \return	The list of object contained in the box passed as parameter
		*			If the list should be empty, returns NULL instead
		*/
		std::list<GameObject*>*	QueryRange(const Collision::AABB* range, bool precise = false);
		/**
		* \brief	Clear the Quadtree, erasing all datas in it and in its children
		*/
		void					Clear();

	private:
		/**
		* \brief	The bounding box of this Quadtree
		*/
		Collision::AABB*		_boundingBox;
		/**
		* \brief	The list of objects contained by this Quadtree
		*/
		std::list<GameObject*>	_objects;
		/**
		* \brief	A child node
		*/
		Quadtree*				_topRight;
		/**
		* \brief	A child node
		*/
		Quadtree*				_botRight;
		/**
		* \brief	A child node
		*/
		Quadtree*				_topLeft;
		/**
		* \brief	A child node
		*/
		Quadtree*				_botLeft;

		/**
		* \brief	Divide this Quadtree, generating all the children nodes
		*/
		void					Divide();
		/**
		* \brief	Check if an object is contained in the given bounding box
		* \param	obj			The object to find in the box
		* \param	boundingBox	The box in which to find the object1
		*/
		bool					ObjectInAABB(GameObject* obj, const Collision::AABB* boundingBox);
	};
}

#endif		// !QUADTREE_H_
