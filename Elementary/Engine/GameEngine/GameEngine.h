/**
* \author	Aymeric Lambolez
*/

#ifndef		GAMEENGINE_H_
# define	GAMEENGINE_H_

#ifdef GAME
# define    RESOURCES_FOLDER "Ressources\\"
#else
# define	RESOURCES_FOLDER "../Engine/Ressources/"
#endif
# define    LEVEL_EXTENSION	".elemlvl"
# define    LEVEL_FOLDER "Levels\\"
# define    MENU_FILE "menus.elem"


#include "Inputs\InputManager.h"
#include "Graphics\GraphicEngine.h"
#include "GameEngine\System.h"
#include "Utils\WindowDatas.h"
#include "Sounds\SoundManager.h"
#include "GameEngine\Quadtree.h"
#include "Game\GameObjectFactory.h"
#include "UI\UIEngine.h"
#include "XML\XmlParser.h"

/**
* \brief	Namespace defining the Game Engine datas
*/
namespace GameEngine{

	/**
	* \brief	Game Engine class
	*/
	class Engine
	{
	public:
		/**
		* \brief	Initialize this object, and all other engine components
		* \return	True if everything went well, false overwise
		*/
		bool	Initialize();
		/**
		* \brief	Run the Game Loop
		*			Returns only when the game is finished
		*/
		void	Run();
		/**
		* \brief	Shutdown this class, and all other engine components
		*/
		void	Shutdown();
		/**
		* \brief	Stops the GameEngine, that will quit at the end of the frame
		*/
		static void	Stop();

		/**
		* \brief	Sets the position where to spawn the player
		* \param	pos		The new position where to spawn the player
		*/
		void	SetSpawningPosition(const Vec2& pos);
		/**
		* \brief	Returns the current position where to spawn the player
		* \return	The position where to spawn the player
		*/
		const Vec2&	GetSpawningPosition()const;
		/**
		* \brief	Resets the player, re-spawning it at the current spawning position, and reseting the datas that needs to be resets
		*/
		void	ResetPlayer()const;
#ifdef GAME
        bool    _buttonHold;

		/**
		* \brief	Exits the pause
		*/
		void    Resume();
		/**
		* \brief	Pause the Game Engine
		*/
		void    Pause();

		/**
		* \brief	Load the different menus
		*/
		bool    LoadMenus();
#endif

		/**
		* \brief	Play the given sound
		* \param	soundFile	The path to the sound to play
		* \param    loop	Should this sound be looping
		*/
		void	PlayingSound(char *soundFile, bool loop);

		/**
		* \brief	Loads the given level
		* \param	levelPath The path to the level to load
		* \return	True if the level has been successfully loaded, false overwise
		*/
		bool	LoadLevel(const char* levelPath);
		/**
		* \brief	Automatically load the next level
		* \return	True if the level has been successfully loaded, false overwise
		*/
		bool	LoadNextLevel();

		/**
		* \brief	Gets the current instance of this class
		* \return	The current instance of the Game Engine
		*/
		static Engine * Instance();

		int		getLoad();
		void	setLoad(int set);

		void	ResetLevels();
		void	LastLevel();


	private:
		/**
		* \brief	Instance of the current object
		*/
		static Engine * _inst;

		/**
		* \brief	Pointer to the Input Manager
		*/
		InputManager	*m_Input;

		/**
		* \brief	Pointer to the Graphic Engine
		*/
		Graphic::GraphicEngine	*m_Graphic;

		/**
		* \brief	Pointer to the System Manager
		*/
		System*			m_System;

		/**
		* \brief	Instance of the UI Engine
		*/
		UIEngine::UI&	m_UI = UIEngine::UI::Instance();

		/**
		* \brief	The position where to spawn the player
		*/
		Vec2			_spawningPosition;

		/**
		* \brief	The list of levels to load
		*/
		list<std::string*> _levels;
		list<std::string*>::iterator _currentLevel;
		bool			_first;
		bool			_last;
		int				_load;
#ifdef GAME
		const char* PREFERENCES_FILE = "Ressources/GameEngine.ini";
#else
		const char* PREFERENCES_FILE = "../Engine/Ressources/GameEngine.ini";
#endif
		unsigned long	_lastFrameTime;
		float			_totalTime;

		Engine();
		Engine(const Engine &);
		~Engine();
		/**
		* \brief		The Game Loop
		* \return		True while everything is normal, false if there was an error or if the Game have to exit
		*/
		bool			GameLoop();
		/**
		* \brief		Loads the preferences of this object
		*/
		void			LoadPreferences();
		/**
		* \brief		Fills the current Quadtree with the currents object
		*/
		void			FillQuadtree();
		/**
		* \brief		Update an object
		* \param		toUpdate			The object to update
		* \param		displayModuleList	The current list of displayModule
		* \param		objectList			The current list of objects to update
		*/
		void			UpdateObject(GameObject* toUpdate, std::list<DisplayModule*> &displayModuleList, std::list<GameObject*>* objectList);
		bool			InitializeLevelsFiles();
	};

	extern s_EngineParameters	engineParameters;
	Quadtree*					GetActualQuadtree();
	/**
	* \brief		Gets the time between this frame and last frame
	* \return		The time between this frame and last frame
	*/
	float						DeltaTime();
}

/**
* \brief	The window datas of the Game, containing usefull informations about the current window state
*/
extern s_WindowDatas		windowDatas;
#ifdef GAME
extern bool                 pause;
#endif

#endif		/* GAMEENGINE_H_ */