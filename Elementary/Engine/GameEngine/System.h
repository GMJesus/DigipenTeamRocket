/**
* \author	Thomas Hannot
*/

#ifndef _SYSTEM_H_
#define _SYSTEM_H_


#define WIN32_LEAN_AND_MEAN


#include "Utils\WindowDatas.h"
#include "Inputs\InputManager.h"
#include "Graphics\GraphicEngine.h"

/**
* \brief	Class responsible of the management with the system
*			Also responsible of the creation of the window
*/
class System
{
public:
	System();
	System(const System&);
	~System();

	/**
	* \brief	Initialize the class and the window
	* \param	datas	The datas to use for the creation of the window
	*/
	bool Initialize(s_WindowDatas &datas);
	/**
	* \brief	Shutdown the class, leading the window to close
	*/
	void Shutdown();

	/**
	* \brief	A callback triggered when a message is sent by the system
	*/
	LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM);
	/**
	* \brief	Initialize the window
	*/
	void InitializeWindows(int&, int&);
	/**
	* \brief	Shutdown the window
	*/
	void ShutdownWindows();

	/**
	* \brief	The datas of the current window
	*/
	s_WindowDatas datas;
};


/////////////////////////
// FUNCTION PROTOTYPES //
/////////////////////////
static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


/////////////
// GLOBALS //
/////////////
static System* ApplicationHandle = 0;


#endif