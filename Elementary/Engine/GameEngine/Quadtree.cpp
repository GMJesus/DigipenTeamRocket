/**
* \author	Aymeric Lambolez
*/

#include	"GameEngine\Quadtree.h"

using namespace GameEngine;

Quadtree::Quadtree(const Vec2& position, const Vec2& size)
{
	_topLeft = NULL;
	_topRight = NULL;
	_botLeft = NULL;
	_botRight = NULL;
	_boundingBox = new Collision::AABB();
	_boundingBox->Initialize(position, size.x, size.y);
}

Quadtree::Quadtree(Collision::AABB* boundingBox)
{
	_topLeft = NULL;
	_topRight = NULL;
	_botLeft = NULL;
	_botRight = NULL;
	_boundingBox = new Collision::AABB();
	_boundingBox->Initialize(boundingBox->Position(), boundingBox->Width(), boundingBox->Height());
}

Quadtree::~Quadtree()
{

}

void	Quadtree::Clear()
{
	_objects.clear();
	if (_topLeft)
	{
		_topLeft->Clear();
		delete(_topLeft);
		_topLeft = NULL;
		_topRight->Clear();
		delete(_topRight);
		_topRight = NULL;
		_botLeft->Clear();
		delete(_botLeft);
		_botLeft = NULL;
		_botRight->Clear();
		delete(_botRight);
		_botRight = NULL;
	}
	if (_boundingBox)
	{
		delete(_boundingBox);
		_boundingBox = NULL;
	}
}

bool	Quadtree::Insert(GameObject* newObject)
{
	if (!newObject->transformModule)
		return false;
	if (!_boundingBox->IsPointInAABB(newObject->transformModule->Position()))
		return false;
	if (_objects.size() < MAX_CAPACITY)
	{
		_objects.push_back(newObject);
		return true;
	}

	if (_topLeft == NULL)
		Divide();

	if (_topLeft->Insert(newObject))
		return true;
	if (_botLeft->Insert(newObject))
		return true;
	if (_topRight->Insert(newObject))
		return true;
	if (_botRight->Insert(newObject))
		return true;

	return false;
}

void	Quadtree::Divide()
{
	Collision::AABB* param = new Collision::AABB();

	param->Initialize(Vec2{ _boundingBox->Position().x - _boundingBox->Width() / 4, _boundingBox->Position().y + _boundingBox->Height() / 4 }, _boundingBox->Width() / 2, _boundingBox->Height() / 2);
	_topLeft = new Quadtree(param);
	param->SetPosition(Vec2{ _boundingBox->Position().x + _boundingBox->Width() / 4, _boundingBox->Position().y + _boundingBox->Height() / 4 });
	_topRight = new Quadtree(param);
	param->SetPosition(Vec2{ _boundingBox->Position().x - _boundingBox->Width() / 4, _boundingBox->Position().y - _boundingBox->Height() / 4 });
	_botLeft = new Quadtree(param);
	param->SetPosition(Vec2{ _boundingBox->Position().x + _boundingBox->Width() / 4, _boundingBox->Position().y - _boundingBox->Height() / 4 });
	_botRight = new Quadtree(param);
	delete (param);
}

bool						Quadtree::ObjectInAABB(GameObject* obj, const Collision::AABB* boundingBox)
{
	if (obj && obj->transformModule)
	{
		if (obj->boxCollisionModule)
		{
			if (obj->boxCollisionModule->GetType() == AABB || obj->boxCollisionModule->GetType() == OBB)
			{
				Collision::AABB* other = new Collision::AABB();

				other->Initialize(obj->transformModule->Position(), obj->boxCollisionModule->GetBoxWidth(), obj->boxCollisionModule->GetBoxHeight());
				bool ret = boundingBox->VsAABB(other);
				delete(other);
				return ret;
			}
			if (obj->boxCollisionModule->GetType() == OBB)
				;
			if (obj->boxCollisionModule->GetType() == Circle)
				;
		}
		else
			return boundingBox->IsPointInAABB(obj->transformModule->Position());
	}
	return obj->transformModule;
}

std::list<GameObject*>*		Quadtree::QueryRange(const Collision::AABB* boundingBox, bool precise)
{
	if (!boundingBox->VsAABB(_boundingBox))
		return NULL;

	std::list<GameObject*>*	inRange = new std::list < GameObject* > ;

	if (!precise)
		inRange->insert(inRange->begin(), _objects.begin(), _objects.end());
	else
	{
		for (std::list<GameObject*>::const_iterator it = _objects.begin(); it != _objects.end(); ++it)
		{
			if (ObjectInAABB(*it, boundingBox))
				inRange->push_back(*it);
		}
	}

	if (_topLeft)
	{
		std::list<GameObject*>* ret = NULL;
		ret = _topLeft->QueryRange(boundingBox, precise);
		if (ret)
		{
			inRange->merge(*ret);
			delete(ret);
		}
		ret = _topRight->QueryRange(boundingBox, precise);
		if (ret)
		{
			inRange->merge(*ret);
			delete(ret);
		}
		ret = _botLeft->QueryRange(boundingBox, precise);
		if (ret)
		{
			inRange->merge(*ret);
			delete(ret);
		}
		ret = _botRight->QueryRange(boundingBox, precise);
		if (ret)
		{
			inRange->merge(*ret);
			delete(ret);
		}
	}
	if (!inRange->size())
	{
		delete (inRange);
		return NULL;
	}
	return inRange;
}
