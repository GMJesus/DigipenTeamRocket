/**
* \author	Thomas Hannot
*/

#include "System.h"
#include "GameEngine\GameEngine.h"
#include "Graphics/DirectXInterface.h"

System::System()
{
    memset(&datas, 0, sizeof(s_WindowDatas));
}


System::System(const System& other)
{
    memset(&datas, 0, sizeof(s_WindowDatas));
}


System::~System()
{
}


bool System::Initialize(s_WindowDatas &extDatas)
{
	// Initialize the width and height of the screen to zero before sending the variables into the function.
	datas.screenWidth = 0;
	datas.screenHeight = 0;

	// Initialize the windows api.
	InitializeWindows(datas.screenWidth, datas.screenHeight);

	extDatas.m_applicationName = datas.m_applicationName;
	extDatas.m_hinstance = datas. m_hinstance;
	extDatas.m_hwnd = datas.m_hwnd;
	extDatas.screenWidth = datas.screenWidth;
	extDatas.screenHeight = datas.screenHeight;
	
	return true;
}


void System::Shutdown()
{
	// Shutdown the window.
    ShutdownWindows();
}

LRESULT CALLBACK System::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{
	return DefWindowProc(hwnd, umsg, wparam, lparam);
}


void System::InitializeWindows(int& screenWidth, int& screenHeight)
{
	WNDCLASSEX wc;
	DEVMODE dmScreenSettings;
	int posX, posY;


	// Get an external pointer to this object.	
	ApplicationHandle = this;

	// Get the instance of this application.
	datas.m_hinstance = GetModuleHandle(NULL);

	// Give the application a name.
	datas.m_applicationName = L"Elementary";

	// Setup the windows class with default settings.
    wc.style         = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc   = WndProc;
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = datas.m_hinstance;
	wc.hIcon		 = LoadIcon(NULL, IDI_WINLOGO);
	wc.hIconSm       = wc.hIcon;
	wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(COLOR_WINDOW + 1);
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = datas.m_applicationName;
	wc.cbSize        = sizeof(WNDCLASSEX);
	
	// Register the window class.
	RegisterClassEx(&wc);

    // Set windows to fullscreen
    //windowDatas.fullScreen = true;

	// Determine the resolution of the clients desktop screen.
	screenWidth  = GetSystemMetrics(SM_CXSCREEN);
	screenHeight = GetSystemMetrics(SM_CYSCREEN);

	// Setup the screen settings depending on whether it is running in full screen or in windowed mode.
	if (windowDatas.fullScreen)
	{
        // Set the position of the window to the top left corner.
        posX = posY = 0;
    }
	else
	{
		// If windowed then set it to 800x600 resolution.
		screenWidth  = windowDatas.screenWidth;
		screenHeight = windowDatas.screenHeight;

		// Place the window in the middle of the screen.
		posX = (GetSystemMetrics(SM_CXSCREEN) - screenWidth)  / 2;
		posY = (GetSystemMetrics(SM_CYSCREEN) - screenHeight) / 2;
	}

#ifndef EDITOR
	// Create the window with the screen settings and get the handle to it.
	datas.m_hwnd = CreateWindowExW(WS_EX_CLIENTEDGE, datas.m_applicationName, datas.m_applicationName, WS_OVERLAPPEDWINDOW,
		posX, posY, screenWidth, screenHeight, NULL, NULL, datas.m_hinstance, NULL);

	// Message Box if Window creation fails
	if (datas.m_hwnd == NULL)
	{
		MessageBoxW(NULL, L"Window Creation Failed!", L"Error!",
			MB_ICONEXCLAMATION | MB_OK);
		return;
	}

	// Bring the window up on the screen and set it as main focus.
	ShowWindow(datas.m_hwnd, SW_SHOW);
	SetForegroundWindow(datas.m_hwnd);
	SetFocus(datas.m_hwnd);
#endif
	// Hide the mouse cursor.
	ShowCursor(true);
}


void System::ShutdownWindows()
{
	// Show the mouse cursor.
	ShowCursor(true);

	// Fix the display settings if leaving full screen mode.
	if(datas.fullScreen)
	{
		ChangeDisplaySettings(NULL, 0);
	}

	// Remove the window.
	DestroyWindow(datas.m_hwnd);
	datas.m_hwnd = NULL;

	// Remove the application instance.
	UnregisterClassW(datas.m_applicationName, datas.m_hinstance);
	datas.m_hinstance = NULL;

	// Release the pointer to this class.
	ApplicationHandle = NULL;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam)
{
    switch (umessage)
    {
        // Check if the window is being destroyed.
    case WM_DESTROY:
    {
                       DestroyWindow(hwnd);
                       break;
    }

        // Check if the window is being closed.
    case WM_CLOSE:
    {
                     PostQuitMessage(0);
                     break;
    }

    case WM_SIZE:
        /*
        **  Resize of the windows
        */

    if (DirectXInterface::Instance().m_swapChain)
    {
        DirectXInterface::Instance().GetDeviceContext()->OMSetRenderTargets(0, 0, 0);

        // Release all outstanding references to the swap chain's buffers.
        DirectXInterface::Instance().m_renderTargetView->Release();

        HRESULT hr;
        // Preserve the existing buffer count and format.
        // Automatically choose the width and height to match the client rect for HWNDs.
        hr = DirectXInterface::Instance().m_swapChain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0);

        // Perform error handling here!

        // Get buffer and create a render-target-view.
        ID3D11Texture2D* pBuffer;
        hr = DirectXInterface::Instance().m_swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D),
            (void**)&pBuffer);
        // Perform error handling here!

        hr = DirectXInterface::Instance().GetDevice()->CreateRenderTargetView(pBuffer, NULL,
            &DirectXInterface::Instance().m_renderTargetView);
        // Perform error handling here!
        pBuffer->Release();

        DirectXInterface::Instance().GetDeviceContext()->OMSetRenderTargets(1, &DirectXInterface::Instance().m_renderTargetView, NULL);

        RECT rect;
        if (GetClientRect(windowDatas.m_hwnd, &rect))
        {
            windowDatas.screenWidth = rect.right - rect.left;
            windowDatas.screenHeight = rect.bottom - rect.top;
        }

        // Set up the viewport.
        D3D11_VIEWPORT vp;
        vp.Width = windowDatas.screenWidth;
        vp.Height = windowDatas.screenHeight;
        vp.MinDepth = 0.0f;
        vp.MaxDepth = 1.0f;
        vp.TopLeftX = 0;
        vp.TopLeftY = 0;
        DirectXInterface::Instance().GetDeviceContext()->RSSetViewports(1, &vp);
    }
    case WM_MOVE:
        /*
        **  Move the windows
        */
#ifdef GAME
        GameEngine::Engine::Instance()->Pause();
#endif

        break;

    case WM_ACTIVATE:
        /*
        **  (Re)activate the windows
        */
    case WM_SETFOCUS:
        /*
        **  When focus on the windows
        */

        // Reset the resolution of the game
        if (windowDatas.fullScreen)
        {
            // Hide the mouse cursor
            ShowCursor(true);
        }
        break;

    case WA_INACTIVE:
        /*
        **  desactivate the windows without the mouse
        **  like Alt + Tab
        */
    case WM_KILLFOCUS:
        /*
        **  When focus is not anymore on the windows
        */
    case SIZE_MINIMIZED:
        /*
        **  When the windows is being minimized
        */

        // Set to pause
#ifdef GAME
        GameEngine::Engine::Instance()->Pause();
#endif
        break;

        // All other messages pass to the message handler in the system class.
	default:
		return ApplicationHandle->MessageHandler(hwnd, umessage, wparam, lparam);
    }
    return 0;
}