/**
* \author	Aymeric Lambolez
*/

#include "GameEngine\GameEngine.h"
#include "UI/Menu/MenuFactory.h"

s_WindowDatas		windowDatas;
#ifdef GAME
bool                pause;
#endif
namespace GameEngine
{
	Engine*				Engine::_inst = NULL;
	Quadtree*			quadtree = NULL;
	s_EngineParameters	engineParameters;
	float				deltaTime = 0.f;
	bool				_done;

	/// TEMPORAIRE
	SoundClass*			backgroundSound;
	FMOD::Channel*		soundChannel;
	///

	Quadtree*	GetActualQuadtree()
	{
		return quadtree;
	}

	float		DeltaTime()
	{
		return deltaTime;
	}

	Engine::Engine()
	{
		m_Input = nullptr;
		m_Graphic = nullptr;
		_done = false;

        m_System = nullptr;
        _lastFrameTime = 0;
        _totalTime = 0;
		_first = true;
		_last = false;
		_load = 0;

#ifdef GAME
        _buttonHold = false;
#endif
	}

	Engine::Engine(const Engine& other)
	{
        m_Input = other.m_Input;
        m_Graphic = other.m_Graphic;

        m_System = other.m_System;
        _lastFrameTime = other._lastFrameTime;
        _totalTime = other._totalTime;

#ifdef GAME
        _buttonHold = other._buttonHold;
#endif
	}

	Engine::~Engine()
	{
	}

	void		Engine::SetSpawningPosition(const Vec2& pos)
	{
		_spawningPosition = pos;
	}

	const Vec2&	Engine::GetSpawningPosition()const
	{
		return _spawningPosition;
	}

	void		Engine::ResetPlayer()const
	{
		Character* player = GameObjectFactory::Inst()->GetPlayer();
		player->transformModule->SetPosition(_spawningPosition);
		player->physicModule->SetVelocity(Vec2(0, 0));
		player->lifeModule->SetHealth(player->lifeModule->GetMaxHealth());
		Graphic::Camera::Instance()->SetPosition(player->transformModule->Position().x, player->transformModule->Position().y, Graphic::Camera::Instance()->GetPosition().z);
	}

	void		Engine::LoadPreferences()
	{
		bool result;

		XML::XmlParser* xmlParser = new XML::XmlParser;
		xmlParser->GetConfig(PREFERENCES_FILE);
		delete (xmlParser);
	}

	void		Engine::ResetLevels()
	{
		_first = true;
		GameObjectFactory::Inst()->ResetMap(false);
		_load = 2;
	}

	bool		Engine::LoadLevel(const char* levelPath)
	{
		std::string name = RESOURCES_FOLDER + std::string(LEVEL_FOLDER);
		name.append(levelPath);
#ifndef GAME
		name.append(LEVEL_EXTENSION);
#endif
		XML::XmlParser* xmlParser = new XML::XmlParser;
		xmlParser->GetLevel(name.c_str());

		if (GameObjectFactory::Inst()->GetPlayer())
		{
			Vec2 pos = GameObjectFactory::Inst()->GetPlayer()->transformModule->Position();
			SetSpawningPosition(pos);
			ResetPlayer();
		}

		delete(xmlParser);
		return true;
	}

	void		Engine::LastLevel()
	{
		GameObjectFactory::Inst()->ResetMap(false);
		int i = 0;
		for (auto lvl = _levels.begin(); lvl != _levels.end(); ++lvl, i++)
		{
			if (i == _levels.size() - 1)
				_currentLevel = lvl;
		}
		_last = true;
		_load = 2;
	}

	bool		Engine::LoadNextLevel()
	{
		if (_first)
		{
			InitializeLevelsFiles();
			_currentLevel = _levels.begin();
			_first = false;
			return LoadLevel((*_currentLevel)->c_str());
		}
		if (!_last)
			++_currentLevel;
		_last = false;
		if (_currentLevel != _levels.end())
			return LoadLevel((*_currentLevel)->c_str());
		_load = 0;
		_lastFrameTime = timeGetTime();
		return false;
	}

	void		Engine::setLoad(int set) { _load = set; }
	int			Engine::getLoad() { return _load; }

	bool		Engine::InitializeLevelsFiles()
	{
		HANDLE dir;
		WIN32_FIND_DATA file;

		dir = FindFirstFile(L"./Ressources/Levels/*", &file);
		if (dir == INVALID_HANDLE_VALUE)
			return false;

		while (FindNextFile(dir, &file))
		{
			wstring ws = file.cFileName;
			std::string toString(ws.begin(), ws.end());
			if (toString.compare(".") != 0 && toString.compare("..") != 0)
			{
				std::string *level = new std::string(toString);
				_levels.push_back(level);
			}
		}
		return true;
	}

#ifdef GAME
    bool		Engine::LoadMenus()
    {
        std::string name = RESOURCES_FOLDER + std::string(MENU_FILE);
		XML::XmlParser* xmlParser = new XML::XmlParser;
        xmlParser->GetMenus(name.c_str());

        delete(xmlParser);
        return true;
    }
#endif

	Engine *	Engine::Instance()
	{
		if (!_inst)
			_inst = new Engine();
		return _inst;
	}

	bool		Engine::Initialize()
	{
		bool result;

		LogWriter::Instance().Initialize();
		LoadPreferences();

		_totalTime = 0;
		m_System = new System;
		if (!m_System)
			return false;

		result = m_System->Initialize(windowDatas);
		if (!result)
		{
			MessageBoxW(windowDatas.m_hwnd, L"Could not initalize the System object", L"Error", MB_OK);
			return false;
		}

		m_Input = &InputManager::Instance();
		if (!m_Input)
			return false;

		result = m_Input->Initialize(windowDatas.m_hinstance, windowDatas.m_hwnd, windowDatas.screenWidth, windowDatas.screenHeight);
		if (!result)
		{
			MessageBoxW(windowDatas.m_hwnd, L"Could not initalize the input object", L"Error", MB_OK);
			return false;
		}

		// Create the graphics object.  This object will handle rendering all the graphics for this application.
		m_Graphic = new Graphic::GraphicEngine();
		if (!m_Graphic)
		{
			return false;
		}

		// Initialize the graphics object.
		result = m_Graphic->Initialize(windowDatas.screenWidth, windowDatas.screenHeight, windowDatas.m_hwnd, windowDatas);
		if (!result)
		{
			return false;
		}

		Graphic::Camera::Instance()->SetPosition(0.0f, 0.0f, -1.0f);
		Graphic::Camera::Instance()->Render();
#ifdef USEDIRECT3D
		result = m_UI.Initialize(DirectXInterface::Instance().GetWorldMatrix(), DirectXInterface::Instance().GetOrthoMatrix(), Graphic::Camera::Instance()->GetViewMatrix(), windowDatas);
#endif
		if (!result)
		{
			LogWriter::Instance().addLogMessage("Could not initialize UI.", Error);
			MessageBoxW(windowDatas.m_hwnd, L"Could not initialize the UI object.", L"Error", MB_OK);
			return false;
		}
		Graphic::Camera::Instance()->SetPosition(0.0f, 0.0f, -10.0f);
		
#ifdef GAME
        LoadMenus();
#endif
        LoadNextLevel();

		/// TEMPORAIRE
		backgroundSound = new SoundClass();
		SoundManager::Instance().createSound(backgroundSound, "./Ressources/Sounds/BackgroundMusic2.mp3");
		SoundManager::Instance().playSound(*backgroundSound, soundChannel, true);
		soundChannel->setVolume(0.05f);
		///

		_lastFrameTime = timeGetTime();
		return true;
	}

	void		Engine::Run()
	{
		MSG msg;
		bool result;
		int k = 0;

		// Initialize the message structure.
		ZeroMemory(&msg, sizeof(MSG));
#ifdef GAME
        UIEngine::MenuFactory::Instance()->GetMenu(0)->GetSwitch(0)->SwitchState(true);
        Resume();
#endif
		// Loop until there is a quit message from the window or the user.
		while (!_done)
		{
			// Handle the windows messages.
			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

			// If windows signals to end the application then exit out.
			if (msg.message == WM_QUIT)
				_done = true;
			else
			{
				// Otherwise do the frame processing.  If frame processing fails then exit.
				result = GameLoop();
				if (!result)
				{
					MessageBoxW(windowDatas.m_hwnd, L"Frame Processing Failed", L"Error", MB_OK);
					_done = true;
				}
			}
#ifdef GAME
			// Check if the user pressed escape and wants to pause.
            if (!_buttonHold && m_Input->IsPressed(Input_Escape) == true)
            {
                _buttonHold = true;
                if (pause)
                    Resume();
                else
                    Pause();
            }
            if (_buttonHold && m_Input->IsReleased(Input_Escape) == true)
                _buttonHold = false;
#endif
		}
		return;
	}

	void		Engine::Stop()
	{
		_done = true;
	}

	void		Engine::FillQuadtree()
	{
		std::map<unsigned int, GameObject*> map = GameObjectFactory::Inst()->GetObjectMap();
		Vec2 cameraPosition = Graphic::Camera::Instance()->Get2DPosition();
		quadtree = new Quadtree(Graphic::Camera::Instance()->Get2DPosition(), Vec2{ 1920, 1080 });
		int total = 0;
		for (std::map<unsigned int, GameObject*>::const_iterator it = map.begin(); it != map.end(); ++it)
		{
			quadtree->Insert(it->second);
			total++;
		}
	}

#ifdef GAME
    void Engine::Pause()
    {
        UIEngine::MenuFactory::Instance()->ActivateMenu(1);
        pause = true;
    }

    void Engine::Resume()
    {
        UIEngine::MenuFactory::Instance()->QuitMenu();
        UIEngine::MenuFactory::Instance()->ActivateMenu(0);
        pause = false;
    }
#endif

	bool		Engine::GameLoop()
	{
		bool result;
		deltaTime = (timeGetTime() - _lastFrameTime) / 1000.0f;
		std::list<DisplayModule*> orderLayer;
		std::list<GameObject*> debugBoxes;

#ifdef GAME
        if (pause)
            deltaTime = 0.0f;
#endif
        _totalTime += deltaTime;
		_lastFrameTime = timeGetTime();

		Graphic::Camera::Instance()->Update(deltaTime, GameObjectFactory::Inst()->GetPlayer());
		FillQuadtree();

		result = m_Input->Frame();
		if (!result)
			return false;

		// Do the frame processing for the graphics engine.
		result = m_Graphic->Update();
		if (!result)
			return false;

		const std::map<unsigned int, GameObject*> map = GameObjectFactory::Inst()->GetNoTransformObjectMap();
		for (std::map<unsigned int, GameObject*>::const_iterator it = map.begin(); it != map.end(); ++it)
		{
			if (it->second->displayModule)
			{
				it->second->displayModule->Update(deltaTime);
				it->second->displayModule->Draw();
			}
		}
		Collision::AABB* box = new Collision::AABB();
		box->Initialize(Graphic::Camera::Instance()->Get2DPosition(), 1920, 1080);
		std::list<GameObject*>* list = quadtree->QueryRange(box);
		delete (box);
		if (list)
		{
			for (auto it = list->begin(); it != list->end(); ++it)
			{
				UpdateObject(*it, orderLayer, list);
				if (GameEngine::engineParameters.debugCollisionBox && (*it)->boxCollisionModule)
				{
					debugBoxes.push_back((*it));
				}
			}
			if (orderLayer.size() > 0)
			{
				orderLayer.sort([](DisplayModule * a, DisplayModule * b) { return a->GetLayer() < b->GetLayer(); });
				for (auto it = orderLayer.begin(); it != orderLayer.end(); ++it)
				{
					(*it)->Draw();
				}
			}

			if (engineParameters.debugCollisionBox)
				for (auto it = debugBoxes.begin(); it != debugBoxes.end(); ++it)
				{
					(*it)->DrawDebug();
				}
			delete(list);
			list = NULL;
		}

		if (!m_UI.Update(deltaTime))
			;//TODO

		m_Graphic->Render();
		GameObjectFactory::Inst()->Update();
		if (_load > 0)
		{
			if (_load == 1)
                if (!LoadNextLevel())
                {
                    Pause();
                    UIEngine::MenuFactory::Instance()->QuitMenu();
                    UIEngine::MenuFactory::Instance()->ActivateMenu(2);
                }
			_load--;
		}

		if (InputManager::Instance().IsPressed(E_Key::Input_V))
			DirectXInterface::Instance().FullScreen(!DirectXInterface::Instance().FullScreen());
		quadtree->Clear();
		delete (quadtree);
		quadtree = NULL;

		return true;
	}

	void		Engine::UpdateObject(GameObject* toUpdate, std::list<DisplayModule*> &displayModuleList, std::list<GameObject*>* objectList)
	{
		if (toUpdate->IsDead())
			return;
		if (toUpdate->physicModule)
		{
			toUpdate->physicModule->Update(deltaTime);
		}
		if (toUpdate->boxCollisionModule)
		{
			toUpdate->boxCollisionModule->Update(deltaTime);
			toUpdate->boxCollisionModule->IsColliding(*objectList);
		}
		if (toUpdate->transformModule)
		{
			toUpdate->transformModule->Update(deltaTime);
		}
		if (toUpdate->stateModule)
		{
			toUpdate->stateModule->Update(deltaTime);
		}
		if (!toUpdate->IsDead() && toUpdate->logicModule)
		{
			toUpdate->logicModule->Update(deltaTime);
		}
		if (!toUpdate->IsDead() && toUpdate->aIModule)
		{
			toUpdate->aIModule->Update(deltaTime);
		}
		if (toUpdate->displayModule)
		{
			toUpdate->displayModule->Update(deltaTime);
			displayModuleList.push_back(toUpdate->displayModule);
		}
	}

	void		Engine::Shutdown()
	{
		// Release the graphics object.
		if (m_Graphic)
		{
			m_Graphic->Shutdown();
			delete m_Graphic;
			m_Graphic = 0;
		}

		if (m_Input)
		{
			m_Input->Shutdown();
			m_Input = 0;
		}
		Graphic::TextureLoader::Instance()->Shutdown();

		///TEMPORAIRE
		SoundManager::Instance().releaseSound(*backgroundSound);
		SoundManager::Instance().releaseChannel(soundChannel);
		///

		GameObjectFactory::Inst()->Shutdown();
		LogWriter::Instance().Shutdown();
		if (_inst)
			delete(_inst);
		_inst = NULL;
	}
}