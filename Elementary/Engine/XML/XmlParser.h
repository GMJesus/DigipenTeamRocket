/**
* \author	Aymeric Lambolez
*/

#ifndef		XMLPARSER_H_
# define	XMLPARSER_H_

#include "External Libraries\TinyXML\tinyxml.h"
#include "Utils\EngineParameters.h"
#include "..\LevelEditor\Utils\EditorParameters.h"
#include "Utils\WindowDatas.h"
#include "Utils\LogWriter.h"
#include "Game\GameObjectFactory.h"
#include "Graphics\TextureLoader.h"
#include "UI/Button.h"
#include "UI/DropDown.h"
#include <vector>

/**
* \brief	Namespace defining the XML datas
*/
namespace XML	{

	class XmlParser
	{
		typedef	bool(XmlParser::*ModuleCreation)(TiXmlElement*, GameObject*);
		typedef	bool(XmlParser::*ObjectCreation)(TiXmlElement*);
	public:
		XmlParser();
		XmlParser(const XmlParser& other);
		~XmlParser();

		/**
		* \brief	Gets the configuration of the engine calling it
		*			Automatically stores parsed datas in the appropriate global variables
		* \param	filepath	The path to the file to load
		* \return	True if parsing was successfull, false overwise
		*/
		bool GetConfig(const char* filepath);

		/**
		* \brief	Loads a level from a file
		*			The GameObjectFactory will be used to create the level
		* \param	filepath	The path to the level to load
		* \return	True if parsing was successful, false overwise
		*/
		bool GetLevel(const char* filepath);

	protected:
		/**
		* \brief	Gets all modules from an object
		* \param	elem	The element containing the modules
		* \param	object	The object in which to stores Modules
		* \return	True if everything went well, false overwise
		*/
		bool ParseModule(TiXmlElement* elem, GameObject* object);

		//Object Creation Function
		//@ClassCreator ObjectFunctionParsingStart
		bool ParseInteractivePlatform(TiXmlElement* elem);
		bool ParseEndOfLevel(TiXmlElement* elem);
		bool ParseCheckpoint(TiXmlElement* elem);
		bool ParseDoor(TiXmlElement* elem);
		bool ParseSwitch(TiXmlElement* elem);
		bool ParseBackAndForthObject(TiXmlElement* elem);
		bool ParseElementalInteractiveObject(TiXmlElement* elem);
		bool ParseAnimatedElement(TiXmlElement* elem);
		bool ParseGun(TiXmlElement* elem);
		bool ParseProjectile(TiXmlElement* elem);
		bool ParseStaticObject(TiXmlElement* elem);
		bool ParseCharacter(TiXmlElement* elem);
		bool ParseParticleSystem(TiXmlElement* elem);
		bool ParsePlayer(TiXmlElement* elem);
		bool ParseActor(TiXmlElement* elem);
		bool ParseBackgroundElement(TiXmlElement* elem);
		bool ParseInvisibleWall(TiXmlElement* elem);
		bool ParseDecorativeElement(TiXmlElement* elem);

		//Module Creation Funtion
		//@ModuleCreator ModuleFunctionParsingStart
		bool ParseInteractivePlatformLogicModule(TiXmlElement* elem, GameObject* object);
		bool ParseEndLevelLogicModule(TiXmlElement* elem, GameObject* object);
		bool ParseCheckpointLogicModule(TiXmlElement* elem, GameObject* object);
		bool ParseDoorLogicModule(TiXmlElement* elem, GameObject* object);
		bool ParseSwitchLogicModule(TiXmlElement* elem, GameObject* object);
		bool ParseInteractiveObjectLogicModule(TiXmlElement* elem, GameObject* object);
		bool ParseBackAndForthBehaviourModule(TiXmlElement* elem, GameObject* object);
		bool ParseStateModule(TiXmlElement* elem, GameObject* object);
		bool ParseGunLogicModule(TiXmlElement* elem, GameObject* object);
		bool ParseProjLogicModule(TiXmlElement* elem, GameObject* object);
		bool ParseSpriteSheetAnimationModule(TiXmlElement* elem, GameObject* object);
		bool ParseCharacterLogicModule(TiXmlElement* elem, GameObject* object);
		bool ParseSpriteModule(TiXmlElement* elem, GameObject* object);
		bool ParseAnimatedSpriteModule(TiXmlElement* elem, GameObject* object);
		bool ParseTransformModule(TiXmlElement* elem, GameObject* object);
		bool ParsePhysicModule(TiXmlElement* elem, GameObject* object);
		bool ParseBoxCollisionModule(TiXmlElement* elem, GameObject* object);
		bool ParseLogicModule(TiXmlElement* elem, GameObject* object);
		bool ParseLifeModule(TiXmlElement* elem, GameObject* object);
		bool ParseSoundModule(TiXmlElement* elem, GameObject* object);
		bool ParseAIModule(TiXmlElement* elem, GameObject* object);
		bool ParseParticleSystemModule(TiXmlElement* elem, GameObject* object);

		//Module Children Creation Function
		bool ParseAnimation(TiXmlElement* elem, SpriteAnimation* animation);

		ModuleCreation*	_moduleCreationFunc;

		const int		_maxAnimationType = 4;
		ObjectCreation*	_objectCreationFunc;

#ifdef GAME
    public:
		/**
		* \brief	Gets all Menus from an XML file
		* \param	filepath	The path to the file to parse
		* \return	True if parsing was successfull, false overwise
		*/
		bool GetMenus(const char* filepath);

    private:
        bool ParseMenu(TiXmlElement* elem);
        bool ParseMenuModule(TiXmlElement* elem, int menuId);

        // MenuModule Creation Funtions
        bool ParseMenuImage(TiXmlElement* elem, int menuId);
        bool ParseMenuButton(TiXmlElement* elem, int menuId);
        bool ParseMenuDropDown(TiXmlElement* elem, int menuId);
        bool ParseMenuSwitch(TiXmlElement* elem, int menuId);

        // MenuModule Parameters
        bool ParseMenuButtonParameters(TiXmlElement* elem, UIEngine::Button* button);
        bool ParseMenuDropDownParameters(TiXmlElement* elem, UIEngine::DropDown* dropDown);

        typedef bool(XmlParser::*MenuModuleCreation)(TiXmlElement*, int);
        vector<MenuModuleCreation> _menuModuleCreationFunc;
#endif
	};

}

extern s_WindowDatas		windowDatas;

#endif
