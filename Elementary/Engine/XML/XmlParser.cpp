/**
* \author	Aymeric Lambolez
*/

#include "XML/XmlParser.h"
#include "UI/Menu/MenuFactory.h"
#include "UI/UISwitch.h"

using namespace XML;

XmlParser::XmlParser()
{
#ifdef GAME
    _menuModuleCreationFunc.push_back(&XmlParser::ParseMenuImage);
    _menuModuleCreationFunc.push_back(&XmlParser::ParseMenuButton);
    _menuModuleCreationFunc.push_back(&XmlParser::ParseMenuDropDown);
    _menuModuleCreationFunc.push_back(&XmlParser::ParseMenuSwitch);
#endif

	_moduleCreationFunc = new ModuleCreation[Module::MODULE_CLASS_NUMBER];
	//@ModuleCreator FuncArrayModuleParsingStart
	_moduleCreationFunc[Module::Type::E_InteractivePlatformLogicModule] = &XmlParser::ParseInteractivePlatformLogicModule;
	_moduleCreationFunc[Module::Type::E_EndLevelLogicModule] = &XmlParser::ParseEndLevelLogicModule;
	_moduleCreationFunc[Module::Type::E_CheckpointLogicModule] = &XmlParser::ParseCheckpointLogicModule;
	_moduleCreationFunc[Module::Type::E_DoorLogicModule] = &XmlParser::ParseDoorLogicModule;
	_moduleCreationFunc[Module::Type::E_SwitchLogicModule] = &XmlParser::ParseSwitchLogicModule;
	_moduleCreationFunc[Module::Type::E_InteractiveObjectLogicModule] = &XmlParser::ParseInteractiveObjectLogicModule;
	_moduleCreationFunc[Module::Type::E_BackAndForthBehaviourModule] = &XmlParser::ParseBackAndForthBehaviourModule;
	_moduleCreationFunc[Module::Type::E_StateModule] = &XmlParser::ParseStateModule;
	_moduleCreationFunc[Module::Type::E_GunLogicModule] = &XmlParser::ParseGunLogicModule;
	_moduleCreationFunc[Module::Type::E_ProjLogicModule] = &XmlParser::ParseProjLogicModule;
	_moduleCreationFunc[Module::Type::E_SpriteSheetAnimationModule] = &XmlParser::ParseSpriteSheetAnimationModule;
    _moduleCreationFunc[Module::Type::E_CharacterLogicModule] = &XmlParser::ParseCharacterLogicModule;
	_moduleCreationFunc[Module::Type::E_SpriteModule] = &XmlParser::ParseSpriteModule;
	_moduleCreationFunc[Module::Type::E_AnimatedSpriteModule] = &XmlParser::ParseAnimatedSpriteModule;
	_moduleCreationFunc[Module::Type::E_BoxCollisionModule] = &XmlParser::ParseBoxCollisionModule;
	_moduleCreationFunc[Module::Type::E_LifeModule] = &XmlParser::ParseLifeModule;
	_moduleCreationFunc[Module::Type::E_LogicModule] = &XmlParser::ParseLogicModule;
	_moduleCreationFunc[Module::Type::E_PhysicModule] = &XmlParser::ParsePhysicModule;
	_moduleCreationFunc[Module::Type::E_SoundModule] = &XmlParser::ParseSoundModule;
	_moduleCreationFunc[Module::Type::E_TransformModule] = &XmlParser::ParseTransformModule;
	_moduleCreationFunc[Module::Type::E_AIModule] = &XmlParser::ParseAIModule;
	_moduleCreationFunc[Module::Type::E_ParticleSystemModule] = &XmlParser::ParseParticleSystemModule;

	_objectCreationFunc = new ObjectCreation[GameObject::GAMEOBJECT_CLASS_NUMBER];
	//@ClassCreator FuncArrayObjectParsingStart
	_objectCreationFunc[GameObject::Type::E_InteractivePlatform] = &XmlParser::ParseInteractivePlatform;
	_objectCreationFunc[GameObject::Type::E_EndOfLevel] = &XmlParser::ParseEndOfLevel;
	_objectCreationFunc[GameObject::Type::E_Checkpoint] = &XmlParser::ParseCheckpoint;
	_objectCreationFunc[GameObject::Type::E_Door] = &XmlParser::ParseDoor;
	_objectCreationFunc[GameObject::Type::E_Switch] = &XmlParser::ParseSwitch;
	_objectCreationFunc[GameObject::Type::E_BackAndForthObject] = &XmlParser::ParseBackAndForthObject;
	_objectCreationFunc[GameObject::Type::E_ElementalInteractiveObject] = &XmlParser::ParseElementalInteractiveObject;
	_objectCreationFunc[GameObject::Type::E_AnimatedElement] = &XmlParser::ParseAnimatedElement;
	_objectCreationFunc[GameObject::Type::E_Gun] = &XmlParser::ParseGun;
	_objectCreationFunc[GameObject::Type::E_Projectile] = &XmlParser::ParseProjectile;
	_objectCreationFunc[GameObject::Type::E_StaticObject] = &XmlParser::ParseStaticObject;
    _objectCreationFunc[GameObject::Type::E_Character] = &XmlParser::ParseCharacter;
	_objectCreationFunc[GameObject::Type::E_ParticleSystem] = &XmlParser::ParseParticleSystem;
	_objectCreationFunc[GameObject::Type::E_Player] = &XmlParser::ParsePlayer;
	_objectCreationFunc[GameObject::Type::E_Actor] = &XmlParser::ParseActor;
	_objectCreationFunc[GameObject::Type::E_BackgroundElement] = &XmlParser::ParseBackgroundElement;
	_objectCreationFunc[GameObject::Type::E_Decorative_Element] = &XmlParser::ParseDecorativeElement;
	_objectCreationFunc[GameObject::Type::E_InvisibleWall] = &XmlParser::ParseInvisibleWall;
}


XmlParser::XmlParser(const XmlParser& other) :
_moduleCreationFunc(other._moduleCreationFunc),
_objectCreationFunc(other._objectCreationFunc)
{
}

XmlParser::~XmlParser()
{
}

bool	XmlParser::GetConfig(const char *filepath)
{
	TiXmlDocument *doc = new TiXmlDocument(filepath);
	
	if (!doc->LoadFile())
	{
		return false;
	}

	TiXmlHandle hdl(doc);
	TiXmlElement *elem = hdl.FirstChildElement().FirstChildElement().Element();
	
	if (!elem)
		return false;
	elem->QueryIntAttribute("ScreenWidth", &windowDatas.screenWidth);
	elem->QueryIntAttribute("ScreenHeight", &windowDatas.screenHeight);
	elem->QueryBoolAttribute("Fullscreen", &windowDatas.fullScreen);
	elem->QueryBoolAttribute("VSync", &windowDatas.vSync);
#ifdef GAME
	elem = elem->NextSiblingElement();
	if (!elem)
		return false;
	elem->QueryBoolAttribute("DebugCollision", &GameEngine::engineParameters.debugCollisionBox);
	elem->QueryBoolAttribute("DrawFPS", &GameEngine::engineParameters.drawFPS);
	elem->QueryBoolAttribute("DebugActive", &GameEngine::engineParameters.debugActive);
#elif EDITOR
	elem = elem->NextSiblingElement();
	if (!elem)
		return false;
	EditorEngine::engineParameters.lastOpenedFile = const_cast<char*>(elem->Attribute("lastOpenedFile"));
	elem->QueryIntAttribute("toolWindowWidth", &EditorEngine::engineParameters.toolWindowWidth);
	elem->QueryIntAttribute("toolWindowHeight", &EditorEngine::engineParameters.toolWindowHeight);
#endif
	return true;
}

bool	XmlParser::GetLevel(const char *filepath)
{
	TiXmlDocument *doc = new TiXmlDocument(filepath);

	if (!doc->LoadFile())
	{
		return false;
	}
	TiXmlHandle hdl(doc);
	TiXmlElement *elem = hdl.FirstChildElement().FirstChildElement().Element();

	if (!elem)
		return false;
	while (elem)
	{		
		int type;
		elem->QueryIntAttribute("type", &type);
		if (type < -1 || type >= GameObject::GAMEOBJECT_CLASS_NUMBER)
			LogWriter::Instance().addLogMessage("[XML PARSER] Wrong Object type", Warning);
		else if (type == -1)
		{
			Character* player = NULL;
			if ((player = GameObjectFactory::Inst()->GetPlayer()))
			{
				Vec2 pos;
				elem->QueryFloatAttribute("x", &pos.x);
				elem->QueryFloatAttribute("y", &pos.y);
				player->transformModule->SetPosition(pos);
			}
			else
				if (!(this->*_objectCreationFunc[GameObject::Type::E_Character])(elem))
					LogWriter::Instance().addLogMessage("[XML PARSER] Cannot instanciate Object", Warning);
		}
		else
		{
			if (!(this->*_objectCreationFunc[type])(elem))
				LogWriter::Instance().addLogMessage("[XML PARSER] Cannot instanciate Object", Warning);
		}
		elem = elem->NextSiblingElement();
	}
	return true;
}

bool	XmlParser::ParseSpriteModule(TiXmlElement* elem, GameObject* object)
{
	if (!object->displayModule)
		return false;
	std::string str(elem->Attribute("path"));
	static_cast<SpriteModule*>(object->displayModule)->SetTexture(Graphic::TextureLoader::Instance()->GetTexture(&std::wstring(str.begin(), str.end())[0]));
	int value;
	elem->QueryIntAttribute("height", &value);
	static_cast<SpriteModule*>(object->displayModule)->SetHeight(value);
	elem->QueryIntAttribute("width", &value);
	static_cast<SpriteModule*>(object->displayModule)->SetWidth(value);
	elem->QueryIntAttribute("x", &value);
	static_cast<SpriteModule*>(object->displayModule)->SetX(value);
	elem->QueryIntAttribute("y", &value);
	static_cast<SpriteModule*>(object->displayModule)->SetY(value);
	if (elem->QueryIntAttribute("layer", &value) != TIXML_SUCCESS)
	{
		object->displayModule->SetLayer(0);
		LogWriter::Instance().addLogMessage("[XML PARSER] Layer not found: default value 0", Warning);
	}
	else
		object->displayModule->SetLayer(value);
	return true;
}

bool	XmlParser::ParseParticleSystemModule(TiXmlElement* elem, GameObject* object)
{
	int iValue = 1;
	float fValue = 1;
	bool bValue = false;
	Vec2 vValue;

	if (!object->displayModule)
		return false;
	std::string str(elem->Attribute("path"));
	static_cast<ParticleSystemModule*>(object->displayModule)->SetParticlesTexture(Graphic::TextureLoader::Instance()->GetTexture(&std::wstring(str.begin(), str.end())[0]));
	elem->QueryFloatAttribute("angle", &fValue);
	static_cast<ParticleSystemModule*>(object->displayModule)->SetAngle(fValue);
	elem->QueryFloatAttribute("lifeTime", &fValue);
	static_cast<ParticleSystemModule*>(object->displayModule)->SetLifeTime(fValue);
	elem->QueryFloatAttribute("VelocityX", &fValue);
	vValue.x = fValue;
	elem->QueryFloatAttribute("VelocityY", &fValue);
	vValue.y = fValue;
	static_cast<ParticleSystemModule*>(object->displayModule)->SetInitialVelocity(vValue);
	elem->QueryIntAttribute("width", &iValue);
	static_cast<ParticleSystemModule*>(object->displayModule)->SetParticlesWidth(iValue);
	elem->QueryIntAttribute("height", &iValue);
	static_cast<ParticleSystemModule*>(object->displayModule)->SetParticlesHeight(iValue);
	elem->QueryBoolAttribute("enableGravity", &bValue);
	static_cast<ParticleSystemModule*>(object->displayModule)->SetGravityEnabled(bValue);

	if (elem->QueryIntAttribute("layer", &iValue) != TIXML_SUCCESS)
	{
		object->displayModule->SetLayer(0);
		LogWriter::Instance().addLogMessage("[XML PARSER] Layer not found: default value 0", Warning);
	}
	else
		object->displayModule->SetLayer(iValue);
	elem->QueryFloatAttribute("spawningRate", &fValue);
	static_cast<ParticleSystemModule*>(object->displayModule)->SetSpawningRate(fValue);

	return true;
}

bool	XmlParser::ParseAnimatedSpriteModule(TiXmlElement* elem, GameObject* object)
{
	int value;
	bool bValue = false;
	int iValue;
	double dValue;
	SpriteAnimation *animation = NULL;

	if (!object->displayModule)
		return false;
	std::string str(elem->Attribute("path"));
	static_cast<AnimatedSpriteModule*>(object->displayModule)->SetTexture(Graphic::TextureLoader::Instance()->GetTexture(&std::wstring(str.begin(), str.end())[0]));
	elem->QueryIntAttribute("height", &value);
	static_cast<AnimatedSpriteModule*>(object->displayModule)->SetInitialHeight(value);
	elem->QueryIntAttribute("width", &value);
	static_cast<AnimatedSpriteModule*>(object->displayModule)->SetInitialWidth(value);
	if (elem->QueryIntAttribute("layer", &value) != TIXML_SUCCESS)
	{
		object->displayModule->SetLayer(0);
		LogWriter::Instance().addLogMessage("[XML PARSER] Layer not found: default value 0", Warning);
	}
	else
		object->displayModule->SetLayer(value);
	elem->QueryBoolAttribute("TranslateX", &bValue);
	if (bValue)
	{
		animation = new SpriteAnimation();
		animation->SetWhich(SpriteAnimation::E_Animated::TranslateX);
		elem->QueryIntAttribute("TXFrom", &iValue);
		animation->SetFrom(iValue);
		elem->QueryIntAttribute("TXTo", &iValue);
		animation->SetTo(iValue);
		elem->QueryDoubleAttribute("TXDuration", &dValue);
		animation->SetDuration(dValue);
		if (elem->QueryBoolAttribute("TXRevert", &bValue) != TIXML_SUCCESS)
			animation->SetRevert(false);
		else
			animation->SetRevert(bValue);
		if (elem->QueryBoolAttribute("TXLoop", &bValue) != TIXML_SUCCESS)
			animation->SetLoop(false);
		else
			animation->SetLoop(bValue);
		static_cast<AnimatedSpriteModule*>(object->displayModule)->_storyboard.push_back(animation);
	}
	bValue = false;
	elem->QueryBoolAttribute("TranslateY", &bValue);
	if (bValue)
	{
		animation = new SpriteAnimation();
		animation->SetWhich(SpriteAnimation::E_Animated::TranslateY);
		elem->QueryIntAttribute("TYFrom", &iValue);
		animation->SetFrom(iValue);
		elem->QueryIntAttribute("TYTo", &iValue);
		animation->SetTo(iValue);
		elem->QueryDoubleAttribute("TYDuration", &dValue);
		animation->SetDuration(dValue);
		if (elem->QueryBoolAttribute("TYRevert", &bValue) != TIXML_SUCCESS)
			animation->SetRevert(false);
		else
			animation->SetRevert(bValue);
		if (elem->QueryBoolAttribute("TYLoop", &bValue) != TIXML_SUCCESS)
			animation->SetLoop(false);
		else
			animation->SetLoop(bValue);
		static_cast<AnimatedSpriteModule*>(object->displayModule)->_storyboard.push_back(animation);
	}
	bValue = false;
	elem->QueryBoolAttribute("ScaleX", &bValue);
	if (bValue)
	{
		animation = new SpriteAnimation();
		animation->SetWhich(SpriteAnimation::E_Animated::Width);
		elem->QueryIntAttribute("SXFrom", &iValue);
		animation->SetFrom(iValue);
		elem->QueryIntAttribute("SXTo", &iValue);
		animation->SetTo(iValue);
		elem->QueryDoubleAttribute("SXDuration", &dValue);
		animation->SetDuration(dValue);
		if (elem->QueryBoolAttribute("SXRevert", &bValue) != TIXML_SUCCESS)
			animation->SetRevert(false);
		else
			animation->SetRevert(bValue);
		if (elem->QueryBoolAttribute("SXLoop", &bValue) != TIXML_SUCCESS)
			animation->SetLoop(false);
		else
			animation->SetLoop(bValue);
		static_cast<AnimatedSpriteModule*>(object->displayModule)->_storyboard.push_back(animation);
	}
	bValue = false;
	elem->QueryBoolAttribute("ScaleY", &bValue);
	if (bValue)
	{
		animation = new SpriteAnimation();
		animation->SetWhich(SpriteAnimation::E_Animated::Height);
		elem->QueryIntAttribute("SYFrom", &iValue);
		animation->SetFrom(iValue);
		elem->QueryIntAttribute("SYTo", &iValue);
		animation->SetTo(iValue);
		elem->QueryDoubleAttribute("SYDuration", &dValue);
		animation->SetDuration(dValue);
		if (elem->QueryBoolAttribute("SYRevert", &bValue) != TIXML_SUCCESS)
			animation->SetRevert(false);
		else
			animation->SetRevert(bValue);
		if (elem->QueryBoolAttribute("SYLoop", &bValue) != TIXML_SUCCESS)
			animation->SetLoop(false);
		else
			animation->SetLoop(bValue);
		static_cast<AnimatedSpriteModule*>(object->displayModule)->_storyboard.push_back(animation);
	}
	bValue = false;
	elem->QueryBoolAttribute("Rotation", &bValue);
	if (bValue)
	{
		animation = new SpriteAnimation();
		animation->SetWhich(SpriteAnimation::E_Animated::Rotation);
		elem->QueryIntAttribute("RFrom", &iValue);
		animation->SetFrom(iValue);
		elem->QueryIntAttribute("RTo", &iValue);
		animation->SetTo(iValue);
		elem->QueryDoubleAttribute("RDuration", &dValue);
		animation->SetDuration(dValue);
		if (elem->QueryBoolAttribute("RRevert", &bValue) != TIXML_SUCCESS)
			animation->SetRevert(false);
		else
			animation->SetRevert(bValue);
		if (elem->QueryBoolAttribute("RLoop", &bValue) != TIXML_SUCCESS)
			animation->SetLoop(false);
		else
			animation->SetLoop(bValue);
		static_cast<AnimatedSpriteModule*>(object->displayModule)->_storyboard.push_back(animation);
	}
	bValue = false;
	return true;
}

bool	XmlParser::ParseAnimation(TiXmlElement* elem, SpriteAnimation* animation)
{
	animation = new SpriteAnimation();
	int iValue;
	double dValue;
	bool bValue;
	elem->QueryIntAttribute("type", &iValue);
	if (iValue < 0 || iValue > _maxAnimationType)
		return false;

	animation->SetWhich((SpriteAnimation::E_Animated)iValue);
	elem->QueryIntAttribute("from", &iValue);
	animation->SetFrom(iValue);
	elem->QueryIntAttribute("to", &iValue);
	animation->SetTo(iValue);
	elem->QueryDoubleAttribute("duration", &dValue);
	animation->SetDuration(dValue);
	if (elem->QueryBoolAttribute("revert", &bValue) != TIXML_SUCCESS)
		animation->SetRevert(false);
	else
		animation->SetRevert(bValue);
	if (elem->QueryBoolAttribute("loop", &bValue) != TIXML_SUCCESS)
		animation->SetLoop(false);
	else
		animation->SetLoop(bValue);
	return true;
}



bool	XmlParser::ParseTransformModule(TiXmlElement* elem, GameObject* object)
{
	if (!object->transformModule)
		return false;
	float value;
	float angle;
	Vec2 val;
	elem->QueryFloatAttribute("x", &value);
	val.x = value;
	elem->QueryFloatAttribute("y", &value);
	val.y = value;
	elem->QueryFloatAttribute("angle", &value);
	angle = value;
	object->transformModule->SetPosition(val);
	object->transformModule->SetRotation(angle);
	const char* parent = elem->Attribute("parent");
	if (parent)
		object->transformModule->ParentTo(parent);

	return true;
}

bool	XmlParser::ParseBoxCollisionModule(TiXmlElement* elem, GameObject* object)
{
	if (!object->boxCollisionModule)
		return false;
	float value;
	elem->QueryFloatAttribute("width", &value);
	object->boxCollisionModule->SetBoxWidth(value);
	elem->QueryFloatAttribute("height", &value);
	object->boxCollisionModule->SetBoxHeight(value);
	object->transformModule->SetRotation(0);
	bool boolValue;

	if (elem->QueryBoolAttribute("collisionEnabled", &boolValue) != TIXML_SUCCESS)
		object->boxCollisionModule->SetCollisionEnabled(true);
	else
		object->boxCollisionModule->SetCollisionEnabled(boolValue);

	if (elem->QueryBoolAttribute("generateHitEvents", &boolValue) != TIXML_SUCCESS)
		object->boxCollisionModule->SetGenerateHitEvents(true);
	else
		object->boxCollisionModule->SetGenerateHitEvents(boolValue);

	object->boxCollisionModule->SetType(OBB);

	return true;
}

bool	XmlParser::ParseLogicModule(TiXmlElement* elem, GameObject* object)
{
	if (!object->logicModule)
		return false;

	return true;
}

bool	XmlParser::ParsePhysicModule(TiXmlElement* elem, GameObject* object)
{
	if (!object->physicModule)
		return false;
	bool value;
	float floatValue;
	elem->QueryBoolAttribute("enableGravity", &value);
	object->physicModule->EnableGravity(value);
	elem->QueryFloatAttribute("mass", &floatValue);
	object->physicModule->SetMass(floatValue);
	elem->QueryFloatAttribute("absorption", &floatValue);
	object->physicModule->SetAbsorption(floatValue);

	return true;
}

bool	XmlParser::ParseLifeModule(TiXmlElement* elem, GameObject* object)
{
	if (!object->lifeModule)
		return false;

	LifeModule*	module = static_cast<LifeModule*>(object->lifeModule);
	bool value = true;
	elem->QueryBoolAttribute("enable", &value);
	module->Enable(value);
	elem->QueryBoolAttribute("immortal", &value);
	module->SetImmortal(value);
	float maxHealth;
	elem->QueryFloatAttribute("maxHealth", &maxHealth);
	module->SetMaxHealth(maxHealth);
	module->SetHealth(maxHealth);

	return true;
}

bool	XmlParser::ParseSoundModule(TiXmlElement* elem, GameObject* object)
{
	if (!object->soundModule)
		return false;
	SoundModule* module = static_cast<SoundModule*>(object->soundModule);
	bool value = true;
	elem->QueryBoolAttribute("loop", &value);
	module->SetLoop(value);
	std::string str(elem->Attribute("path"));
	module->SetPath(str);
	float volume;
	elem->QueryFloatAttribute("volume", &volume);
	module->SetVolume(volume);
	return true;
}

bool	XmlParser::ParseAIModule(TiXmlElement* elem, GameObject* object)
{
	if (!object->aIModule)
		return false;
	return true;
}

bool	XmlParser::ParseModule(TiXmlElement* elem, GameObject* object)
{
	TiXmlElement* child = elem->FirstChildElement();
	while (child)
	{
		int type;
		child->QueryIntAttribute("type", &type);
		if (type < 0 || type >= Module::MODULE_CLASS_NUMBER)
		{
            LogWriter::Instance().addLogMessage("[XML PARSER] Wrong Module type", Warning);
			return false;
		}
		else
		{
			if (!(this->*_moduleCreationFunc[type])(child, object))
			{
				LogWriter::Instance().addLogMessage("[XML PARSER] Cannot instanciate Module", Warning);
				return false;
			}
		}
		child = child->NextSiblingElement();
	}
	return true;
}

bool XmlParser::ParseActor(TiXmlElement* elem)
{
	bool ret = true;
	const char*	name = elem->Attribute("name");
	GameObject* object = GameObjectFactory::Inst()->CreateGameObject(GameObject::Type::E_Actor, name);

	if (!ParseModule(elem, object))
	{
		GameObjectFactory::Inst()->DeleteObject(object->Id(), true);
		ret = false;
	}

	return ret;
}

bool XmlParser::ParseBackgroundElement(TiXmlElement* elem)
{
	bool ret = true;
	const char*	name = elem->Attribute("name");
	GameObject* object = GameObjectFactory::Inst()->CreateGameObject(GameObject::Type::E_BackgroundElement, name);

	if (!ParseModule(elem, object))
	{
		GameObjectFactory::Inst()->DeleteObject(object->Id(), true);
		ret = false;
	}

	return ret;
}

bool XmlParser::ParseInvisibleWall(TiXmlElement* elem)
{
	bool ret = true;
	const char*	name = elem->Attribute("name");
	GameObject* object = GameObjectFactory::Inst()->CreateGameObject(GameObject::Type::E_InvisibleWall, name);

	if (!ParseModule(elem, object))
	{
		GameObjectFactory::Inst()->DeleteObject(object->Id(), true);
		ret = false;
	}

	return ret;
}

bool XmlParser::ParseDecorativeElement(TiXmlElement* elem)
{
	bool ret = true;
	const char*	name = elem->Attribute("name");
	GameObject* object = GameObjectFactory::Inst()->CreateGameObject(GameObject::Type::E_Decorative_Element, name);

	if (!ParseModule(elem, object))
	{
		GameObjectFactory::Inst()->DeleteObject(object->Id(), true);
		ret = false;
	}

	return ret;
}

bool XmlParser::ParsePlayer(TiXmlElement* elem)
{
	bool ret = true;
	const char*	name = elem->Attribute("name");
	GameObject* object = GameObjectFactory::Inst()->CreateGameObject(GameObject::Type::E_Player, name);

	if (!ParseModule(elem, object))
	{
		GameObjectFactory::Inst()->DeleteObject(object->Id(), true);
		ret = false;
	}

	return ret;
}

bool XmlParser::ParseParticleSystem(TiXmlElement* elem)
{
	bool ret = true;
	const char*	name = elem->Attribute("name");
	GameObject* object = GameObjectFactory::Inst()->CreateGameObject(GameObject::Type::E_ParticleSystem, name);

	if (!ParseModule(elem, object))
	{
		GameObjectFactory::Inst()->DeleteObject(object->Id(), true);
		ret = false;
	}

	return ret;
}

bool	XmlParser::ParseSpriteSheetAnimationModule(TiXmlElement* elem, GameObject* object)
{
	if (!object->displayModule)
		return false;
	SpriteSheetAnimationModule*	module = static_cast<SpriteSheetAnimationModule*>(object->displayModule);
	void* value = new bool;
	elem->QueryBoolAttribute("enable", (bool*)value);
	module->Enable(*(bool*)(value));
	elem->QueryBoolAttribute("isVisible", (bool*)value);
	module->SetVisibility(*(bool*)(value));
	delete(value);
	value = new int;
	elem->QueryIntAttribute("layer", (int*)value);
	module->SetLayer(*(int*)(value));
	elem->QueryIntAttribute("x", (int*)value);
	module->SetX(*(int*)(value));
	elem->QueryIntAttribute("y", (int*)value);
	module->SetY(*(int*)(value));
	elem->QueryIntAttribute("totalRaw", (int*)value);
	module->SetTotalRaw(*(int*)(value));
	elem->QueryIntAttribute("totalColumn", (int*)value);
	module->SetTotalColumn(*(int*)(value));
	delete(value);
	module->SetSpriteSheetPath(const_cast<char*>(elem->Attribute("spriteSheetPath")));
	Vec2 tmpVec2;
	value = new float;
	elem->QueryFloatAttribute("startingPosX", (float*)value);
	tmpVec2.x = *(float*)(value);
	elem->QueryFloatAttribute("startingPosY", (float*)value);
	tmpVec2.y = *(float*)(value);
	module->SetStartingPos(tmpVec2);
	delete(value);
	value = new int;
	elem->QueryIntAttribute("spriteSheetWidth", (int*)value);
	module->SetSpriteSheetWidth(*(int*)(value));
	elem->QueryIntAttribute("spriteSheetHeight", (int*)value);
	module->SetSpriteSheetHeight(*(int*)(value));
	elem->QueryIntAttribute("SpriteWidth", (int*)value);
	module->SetSpriteWidth(*(int*)(value));
	elem->QueryIntAttribute("SpriteHeight", (int*)value);
	module->SetSpriteHeight(*(int*)(value));
	elem->QueryIntAttribute("frameNumber", (int*)value);
	module->SetFrameNumber(*(int*)(value));
	delete(value);
	value = new int;
	elem->QueryFloatAttribute("duration", (float*)value);
	module->SetDuration(*(float*)(value));
	delete(value);
	return true;
}

bool	XmlParser::ParseCharacterLogicModule(TiXmlElement* elem, GameObject* object)
{
    if (!object->logicModule)
        return false;
    CharacterLogicModule*	module = static_cast<CharacterLogicModule*>(object->logicModule);
    bool value = true;
    elem->QueryBoolAttribute("enable", &value);
    module->Enable(value);
    return true;
}

bool XmlParser::ParseCharacter(TiXmlElement* elem)
{
    bool ret = true;
    const char*	name = elem->Attribute("name");
    GameObject* object = GameObjectFactory::Inst()->CreateGameObject(GameObject::Type::E_Character, name);

    if (!ParseModule(elem, object))
    {
        GameObjectFactory::Inst()->DeleteObject(object->Id(), true);
        ret = false;
    }

    return ret;
}

bool	XmlParser::ParseProjLogicModule(TiXmlElement* elem, GameObject* object)
{
	if (!object->logicModule)
		return false;
	ProjLogicModule*	module = static_cast<ProjLogicModule*>(object->logicModule);
	void* value = new bool;
	elem->QueryBoolAttribute("enable", (bool*)value);
	module->Enable(*(bool*)(value));
    delete(value);
	return true;
}

bool	XmlParser::ParseGunLogicModule(TiXmlElement* elem, GameObject* object)
{
	if (!object->logicModule)
		return false;
	GunLogicModule*	module = static_cast<GunLogicModule*>(object->logicModule);
	void* value = new bool;
	elem->QueryBoolAttribute("enable", (bool*)value);
	module->Enable(*(bool*)(value));
	delete(value);
	return true;
}

bool XmlParser::ParseStaticObject(TiXmlElement* elem)
{
	bool ret = true;
	const char*	name = elem->Attribute("name");
	GameObject* object = GameObjectFactory::Inst()->CreateGameObject(GameObject::Type::E_StaticObject, name);

	if (!ParseModule(elem, object))
	{
		GameObjectFactory::Inst()->DeleteObject(object->Id(), true);
		ret = false;
	}

	return ret;
}

bool XmlParser::ParseProjectile(TiXmlElement* elem)
{
	bool ret = true;
	const char*	name = elem->Attribute("name");
	GameObject* object = GameObjectFactory::Inst()->CreateGameObject(GameObject::Type::E_Projectile, name);

	if (!ParseModule(elem, object))
	{
		GameObjectFactory::Inst()->DeleteObject(object->Id(), true);
		ret = false;
	}

	return ret;
}

bool XmlParser::ParseGun(TiXmlElement* elem)
{
	bool ret = true;
	const char*	name = elem->Attribute("name");
	GameObject* object = GameObjectFactory::Inst()->CreateGameObject(GameObject::Type::E_Gun, name);

	if (!ParseModule(elem, object))
	{
		GameObjectFactory::Inst()->DeleteObject(object->Id(), true);
		ret = false;
	}

	return ret;
}

bool	XmlParser::ParseStateModule(TiXmlElement* elem, GameObject* object)
{
	if (!object->stateModule)
		return false;
	StateModule* module = static_cast<StateModule*>(object->stateModule);
	void* value = new bool;
	elem->QueryBoolAttribute("enable", (bool*)value);
	module->Enable(*(bool*)(value));
    delete(value);
    value = new int;
	elem->QueryIntAttribute("state", (int*)value);
	module->SetState(*(char*)(value));
    delete(value);
    value = new float;
	elem->QueryFloatAttribute("propagationTime", (float*)value);
	module->SetPropagationTime(*(float*)(value));
	elem->QueryFloatAttribute("destroyTime", (float*)value);
	module->SetDestroyTime(*(float*)(value));
    delete(value);
	Vec2 tmpVec2;
	value = new float;
	elem->QueryFloatAttribute("fireStartX", (float*)value);
	tmpVec2.x = *(float*)(value);
	elem->QueryFloatAttribute("fireStartY", (float*)value);
	tmpVec2.y = *(float*)(value);
	module->SetFireStart(tmpVec2);
	delete(value);
    value = new int;
	elem->QueryIntAttribute("fireLength", (int*)value);
	module->SetFireLength(*(int*)(value));
    delete(value);
	value = new float;
	elem->QueryFloatAttribute("waterStartX", (float*)value);
	tmpVec2.x = *(float*)(value);
	elem->QueryFloatAttribute("waterStartY", (float*)value);
	tmpVec2.y = *(float*)(value);
	module->SetWaterStart(tmpVec2);
	delete(value);
    value = new int;
	elem->QueryIntAttribute("waterLength", (int*)value);
	module->SetWaterLength(*(int*)(value));
    delete(value);
    value = new float;
	elem->QueryFloatAttribute("ElectricStartX", (float*)value);
	tmpVec2.x = *(float*)(value);
	elem->QueryFloatAttribute("ElectricStartY", (float*)value);
	tmpVec2.y = *(float*)(value);
	module->SetElectricStart(tmpVec2);
	delete(value);
    value = new int;
	elem->QueryIntAttribute("ElectricLength", (int*)value);
	module->SetElectricLength(*(int*)(value));
    delete(value);
	return true;
}

bool XmlParser::ParseElementalInteractiveObject(TiXmlElement* elem)
{
	bool ret = true;
	const char*	name = elem->Attribute("name");
	GameObject* object = GameObjectFactory::Inst()->CreateGameObject(GameObject::Type::E_ElementalInteractiveObject, name);
	
	if (!ParseModule(elem, object))
	{
		GameObjectFactory::Inst()->DeleteObject(object->Id(), true);
		ret = false;
	}
	return ret;
}

bool XmlParser::ParseAnimatedElement(TiXmlElement* elem)
{
	bool ret = true;
	const char*	name = elem->Attribute("name");
	GameObject* object = GameObjectFactory::Inst()->CreateGameObject(GameObject::Type::E_AnimatedElement, name);

	if (!ParseModule(elem, object))
	{
		GameObjectFactory::Inst()->DeleteObject(object->Id(), true);
		ret = false;
	}
	//static_cast<AnimatedSpriteModule*>(object->displayModule)->PlayAll();

	return ret;
}

bool	XmlParser::ParseBackAndForthBehaviourModule(TiXmlElement* elem, GameObject* object)
{
	if (!object->aIModule)
		return false;
	BackAndForthBehaviourModule*	module = static_cast<BackAndForthBehaviourModule*>(object->aIModule);
	void* value = new bool;
	elem->QueryBoolAttribute("enable", (bool*)value);
	module->Enable(*(bool*)(value));
	delete(value);
	value = new float;
	elem->QueryFloatAttribute("speed", (float*)value);
	module->SetSpeed(*(float*)(value));
	delete(value);
	value = new bool;
	elem->QueryBoolAttribute("ignoreGroundModification", (bool*)value);
	module->IgnoreGroundModification(*(bool*)(value));
	delete(value);
	return true;
}

bool XmlParser::ParseBackAndForthObject(TiXmlElement* elem)
{
	bool ret = true;
	const char*	name = elem->Attribute("name");
	GameObject* object = GameObjectFactory::Inst()->CreateGameObject(GameObject::Type::E_BackAndForthObject, name);

	if (!ParseModule(elem, object))
	{
		GameObjectFactory::Inst()->DeleteObject(object->Id(), true);
		ret = false;
	}

	return ret;
}

bool	XmlParser::ParseInteractiveObjectLogicModule(TiXmlElement* elem, GameObject* object)
{
	if (!object->logicModule)
		return false;
	InteractiveObjectLogicModule* module = static_cast<InteractiveObjectLogicModule*>(object->logicModule);
	auto value = new bool;
	elem->QueryBoolAttribute("enable", value);
	module->Enable(*value);
    delete(value);
    value = new bool;
	elem->QueryBoolAttribute("SwitchActive", value);
	module->SwitchActive(*value);
    delete(value);
	return true;
}

bool	XmlParser::ParseSwitchLogicModule(TiXmlElement* elem, GameObject* object)
{
	if (!object->logicModule)
		return false;
	SwitchLogicModule*	module = static_cast<SwitchLogicModule*>(object->logicModule);
	auto value = new bool;
	elem->QueryBoolAttribute("enable", value);
	module->Enable(*value);
    delete(value);
	module->SetObjectToInteract(elem->Attribute("objectToInteract"));
	return true;
}

bool	XmlParser::ParseDoorLogicModule(TiXmlElement* elem, GameObject* object)
{
	if (!object->logicModule)
		return false;
	DoorLogicModule*	module = static_cast<DoorLogicModule*>(object->logicModule);
	auto value = new bool;
	elem->QueryBoolAttribute("enable", value);
	module->Enable(*value);
    delete(value);
    value = new bool;
	elem->QueryBoolAttribute("isOpen", value);
	module->IsOpen(*value);
    delete(value);
	return true;
}

bool XmlParser::ParseSwitch(TiXmlElement* elem)
{
	bool ret = true;
	const char*	name = elem->Attribute("name");
	GameObject* object = GameObjectFactory::Inst()->CreateGameObject(GameObject::Type::E_Switch, name);

	if (!ParseModule(elem, object))
	{
		GameObjectFactory::Inst()->DeleteObject(object->Id(), true);
		ret = false;
	}

	return ret;
}

bool XmlParser::ParseDoor(TiXmlElement* elem)
{
	bool ret = true;
	const char*	name = elem->Attribute("name");
	GameObject* object = GameObjectFactory::Inst()->CreateGameObject(GameObject::Type::E_Door, name);

	if (!ParseModule(elem, object))
	{
		GameObjectFactory::Inst()->DeleteObject(object->Id(), true);
		ret = false;
	}

	return ret;
}

bool	XmlParser::ParseCheckpointLogicModule(TiXmlElement* elem, GameObject* object)
{
	if (!object->logicModule)
		return false;
	CheckpointLogicModule*	module = static_cast<CheckpointLogicModule*>(object->logicModule);
	auto value = new bool;
	elem->QueryBoolAttribute("enable", value);
	module->Enable(*value);
    delete(value);
	return true;
}

bool XmlParser::ParseCheckpoint(TiXmlElement* elem)
{
	bool ret = true;
	const char*	name = elem->Attribute("name");
	GameObject* object = GameObjectFactory::Inst()->CreateGameObject(GameObject::Type::E_Checkpoint, name);

	if (!ParseModule(elem, object))
	{
		GameObjectFactory::Inst()->DeleteObject(object->Id(), true);
		ret = false;
	}

	return ret;
}

#ifdef GAME
bool    XmlParser::GetMenus(const char* filepath)
{
    auto doc = new TiXmlDocument(filepath);

    if (!doc->LoadFile())
    {
        return false;
    }
    TiXmlHandle hdl(doc);
    auto elem = hdl.FirstChildElement().FirstChildElement().Element();

    if (!elem)
        return false;
    while (elem)
    {
        if (!ParseMenu(elem))
            LogWriter::Instance().addLogMessage("[XML PARSER] Cannot instanciate Menu", Warning);
        elem = elem->NextSiblingElement();
    }

    return true;
}

bool XmlParser::ParseMenu(TiXmlElement* elem)
{
    string str(elem->Attribute("backgroundFile"));
    WCHAR* backgroundFile = new WCHAR[str.size() + 1];
    mbstowcs(backgroundFile, str.c_str(), str.size() + 1);

    auto positionX = 0.0f;
    auto positionY = 0.0f;
    auto width = 0.0f;
    auto height = 0.0f;
    elem->QueryFloatAttribute("x", &positionX);
    elem->QueryFloatAttribute("y", &positionY);
    elem->QueryFloatAttribute("width", &width);
    elem->QueryFloatAttribute("height", &height);

    auto menu = new UIEngine::Menu();
    menu->Initialize(backgroundFile, positionX, positionY, width, height);

    auto menuId = UIEngine::MenuFactory::Instance()->AddMenu(menu);

    return ParseMenuModule(elem, menuId);
}

bool XmlParser::ParseMenuModule(TiXmlElement* elem, int menuId)
{
    auto child = elem->FirstChildElement();
    while (child)
    {
        int type;
        child->QueryIntAttribute("type", &type);
        if (type < 0 || type >= _menuModuleCreationFunc.size())
        {
            LogWriter::Instance().addLogMessage("[XML PARSER] Wrong MenuModule type", Warning);
            return false;
        }
        if (!(this->*_menuModuleCreationFunc.at(type))(child, menuId))
        {
            LogWriter::Instance().addLogMessage("[XML PARSER] Cannot instanciate MenuModule", Warning);
            return false;
        }
        child = child->NextSiblingElement();
    }
    return true;
}

bool XmlParser::ParseMenuImage(TiXmlElement* elem, int menuId)
{
    string str(elem->Attribute("backgroundFile"));
    WCHAR* backgroundFile = new WCHAR[str.size() + 1];
    mbstowcs(backgroundFile, str.c_str(), str.size() + 1);

    auto positionX = 0.0f;
    auto positionY = 0.0f;
    auto width = 0.0f;
    auto height = 0.0f;
    elem->QueryFloatAttribute("x", &positionX);
    elem->QueryFloatAttribute("y", &positionY);
    elem->QueryFloatAttribute("width", &width);
    elem->QueryFloatAttribute("height", &height);

    auto image = new UIEngine::Image;
    if (!image->Initialize(backgroundFile, positionX, positionY, width, height))
        return false;

    UIEngine::MenuFactory::Instance()->AddImage(image, menuId);

    return true;
}

bool XmlParser::ParseMenuButton(TiXmlElement* elem, int menuId)
{
    string str(elem->Attribute("backgroundFile"));
    WCHAR* backgroundFile = new WCHAR[str.size() + 1];
    mbstowcs(backgroundFile, str.c_str(), str.size() + 1);

    auto positionX = 0.0f;
    auto positionY = 0.0f;
    auto width = 0.0f;
    auto height = 0.0f;
    elem->QueryFloatAttribute("x", &positionX);
    elem->QueryFloatAttribute("y", &positionY);
    elem->QueryFloatAttribute("width", &width);
    elem->QueryFloatAttribute("height", &height);

    int value;
    elem->QueryIntAttribute("value", &value);

    auto button = new UIEngine::Button;
    if (!button->Initialize(backgroundFile, positionX, positionY, width, height))
        return false;

    if (!ParseMenuButtonParameters(elem, button))
        return false;

    int callbackId;
    elem->QueryIntAttribute("callback", &callbackId);

    UIEngine::MenuFactory::Instance()->AddButton(button, menuId, callbackId);

    return true;
}

bool XmlParser::ParseMenuDropDown(TiXmlElement* elem, int menuId)
{
    string str(elem->Attribute("firstCase"));
    auto firstCase = new WCHAR[str.size() + 1];
    mbstowcs(firstCase, str.c_str(), str.size() + 1);

    str = elem->Attribute("defaultCase");
    auto defaultCase = new WCHAR[str.size() + 1];
    mbstowcs(defaultCase, str.c_str(), str.size() + 1);

    str = elem->Attribute("onCoverCase");
    auto onCoverCase = new WCHAR[str.size() + 1];
    mbstowcs(onCoverCase, str.c_str(), str.size() + 1);

    auto maxLength = 0;
    auto maxBoxes = 0;
    elem->QueryIntAttribute("maxLength", &maxLength);
    elem->QueryIntAttribute("maxBoxes", &maxBoxes);

    auto positionX = 0.0f;
    auto positionY = 0.0f;
    auto width = 0.0f;
    auto height = 0.0f;
    elem->QueryFloatAttribute("x", &positionX);
    elem->QueryFloatAttribute("y", &positionY);
    elem->QueryFloatAttribute("width", &width);
    elem->QueryFloatAttribute("height", &height);

    auto dropDown = new UIEngine::DropDown;
    if (!dropDown->Initialize(firstCase, defaultCase, onCoverCase,
        maxLength, maxBoxes,
        positionX, positionY, width, height))
        return false;
    
    int callbackId;
    elem->QueryIntAttribute("callback", &callbackId);

    ParseMenuDropDownParameters(elem, dropDown);
    UIEngine::MenuFactory::Instance()->AddDropDown(dropDown, menuId, callbackId);

    return true;
}

bool XmlParser::ParseMenuSwitch(TiXmlElement* elem, int menuId)
{
    string str(elem->Attribute("onFile"));
    WCHAR* onFile = new WCHAR[str.size() + 1];
    mbstowcs(onFile, str.c_str(), str.size() + 1);

    str = elem->Attribute("offFile");
    WCHAR* offFile = new WCHAR[str.size() + 1];
    mbstowcs(offFile, str.c_str(), str.size() + 1);

    auto positionX = 0.0f;
    auto positionY = 0.0f;
    auto width = 0.0f;
    auto height = 0.0f;
    elem->QueryFloatAttribute("x", &positionX);
    elem->QueryFloatAttribute("y", &positionY);
    elem->QueryFloatAttribute("width", &width);
    elem->QueryFloatAttribute("height", &height);

    auto s = new UIEngine::UISwitch;
    if (!s->Initialize(onFile, offFile, positionX, positionY, width, height))
        return false;

    UIEngine::MenuFactory::Instance()->AddSwitch(s, menuId);

    return true;
}

bool XmlParser::ParseMenuButtonParameters(TiXmlElement* elem, UIEngine::Button* button)
{
    auto child = elem->FirstChildElement();
    while (child)
    {
        auto value = 0;
        child->QueryIntAttribute("value", &value);

        button->addValue(value);

        child = child->NextSiblingElement();
    }
    return true;
}

bool XmlParser::ParseMenuDropDownParameters(TiXmlElement* elem, UIEngine::DropDown* dropDown)
{
    auto child = elem->FirstChildElement();
    while (child)
    {
        auto value = strdup(child->Attribute("value"));
        dropDown->AddChoice(value);

        child = child->NextSiblingElement();
    }
    return true;
}
#endif

bool	XmlParser::ParseEndLevelLogicModule(TiXmlElement* elem, GameObject* object)
{
	if (!object->logicModule)
		return false;
	EndLevelLogicModule*	module = static_cast<EndLevelLogicModule*>(object->logicModule);
	auto value = new bool;
	elem->QueryBoolAttribute("enable", value);
	module->Enable(*value);
    delete(value);
	return true;
}

bool XmlParser::ParseEndOfLevel(TiXmlElement* elem)
{
	bool ret = true;
	const char*	name = elem->Attribute("name");
	GameObject* object = GameObjectFactory::Inst()->CreateGameObject(GameObject::Type::E_EndOfLevel, name);

	if (!ParseModule(elem, object))
	{
		GameObjectFactory::Inst()->DeleteObject(object->Id(), true);
		ret = false;
	}

	return ret;
}

bool	XmlParser::ParseInteractivePlatformLogicModule(TiXmlElement* elem, GameObject* object)
{
	if (!object->logicModule)
		return false;
	InteractivePlatformLogicModule*	module = static_cast<InteractivePlatformLogicModule*>(object->logicModule);
	void* value = NULL;
value = new bool;
	elem->QueryBoolAttribute("enable", (bool*)value);
	module->Enable(*(bool*)(value));
delete(value);
value = new bool;
	elem->QueryBoolAttribute("isActivate", (bool*)value);
	module->IsActivate(*(bool*)(value));
delete(value);
value = new float;
	Vec2 tmpVec2;
	elem->QueryFloatAttribute("posStartX", (float*)value);
	tmpVec2.x = *(float*)(value);
	elem->QueryFloatAttribute("posStartY", (float*)value);
	tmpVec2.y = *(float*)(value);
	module->SetPosStart(tmpVec2);
	elem->QueryFloatAttribute("posEndX", (float*)value);
	tmpVec2.x = *(float*)(value);
	elem->QueryFloatAttribute("posEndY", (float*)value);
	tmpVec2.y = *(float*)(value);
	module->SetPosEnd(tmpVec2);
	elem->QueryFloatAttribute("posPassByX", (float*)value);
	tmpVec2.x = *(float*)(value);
	elem->QueryFloatAttribute("posPassByY", (float*)value);
	tmpVec2.y = *(float*)(value);
	module->SetPosPassBy(tmpVec2);
	delete(value);
	return true;
}

bool XmlParser::ParseInteractivePlatform(TiXmlElement* elem)
{
	bool ret = true;
	const char*	name = elem->Attribute("name");
	GameObject* object = GameObjectFactory::Inst()->CreateGameObject(GameObject::Type::E_InteractivePlatform, name);

	if (!ParseModule(elem, object))
	{
		GameObjectFactory::Inst()->DeleteObject(object->Id(), true);
		ret = false;
	}

	return ret;
}
