/**
* \file		 Physics
* \author    Baptiste DUMAS
* \brief     File with namespace PhysicEngine
*/

#ifndef PHYSICS_H_
# define PHYSICS_H_

#include "Collision.h"
#include <vector>

/**
* \brief PhysicsEngine contain function of basic force calculus
*/
namespace PhysicEngine {
	/**
	* \brief Summed all vector2 force
	* \param forces	List of vector forces
	* \param isGravityEnable	If true gravity will be added to all forces
	* \return	Vector2 force
	*/
	Vec2*			SumForces(const std::vector<Vec2>* forces, bool isGravityEnable);
	/**
	* \brief Calculus of acceleration vector
	* \param summedForce	All force that affect the related object
	* \param acceleration	Inverse of the related object mass
	* \return	Vector2 acceleration
	*/
	Vec2			Acceleration(const Vec2& summedForce, const float& invMass);
	/**
	* \brief	Calculus of the velocity
	* \param velocity	Old velocity of the related object
	* \param acceleration	Acceleration of the related object
	* \param dt	DeltaTime of the game
	* \return Vector2 velocity
	*/
	Vec2			Velocity(const Vec2& velocity, const Vec2& acceleration, float dt);
	/**
	* \brief	Calculus of the next postion
	* \param pos	Current position
	* \param velocity	Current velocity
	* \param dt	DeltaTime of the game
	*/
	Vec2			Position(const Vec2& pos, const Vec2& velocity, float dt);
	Vec2			UpdateForce(float mass, const Vec2& acceleration);
	extern float	GRAVITY;
}

#endif /* PHYSICS_H_ */