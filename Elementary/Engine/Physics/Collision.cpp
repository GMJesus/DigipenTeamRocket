#include "Collision.h"
#include <iostream>
#include <fstream>
#include <string>


Collision::AABB	*Collision::CreateAABB(Vec2 pos, float width, float height)
{
	Collision::AABB *aabb;

	aabb = new Collision::AABB();
	aabb->Initialize(pos, width, height);
	return aabb;
}

Collision::OBB		*Collision::CreateOBB(Vec2 pos, float width, float height, double angle)
{
	OBB	*obb;

	obb = new OBB();
	obb->Initialize(pos, width, height, angle);
	return obb;
}

Collision::Circle	*Collision::CreateCircle(float radius, Vec2 pos)
{
	Circle	*circle;

	circle = new Circle();
	circle->Initialize(radius, pos);
	return circle;
}

void	Collision::testCollide()
{
	Vec2 pos = { 0, 0 };
	AABB *aabb = Collision::CreateAABB(pos, 108.f, 10.f);

	Vec2 posaabb = { 0, 40 };
	AABB *aabb2 = Collision::CreateAABB(posaabb, 20.f, 10.f);

	Vec2 pos2 = { 0, 40 };
	OBB *obb = Collision::CreateOBB(pos2, 100, 20, 20);

	Vec2 posobb = { 0, 0 };
	OBB *obb2 = Collision::CreateOBB(posobb, 1080, 20, 20);

	std::ofstream file;
	file.open("test.txt");
	std::streambuf* sbuf = std::cout.rdbuf();
	std::cout.rdbuf(file.rdbuf());

	//std::cout << aabb.min.x << "\n" << aabb.max.x << "\n" << aabb.Position.x << std::endl;
	//std::cout << aabb2.min.x << "\n" << aabb2.max.x << "\n" << aabb2.Position.x << std::endl;
	//std::cout << AABBvsAABB(&aabb, &aabb2) << std::endl;
	//std::cout << circle->VsCircle(circle2) << std::endl;
//	std::cout << obb->VsOBB(obb2) << std::endl;

	
}