#include "RayCast.h"
#include "GameEngine\GameEngine.h"
#include "Collision.h"

using namespace GameEngine;

GameObject*		 RayCast(const Vec2 start, const Vec2 end, bool ignoreNonPhysicalObjects)
{
	Vec2 ray = end - start;
	Vec2 pos = start + (ray / 2);
	float width = sqrt((ray.x * ray.x) + (ray.y * ray.y));

	Collision::AABB * box;
	if (abs(ray.x) > abs(ray.y))
		box = Collision::CreateAABB(pos, width, 1);
	else
		box = Collision::CreateAABB(pos, 1, width);

	Quadtree * zone;
	std::list<GameObject*> * objectList;

	zone = GetActualQuadtree();
	if (zone)
		objectList = zone->QueryRange(box);

	GameObject * returnValue = NULL;
	float nearestDist = -1;
	if (objectList)
	{
		for (std::list<GameObject *>::iterator it = objectList->begin(); it != objectList->end(); ++it)
		{
			GameObject * object = (*it);
			if (object->boxCollisionModule && (object->boxCollisionModule->CollisionEnabled() || !ignoreNonPhysicalObjects))
			{
				bool collide = object->boxCollisionModule->CollideVersusLine(start, end);
				if (collide)
				{
					Vec2 rayStartObject = object->transformModule->Position() - start;
					float distStartObject = sqrt((rayStartObject.x * rayStartObject.x) + (rayStartObject.y * rayStartObject.y));
					if (distStartObject < nearestDist || nearestDist == -1)
					{
						returnValue = object;
						nearestDist = distStartObject;
					}
				}
			}
		}
	}
	delete (objectList);
	objectList = NULL;
	delete(box);
	box = NULL;
	return returnValue;
}