#ifndef CIRCLE_H_
# define CIRCLE_H_

#include "Utils\Vec2.h"

namespace Collision
{

	class Circle
	{
	public:
		Circle();
		~Circle();

		void	Initialize(float radius, Vec2 pos);
		bool	VsCircle(Circle* collide);

	private:
		float	radius;
		Vec2	pos;
	};

}

#endif /* CIRCLE_H_ */