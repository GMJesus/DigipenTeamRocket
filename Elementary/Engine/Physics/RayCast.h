#pragma once

#ifndef RAYCAST_H_
# define RAYCAST_H_

#include <math.h>

#include "Utils/Vec2.h"

class GameObject;

GameObject*		RayCast(const Vec2 start, const Vec2 end, bool ignoreUnphysicalObjects = true);

#endif /* RAYCAST_H_ */