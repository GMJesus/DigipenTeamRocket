#include "AABB.h"
#include "UI\UIEngine.h"

//#include <math.h>

using namespace Collision;

AABB::AABB()
{
	_pos = { 0, 0 };
	_width = 0;
	_height = 0;
	_min = Vec2{ 0.f, 0.f };
	_max = Vec2{ 0.f, 0.f };
}


AABB::~AABB()
{
}

void	AABB::Initialize(Vec2 pos, float width, float height)
{
	this->_pos = pos;
	this->_width = width;
	this->_height = height;

	_min = CalcMin();
	_max = CalcMax();

	

	UpdateCorners();
}

Vec2	AABB::CalcMax()const
{
	Vec2 maxt;
	maxt.x = this->_pos.x + (this->_width / 2.f);
	maxt.y = this->_pos.y + (this->_height / 2.f);
	
	return maxt;
}

Vec2	AABB::CalcMin()const
{
	Vec2 mint(this->_pos.x - (this->_width / 2.f), this->_pos.y - (this->_height / 2.f));
	
	return mint;
}

bool	AABB::IsPointInAABB(Vec2 point)const
{
	//Vec2 max = this->CalcMax();
	//Vec2 min = this->CalcMin();

	//if ((point.x <= max.x && point.x >= min.x) &&
	//	(point.y <= max.x && point.y >= min.y))

	if (point.x >= _pos.x - (_width / 2) && point.x <= _pos.x + (_width / 2) &&
		point.y >= _pos.y - (_height / 2) && point.y <= _pos.y + (_height / 2))
		return true;

	return false;
}

bool	AABB::VsAABB(AABB *collide)const
{
	
	if ((this->_pos.x + (this->_width / 2.f)) < (collide->_pos.x - (collide->_width / 2.f)) || (this->_pos.x - (this->_width / 2.f)) > (collide->_pos.x + (collide->_width / 2.f)))
	{
		return false;
	}
	if ((this->_pos.y + (this->_height / 2.f)) < (collide->_pos.y - (collide->_height / 2.f)) || (this->_pos.y - (this->_height / 2.f)) > (collide->_pos.y + (collide->_height / 2.f)))
	{
		return false;
	}
	return true;
	//if (this->_max.x < collide->_min.x || this->_min.x > collide->_max.x)
	//	return false;
	//if (this->_max.y < collide->_min.y || this->_min.y > collide->_max.y)
	//	return false;
	
}

bool	AABB::VsOBB(const OBB *other)const
{
	return VsOBBOneAxis(other) && other->VsAABBOneAxis(this);
}

bool	AABB::VsOBBOneAxis(const OBB* other)const
{
	for (int i = 0; i < 2; ++i)
	{
		float tmp = other->GetCorner(0).x * _axis[i].x + other->GetCorner(0).y * _axis[i].y;
		float tmpMin = tmp;
		float tmpMax = tmp;

		for (int j = 1; j < 4; ++j)
		{
			tmp = other->GetCorner(j).x * _axis[i].x + other->GetCorner(j).y * _axis[i].y;
			if (tmp < tmpMin)
				tmpMin = tmp;
			else if (tmp > tmpMax)
				tmpMax = tmp;
		}
		if ((tmpMin > 1 + _origin[i]) || (tmpMax < _origin[i]))
			return false;
	}
	return true;
}


void	Collision::AABB::UpdateCorners()
{
	Vec2	x(1, 0 );
	Vec2	y(0, 1 );

	x *= _width / 2;
	y *= _height / 2;

	_corner[0] = _pos - x - y;
	_corner[1] = _pos + x - y;
	_corner[2] = _pos + x + y;
	_corner[3] = _pos - x + y;

	UpdateAxis();
}

void	Collision::AABB::MoveTo(const Vec2& pos)
{
	Vec2 center = (_corner[0] + _corner[1] + _corner[2] + _corner[3]) * 0.25f;
	Vec2 translation = pos - center;

	for (int i = 0; i < 4; ++i)
	{
		_corner[i] += translation;
	}
	UpdateAxis();
	_min = CalcMin();
	_max = CalcMax();
}

void	Collision::AABB::UpdateAxis()
{
	_axis[0] = _corner[1] - _corner[0];
	_axis[1] = _corner[3] - _corner[0];

	for (int i = 0; i < 2; ++i)
	{
		_axis[i] /= _axis[i].SquaredLength();
		_origin[i] = _corner[0].Dot(_axis[i]);
	}
}

float	AABB::Width()const { return _width; }
float	AABB::Height()const { return _height;}
const	Vec2&	AABB::Position()const { return _pos; }
const	Vec2&	AABB::GetMax()const { return _max; }
const	Vec2&	AABB::GetMin()const { return _min; }
const	Vec2&	AABB::GetCorners(int i)const { return _corner[i]; }


void		AABB::SetPosition(const Vec2& pos)
{
	this->_pos = pos;
	this->_min = this->CalcMin();
	this->_max = this->CalcMax();
	this->UpdateCorners();
}

void		AABB::SetWidth(float width)
{
	this->_width = width;
	this->_min = this->CalcMin();
	this->_max = this->CalcMax();
	this->UpdateCorners();
}

void		AABB::SetHeight(float height)
{
	this->_height = height;
	this->_min = this->CalcMin();
	this->_max = this->CalcMax();
	this->UpdateCorners();
}