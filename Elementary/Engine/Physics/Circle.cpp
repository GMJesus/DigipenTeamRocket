#include "Circle.h"

using namespace Collision;

Circle::Circle()
{
}


Circle::~Circle()
{
}

void	Circle::Initialize(float radius, Vec2 pos)
{
	this->radius = radius;
	this->pos = pos;
}

bool	Circle::VsCircle(Circle* collide)
{
	float xd = this->pos.x - collide->pos.x;
	float yd = this->pos.y - collide->pos.y;

	float sumRad = this->radius + collide->radius;
	float sqrRadius = sumRad * sumRad;

	float distSqr = (xd * xd) + (yd * yd);
	if (distSqr <= sqrRadius)
		return true;
	return false;
}