#ifndef COLLISION_H_
# define COLLISION_H_

#include "AABB.h"
#include "Physics/Circle.h"
#include "OBB.h"
#include "Utils/LogWriter.h"

namespace Collision
{
	static bool		AABBvsAABB(AABB* a, AABB* b);
	AABB			*CreateAABB(Vec2 pos, float width, float height);
	Circle			*CreateCircle(float radius, Vec2 pos);
	OBB				*CreateOBB(Vec2 pos, float width, float height, double angle);
	void			testCollide();
}

#endif /* COLLISION_H_ */