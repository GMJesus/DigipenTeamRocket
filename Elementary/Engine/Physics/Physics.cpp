#include "Physics.h"

float PhysicEngine::GRAVITY = 92.8f;

Vec2*	PhysicEngine::SumForces(const std::vector<Vec2>* forces, bool isGravityEnable)
{
	Vec2*	direction = new Vec2(0, 0);
	if (forces && forces->size())
	{
		for (std::vector<Vec2>::const_iterator it = forces->begin(); it != forces->end(); ++it)
		{
			*direction += (*it);
		}
	}
	if (isGravityEnable)
		*direction += Vec2(0, -GRAVITY);
	return direction;
}

Vec2	PhysicEngine::Acceleration(const Vec2& summedForce, const float& invMass)
{
	return (summedForce * invMass);
}

Vec2	PhysicEngine::Velocity(const Vec2& velocity, const Vec2& acceleration, float dt)
{
	return (velocity + acceleration * dt);
}

Vec2	PhysicEngine::Position(const Vec2& pos, const Vec2& velocity, float dt)
{
	return (pos + velocity * dt);
}

Vec2	PhysicEngine::UpdateForce(float mass, const Vec2& acceleration)
{
	return (mass * acceleration);
}