/**
* \file		 OBB
* \author    Baptiste DUMAS
* \brief     File with OBB Class that handle collision
*/

#ifndef OBB_H_
# define OBB_H_

#include "Utils/Vec2.h"

struct Contact;

namespace Collision
{
	class AABB;
	/**
	* \brief	Oriented bounding box Class
	*/
	class OBB
	{
	public:
		OBB();
		~OBB();

		/**
		* \brief	Initialize an OBB
		* \param pos	Postion of the OBB
		* \param width	Width of the OBB
		* \param height	Height of the OBB
		* \param angle	Angle of the OBB
		*/
		void	Initialize(Vec2 pos, float width, float height, double angle);
		/**
		* \brief Check if there a collision with another OBB, and filled a Contact structure
		* \param other	The other OBB
		* \param contact	Contact structure that contain contact information
		* \return	True if there is collision
		*/
		bool	VsOBB(const OBB* other, Contact *contact);
		bool	VsAABBOneAxis(const AABB* other) const;
		/**
		* \brief Check if there a collision with an AABB
		* \param other	The other AABB
		* \return	True if there is collision
		*/
		bool	VsAABB(const AABB* other) const;
		/**
		* \brief Check if there a collision between two point
		* \param begin	First point
		* \param end	Second point
		* \return	True if there is collision
		*/
		bool	VsLine(const Vec2 begin, const Vec2 end) const;
		/**
		* \brief Move the OBB to a position
		* \param pos	Vector2 position
		*/
		void	MoveTo(const Vec2& pos);

		/**
		* \brief	Get a corner of OBB
		* \param i	Corner numero
		* \return	Vector2 of the corner's position
		*/
		const Vec2&		GetCorner(int i)const;
		const Vec2&		Pos()const;
		const float&	Width()const;
		const float&	Height()const;
		const double&	Angle()const;

		void	SetPos(const Vec2& pos);
		void	SetWidth(const float& width);
		void	SetHeight(const float& height);
		void	SetAngle(const double& angle);

	private:

		Vec2	_pos;
		float	_width;
		float	_height;
		double	_angle;

		Vec2	_corner[4];
		Vec2	_axis[2];
		float	_origin[2];

		void	UpdateAxis();
		bool	VsOBBOneAxis(const OBB* other) const;
	};

}

#endif /* OBB_H_ */