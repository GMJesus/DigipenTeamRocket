#ifndef AABB_H_
# define AABB_H_

#pragma warning (disable : 4005)

#include "Utils/Vec2.h"
#include "Utils/LogWriter.h"
#include "OBB.h"

namespace Collision
{
	class AABB
	{
	public:
		AABB();
		~AABB();

		void		Initialize(Vec2 pos, float width, float height);
		bool		IsPointInAABB(Vec2 point)const;
		bool		VsAABB(AABB *collide)const;
		bool		VsOBBOneAxis(const OBB *other)const;
		bool		VsOBB(const OBB *other)const;

		float		Width()const;
		float		Height()const;
		const Vec2&	Position()const;
		const Vec2&	GetMin()const;
		const Vec2& GetMax()const;
		const Vec2& GetCorners(int i)const;

		void		SetWidth(float width);
		void		SetHeight(float height);
		void		SetPosition(const Vec2& position);
		void		MoveTo(const Vec2& pos);

		Vec2		CalcMax()const;
		Vec2		CalcMin()const;

	private:
		Vec2	_pos;
		float	_width;
		float	_height;

		Vec2	_min;
		Vec2	_max;

		Vec2	_corner[4];
		Vec2	_axis[2];
		float	_origin[2];

		void	UpdateAxis();
		void	UpdateCorners();
	};
}

#endif /* AABB_H_ */