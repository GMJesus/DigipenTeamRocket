#include "OBB.h"
#include "AABB.h"
#include "Modules/BoxCollisionModule.h"
#include "UI/UIEngine.h"

Collision::OBB::OBB()
{
}

Collision::OBB::~OBB()
{
}

void	Collision::OBB::Initialize(Vec2 pos, float width, float height, double angle)
{
	this->_pos = pos;
	this->_width = width;
	this->_height = height;
	this->_angle = angle;

	Vec2	x(cos(angle), sin(angle));
	Vec2	y(-sin(angle), cos(angle));

	x *= width / 2;
	y *= height / 2;

	_corner[0] = pos - x - y;
	_corner[1] = pos + x - y;
	_corner[2] = pos + x + y;
	_corner[3] = pos - x + y;

	UpdateAxis();
}

void	Collision::OBB::MoveTo(const Vec2& pos)
{
	Vec2 center = (_corner[0] + _corner[1] + _corner[2] + _corner[3]) / 4;
	Vec2 translation = pos - center;
	_pos = pos;

	for (int i = 0; i < 4; ++i)
	{
		_corner[i] += translation;
	}
	UpdateAxis();
}

void	Collision::OBB::UpdateAxis()
{
	_axis[0] = _corner[1] - _corner[0];
	_axis[1] = _corner[3] - _corner[0];
	
	for (int i = 0; i < 2; ++i)
	{
		_axis[i] /= _axis[i].SquaredLength();
		_origin[i] = _corner[0].Dot(_axis[i]);
	}
}

bool	Collision::OBB::VsOBBOneAxis(const OBB* other) const
{
	for (int i = 0; i < 2; ++i)
	{
		float tmp = other->_corner[0].Dot(_axis[i]);
		float tmpMin = tmp;
		float tmpMax = tmp;

		for (int j = 1; j < 4; ++j)
		{
			tmp = other->_corner[j].Dot(_axis[i]);
			if (tmp < tmpMin)
				tmpMin = tmp;
			else if (tmp > tmpMax)
				tmpMax = tmp;
		}
		if ((tmpMin > 1 + _origin[i]) || (tmpMax < _origin[i]))
			return false;
	}
	return true;
}

bool	Collision::OBB::VsAABBOneAxis(const AABB* other) const
{
	for (int i = 0; i < 2; ++i)
	{
		float tmp = other->GetCorners(0).Dot(_axis[i]);
		float tmpMin = tmp;
		float tmpMax = tmp;

		for (int j = 1; j < 4; ++j)
		{
			tmp = other->GetCorners(j).Dot(_axis[i]);
			if (tmp < tmpMin)
				tmpMin = tmp;
			else if (tmp > tmpMax)
				tmpMax = tmp;
		}
		if ((tmpMin > 1 + _origin[i]) || (tmpMax < _origin[i]))
			return false;
	}
	return true;
}

bool	Collision::OBB::VsOBB(const OBB* other, Contact *contact)
{
		// Vector from A to B
	Vec2 n = other->Pos() - _pos;

	// Calculate half extents along x axis for each object
	float a_extent = _width / 2;
	float b_extent = other->Width() / 2;

		// Calculate overlap on x axis
	float x_overlap = (a_extent + b_extent) - abs(n.x);

		// SAT test on x axis
	if (x_overlap > 0)
	{
		// Calculate half extents along x axis for each object
		a_extent = _height / 2;
		b_extent = other->Height() / 2;

		// Calculate overlap on y axis
		float y_overlap = (a_extent + b_extent) - abs(n.y);

		// SAT test on y axis
		if (y_overlap > 0)
		{
			// Find out which axis is axis of least penetration
			if (x_overlap < y_overlap)
			{
				// Point towards B knowing that n points from A to B
				if (n.x < 0)
					contact->normal = Vec2(-1, 0);
				else
					contact->normal = Vec2(1, 0);
				contact->penetration = x_overlap;
				return true;
			}
			else
			{
				// Point toward B knowing that n points from A to B
				if (n.y < 0)
					contact->normal = Vec2(0, -1);
				else
					contact->normal = Vec2(0, 1);
				contact->penetration = y_overlap;
				return true;
			}
		}
	}
	return false;
}

bool	Collision::OBB::VsAABB(const AABB* other) const
{
	return VsAABBOneAxis(other) && other->VsOBBOneAxis(this);
}

bool	Collision::OBB::VsLine(const Vec2 begin, const Vec2 end) const
{
	for (int i = 0; i < 2; ++i)
	{
		float tmpMin = begin.Dot(_axis[i]);
		float tmpMax = end.Dot(_axis[i]);
		if (tmpMin > tmpMax)
		{
			float tmp = tmpMin;
			tmpMin = tmpMax;
			tmpMax = tmp;
		}
		if ((tmpMin > 1 + _origin[i]) || (tmpMax < _origin[i]))
			return false;
	}
	return true;
}

void			Collision::OBB::SetPos(const Vec2& pos) { this->Initialize(pos, _width, _height, _angle); }
void			Collision::OBB::SetWidth(const float& width) { this->Initialize(_pos, width, _height, _angle); }
void			Collision::OBB::SetHeight(const float& height) { this->Initialize(_pos, _width, height, _angle); }
void			Collision::OBB::SetAngle(const double& angle) { this->Initialize(_pos, _width, _height, angle); }

const Vec2&		Collision::OBB::GetCorner(int i)const { return this->_corner[i]; }
const Vec2&		Collision::OBB::Pos()const { return _pos; }
const float&	Collision::OBB::Width()const { return _width; }
const float&	Collision::OBB::Height()const { return _height; }
const double&	Collision::OBB::Angle()const { return _angle; }