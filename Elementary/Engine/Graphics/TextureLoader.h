/**
* \author	Aymeric Lambolez
*/

#ifndef		TEXTURELOADER_H_
# define	TEXTURELOADER_H_

#pragma warning (disable : 4005)

#include	<string>
#include	<map>
#include	"Graphics\TexturePlane.h"
#include	"Graphics\DirectXInterface.h"
#include	"Utils\LogWriter.h"

namespace Graphic
{
	/**
	* \brief	Helper class to Load and Release Textures
	*/
	class TextureLoader
	{
	private:
		TextureLoader();
		~TextureLoader();

		/**
		* \brief	Delete and Release the given Texture
		*/
		void EraseTexture(Texture* tex);

		/**
		* \brief	Instance of this class
		*/
		static TextureLoader*	inst;
		/**
		* \brief	The Textures loaded by this object
		*/
		std::map<std::string, Texture*>	_texMap;
		/**
		* \brief	Stores the number of object that uses each texture
		*			When a texture is use by 0 object, the texture will be deleted
		*/
		std::map<Texture*, unsigned int> _texUseMap;

	public:
		/**
		* \brief	Gets the instance of this class
		*/
		static			TextureLoader*	Instance();
		/**
		* \brief	Shutdown this class, releasing all remaining textures
		*/
		void			Shutdown();

		/**
		* \brief	Gets the texture related to the WCHAR* passed as parameter
		*			If no texture already correspond to this WCHAR*, it will create a new one base on the path received
		* \param	name	The path to the texture to load
		* \return	The texture corresponding to the path, or NULL if the texture cannot be loaded
		*/
		Texture*		GetTexture(WCHAR* path);
		/**
		* \brief	Alert this class that one instance of a texture isn't used anymore
		*			If no more object uses this Texture, it will be Released
		* \param	tex		The Texture to Unlink
		*/
		void			UnlinkTexture(Texture* tex);
	};

}

#endif		// !TEXTURELOADER_H_
