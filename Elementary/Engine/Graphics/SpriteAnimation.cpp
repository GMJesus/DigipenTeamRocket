/**
* \author	Emeric Caramanna
*/
#include "SpriteAnimation.h"

SpriteAnimation::SpriteAnimation()
{
	_from = 0;
	_to = 0;
	_current = 0;
	_duration = 0;
	_step = 0;
	_speed = 0;
	_play = false;
	_revert = false;
	_loop = false;
	_back = false;
	E_Animated _animated = (E_Animated)0;
}

SpriteAnimation::~SpriteAnimation()
{
}

void SpriteAnimation::Play()
{
	_play = true;
}

void SpriteAnimation::Pause()
{
	_play = false;
}

void SpriteAnimation::Stop()
{
	_play = false;
	_current = _from;
}

void SpriteAnimation::SetWhich(SpriteAnimation::E_Animated which)
{
	_animated = which;
}

SpriteAnimation::E_Animated SpriteAnimation::GetAnimated()
{
	return _animated;
}

double SpriteAnimation::Update(float deltaTime)
{
	_step = _speed * deltaTime;
	if (!_back)
	{
		_current += _step;
		if (_from < _to)
		{
			if (_current >= _to)
			{
				if (_loop && !_revert)
				{
					_current = _from;
				}
				else if (_revert)
				{
					_current = _to;
					_step *= -1;
					_back = true;
				}
				else if (!_loop)
				{
					_current = _from;
					_play = false;
				}
			}
		}
		else if (_from > _to)
		{
			if (_current <= _to)
			{
				if (_loop && !_revert)
				{
					_current = _from;
				}
				else if (_revert)
				{
					_current = _to;
					_step *= -1;
					_back = true;
				}
				else if (!_loop)
				{
					_current = _from;
					_play = false;
				}
			}
		}
	}
	else
	{
		_current -= _step;
		if (_from > _to)
		{
			if (_current >= _from)
			{
				if (_loop)
				{
					_current = _from;
					_step *= -1;
					_back = false;
				}
				else
				{
					_current = _from;
					_play = false;
					_back = false;
				}
			}
		}		
		else if (_from < _to)
		{
			if (_current <= _from)
			{
				if (_loop)
				{
					_current = _from;
					_step *= -1;
					_back = false;
				}
				else
				{
					_current = _from;
					_play = false;
					_back = false;
				}
			}
		}
	}
	if (_from == _to)
	{
		_current = _from;
		_play = false;
	}
	return _current;
}

void SpriteAnimation::SetFrom(double from)
{
	_from = from;
}

void SpriteAnimation::SetTo(double to)
{
	_to = to;
}

void SpriteAnimation::SetCurrent(double current)
{
	_current = current;
	if (_from < _to)
	{
		if (_current < _from)
		{
			_current = _from;
		}
		else if (_current > _to)
		{
			_current = _to;
		}
	}
	else
	{
		if (_current > _from)
		{
			_current = _from;
		}
		else if (_current < _to)
		{
			_current = _from;
		}
	}
}

void SpriteAnimation::SetDuration(double duration)
{
	if (duration < 0)
	{
		duration *= -1;
	}
	_duration = duration;
	_speed = (_to - _from) / _duration;
}

void SpriteAnimation::SetRevert(bool revert)
{
	_revert = revert;
}

void SpriteAnimation::SetLoop(bool loop)
{
	_loop = loop;
}

double SpriteAnimation::GetFrom()
{
	return _from;
}

double SpriteAnimation::GetTo()
{
	return _to;
}

double SpriteAnimation::GetDuration()
{
	return _duration;
}

double SpriteAnimation::GetCurrent()
{
	return _current;
}

bool SpriteAnimation::isPlaying()
{
	return _play;
}

bool SpriteAnimation::isRevert()
{
	return _revert;
}

bool SpriteAnimation::isLooping()
{
	return _loop;
}