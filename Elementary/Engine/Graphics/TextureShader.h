/**
* \author	Emeric Caramanna
*/
#ifndef _TextureShader_H_
#define _TextureShader_H_

#pragma warning (disable : 4005)

#include "Graphics/DirectXInterface.h"
#include <fstream>
using namespace std;


/**
* \brief	Helper to use DirectX shaders
*/
class TextureShader
{
private:
#ifdef USEDIRECT3D
	/**
	* \brief	Shader parameter
	*/
	struct MatrixBufferType
	{
		D3DXMATRIX world;
		D3DXMATRIX view;
		D3DXMATRIX projection;
	};
#endif
public:
	TextureShader();
	TextureShader(const TextureShader&);
	~TextureShader();

#ifdef USEDIRECT3D
	/**
	* \brief	Initialize this object
	*/
	bool Initialize(ID3D11Device*, HWND);
	/**
	* \brief	Shutdown and release all datas
	*/
	void Shutdown();
	/**
	* \brief	Send the texture to the graphic render
	*/
	bool Render(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix,
		D3DXMATRIX projectionMatrix, ID3D11ShaderResourceView* texture);
#endif

private:
#ifdef USEDIRECT3D

	bool InitializeShader(ID3D11Device*, HWND, WCHAR*, WCHAR*);
	void ShutdownShader();
	void OutputShaderErrorMessage(ID3D10Blob*, HWND, WCHAR*);

	bool SetShaderParameters(ID3D11DeviceContext*, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*);
	void RenderShader(ID3D11DeviceContext*, int);
#endif

private:
#ifdef USEDIRECT3D

	ID3D11VertexShader* m_vertexShader;
	ID3D11PixelShader* m_pixelShader;
	ID3D11InputLayout* m_layout;
	ID3D11Buffer* m_matrixBuffer;
	ID3D11SamplerState* m_sampleState;
#endif

};

#endif