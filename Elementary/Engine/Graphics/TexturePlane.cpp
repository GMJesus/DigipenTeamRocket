/**
* \author	Aymeric Lambolez
*/
#include "TexturePlane.h"
#include "TextureLoader.h"
#include "UI/UIEngine.h"

namespace Graphic
{

	TexturePlane::TexturePlane()
	{
#ifdef USEDIRECT3D
		m_vertexBuffer = 0;
		m_indexBuffer = 0;
#endif
		m_Texture = NULL;
		uvs[0] = Vec2(0.f, 0.f);
		uvs[1] = Vec2(1.f, 0.f);
		uvs[2] = Vec2(1.f, 1.f);
		uvs[3] = Vec2(0.f, 1.f);
		changed = true;
	}


	TexturePlane::TexturePlane(const TexturePlane& other)
	{
	}


	TexturePlane::~TexturePlane()
	{
	}

	const char*	TexturePlane::GetTexturePath()const
	{
		if (!m_Texture)
			return NULL;
		return m_Texture->GetPath();
	}

#ifdef USEDIRECT3D
	bool TexturePlane::Initialize(ID3D11Device* device, WCHAR* textureFilename, int TexturePlaneWidth, int TexturePlaneHeight)
	{
		bool result;

		OutputDebugStringW(textureFilename);

		// Store the size in pixels that this TexturePlane should be rendered at.
		SetWidth(TexturePlaneWidth);
		SetHeight(TexturePlaneHeight);

		// Initialize the vertex and index buffers.
		result = InitializeBuffers(device);
		if (!result)
		{
			return false;
		}

		// Load the texture for this TexturePlane.
		if (textureFilename)
			SetTexture(Graphic::TextureLoader::Instance()->GetTexture(textureFilename));
		changed = true;

		return true;
	}

	bool TexturePlane::InitializeBuffers(ID3D11Device* device)
	{
		VertexType* vertices;
		unsigned long* indices;
		D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
		D3D11_SUBRESOURCE_DATA vertexData, indexData;
		HRESULT result;
		int i;


		// Set the number of vertices in the vertex array.
		m_vertexCount = 6;

		// Set the number of indices in the index array.
		m_indexCount = m_vertexCount;

		// Create the vertex array.
		vertices = new VertexType[m_vertexCount];
		if (!vertices)
		{
			return false;
		}

		// Create the index array.
		indices = new unsigned long[m_indexCount];
		if (!indices)
		{
			return false;
		}

		// Initialize vertex array to zeros at first.
		memset(vertices, 0, (sizeof(VertexType) * m_vertexCount));

		// Load the index array with data.
		for (i = 0; i < m_indexCount; i++)
		{
			indices[i] = i;
		}

		// Set up the description of the static vertex buffer.
		vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
		vertexBufferDesc.ByteWidth = sizeof(VertexType) * m_vertexCount;
		vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		vertexBufferDesc.MiscFlags = 0;
		vertexBufferDesc.StructureByteStride = 0;

		// Give the subresource structure a pointer to the vertex data.
		vertexData.pSysMem = vertices;
		vertexData.SysMemPitch = 0;
		vertexData.SysMemSlicePitch = 0;

		// Now create the vertex buffer.
		result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
		if (FAILED(result))
		{
			return false;
		}

		// Set up the description of the static index buffer.
		indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
		indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_indexCount;
		indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		indexBufferDesc.CPUAccessFlags = 0;
		indexBufferDesc.MiscFlags = 0;
		indexBufferDesc.StructureByteStride = 0;

		// Give the subresource structure a pointer to the index data.
		indexData.pSysMem = indices;
		indexData.SysMemPitch = 0;
		indexData.SysMemSlicePitch = 0;

		// Create the index buffer.
		result = device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
		if (FAILED(result))
		{
			return false;
		}

		// Release the arrays now that the vertex and index buffers have been created and loaded.
		delete[] vertices;
		vertices = 0;

		delete[] indices;
		indices = 0;

		FillBuffers(DirectXInterface::Instance().GetDeviceContext());
		return true;
	}

	bool TexturePlane::Render(ID3D11DeviceContext* deviceContext, D3DXMATRIX mat)
	{
		bool result;

		// Re-build the dynamic vertex buffer for rendering to possibly a different location on the screen.
		if (changed)
		{
			changed = false;
			result = FillBuffers(deviceContext);
			if (!result)
			{
				return false;
			}
		}

		// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
		RenderBuffers(deviceContext);

		mat._41 *= PPU;
		mat._42 *= PPU;
		if (m_Texture)
		{
			result = m_Texture->Render(mat);
			if (!result)
			{
				return false;
			}
		}

		return true;
	}

	bool TexturePlane::Render(ID3D11DeviceContext* deviceContext, float positionX, float positionY, float angle)
	{
		bool result;

		// Re-build the dynamic vertex buffer if the width or height has changed
		if (changed)
		{
			changed = false;
			result = FillBuffers(deviceContext);
			if (!result)
			{
				return false;
			}
		}

		// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
		RenderBuffers(deviceContext);

		float radAngle = angle * M_PI / 180.f;
		//Calculate the object WorldMatrix
		D3DXMATRIX mat = D3DXMATRIX(
			cos(radAngle),		sin(radAngle),	0.f,	0.f,
			sin(radAngle) * -1,	cos(radAngle),	0.f,	0.f,
			0.f,				0.f,			1.f,	0.f,
			0.f,				0.f,			0.f,	1.f
			);

		//Translate
		D3DXMATRIX translationMatrix = D3DXMATRIX(
			1.f,				0.f,				0.f,	0.f,
			0.f,				1.f,				0.f,	0.f,
			0.f,				0.f,				1.f,	0.f,
			positionX * PPU,	positionY * PPU,	0.f,	1.f
			);
		mat *= translationMatrix;

		if (m_Texture)
		{
			result = m_Texture->Render(mat);
			if (!result)
			{
				return false;
			}
		}

		return true;
	}

	bool TexturePlane::FillBuffers(ID3D11DeviceContext* deviceContext)
	{
		float left, right, top, bottom;
		float s, c;
		VertexType* vertices;
		D3D11_MAPPED_SUBRESOURCE mappedResource;
		VertexType* verticesPtr;
		HRESULT result;

		// Calculate the screen coordinates of the left side of the TexturePlane.
		left = (float)_texturePlaneWidth * -.5f;

		// Calculate the screen coordinates of the right side of the TexturePlane.
		right = left + (float)_texturePlaneWidth;

		// Calculate the screen coordinates of the top of the TexturePlane.
		top = (float)_texturePlaneHeight * .5f;

		// Calculate the screen coordinates of the bottom of the TexturePlane.
		bottom = top - (float)_texturePlaneHeight;

		// Create the vertex array.
		vertices = new VertexType[m_vertexCount];
		if (!vertices)
		{
			return false;
		}

		s = sin(0);
		c = cos(0);
		Point LT, LB, RT, RB, C;

		LT.x = left;
		LT.y = top;

		LB.x = left;
		LB.y = bottom;

		RT.x = right;
		RT.y = top;

		RB.x = right;
		RB.y = bottom;

		C.x = (LT.x + RB.x) / 2;
		C.y = (LT.y + RB.y) / 2;

		// translate point back to origin:
		LT.x -= C.x;
		LT.y -= C.y;

		// rotate point
		float xnew = LT.x * c - LT.y * s;
		float ynew = LT.x * s + LT.y * c;

		// translate point back:
		LT.x = xnew + C.x;
		LT.y = ynew + C.y;

		// translate point back to origin:
		LB.x -= C.x;
		LB.y -= C.y;

		// rotate point
		xnew = LB.x * c - LB.y * s;
		ynew = LB.x * s + LB.y * c;

		// translate point back:
		LB.x = xnew + C.x;
		LB.y = ynew + C.y;

		// translate point back to origin:
		RT.x -= C.x;
		RT.y -= C.y;

		// rotate point
		xnew = RT.x * c - RT.y * s;
		ynew = RT.x * s + RT.y * c;

		// translate point back:
		RT.x = xnew + C.x;
		RT.y = ynew + C.y;

		// translate point back to origin:
		RB.x -= C.x;
		RB.y -= C.y;

		// rotate point
		xnew = RB.x * c - RB.y * s;
		ynew = RB.x * s + RB.y * c;

		// translate point back:
		RB.x = xnew + C.x;
		RB.y = ynew + C.y;



		// Load the vertex array with data.
		// First triangle.
		vertices[0].position = D3DXVECTOR3(LT.x, LT.y, 0.0f);  // Top left.
		vertices[0].texture = D3DXVECTOR2(uvs[0].x, uvs[0].y);

		vertices[1].position = D3DXVECTOR3(RB.x, RB.y, 0.0f);  // Bottom right.
		vertices[1].texture = D3DXVECTOR2(uvs[2].x, uvs[2].y);

		vertices[2].position = D3DXVECTOR3(LB.x, LB.y, 0.0f);  // Bottom left.
		vertices[2].texture = D3DXVECTOR2(uvs[3].x, uvs[3].y);

		// Second triangle.
		vertices[3].position = D3DXVECTOR3(LT.x, LT.y, 0.0f);  // Top left.
		vertices[3].texture = D3DXVECTOR2(uvs[0].x, uvs[0].y);

		vertices[4].position = D3DXVECTOR3(RT.x, RT.y, 0.0f);  // Top right.
		vertices[4].texture = D3DXVECTOR2(uvs[1].x, uvs[1].y);

		vertices[5].position = D3DXVECTOR3(RB.x, RB.y, 0.0f);  // Bottom right.
		vertices[5].texture = D3DXVECTOR2(uvs[2].x, uvs[2].y);

		// Lock the vertex buffer so it can be written to.
		result = deviceContext->Map(m_vertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
		if (FAILED(result))
		{
			return false;
		}

		// Get a pointer to the data in the vertex buffer.
		verticesPtr = (VertexType*)mappedResource.pData;

		// Copy the data into the vertex buffer.
		memcpy(verticesPtr, (void*)vertices, (sizeof(VertexType) * m_vertexCount));

		// Unlock the vertex buffer.
		deviceContext->Unmap(m_vertexBuffer, 0);

		// Release the vertex array as it is no longer needed.
		delete[] vertices;
		vertices = 0;

		return true;
	}

	void TexturePlane::RenderBuffers(ID3D11DeviceContext* deviceContext)
	{
		unsigned int stride;
		unsigned int offset;


		// Set vertex buffer stride and offset.
		stride = sizeof(VertexType);
		offset = 0;

		// Set the vertex buffer to active in the input assembler so it can be rendered.
		deviceContext->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

		// Set the index buffer to active in the input assembler so it can be rendered.
		deviceContext->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

		// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
		deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		return;
	}
#endif

	void	TexturePlane::SetUVs(const Vec2& TL, const Vec2& TR, const Vec2& BR, const Vec2& BL)
	{
		uvs[0] = TL;
		uvs[1] = TR;
		uvs[2] = BR;
		uvs[3] = BL;
		changed = true;
	}

	void TexturePlane::Shutdown()
	{
		// Release the TexturePlane texture.
		//ReleaseTexture(); Don't needed since the TextureLoader will free it

		// Shutdown the vertex and index buffers.
		ShutdownBuffers();
		Graphic::TextureLoader::Instance()->UnlinkTexture(m_Texture);
		return;
	}

	void TexturePlane::ShutdownBuffers()
	{
#ifdef USEDIRECT3D
		// Release the index buffer.
		if (m_indexBuffer)
		{
			m_indexBuffer->Release();
			m_indexBuffer = 0;
		}

		// Release the vertex buffer.
		if (m_vertexBuffer)
		{
			m_vertexBuffer->Release();
			m_vertexBuffer = 0;
		}
#endif
		return;
	}

	void TexturePlane::SetHeight(int height)
	{
		if (_texturePlaneHeight != height)
			changed = true;
		_texturePlaneHeight = height;
	}

	void TexturePlane::SetWidth(int width)
	{
		if (_texturePlaneWidth != width)
			changed = true;
		_texturePlaneWidth = width;
	}

	void TexturePlane::SetTexture(Texture* newTex)
	{
		if (newTex != m_Texture)
		{
			Graphic::TextureLoader::Instance()->UnlinkTexture(m_Texture);
			m_Texture = newTex;
		}
	}

	int	TexturePlane::GetWidth()
	{
		return _texturePlaneWidth;
	}
	int TexturePlane::GetHeight()
	{
		return _texturePlaneHeight;
	}
	void	TexturePlane::Flip()
	{
		Vec2 tmp = uvs[0];
		uvs[0] = uvs[1];
		uvs[1] = tmp;
		tmp = uvs[2];
		uvs[2] = uvs[3];
		uvs[3] = tmp;
		changed = true;
	}
}
