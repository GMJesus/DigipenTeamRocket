/**
* \author	Emeric Caramanna
*/
#include "Particle.h"
#include "UI/UIEngine.h"

Particle::Particle(float lifeTime, Graphic::TexturePlane* texture, Vec2 initialVelocity, Vec2 initialPosition, int width, int height, bool gravity)
{
	_lifeTime = lifeTime;
	_velocity = _initialVelocity = initialVelocity;
	_position = _initialPosition = _initialPosition;
	_width = width;
	_height = height;
	_texturePlane = texture;
	if (_texturePlane)
	{
		_texturePlane->SetHeight(_height);
		_texturePlane->SetWidth(_width);
	}
	_gravityEnabled = gravity;
}

Particle::Particle()
{
}

Particle::Particle(const Particle& element)
{
	_lifeTime = element._lifeTime;
	_velocity = element._velocity;
	_position = element._position;
	_width = element._width;
	_height = element._height;
	_texturePlane = element._texturePlane;
	if (_texturePlane)
	{
		_texturePlane->SetHeight(_height);
		_texturePlane->SetWidth(_width);
	}
	_gravityEnabled = element._gravityEnabled;
	_alive = element._alive;
}

Particle& Particle::operator=(const Particle& element)
{
	_lifeTime = element._lifeTime;
	_velocity = element._velocity;
	_position = element._position;
	_width = element._width;
	_height = element._height;
	_texturePlane = element._texturePlane;
	if (_texturePlane)
	{
		_texturePlane->SetHeight(_height);
		_texturePlane->SetWidth(_width);
	}
	_gravityEnabled = element._gravityEnabled;
	_alive = element._alive;
	return (*this);
}

void	Particle::Launch(float lifetime, Vec2 position, Vec2 velocity)
{
	_lifeTime = lifetime;
	_velocity = velocity;
	_position = position;
	_alive = true;
}

bool	Particle::IsAlive(){ return _alive; }

Particle::~Particle()
{
}

void Particle::Update(float deltaTime)
{
	if (_texturePlane)
	{
		if (_alive)
		{
			_lifeTime -= deltaTime;
			_position = _position + _velocity * deltaTime;
			Vec2* sumForces = PhysicEngine::SumForces(&_forces, _gravityEnabled);
			_velocity += *sumForces * deltaTime;
			delete(sumForces);
			if (_lifeTime <= 0)
				_alive = false;
		}
	}
}

void	Particle::Destroy()
{
}

float Particle::GetLifeTime(){ return _lifeTime; }

void	Particle::Draw()
{
	if (_alive)
	{
#ifdef USEDIRECT3D
		if (!_texturePlane->Render(DirectXInterface::Instance().GetDeviceContext(), _position.x, _position.y, 0))
			LogWriter::Instance().addLogMessage("Particle : Cannot draw", E_MsgType::Warning);
#endif
	}
}