/**
* \author	Emeric Caramanna
*/
#pragma once
#include <string>
#include "../Utils/LogWriter.h"

/**
* \brief Class used to describe animations
*/
class SpriteAnimation
{
public:
	/**
	* \brief Type of Animation
	*/
	enum E_Animated
	{
		TranslateX = 0,
		TranslateY = 1,
		Rotation = 2,
		Width = 3,
		Height = 4
	};
private:
	/**
	* \brief Start value of the animation
	*/
	double _from;
	/**
	* \brief End value of the animation
	*/
	double _to;
	double _current;
	/**
	* \brief Duration in seconds of the animation
	*/
	double _duration;
	double _step;
	double _speed;
	bool _play;
	/**
	* \brief If true when the animation reach To go back to From
	*/
	bool _revert;
	/**
	* \brief If true the animation is always played
	*/
	bool _loop;
	bool _back;
	E_Animated _animated;

public:
	SpriteAnimation();
	~SpriteAnimation();
	/**
	* \brief Plays the animation from where it was left
	*/
	void Play();
	/**
	* \brief Pause the animation
	*/
	void Pause();
	/**
	* \brief Stop the animation and reset it
	*/
	void Stop();
	double Update(float deltaTime);

	void SetWhich(E_Animated which);
	void SetFrom(double from);
	void SetTo(double to);
	void SetCurrent(double current);
	void SetDuration(double duration);
	void SetRevert(bool revert);
	void SetLoop(bool loop);

	double GetFrom();
	double GetTo();
	double GetDuration();
	double GetCurrent();
	E_Animated GetAnimated();
	bool isPlaying();
	bool isRevert();
	bool isLooping();


};

