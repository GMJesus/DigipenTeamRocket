/**
* \author	Aymeric Lambolez
*/
#ifndef _CAMERA_H_
#define _CAMERA_H_

#pragma warning (disable : 4005)

#include "Graphics/DirectXInterface.h"
#include "Game\GameObject.h"
#include "Inputs/InputManager.h"
#include "Utils\Vec2.h"

#define	PIXEL_PER_UNIT 10
#define	PPU PIXEL_PER_UNIT


/**
* \brief	Namespace defining Graphic datas
*/
namespace Graphic
{
	/**
	* \brief	Class for Camera Management
	*/
	class Camera
	{
	public:
		void SetPosition(float, float, float);
		/**
		* \brief	Move the camera so it faces the given position
		* \param	pos	Position to face
		*/
		void MoveTo(const Vec2& pos);
		/**
		* \brief	Translate the camera along X and Y axys
		* \param	translation	The vector to add to the position of the camera
		*/
		void Translate(const Vec2&translation);

#ifdef USEDIRECT3D
		D3DXVECTOR3 GetPosition();
		/**
		* \brief	Gets the View Matrix of this camera
		*			The view matrix is calculated once per frame
		* \return	The View Matrix of this camera
		*/
		const D3DXMATRIX& GetViewMatrix();
#endif
		const Vec2	Get2DPosition();
		/**
		* \brief	Move the camera along the X Axys
		* \param	x	The value of the translation
		*/
		void		MoveX(float x);
		/**
		* \brief	Move the camera along the Y Axys
		* \param	y	The value of the translation
		*/
		void		MoveY(float y);
		/**
		* \brief	Convert a pixel position to a World position, based on the camera position
		* \param	x	The x value of the pixel position
		* \param	y	The y value of the pixel position
		* \return	The converted position in an allocated vector
		*			This vector should be free
		*/
		Vec2*		ConvertPixelPositionToWorldPosition(int x, int y);
		/**
		* \brief	Returns the 2D World coordinate of the mouse
		* \return	The mouse position in an allocated vector
		*			This vector should be free
		*/
		Vec2*		GetMouseWorldPosition();

		/**
		* \brief	Calculate the View Matrix based on the current Camera Position
		*/
		void Render();
		/**
		* \brief	Update the camera, making it follow its target if there is one
		* \param	deltaTime	The time since last frame
		* \param	focus		The object to follow
		*						If NULL this function will not move the camera
		*/
		void Update(float deltaTime, GameObject* focus);

		/**
		* \brief	Gets the instance of the camera
		*/
		static Camera*	Instance();

	private:
		Camera();
		Camera(const Camera&);
		~Camera();

		float	m_positionX, m_positionY, m_positionZ;
#ifdef USEDIRECT3D
		D3DXMATRIX m_viewMatrix;
#endif
		static Camera*	inst;
	};

}

#endif