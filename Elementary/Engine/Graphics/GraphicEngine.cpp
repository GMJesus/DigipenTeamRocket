/**
* \author	Emeric Caramanna
*/
#include "Graphics\GraphicEngine.h"

namespace Graphic
{

	GraphicEngine::GraphicEngine()
	{
	}


	GraphicEngine::GraphicEngine(const GraphicEngine& other)
	{
	}


	GraphicEngine::~GraphicEngine()
	{
	}


	bool GraphicEngine::Initialize(int screenWidth, int screenHeight, HWND hwnd, s_WindowDatas &datas)
	{
		bool result;

		// Set the initial position of the camera.
		Camera::Instance()->SetPosition(0.0f, 0.0f, -10.0f);
		LogWriter::Instance().addLogMessage("Initializing Camera Position.", Log);

		// Initialize the Direct3D object.
		result = m_DXInterface.Initialize(screenWidth, screenHeight, datas.vSync, hwnd, datas.fullScreen, SCREEN_DEPTH, SCREEN_NEAR);
		if (!result)
		{
			MessageBoxW(hwnd, L"Could not initialize Direct3D.", L"Error", MB_OK);
			LogWriter::Instance().addLogMessage("Could not initialize Direct3D.", Error);
			return false;
		}
		else
			LogWriter::Instance().addLogMessage("Initializing Direct3D.", Log);

		LogWriter::Instance().addLogMessage("Graphic Engine Initialized");
		return true;
	}

	void GraphicEngine::Shutdown()
	{

	}

	bool GraphicEngine::Update()
	{
		// Clear the buffers to begin the scene.
		m_DXInterface.BeginScene(0.0f, 0.0f, 0.0f, 1.0f);

		// Generate the view matrix based on the camera's position.
		Camera::Instance()->Render();

		// Turn off the Z buffer to begin all 2D rendering.
		m_DXInterface.TurnZBufferOff();

		// Turn on the alpha blending before rendering the text.
		m_DXInterface.TurnOnAlphaBlending();

		return true;
	}

#ifdef USEDIRECT3D
	ID3D11Texture2D *CreateBorder(ID3D11Device *myDevice, int w, int h)
	{
		ID3D11Texture2D *tex;
		D3D11_TEXTURE2D_DESC tdesc;
		D3D11_SUBRESOURCE_DATA tbsd;

		int *buf = new int[w*h];
		if (!buf)
			return NULL;

		for (int i = 0; i < h; i++)
			for (int j = 0; j < w; j++)
			{
				buf[i*w + j] = 0xffffffff;
			}

		tbsd.pSysMem = (void *)buf;
		tbsd.SysMemPitch = w * 4;
		tbsd.SysMemSlicePitch = w*h * 4;

		tdesc.Width = w;
		tdesc.Height = h;
		tdesc.MipLevels = 1;
		tdesc.ArraySize = 1;

		tdesc.SampleDesc.Count = 1;
		tdesc.SampleDesc.Quality = 0;
		tdesc.Usage = D3D11_USAGE_DEFAULT;
		tdesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		tdesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		tdesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		
		tdesc.CPUAccessFlags = 0;
		tdesc.MiscFlags = 0;

		if (FAILED(myDevice->CreateTexture2D(&tdesc, &tbsd, &tex)))
			return(0);


		delete[] buf;

		return(tex);
	}
#endif

	bool GraphicEngine::Render()
	{

		m_DXInterface.TurnOffAlphaBlending();
		m_DXInterface.TurnZBufferOn();
		m_DXInterface.EndScene();
		return true;
	}

}