/**
* \author	Aymeric Lambolez
*/
#ifndef _Texture_H_
#define _Texture_H_

#pragma warning (disable : 4005)

#include "Graphics/DirectXInterface.h"

#include "Graphics\TextureShader.h"
#include "Graphics\DirectXInterface.h"
#include "Graphics\Camera.h"
#include "Utils\WindowDatas.h"

namespace Graphic
{

	/**
	* \brief	Encapsulation of DirectX texture
	*/
	class Texture
	{
	public:
		Texture();
		Texture(const Texture&);
		~Texture();
#ifdef USEDIRECT3D
		/**
		* \brief	Initialize the object, loading the given file
		* \param	device	The DirectX device
		* \param	file	The file to load
		* \return	True if the file has been load, false overwise
		*/
		bool Initialize(ID3D11Device* device, WCHAR* file);
		/**
		* \brief	Initialize the object, setting the given texture
		* \param	device	The DirectX device
		* \param	texture	The texture to store
		* \return	True
		*/
		bool Initialize(ID3D11Device* device, ID3D11Resource* texture);
		/**
		* \brief	Shutdown and release all datas
		*/
		void Shutdown();
		/**
		* \brief	Send the texture to the graphic render
		*/
		bool Render(const D3DXMATRIX& objMatrix);
#endif

#ifdef USEDIRECT3D
		/**
		* \brief	Return the DirectX texture
		*/
		ID3D11ShaderResourceView* GetTexture();
#endif

		/**
		* \brief	Returns the path to the loaded texture
		*/
		const char*					GetPath()const;
	private:
#ifdef USEDIRECT3D
		ID3D11ShaderResourceView*	m_texture;
#endif

		/**
		* \brief	The texture Shader used to draw this object
		*/
		TextureShader*				_texShader;
		char*						_path;
	};

}

#endif