/**
* \author	Emeric Caramanna
*/
#pragma once

#include "Graphics/TexturePlane.h"
#include "Physics/Physics.h"

/**
* \brief Class used to define a single particle
*/
class Particle
{
private:

	float			_lifeTime;
	Graphic::TexturePlane *_texturePlane;
	/**
	* \brief Forces applied to the movement of the particle
	*/
	std::vector<Vec2>	_forces;

	Vec2			_velocity;
	Vec2			_position;
	Vec2			_initialPosition;
	Vec2			_initialVelocity;
	float			_initialLifeTime;
	int				_width;
	int				_height;
	bool			_gravityEnabled;
	/**
	* \brief False when the particle is not displayed
	*/
	bool			_alive = true;
public:
	Particle();
	/**
	* \brief Constructor of the Particle class
	* \param lifeTime The duration while the particle is displayed
	* \param texture A pointer to the TexturePlane used by the particle
	* \param initialVelocity The direction and magnitude of the particle when it is created
	* \param initialPosition The initial position of the particle
	* \param width The width of the particle
	* \param height The height of the particle
	* \param gravity True if the gravity affects the movement of the particle
	*/
	Particle(float lifeTime, Graphic::TexturePlane* texture, Vec2 initialVelocity, Vec2 initialPosition, int width, int height, bool gravity);

	Particle(const Particle& element);
	~Particle(); 

	Particle& operator=(const Particle& element);

	/**
	* \brief Launch the particle with the given life time, position and velocity
	*/
	void	Launch(float lifetime, Vec2 position, Vec2 velocity);

	bool	IsAlive();

	void	Update(float deltaTime);
	void	Draw();
	void	Destroy();

	float	GetLifeTime();
	Vec2	GetPosition();

};

