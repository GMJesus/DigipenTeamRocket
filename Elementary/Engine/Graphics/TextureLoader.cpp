/**
* \author	Aymeric Lambolez
*/
#include	"Graphics\TextureLoader.h"
#include	"UI/UIEngine.h"

namespace Graphic
{
	TextureLoader*	TextureLoader::inst = NULL;

	TextureLoader::TextureLoader()
	{

	}

	TextureLoader::~TextureLoader()
	{

	}

	TextureLoader*		TextureLoader::Instance()
	{
		if (!inst)
			inst = new TextureLoader();
		return inst;
	}

	Texture*			TextureLoader::GetTexture(WCHAR* path)
	{
		Texture* tex = NULL;
		wstring tmp = path;
		std::string str(tmp.begin(), tmp.end());

		try
		{
			tex = _texMap.at(str);
			_texUseMap[tex] += 1;
		}
		catch (std::out_of_range &e)
		{
			tex = new Texture();
#ifdef USEDIRECT3D
			if (!tex->Initialize(DirectXInterface::Instance().GetDevice(), path))
			{
				LogWriter::Instance().addLogMessage("Cannot open file : " + str, E_MsgType::Warning);
			}
#endif
			_texUseMap[tex] = 1;
		}
		_texMap[str] = tex;
		return tex;
	}

	void				TextureLoader::UnlinkTexture(Texture* tex)
	{
		_texUseMap[tex] -= 1;
		if (_texUseMap[tex] <= 0)
		{
			EraseTexture(tex);
		}
	}

	void				TextureLoader::EraseTexture(Texture* tex)
	{
		_texUseMap.erase(tex);
		for (std::map<std::string, Texture*>::const_iterator it = _texMap.begin(); it != _texMap.end(); ++it)
		{
			if (it->second == tex)
			{
				tex->Shutdown();
				delete (tex);
				tex = NULL;
				_texMap.erase(it);
				return;
			}
		}
	}

	void				TextureLoader::Shutdown()
	{
		while (_texMap.size())
		{
			Texture* tex = _texMap.begin()->second;
			tex->Shutdown();
			delete (tex);
			tex = NULL;
			_texMap.erase(_texMap.begin());
		}
	}
}