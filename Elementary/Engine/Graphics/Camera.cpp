/**
* \author	Aymeric Lambolez
*/
#include "Camera.h"
#include "../Utils/WindowDatas.h"
#include "Modules/TransformModule.h"
#include "UI/UIEngine.h"

namespace Graphic
{
	Camera*		Camera::inst = NULL;

	Camera::Camera()
	{
		m_positionX = 0.0f;
		m_positionY = 0.0f;
		m_positionZ = 0.0f;
	}


	Camera::Camera(const Camera& other)
	{
	}


	Camera::~Camera()
	{
	}


	void Camera::SetPosition(float x, float y, float z)
	{
		m_positionX = x;
		m_positionY = y;
		m_positionZ = z;
		return;
	}

#ifdef USEDIRECT3D
	D3DXVECTOR3 Camera::GetPosition()
	{
		return D3DXVECTOR3(m_positionX, m_positionY, m_positionZ);
	}

	const D3DXMATRIX& Camera::GetViewMatrix()
	{
		return m_viewMatrix;
	}
#endif

	void		Camera::MoveTo(const Vec2& pos)
	{
		m_positionX = pos.x;
		m_positionY = pos.y;
	}

	void		Camera::Translate(const Vec2& translation)
	{
		m_positionX += translation.x;
		m_positionY += translation.y;
	}

	void		Camera::MoveX(float x)
	{
		m_positionX += x;
	}

	void		Camera::MoveY(float y)
	{
		m_positionY += y;
	}


	void Camera::Update(float deltaTime, GameObject* focus)
	{
		//Makes the camera follow the given object
		if (focus && focus->transformModule)
		{
			Vec2 objPos = focus->transformModule->Position();
			m_positionX += (objPos.x - m_positionX) * (deltaTime * 2);
			m_positionY += (objPos.y - m_positionY) * (deltaTime * 2);
		}
	}

	void Camera::Render()
	{
#ifdef USEDIRECT3D
		D3DXVECTOR3 up, position, lookAt;


		// Setup the vector that points upwards.
		up.x = 0.0f;
		up.y = 1.0f;
		up.z = 0.0f;

		// Setup the position of the camera in the world.
		position.x = m_positionX * PPU;
		position.y = m_positionY * PPU;
		position.z = -10.f;

		// Setup where the camera is looking by default.
		lookAt.x = m_positionX * PPU;
		lookAt.y = m_positionY * PPU;
		lookAt.z = 0.0f;


		// Finally create the view matrix from the three updated vectors.
		D3DXMatrixLookAtLH(&m_viewMatrix, &position, &lookAt, &up);
#endif
		return;
	}


	Camera*		Camera::Instance()
	{
		if (!inst)
			inst = new Camera();
		return inst;
	}

	const Vec2		Camera::Get2DPosition()
	{
		return Vec2(m_positionX, m_positionY);
	}

	Vec2*			Camera::ConvertPixelPositionToWorldPosition(int x, int y)
	{
		// Convert the pixel position to a world position
		return new Vec2(float((float)(x - windowDatas.screenWidth / 2.f) / (float)PPU) + m_positionX, -1.f * float((float)(y - windowDatas.screenHeight / 2.f)/ (float)PPU) + m_positionY);
	}

	Vec2*			Camera::GetMouseWorldPosition()
	{
		int x;
		int y;
		InputManager::Instance().GetMouseLocation(x, y);
		return ConvertPixelPositionToWorldPosition(x, y);
	}
}