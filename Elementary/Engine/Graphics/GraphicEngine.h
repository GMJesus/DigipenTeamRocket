/**
* \author	Emeric Caramanna
*/
#ifndef _GRAPHICENGINE_H_
#define _GRAPHICENGINE_H_


#include <windows.h>


#include "Graphics/DirectXInterface.h"
#include "Graphics/Camera.h"
#include "Graphics/TextureShader.h"
#include "Graphics/TexturePlane.h"
#include "UI/UIEngine.h"
#include "Utils/LogWriter.h"


const float SCREEN_DEPTH = 1000.0f;
const float SCREEN_NEAR = 0.1f;


namespace Graphic
{

	/**
	* \brief	Class for Graphic Management
	*/
	class GraphicEngine
	{
	public:
		GraphicEngine();
		GraphicEngine(const GraphicEngine&);
		~GraphicEngine();

		/**
		* \brief	Initialize this object and the DirectXInterface Object
		*/
		bool Initialize(int, int, HWND, s_WindowDatas &);
		/**
		* \brief	Shutdown this class and all related object
		*/
		void Shutdown();
		/**
		* \brief	Update the Graphic Engine
		*/
		bool Update();
		/**
		* \brief	Print all object drawed on the screen
		*/
		bool Render();

	private:
		struct CUSTOMVERTEX
		{
			FLOAT x, y, z, rhw;    // from the D3DFVF_XYZRHW flag
			DWORD color;    // from the D3DFVF_DIFFUSE flag
		};

		UIEngine::UI &p_UI = UIEngine::UI::Instance();
		DirectXInterface &m_DXInterface = DirectXInterface::Instance();
	};

#ifdef USEDIRECT3D
	/**
	* \brief	Creates a white texture, used for border
	* \param	myDevice	DirectX device
	* \param	w			The width of the texture to generate
	* \param	h			The height of the texture to generate
	* \return	The generated texture
	*/
	ID3D11Texture2D *CreateBorder(ID3D11Device *myDevice, int w, int h);
#endif
}

#endif