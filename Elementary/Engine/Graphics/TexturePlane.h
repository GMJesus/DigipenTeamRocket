/**
* \author	Aymeric Lambolez
*/
#ifndef _TEXTUREPLANE_H_
#define _TEXTUREPLANE_H_

#pragma warning (disable : 4005)

#define _USE_MATH_DEFINES

#include "Graphics/DirectXInterface.h"
#include <math.h>


#include	"Texture.h"
#include	"Utils\WindowDatas.h"
#include	"Utils/LogWriter.h"


namespace Graphic
{

	/**
	* \brief	Object on which we can put a texture, to render images
	*/
	class TexturePlane
	{
	private:
#ifdef USEDIRECT3D
		/**
		* \brief	Structure defining a vertex for our objects
		*/
		struct VertexType
		{
			D3DXVECTOR3 position;
			D3DXVECTOR2 texture;
		};
#endif
		/**
		* \brief	Simple structure to store a point datas
		*/
		struct Point
		{
			float x; /// \brief the x position of this point
			float y; /// \brief the y position of this point
		};
	public:
		TexturePlane();
		TexturePlane(const TexturePlane&);
		~TexturePlane();

#ifdef USEDIRECT3D
		/**
		* \brief	Initialize this object
		* \param	device	The DirectX device of the game
		* \param	path	The path to the resource to load
		*					If NULL, no resource will be load and the object will not appear on the screen
		* \param	width	The width of this object
		*					If 0, the object will not be visible
		* \param	height	The height of this object
		*					If 0, the object will not be visible
		* \return	True if everything went well, false overwise
		*/
		bool Initialize(ID3D11Device* device, WCHAR* path = NULL, int width = 0, int height = 0);
		/**
		* \brief	Render the stored resource at the given position, with the given rotation
		* \param	context		The DirectX context of the game
		* \param	positionX	The X position where to render the object, in game coordinates
		* \param	positionY	The Y position where to render the object, in game coordinates
		* \param	angle		The angle of the object to draw
		* \return	True if everything went well, false overwise
		*/
		bool Render(ID3D11DeviceContext* context, float positionX, float positionY, float angle);
		/**
		* \brief	Render the stored resource at the given world matrix
		* \param	context		The DirecX context of the game
		* \param	mat			The matrix of the object to render
		* \return	True if everything went well, false overwise
		*/
		bool Render(ID3D11DeviceContext* context, D3DXMATRIX mat);
#endif
		/**
		* \brief	Shutdown this object, release everything that need to be released
		*/
		void Shutdown();

		int	GetWidth();
		int GetHeight();
		void SetTexture(Texture* tex);
		void SetWidth(int width);
		void SetHeight(int height);
		/**
		* \brief	Gets the path to the resource used by this object
		* \return	The path to the resource used by this object
		*/
		const char*	GetTexturePath()const;
		/**
		* \brief	Allows the modification of the UVs of this object
		* \param	TL	The Top Left UV of this object
		* \param	TR	The Top Right UV of this object
		* \param	BR	The Bottom Right UV of this object
		* \param	BL	The Bottom Left UV of this object
		*/
		void	SetUVs(const Vec2& TL, const Vec2& TR, const Vec2& BR, const Vec2& BL);
		/**
		* \brief	Flip the stored texture along the X axys, by switching UVs position
		*/
		void	Flip();

	private:
#ifdef USEDIRECT3D
		/**
		* \brief	Initialize Vertex and Index buffers of this object
		*/
		bool InitializeBuffers(ID3D11Device*);
		/**
		* \brief	Fill the buffers with the value stored by this object
		*/
		bool FillBuffers(ID3D11DeviceContext*);
		/**
		* \brief	Render the buffers stored by this object
		*/
		void RenderBuffers(ID3D11DeviceContext*);
#endif
		/**
		* \brief	Shutdown the buffer stored by this object
		*/
		void ShutdownBuffers();

	private:
#ifdef USEDIRECT3D
		ID3D11Buffer *m_vertexBuffer, *m_indexBuffer;
#endif
		int m_vertexCount, m_indexCount;
		/**
		* \brief	The Texture to map on the object
		*/
		Texture* m_Texture;
		int _texturePlaneWidth, _texturePlaneHeight;
		LogWriter &log = LogWriter::Instance();
		/**
		* \brief	Does the state of this object changed since last frame ?
		*			If true, the buffers will be updated to fit the new state of the object
		*			State changes include width changes, height changes and UVs changes
		*/
		bool changed;

		Vec2	uvs[4];
	};

}

#endif