/**
* \author	Aymeric Lambolez
*/
#include "Texture.h"
#include "UI/UIEngine.h"
namespace Graphic
{

	Texture::Texture()
	{
#ifdef USEDIRECT3D
		m_texture = NULL;
		_texShader = NULL;
		_path = NULL;
#endif
	}


	Texture::Texture(const Texture& other)
	{
	}


	Texture::~Texture()
	{
	}

	const char*	Texture::GetPath()const
	{
		return _path;
	}

#ifdef USEDIRECT3D
	bool Texture::Initialize(ID3D11Device* device, WCHAR* filename)
	{
		HRESULT result;

		// Load the texture in.
		result = D3DX11CreateShaderResourceViewFromFileW(device, filename, NULL, NULL, &m_texture, NULL);
		_texShader = new TextureShader();
		_texShader->Initialize(device, windowDatas.m_hwnd);
		if (FAILED(result))
		{
			return false;
		}
		_path = new char[255];
		wcstombs(_path, filename, 255);
		return true;
	}

	bool Texture::Initialize(ID3D11Device* device, ID3D11Resource* file)
	{
		HRESULT result;
		result = DirectXInterface::Instance().GetDevice()->CreateShaderResourceView(file, NULL, &m_texture);
		if (FAILED(result))
		{
			m_texture = NULL;
			return false;
		}
		_texShader = new TextureShader();
		_texShader->Initialize(device, windowDatas.m_hwnd);
		return true;
	}


	void Texture::Shutdown()
	{
		// Release the texture resource.
		//if (m_texture != NULL)
		//{
		//	m_texture->Release();
		//	m_texture = NULL;
		//}

		if (_texShader)
		{
			_texShader->Shutdown();
			delete(_texShader);
			_texShader = NULL;
		}
		if (_path)
		{
			delete(_path);
			_path = NULL;
		}
		return;
	}

	ID3D11ShaderResourceView* Texture::GetTexture()
	{
		return m_texture;
	}

	bool Texture::Render(const D3DXMATRIX& objMatrix)
	{
		if (m_texture)
			return _texShader->Render(DirectXInterface::Instance().GetDeviceContext(), 6, objMatrix, Camera::Instance()->GetViewMatrix(), DirectXInterface::Instance().GetOrthoMatrix(), m_texture);
		else
			return false;
	}
#endif

}