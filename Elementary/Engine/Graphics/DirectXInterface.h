/**
* \author	Aymeric Lambolez
*/
# ifndef DIRECTXINTERFACE_H_
# define DIRECTXINTERFACE_H_


#ifdef USEDIRECT3D
# pragma comment(lib, "dxgi.lib")
# pragma comment(lib, "d3d11.lib")
# pragma comment(lib, "d3dx11.lib")
# pragma comment(lib, "d3dx10.lib")
#endif //	USEDIRECT3D

#pragma warning (disable : 4005)
#ifdef USEDIRECT3D
# include "External libraries\DirectX\Include\dxgi.h"
# include "External libraries\DirectX\Include\d3dcommon.h"
# include "External libraries\DirectX\Include\d3d11.h"
# include "External libraries\DirectX\Include\d3dx10math.h"
# include "External libraries\DirectX\Include\d3dx11async.h"
# include "External libraries\DirectX\Include\d3dx11tex.h"
#endif //	USEDIRECT3D

#ifdef USEDIRECT2D
#include <Windows.h>
//#include <d2d1_1.h>
//#include <d2d1_1helper.h>
//#include <d2d1.h>
//#include <d2d1helper.h>
#include "External libraries\DirectX\Include\d2d1.h"
#include "External libraries\DirectX\Include\d2d1helper.h"
#include "External libraries\DirectX\Include\dwrite.h"
#endif //	USEDIRECT2D

/**
* \brief	Helper class for DirectX uses
*/
class DirectXInterface
{
public:
	static DirectXInterface &Instance();
	/**
	* \brief	Initialize the helper class
	* \param	screenWidth Width of the screen
	* \param	screenHeight Height of the screen
	* \param	vsync True if the vertical synchro is to be used
	* \param	hwnd Handler for the window
	* \param	fullscreen True if the window is to be in full screen mode
	* \param	screenDepth Defines the screen depth
	* \param	screenNear Defines the screen near
	*/
	bool Initialize(int screenWidth, int screenHeight, bool vsync, HWND hwnd, bool fullscreen, float screenDepth, float screenNear);
	void Shutdown();

	/**
	* \brief	Specify the clear rendering color
	* \param	red Red value of the clear color
	* \param	green Green value of the clear color
	* \param	blue Blue value of the clear color
	* \param	alpha Alpha value of the clear color
	*/
	void BeginScene(float red, float green, float blue, float alpha);
	void EndScene();

	/**
	* \brief	Set the screen mode of the window
	* \param	fullscreen	If true, sets the window to fullscreen, if not, set the window to windowed
	*/
	void FullScreen(bool fullscreen);

	/**
	* \brief	Get the screen mode of the window
	* \return	True if the window is in fullscreen mode, false overwise
	*/
	bool FullScreen();

#ifdef USEDIRECT3D
	ID3D11Device* GetDevice();
	ID3D11DeviceContext* GetDeviceContext();
#endif //	USEDIRECT3D

#ifdef USEDIRECT3D
	const D3DXMATRIX& GetProjectionMatrix()const;
	const D3DXMATRIX& GetWorldMatrix()const;
	const D3DXMATRIX& GetOrthoMatrix()const;
#endif //	USEDIRECT3D

	void GetVideoCardInfo(char*, int&);

	void TurnZBufferOn();
	void TurnZBufferOff();

	void TurnOnAlphaBlending();
	void TurnOffAlphaBlending();

	bool m_vsync_enabled;
	int m_videoCardMemory;
	char m_videoCardDescription[128];

#ifdef USEDIRECT3D
	IDXGISwapChain* m_swapChain;
	ID3D11Device* m_device;
	ID3D11DeviceContext* m_deviceContext;
	ID3D11RenderTargetView* m_renderTargetView;
	ID3D11Texture2D* m_depthStencilBuffer;
	ID3D11DepthStencilState* m_depthStencilState;
	ID3D11DepthStencilView* m_depthStencilView;
	ID3D11RasterizerState* m_rasterState;
	D3DXMATRIX m_projectionMatrix;
	D3DXMATRIX m_worldMatrix;
	D3DXMATRIX m_orthoMatrix;
	ID3D11DepthStencilState* m_depthDisabledStencilState;
	ID3D11BlendState* m_alphaEnableBlendingState;
	ID3D11BlendState* m_alphaDisableBlendingState;
#endif //	USEDIRECT3D

private:

	DirectXInterface& operator= (const DirectXInterface &) {}
	DirectXInterface(const DirectXInterface &) {}

	static DirectXInterface m_instance;
	DirectXInterface();
	~DirectXInterface();

};
# endif //	!DIRECTXINTERFACE_H_
