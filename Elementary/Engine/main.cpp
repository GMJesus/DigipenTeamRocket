////////////////////////////////////////////////////////////////////////////////
// Filename: main.cpp
////////////////////////////////////////////////////////////////////////////////
#pragma warning (disable : 4005)

#include "GameEngine/GameEngine.h"

using namespace GameEngine;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pScmdline, int iCmdshow)
{
	bool result;
	
	
	// Initialize and run the GameEngine object.
	result = Engine::Instance()->Initialize();
	if(result)
	{
		Engine::Instance()->Run();
	}

	// Shutdown and release the GameEngine object.
	Engine::Instance()->Shutdown();

	return 0;
}