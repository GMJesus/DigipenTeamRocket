\hypertarget{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s}{}\section{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S Struct Reference}
\label{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s}\index{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S@{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S}}


The \hyperlink{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s}{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S} structure specifies the metrics of an individual glyph. The units depend on how the metrics are obtained.  




{\ttfamily \#include $<$D\+Write.\+h$>$}

\subsection*{Public Attributes}
\begin{DoxyCompactItemize}
\item 
I\+N\+T32 \hyperlink{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_ad84ed174c78a699c5e01ac3eae74e778}{left\+Side\+Bearing}
\begin{DoxyCompactList}\small\item\em Specifies the X offset from the glyph origin to the left edge of the black box. The glyph origin is the current horizontal writing position. A negative value means the black box extends to the left of the origin (often true for lowercase italic \textquotesingle{}f\textquotesingle{}). \end{DoxyCompactList}\item 
U\+I\+N\+T32 \hyperlink{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_aabf468f57c33218bf4f9057de73db42e}{advance\+Width}
\begin{DoxyCompactList}\small\item\em Specifies the X offset from the origin of the current glyph to the origin of the next glyph when writing horizontally. \end{DoxyCompactList}\item 
I\+N\+T32 \hyperlink{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_a71623d61bacb04b7949c0b1ede1f9e25}{right\+Side\+Bearing}
\begin{DoxyCompactList}\small\item\em Specifies the X offset from the right edge of the black box to the origin of the next glyph when writing horizontally. The value is negative when the right edge of the black box overhangs the layout box. \end{DoxyCompactList}\item 
I\+N\+T32 \hyperlink{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_a30f17928d80dbf14caea21544ef20648}{top\+Side\+Bearing}
\begin{DoxyCompactList}\small\item\em Specifies the vertical offset from the vertical origin to the top of the black box. Thus, a positive value adds whitespace whereas a negative value means the glyph overhangs the top of the layout box. \end{DoxyCompactList}\item 
U\+I\+N\+T32 \hyperlink{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_aff7119b80b578754a197615261eae3b2}{advance\+Height}
\begin{DoxyCompactList}\small\item\em Specifies the Y offset from the vertical origin of the current glyph to the vertical origin of the next glyph when writing vertically. (Note that the term \char`\"{}origin\char`\"{} by itself denotes the horizontal origin. The vertical origin is different. Its Y coordinate is specified by vertical\+Origin\+Y value, and its X coordinate is half the advance\+Width to the right of the horizontal origin). \end{DoxyCompactList}\item 
I\+N\+T32 \hyperlink{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_ac0b509e5afe3c176a4e9c5122209e9c1}{bottom\+Side\+Bearing}
\begin{DoxyCompactList}\small\item\em Specifies the vertical distance from the black box\textquotesingle{}s bottom edge to the advance height. Positive when the bottom edge of the black box is within the layout box. Negative when the bottom edge of black box overhangs the layout box. \end{DoxyCompactList}\item 
I\+N\+T32 \hyperlink{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_a039cfd6332f708e69a5d564ff6c9fedd}{vertical\+Origin\+Y}
\begin{DoxyCompactList}\small\item\em Specifies the Y coordinate of a glyph\textquotesingle{}s vertical origin, in the font\textquotesingle{}s design coordinate system. The y coordinate of a glyph\textquotesingle{}s vertical origin is the sum of the glyph\textquotesingle{}s top side bearing and the top (i.\+e. y\+Max) of the glyph\textquotesingle{}s bounding box. \end{DoxyCompactList}\end{DoxyCompactItemize}


\subsection{Detailed Description}
The \hyperlink{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s}{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S} structure specifies the metrics of an individual glyph. The units depend on how the metrics are obtained. 



\subsection{Member Data Documentation}
\hypertarget{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_aff7119b80b578754a197615261eae3b2}{}\index{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S@{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S}!advance\+Height@{advance\+Height}}
\index{advance\+Height@{advance\+Height}!D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S@{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S}}
\subsubsection[{advance\+Height}]{\setlength{\rightskip}{0pt plus 5cm}U\+I\+N\+T32 D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S\+::advance\+Height}\label{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_aff7119b80b578754a197615261eae3b2}


Specifies the Y offset from the vertical origin of the current glyph to the vertical origin of the next glyph when writing vertically. (Note that the term \char`\"{}origin\char`\"{} by itself denotes the horizontal origin. The vertical origin is different. Its Y coordinate is specified by vertical\+Origin\+Y value, and its X coordinate is half the advance\+Width to the right of the horizontal origin). 

\hypertarget{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_aabf468f57c33218bf4f9057de73db42e}{}\index{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S@{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S}!advance\+Width@{advance\+Width}}
\index{advance\+Width@{advance\+Width}!D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S@{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S}}
\subsubsection[{advance\+Width}]{\setlength{\rightskip}{0pt plus 5cm}U\+I\+N\+T32 D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S\+::advance\+Width}\label{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_aabf468f57c33218bf4f9057de73db42e}


Specifies the X offset from the origin of the current glyph to the origin of the next glyph when writing horizontally. 

\hypertarget{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_ac0b509e5afe3c176a4e9c5122209e9c1}{}\index{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S@{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S}!bottom\+Side\+Bearing@{bottom\+Side\+Bearing}}
\index{bottom\+Side\+Bearing@{bottom\+Side\+Bearing}!D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S@{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S}}
\subsubsection[{bottom\+Side\+Bearing}]{\setlength{\rightskip}{0pt plus 5cm}I\+N\+T32 D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S\+::bottom\+Side\+Bearing}\label{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_ac0b509e5afe3c176a4e9c5122209e9c1}


Specifies the vertical distance from the black box\textquotesingle{}s bottom edge to the advance height. Positive when the bottom edge of the black box is within the layout box. Negative when the bottom edge of black box overhangs the layout box. 

\hypertarget{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_ad84ed174c78a699c5e01ac3eae74e778}{}\index{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S@{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S}!left\+Side\+Bearing@{left\+Side\+Bearing}}
\index{left\+Side\+Bearing@{left\+Side\+Bearing}!D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S@{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S}}
\subsubsection[{left\+Side\+Bearing}]{\setlength{\rightskip}{0pt plus 5cm}I\+N\+T32 D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S\+::left\+Side\+Bearing}\label{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_ad84ed174c78a699c5e01ac3eae74e778}


Specifies the X offset from the glyph origin to the left edge of the black box. The glyph origin is the current horizontal writing position. A negative value means the black box extends to the left of the origin (often true for lowercase italic \textquotesingle{}f\textquotesingle{}). 

\hypertarget{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_a71623d61bacb04b7949c0b1ede1f9e25}{}\index{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S@{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S}!right\+Side\+Bearing@{right\+Side\+Bearing}}
\index{right\+Side\+Bearing@{right\+Side\+Bearing}!D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S@{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S}}
\subsubsection[{right\+Side\+Bearing}]{\setlength{\rightskip}{0pt plus 5cm}I\+N\+T32 D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S\+::right\+Side\+Bearing}\label{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_a71623d61bacb04b7949c0b1ede1f9e25}


Specifies the X offset from the right edge of the black box to the origin of the next glyph when writing horizontally. The value is negative when the right edge of the black box overhangs the layout box. 

\hypertarget{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_a30f17928d80dbf14caea21544ef20648}{}\index{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S@{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S}!top\+Side\+Bearing@{top\+Side\+Bearing}}
\index{top\+Side\+Bearing@{top\+Side\+Bearing}!D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S@{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S}}
\subsubsection[{top\+Side\+Bearing}]{\setlength{\rightskip}{0pt plus 5cm}I\+N\+T32 D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S\+::top\+Side\+Bearing}\label{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_a30f17928d80dbf14caea21544ef20648}


Specifies the vertical offset from the vertical origin to the top of the black box. Thus, a positive value adds whitespace whereas a negative value means the glyph overhangs the top of the layout box. 

\hypertarget{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_a039cfd6332f708e69a5d564ff6c9fedd}{}\index{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S@{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S}!vertical\+Origin\+Y@{vertical\+Origin\+Y}}
\index{vertical\+Origin\+Y@{vertical\+Origin\+Y}!D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S@{D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S}}
\subsubsection[{vertical\+Origin\+Y}]{\setlength{\rightskip}{0pt plus 5cm}I\+N\+T32 D\+W\+R\+I\+T\+E\+\_\+\+G\+L\+Y\+P\+H\+\_\+\+M\+E\+T\+R\+I\+C\+S\+::vertical\+Origin\+Y}\label{struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s_a039cfd6332f708e69a5d564ff6c9fedd}


Specifies the Y coordinate of a glyph\textquotesingle{}s vertical origin, in the font\textquotesingle{}s design coordinate system. The y coordinate of a glyph\textquotesingle{}s vertical origin is the sum of the glyph\textquotesingle{}s top side bearing and the top (i.\+e. y\+Max) of the glyph\textquotesingle{}s bounding box. 



The documentation for this struct was generated from the following file\+:\begin{DoxyCompactItemize}
\item 
C\+:/\+Users/aymer\+\_\+000/\+Desktop/\+Digipen/\+Digipen\+Team\+Rocket/\+Elementary/\+Engine/\+External libraries/\+Direct\+X/\+Include/D\+Write.\+h\end{DoxyCompactItemize}
