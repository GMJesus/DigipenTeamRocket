var searchData=
[
  ['r',['r',['../struct_fl___scroll_1_1_scroll_info_1_1_fl___region___l_r_t_b.html#a413bf20feae2a1813192fd2df013aea3',1,'Fl_Scroll::ScrollInfo::Fl_Region_LRTB']]],
  ['readingdirection',['readingDirection',['../struct_d_w_r_i_t_e___u_n_d_e_r_l_i_n_e.html#a9814c21633d2a33e3b81d5900c3668f2',1,'DWRITE_UNDERLINE::readingDirection()'],['../struct_d_w_r_i_t_e___s_t_r_i_k_e_t_h_r_o_u_g_h.html#aa6a05c7dfa662dff38d1dd0b5d4b7618',1,'DWRITE_STRIKETHROUGH::readingDirection()']]],
  ['remainingtime',['remainingTime',['../struct_u_i_engine_1_1_u_i_1_1s___temp_message.html#a0f9d1d33d796d623e03c4f66bd48f28d',1,'UIEngine::UI::s_TempMessage']]],
  ['reserved',['reserved',['../struct_d_w_r_i_t_e___s_h_a_p_i_n_g___t_e_x_t___p_r_o_p_e_r_t_i_e_s.html#a338e9168214af382d882639ff3c9141f',1,'DWRITE_SHAPING_TEXT_PROPERTIES::reserved()'],['../struct_d_w_r_i_t_e___s_h_a_p_i_n_g___g_l_y_p_h___p_r_o_p_e_r_t_i_e_s.html#aef7633039f5fa237a34d53c46b211a45',1,'DWRITE_SHAPING_GLYPH_PROPERTIES::reserved()']]],
  ['right',['right',['../struct_d_w_r_i_t_e___o_v_e_r_h_a_n_g___m_e_t_r_i_c_s.html#a009bbc72b36f70fe1a6c3f1d5c17c5bb',1,'DWRITE_OVERHANG_METRICS']]],
  ['rightsidebearing',['rightSideBearing',['../struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s.html#a71623d61bacb04b7949c0b1ede1f9e25',1,'DWRITE_GLYPH_METRICS']]],
  ['runheight',['runHeight',['../struct_d_w_r_i_t_e___u_n_d_e_r_l_i_n_e.html#afe2cbae722bb666f6ad60f3c33508913',1,'DWRITE_UNDERLINE']]]
];
