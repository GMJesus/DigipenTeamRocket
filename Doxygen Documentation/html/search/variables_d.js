var searchData=
[
  ['m11',['m11',['../struct_d_w_r_i_t_e___m_a_t_r_i_x.html#abbe0cb967633767554e8e74d958ad6ae',1,'DWRITE_MATRIX']]],
  ['m12',['m12',['../struct_d_w_r_i_t_e___m_a_t_r_i_x.html#abcb3515a87ea9c26424ca2944abef4f2',1,'DWRITE_MATRIX']]],
  ['m21',['m21',['../struct_d_w_r_i_t_e___m_a_t_r_i_x.html#a63dabe6a269d63d41a66fe45542428a5',1,'DWRITE_MATRIX']]],
  ['m22',['m22',['../struct_d_w_r_i_t_e___m_a_t_r_i_x.html#a11d3af3bc26ed0945104faac181c991f',1,'DWRITE_MATRIX']]],
  ['manage_5ffavorites_5flabel',['manage_favorites_label',['../class_fl___file___chooser.html#a5694e2148a8ac3f0c9c74e4b9eafcc40',1,'Fl_File_Chooser']]],
  ['max_5fcapacity',['MAX_CAPACITY',['../class_game_engine_1_1_quadtree.html#a99d2016152cdb6ccc7fb79e5ac6ff7f1',1,'GameEngine::Quadtree']]],
  ['maxbidireorderingdepth',['maxBidiReorderingDepth',['../struct_d_w_r_i_t_e___t_e_x_t___m_e_t_r_i_c_s.html#a746f3ff54eeeb931e9e165af7ca004ec',1,'DWRITE_TEXT_METRICS']]],
  ['mbuf',['mBuf',['../class_fl___text___buffer.html#a10ded10101d8f411286d14c8e1f60865',1,'Fl_Text_Buffer']]],
  ['mcanundo',['mCanUndo',['../class_fl___text___buffer.html#ac514cc66a88582bb6828bc421acf0f09',1,'Fl_Text_Buffer']]],
  ['mcbargs',['mCbArgs',['../class_fl___text___buffer.html#a0abe9b31f4a138137ce01af8e011f2df',1,'Fl_Text_Buffer']]],
  ['mcursorposhint',['mCursorPosHint',['../class_fl___text___buffer.html#a50e7d6784656a3d4c2f928e002c75e85',1,'Fl_Text_Buffer']]],
  ['measuringmode',['measuringMode',['../struct_d_w_r_i_t_e___u_n_d_e_r_l_i_n_e.html#ac88a64c2b1d3114be9c1a948736ad412',1,'DWRITE_UNDERLINE::measuringMode()'],['../struct_d_w_r_i_t_e___s_t_r_i_k_e_t_h_r_o_u_g_h.html#a8ba2fcee63a2b77a0f08345c7f743342',1,'DWRITE_STRIKETHROUGH::measuringMode()']]],
  ['mend',['mEnd',['../class_fl___text___selection.html#ac2871c01124862070a9eb68af7c0b171',1,'Fl_Text_Selection']]],
  ['menus',['menus',['../class_u_i_engine_1_1_menu_factory.html#af53ae3ca0c6f7efddc7dfbcf6950fd95',1,'UIEngine::MenuFactory']]],
  ['message',['message',['../struct_u_i_engine_1_1_u_i_1_1s___temp_message.html#a6915762da7f6d962585d172d74b44604',1,'UIEngine::UI::s_TempMessage']]],
  ['mgapend',['mGapEnd',['../class_fl___text___buffer.html#a77204ed3084eaceee8bb67f35e0c9223',1,'Fl_Text_Buffer']]],
  ['mgapstart',['mGapStart',['../class_fl___text___buffer.html#a4bea04adc6132e9ee3cef0f46536a326',1,'Fl_Text_Buffer']]],
  ['mhighlight',['mHighlight',['../class_fl___text___buffer.html#a6a78d10cc4437d3029e347b9ded8cddb',1,'Fl_Text_Buffer']]],
  ['mlength',['mLength',['../class_fl___text___buffer.html#af4d85a8ab1f6d681daf42c834339fdc7',1,'Fl_Text_Buffer']]],
  ['mmodifyprocs',['mModifyProcs',['../class_fl___text___buffer.html#abc7f5a7cbd137b13dc74410831282589',1,'Fl_Text_Buffer']]],
  ['mnmodifyprocs',['mNModifyProcs',['../class_fl___text___buffer.html#a415382fd677cc6c9572dabb866b6f27d',1,'Fl_Text_Buffer']]],
  ['mnpredeleteprocs',['mNPredeleteProcs',['../class_fl___text___buffer.html#aae73a1f6d1290fe8813d185e2de866e8',1,'Fl_Text_Buffer']]],
  ['mpredeletecbargs',['mPredeleteCbArgs',['../class_fl___text___buffer.html#a9c8e388b568aa6ff98bb8703272ac01b',1,'Fl_Text_Buffer']]],
  ['mpredeleteprocs',['mPredeleteProcs',['../class_fl___text___buffer.html#a9acb4950f29d6aa64a29e4ebe46e72f9',1,'Fl_Text_Buffer']]],
  ['mpreferredgapsize',['mPreferredGapSize',['../class_fl___text___buffer.html#abadec9509dcd31dbb342044ec3c82fc2',1,'Fl_Text_Buffer']]],
  ['mprimary',['mPrimary',['../class_fl___text___buffer.html#aa2c392f5efd9f75762ced592a75b9860',1,'Fl_Text_Buffer']]],
  ['msecondary',['mSecondary',['../class_fl___text___buffer.html#a05ed59fc23a6b565a99bf80623aaa77a',1,'Fl_Text_Buffer']]],
  ['mselected',['mSelected',['../class_fl___text___selection.html#a7ed85098de59ea67762b54cbccb08c0e',1,'Fl_Text_Selection']]],
  ['mstart',['mStart',['../class_fl___text___selection.html#ae4abfc4286beaabbca38c42a2681d730',1,'Fl_Text_Selection']]],
  ['mtabdist',['mTabDist',['../class_fl___text___buffer.html#adc81abbabaf8f718aac4b96ffba73e89',1,'Fl_Text_Buffer']]]
];
