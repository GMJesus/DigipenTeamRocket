var searchData=
[
  ['name',['name',['../struct_fl___help___link.html#a0ccda15c03d1015cb2846c66d65213c8',1,'Fl_Help_Link::name()'],['../struct_fl___help___target.html#af278961a4d3d04fab6e52c2ea623bd1e',1,'Fl_Help_Target::name()'],['../struct_fl___paged___device_1_1page__format.html#ab281b2d2443be0bb9f7fbe59c87346bd',1,'Fl_Paged_Device::page_format::name()']]],
  ['nametag',['nameTag',['../struct_d_w_r_i_t_e___f_o_n_t___f_e_a_t_u_r_e.html#a7c24b482ccce25d66a03f946097ad41b',1,'DWRITE_FONT_FEATURE']]],
  ['new_5fdirectory_5flabel',['new_directory_label',['../class_fl___file___chooser.html#ad0a4168c7ac776a5e6f28c788c0f0766',1,'Fl_File_Chooser']]],
  ['new_5fdirectory_5ftooltip',['new_directory_tooltip',['../class_fl___file___chooser.html#a515a4eddc7c1991f8d243f1da2cb6c30',1,'Fl_File_Chooser']]],
  ['newlinelength',['newlineLength',['../struct_d_w_r_i_t_e___l_i_n_e___m_e_t_r_i_c_s.html#a5e37eea77d079c36d36f2aaabeb39a79',1,'DWRITE_LINE_METRICS']]],
  ['next',['next',['../struct_fl___check___browser_1_1cb__item.html#a14f9cfca4967cb8a4ed7d12bf88962b0',1,'Fl_Check_Browser::cb_item::next()'],['../struct_fl___text___editor_1_1_key___binding.html#ab9b34380e32d84f35c000e29250349c9',1,'Fl_Text_Editor::Key_Binding::next()']]],
  ['nfonts_5f',['nfonts_',['../struct_fl___help___font___stack.html#a316766f09e93398a71fc3bb4731be19c',1,'Fl_Help_Font_Stack']]]
];
