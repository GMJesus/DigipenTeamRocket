var searchData=
[
  ['l',['l',['../struct_fl___scroll_1_1_scroll_info_1_1_fl___region___l_r_t_b.html#a6591ca035deb9d0c303e93f58c821704',1,'Fl_Scroll::ScrollInfo::Fl_Region_LRTB']]],
  ['labelcolor_5f',['labelcolor_',['../struct_fl___menu___item.html#a859fe109e269f057d4b216e5188f5e6e',1,'Fl_Menu_Item']]],
  ['labelfont_5f',['labelfont_',['../struct_fl___menu___item.html#ad82ab3107c63ed28bda8749fe3f14f32',1,'Fl_Menu_Item']]],
  ['labelsize_5f',['labelsize_',['../struct_fl___menu___item.html#a286bd88b6597d557b6ebec29dea7a942',1,'Fl_Menu_Item']]],
  ['labeltype_5f',['labeltype_',['../struct_fl___menu___item.html#ac4aadd19ae4889935c5f8a8a3da381d3',1,'Fl_Menu_Item']]],
  ['layoutheight',['layoutHeight',['../struct_d_w_r_i_t_e___t_e_x_t___m_e_t_r_i_c_s.html#a9a7395d61d39bb664182938b1deb5cda',1,'DWRITE_TEXT_METRICS']]],
  ['layoutwidth',['layoutWidth',['../struct_d_w_r_i_t_e___t_e_x_t___m_e_t_r_i_c_s.html#a5d495c80dcde1b91fc5dfe5f0952f8a2',1,'DWRITE_TEXT_METRICS']]],
  ['left',['left',['../struct_d_w_r_i_t_e___t_e_x_t___m_e_t_r_i_c_s.html#ad39e7fee399a2124a265b085fe0937c1',1,'DWRITE_TEXT_METRICS::left()'],['../struct_d_w_r_i_t_e___o_v_e_r_h_a_n_g___m_e_t_r_i_c_s.html#aec1275e4e1558efa7051913babd405d7',1,'DWRITE_OVERHANG_METRICS::left()'],['../struct_d_w_r_i_t_e___h_i_t___t_e_s_t___m_e_t_r_i_c_s.html#aba9466175a17b2f55367495454f0f5ea',1,'DWRITE_HIT_TEST_METRICS::left()']]],
  ['leftsidebearing',['leftSideBearing',['../struct_d_w_r_i_t_e___g_l_y_p_h___m_e_t_r_i_c_s.html#ad84ed174c78a699c5e01ac3eae74e778',1,'DWRITE_GLYPH_METRICS']]],
  ['length',['length',['../struct_d_w_r_i_t_e___t_e_x_t___r_a_n_g_e.html#a517c4b659d087769b3c6f0af4c385279',1,'DWRITE_TEXT_RANGE::length()'],['../struct_d_w_r_i_t_e___l_i_n_e___m_e_t_r_i_c_s.html#aa1ae002bb6ff212e3966ce9b67d5b235',1,'DWRITE_LINE_METRICS::length()'],['../struct_d_w_r_i_t_e___c_l_u_s_t_e_r___m_e_t_r_i_c_s.html#a446b58ca75cbf43ea2717ecd7ab2fcf0',1,'DWRITE_CLUSTER_METRICS::length()'],['../struct_d_w_r_i_t_e___h_i_t___t_e_s_t___m_e_t_r_i_c_s.html#aae5156cd5eb42247a34e125d2a178c5f',1,'DWRITE_HIT_TEST_METRICS::length()']]],
  ['lh_5f',['lh_',['../struct_fl___window_1_1shape__data__type.html#a23c9b0905fdb1caad79ae32b4305b1d1',1,'Fl_Window::shape_data_type']]],
  ['linecount',['lineCount',['../struct_d_w_r_i_t_e___t_e_x_t___m_e_t_r_i_c_s.html#aa6224e968233d42a65bbf914dd807488',1,'DWRITE_TEXT_METRICS']]],
  ['linegap',['lineGap',['../struct_d_w_r_i_t_e___f_o_n_t___m_e_t_r_i_c_s.html#ac035653c640ed8289a61da53b4b425e7',1,'DWRITE_FONT_METRICS']]],
  ['localename',['localeName',['../struct_d_w_r_i_t_e___g_l_y_p_h___r_u_n___d_e_s_c_r_i_p_t_i_o_n.html#a4260be36f5be712225969501e0946c8f',1,'DWRITE_GLYPH_RUN_DESCRIPTION::localeName()'],['../struct_d_w_r_i_t_e___u_n_d_e_r_l_i_n_e.html#a4345362bbe42a16ad29efe369bb849f2',1,'DWRITE_UNDERLINE::localeName()'],['../struct_d_w_r_i_t_e___s_t_r_i_k_e_t_h_r_o_u_g_h.html#a620471c2fd023a9fa9865fad836bd589',1,'DWRITE_STRIKETHROUGH::localeName()']]],
  ['lw_5f',['lw_',['../struct_fl___window_1_1shape__data__type.html#aa2192e27474e3d8bcae2ddaebc8e0090',1,'Fl_Window::shape_data_type']]]
];
